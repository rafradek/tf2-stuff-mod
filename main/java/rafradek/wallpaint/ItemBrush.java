package rafradek.wallpaint;

import java.util.ArrayList;
import java.util.List;

import rafradek.wallpaint.WallPaintMessage.UpdateSend;
import rafradek.wallpaint.core.WallPaintOverridedMethods;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.IGrowable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.util.ForgeDirection;

public class ItemBrush extends Item {
	public IIcon theIcon;
	public IIcon blankIcon;
	public ItemBrush(){
		//this.setMaxDamage(type.durablity);
		this.setMaxStackSize(1);
		this.setCreativeTab(WallPaint.tabPaint);
		this.setUnlocalizedName("brush");
		this.setMaxDamage(700);
		this.setTextureName("wallpaint:brush");
	}
	@SuppressWarnings("unchecked")
	public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_, World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {
		if(p_77648_3_.isRemote&&!(p_77648_3_.getBlock(p_77648_4_, p_77648_5_, p_77648_6_) instanceof BlockBush)){
			int paintcolor=-1;
			int paintcolor2=-1;
			//int shape=0;
			int bucket=-1;
			for(int i=0;i<p_77648_2_.inventory.mainInventory.length;i++){
				ItemStack stack=p_77648_2_.inventory.mainInventory[i];
				if(stack!=null && stack.getItem() instanceof ItemPaintBucket && stack.hasTagCompound()){
					bucket=i;
					paintcolor=15-stack.getTagCompound().getByte("color")&15;
					paintcolor2=15-(WallPaint.unSign(stack.getTagCompound().getByte("color"))>>4);
					if(!stack.stackTagCompound.hasKey("cnv")){
						paintcolor2=paintcolor;
					}
					/*if(stack.stackTagCompound.hasKey("shape")){
						shape=(stack.getTagCompound().getByte("shape")&4)<<31;
						
					}*/
					//System.out.println("color2: "+paintcolor2);
					//System.out.println("fdfsfds: "+paintcolor);
					break;
				}
			}
			if(paintcolor==-1)
				return false;
			boolean add=true;
			boolean semiColor=paintcolor!=0||paintcolor2!=0||!(p_77648_3_.getBlock(p_77648_4_, p_77648_5_, p_77648_6_).colorMultiplier(p_77648_3_, p_77648_4_, p_77648_5_, p_77648_6_)==16777215
					&& WallPaintOverridedMethods.findTextureReplacement(p_77648_3_.getBlock(p_77648_4_, p_77648_5_, p_77648_6_).getIcon(p_77648_3_, p_77648_4_, p_77648_5_, p_77648_6_, p_77648_7_)) 
					== p_77648_3_.getBlock(p_77648_4_, p_77648_5_, p_77648_6_).getIcon(p_77648_3_, p_77648_4_, p_77648_5_, p_77648_6_, p_77648_7_));
			int[] finalValue=null;
			if(!WallPaint.proxy.getSidedMap().containsKey(p_77648_3_.getChunkFromBlockCoords(p_77648_4_, p_77648_6_))){
				WallPaint.proxy.getSidedMap().put(p_77648_3_.getChunkFromBlockCoords(p_77648_4_, p_77648_6_), new ArrayList<int[]>());
			}
			ArrayList<int[]> list=WallPaint.proxy.getSidedMap().get(p_77648_3_.getChunkFromBlockCoords(p_77648_4_, p_77648_6_));
			for(int[] array:list){
				if(array[0]==p_77648_4_&&array[1]==p_77648_5_&&array[2]==p_77648_6_){
					add=false;
					if((array[3]&WallPaint.sideConvert(p_77648_7_))==0){
						if(semiColor)
							array[4]+=WallPaint.sideConvert(p_77648_7_);
						array[3]+=WallPaint.sideConvert(p_77648_7_);
						//array[3]+=shape;
						array[3]+=paintcolor<<(6+4*p_77648_7_);
						array[4]+=paintcolor2<<(6+4*p_77648_7_);
						finalValue=array;
					}
					else if((array[4]&WallPaint.sideConvert(p_77648_7_))!=0&&(array[3]>>(6+4*p_77648_7_)&15)==paintcolor&&(array[4]>>(6+4*p_77648_7_)&15)==paintcolor2){
						array[4]-=WallPaint.sideConvert(p_77648_7_);
						finalValue=array;
					}
				}
			}
			if(add){
				
				finalValue=new int[]{p_77648_4_,p_77648_5_,p_77648_6_,WallPaint.sideConvert(p_77648_7_)+(paintcolor<<(6+4*p_77648_7_)),semiColor?WallPaint.sideConvert(p_77648_7_)+(paintcolor2<<(6+4*p_77648_7_)):(paintcolor2<<(6+4*p_77648_7_))};
				list.add(finalValue);
				//System.out.println("utworzono: "+WallPaint.sideConvert(p_77648_7_));
			}
			if(finalValue!=null){
				Minecraft.getMinecraft().theWorld.markBlockForUpdate(p_77648_4_,p_77648_5_,p_77648_6_);
				WallPaint.network.sendToServer(new UpdateSend(finalValue,bucket));
				return true;
			}
		}
		return false;
    }
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister)
    {
        super.registerIcons(par1IconRegister);
        this.theIcon = par1IconRegister.registerIcon(this.getIconString() + "_overlay");
        this.blankIcon = par1IconRegister.registerIcon(this.getIconString() + "_blank");
    }
	/*@SuppressWarnings("rawtypes")
	@SideOnly(Side.CLIENT)
    public void getSubItems(Item p_150895_1_, CreativeTabs p_150895_2_, List p_150895_3_)
    {
		p_150895_3_.add(new ItemStack(p_150895_1_));
        for (int i = 0; i < 16; ++i)
        {
        	ItemStack stack=new ItemStack(WallPaint.paintBucket);
        	stack.setTagCompound(new NBTTagCompound());
        	stack.stackTagCompound.setByte("color", (byte) (i+(i<<4)));
        	stack.stackTagCompound.setBoolean("cnv", true);
            p_150895_3_.add(stack);
        }
    }*/
	@SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
		if(par1ItemStack.hasTagCompound()&&(par1ItemStack.getTagCompound().getByte("color")&15)==(par1ItemStack.getTagCompound().getByte("color")>>4)){
			return par2 == 0 ? 16777215 : ItemDye.field_150922_c[par1ItemStack.getTagCompound().getByte("color")&15];
		}
		else if(par1ItemStack.hasTagCompound()){
			byte color=par1ItemStack.getTagCompound().getByte("color");
			int color1=ItemDye.field_150922_c[color&15];
			int color2=ItemDye.field_150922_c[WallPaint.unSign(color)>>4];
			int r1=color1>>16&255;
			int g1=color1>>8&255;
			int b1=color1&255;
			int max1=Math.max(r1, Math.max(g1, b1));
			int r2=color2>>16&255;
			int g2=color2>>8&255;
			int b2=color2&255;
			int max2=Math.max(r2, Math.max(g2, b2));
			r1=(r1+r2)/2;
			g1=(g1+g2)/2;
			b1=(b1+b2)/2;
			int maxsr=(max1+max2)/2;
			max1=Math.max(r1, Math.max(g1, b1));
			r1=r1*maxsr/max1;
			g1=g1*maxsr/max1;
			b1=b1*maxsr/max1;
			int endcolor=(r1<<16)+(g1<<8)+b1;
			//System.out.println("1: "+r1+" "+g1+" "+b1+" "+"2: "+r2+" "+g2+" "+b2+" en "+endcolor);
			return par2 == 0 ? 16777215 : endcolor;
		}
		else{
			return 16777215;
		}
    }
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(ItemStack stack, int pass)
    {
        return pass > 0 ? (stack.hasTagCompound()?this.theIcon:this.blankIcon) : super.getIcon(stack, pass);
    }
}
