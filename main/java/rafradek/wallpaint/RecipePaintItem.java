package rafradek.wallpaint;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecipePaintItem implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting p_77569_1_, World p_77569_2_) {
		boolean containItem=false;
		boolean containPaint=false;
		int oldColor=0;
		int paintColor=0;
		for(int i=0;i<p_77569_1_.getSizeInventory();i++){
			ItemStack stack=p_77569_1_.getStackInSlot(i);
			if(stack!=null&&!(stack.getItem() instanceof ItemPaintBucket)&&!(stack.getItem() instanceof ItemBlock)&&!(stack.getItem() instanceof ItemArmor)){
				if(!containItem){
					containItem=true;
					oldColor=stack.hasTagCompound()&&stack.stackTagCompound.hasKey("RColorFilter")?stack.stackTagCompound.getByte("RColorFilter"):0xFFFFFFFF;
				}
				else
					return false;
			}
			else if(stack!=null&&stack.getItem() instanceof ItemPaintBucket){
				if(!containPaint){
					containPaint=true;
					paintColor=stack.stackTagCompound.getByte("color");
				}
				else
					return false;
			}
			else if(stack!=null&&(stack.getItem() instanceof ItemBlock||stack.getItem() instanceof ItemArmor)){
				return false;
			}
		}
		return containItem && containPaint && oldColor != paintColor;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting p_77572_1_) {
		ItemStack stackItem=null;
		ItemStack stackPaint=null;
		for(int i=0;i<p_77572_1_.getSizeInventory();i++){
			ItemStack stack=p_77572_1_.getStackInSlot(i);
			if(stack!=null&&!(stack.getItem() instanceof ItemPaintBucket)){
				stackItem=stack;
			}
			else if(stack!=null&&stack.getItem() instanceof ItemPaintBucket){
				stackPaint=stack;
			}
		}
		if(stackItem==null || stackPaint==null) return null;
		ItemStack result=stackItem.copy();
		result.stackSize=1;
		if(!result.hasTagCompound()){
			result.stackTagCompound=new NBTTagCompound();
		}
		result.stackTagCompound.setByte("RColorFilter", stackPaint.stackTagCompound.getByte("color"));
		return result;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public ItemStack getRecipeOutput() {
		// TODO Auto-generated method stub
		return null;
	}

}
