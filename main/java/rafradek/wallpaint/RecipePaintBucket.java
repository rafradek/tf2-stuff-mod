package rafradek.wallpaint;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecipePaintBucket implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting p_77569_1_, World p_77569_2_) {
		int dyeColor[]=new int[3];
		boolean hasSlimeball=p_77569_1_.getStackInSlot(4) != null && p_77569_1_.getStackInSlot(4).getItem() == Items.slime_ball;
		boolean hasBucket=p_77569_1_.getStackInSlot(7) != null && p_77569_1_.getStackInSlot(7).getItem() == Items.water_bucket;
		for(int i=0;i<3;i++){
			if(p_77569_1_.getStackInSlot(i) != null && p_77569_1_.getStackInSlot(i).getItem() instanceof ItemDye){
				dyeColor[i]=p_77569_1_.getStackInSlot(i).getItemDamage();
			}
			else{
				return false;
			}
		}
		boolean emptyPlaces=p_77569_1_.getStackInSlot(3) == null && p_77569_1_.getStackInSlot(5) == null && p_77569_1_.getStackInSlot(6) == null && p_77569_1_.getStackInSlot(8) == null;
		return hasBucket && hasSlimeball && emptyPlaces && (dyeColor[0]==dyeColor[1]||dyeColor[0]==dyeColor[2]||dyeColor[1]==dyeColor[2]);
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting p_77572_1_) {
		int color1=-1;
		int color2=-1;
		for(int i=0;i<3;i++){
			if(color1==-1){
				color1=p_77572_1_.getStackInSlot(i).getItemDamage();
				color2=p_77572_1_.getStackInSlot(i).getItemDamage();
				
			}
			else if(p_77572_1_.getStackInSlot(i).getItemDamage()!=color1){
				if(color1>p_77572_1_.getStackInSlot(i).getItemDamage()){
					color2=p_77572_1_.getStackInSlot(i).getItemDamage();
				}
				else{
					color2=color1;
					color1=p_77572_1_.getStackInSlot(i).getItemDamage();
				}
			}
		}
		ItemStack stack=new ItemStack(WallPaint.paintBucket);
		stack.stackTagCompound=new NBTTagCompound();
		stack.stackTagCompound.setByte("color", (byte) (color1+(color2<<4)));
		stack.stackTagCompound.setBoolean("cnv", true);
		return stack;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public ItemStack getRecipeOutput() {
		// TODO Auto-generated method stub
		return null;
	}

}
