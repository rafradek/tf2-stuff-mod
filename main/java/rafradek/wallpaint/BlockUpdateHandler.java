package rafradek.wallpaint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rafradek.wallpaint.WallPaintMessage.UpdateSend;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.chunk.Chunk;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class BlockUpdateHandler implements IMessageHandler<WallPaintMessage.UpdateSend, IMessage> {

	@Override
	public IMessage onMessage(UpdateSend message, MessageContext ctx) {
			setBlock(message, ClientProxy.map,Minecraft.getMinecraft().theWorld.getChunkFromBlockCoords(message.data[0], message.data[2]));
			Minecraft.getMinecraft().theWorld.markBlockForUpdate(message.data[0], message.data[1], message.data[2]);
		return null;
	}

	public static class ServerSideHandler implements IMessageHandler<WallPaintMessage.UpdateSend, IMessage> {

		@Override
		public IMessage onMessage(UpdateSend message, MessageContext ctx) {
			ItemStack stack=ctx.getServerHandler().playerEntity.getHeldItem();
			ItemStack bucket=message.currColor>-1?ctx.getServerHandler().playerEntity.inventory.mainInventory[message.currColor]:null;
			if(MinecraftServer.getServer().isSinglePlayer()||(ctx.getServerHandler().playerEntity.getDistanceSq(message.data[0], message.data[1], message.data[2])<121 
					&& stack != null &&((stack.getItem() instanceof ItemBrush && bucket !=null) ||stack.getItem() instanceof ItemScraper))){
				
				setBlock(message, CommonProxy.map,ctx.getServerHandler().playerEntity.worldObj.getChunkFromBlockCoords(message.data[0], message.data[2]));
				//WallPaint.logger.info("Paint updated");
				stack.damageItem(1, ctx.getServerHandler().playerEntity);
				if(stack.getItem() instanceof ItemBrush){
					byte currColor=-1;
					if(bucket != null && bucket.getItem() instanceof ItemPaintBucket&&bucket.hasTagCompound()){
						if(!bucket.getTagCompound().hasKey("cnv")){
							bucket.stackTagCompound.setByte("color", (byte) (bucket.getTagCompound().getByte("color")+(bucket.getTagCompound().getByte("color")<<4)));
							bucket.stackTagCompound.setBoolean("cnv", true);
						}
						currColor=bucket.stackTagCompound.getByte("color");
						if(bucket.getItemDamage()<bucket.getMaxDamage())
							bucket.damageItem(1, ctx.getServerHandler().playerEntity);
						else
							ctx.getServerHandler().playerEntity.inventory.setInventorySlotContents(message.currColor, new ItemStack(Items.bucket));
					}
					if(!stack.hasTagCompound())
						stack.setTagCompound(new NBTTagCompound());
					stack.stackTagCompound.setByte("color", currColor);
				}
				
					
				if(!MinecraftServer.getServer().isSinglePlayer()){
					WallPaint.network.sendToDimension(message, ctx.getServerHandler().playerEntity.worldObj.provider.dimensionId);
				}
			}
			return null;
		}

	}
	public static void setBlock(UpdateSend message, Map<Chunk,ArrayList<int[]>> map,Chunk chunk){
		if(!map.containsKey(chunk)){
			if(message.data[3]==0){
				return;
			}
			map.put(chunk, new ArrayList<int[]>());
		}
		//System.out.println("x: "+message.data[0]+"y: "+message.data[1]+"z: "+message.data[2]+" side: "+message.data[3]);
		boolean add=true;
		Iterator<int[]> iterator=map.get(chunk).iterator();
		while(iterator.hasNext()){
			int[] array=iterator.next();
			if(array[0]==message.data[0]&&array[1]==message.data[1]&&array[2]==message.data[2]){
				if(message.data[3]==0){
					iterator.remove();
				}
				else{
					array[3]=message.data[3];
					array[4]=message.data[4];
				}
				add=false;
				break;
			}
		}
		if(add){
			map.get(chunk).add(message.data);
		}
		
	}
}
