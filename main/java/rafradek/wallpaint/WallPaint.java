package rafradek.wallpaint;

import static net.minecraftforge.oredict.RecipeSorter.Category.SHAPELESS;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rafradek.wallpaint.WallPaintMessage.ArrayRequest;
import rafradek.wallpaint.WallPaintMessage.ArrayReturn;
import rafradek.wallpaint.WallPaintMessage.UpdateSend;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = "rafradek_wallpaint", name =  "Wall Painter", version = "1.5.2")
public class WallPaint {
	public static ItemBrush paintBrush;
	public static ItemScraper scraper;
	public static ItemPaintBucket paintBucket;
	public static SimpleNetworkWrapper network;
	public static ArrayList<int[]> paints=new ArrayList<int[]>();
	@SidedProxy(modId="rafradek_wallpaint",clientSide="rafradek.wallpaint.ClientProxy", serverSide="rafradek.wallpaint.CommonProxy")
	public static CommonProxy proxy;
	public static CreativeTabs tabPaint;
	public static float[][] dyesFloatVal=new float[16][3];
	public static CreativeTabs tabPaintAll;
	public static final Logger logger = LogManager.getLogger("wallpaint");
	@Mod.EventHandler
    public void init(FMLPreInitializationEvent event)
    {
		//EntityRegistry.registerModEntity(EntityPaint.class, "paint_raf", 1, this, 80,Integer.MAX_VALUE, false);
		tabPaint=new CreativeTabs("wallpaint"){
			@Override
			public Item getTabIconItem() {
				return paintBrush;
			}
		};
		tabPaintAll=new CreativeTabs("wallpaintall"){
			@Override
			public Item getTabIconItem() {
				return paintBucket;
			}
		};
		GameRegistry.registerItem(paintBrush=new ItemBrush(),"brushraf");
		GameRegistry.registerItem(paintBucket=new ItemPaintBucket(),"paintbucketraf");
		GameRegistry.registerItem(scraper=new ItemScraper(),"scraperraf");
		GameRegistry.addRecipe(new RecipePaintBucket());
		for(int i=0;i<16;i++){
			ItemStack stack=new ItemStack(paintBucket);
			stack.stackTagCompound=new NBTTagCompound();
			stack.stackTagCompound.setByte("color", (byte) (i+(i<<4)));
			stack.stackTagCompound.setBoolean("cnv", true);
			GameRegistry.addRecipe(stack, new Object[]{"XXX"," Y "," Z ",'X',new ItemStack(Items.dye,1,i),'Y',Items.slime_ball,'Z',Items.water_bucket});
		}
		/*for(int i=1;i<16;i++){
			ItemStack stack=new ItemStack(paintBucket);
			stack.stackTagCompound=new NBTTagCompound();
			stack.stackTagCompound.setByte("color", (byte) (i<<4));
			stack.stackTagCompound.setBoolean("cnv", true);
			ItemStack stackToConvert=new ItemStack(paintBucket);
			stack.stackTagCompound=new NBTTagCompound();
			stack.stackTagCompound.setByte("color", (byte) (i));
			stack.stackTagCompound.setBoolean("cnv", true);
			GameRegistry.addShapelessRecipe(stack, new Object[]{stackToConvert,Items.wheat_seeds,Items.wheat_seeds,Items.wheat_seeds});
		}*/
		GameRegistry.addRecipe(new RecipePaintItem());
		GameRegistry.addRecipe(new RecipeMixPaint());
		GameRegistry.addRecipe(new RecipeGrassPaint());
		GameRegistry.addRecipe(new ItemStack(scraper), new Object[]{"XXX"," X "," Y ",'X',Items.iron_ingot,'Y',Items.stick});
		GameRegistry.addRecipe(new ItemStack(paintBrush), new Object[]{"XXX"," YY"," Z ",'X',Items.string,'Y',Items.iron_ingot,'Z',Items.stick});
		GameRegistry.addRecipe(new ItemStack(paintBrush), new Object[]{"XXX","YY "," Z ",'X',Items.string,'Y',Items.iron_ingot,'Z',Items.stick});
		RecipeSorter.register("wallpaint:PaintItem",     ShapedOreRecipe.class,    SHAPELESS,    "after:minecraft:shapeless");
        //register("forge:shapelessore",  ShapelessOreRecipe.class, SHAPELESS, "after:minecraft:shaped before:minecraft:shapeless");
		//GameRegistry.registerBlock(new BlockMimic(), "mimicraf");
		network=NetworkRegistry.INSTANCE.newSimpleChannel("rafradek_wallpaint");
		network.registerMessage(ArrayMessageHandler.class, ArrayRequest.class, 0, Side.SERVER);
		network.registerMessage(ArrayMessageHandler.ArrayReturnMessageHandler.class, ArrayReturn.class, 1, Side.CLIENT);
		network.registerMessage(BlockUpdateHandler.class, UpdateSend.class, 2, Side.CLIENT);
		network.registerMessage(BlockUpdateHandler.ServerSideHandler.class, UpdateSend.class, 3, Side.SERVER);
		//GameRegistry.registerBlock(blockLaserEmitter=(BlockLaserEmitter) new BlockLaserEmitter().setHardness(3.5F).setStepSound(Block.soundTypeStone).setBlockName("dispenser").setBlockTextureName("laseremitter"), "laserrafblock");
		FMLCommonHandler.instance().bus().register(new WallPaintEventHandler());
		for(int i=0;i<16;i++){
			dyesFloatVal[15-i][0]=(float)((ItemDye.field_150922_c[i]>>16)&255)/255f;
			dyesFloatVal[15-i][1]=(float)((ItemDye.field_150922_c[i]>>8)&255)/255f;
			dyesFloatVal[15-i][2]=(float)((ItemDye.field_150922_c[i])&255)/255f;
			//System.out.println("color: "+i+" r: "+dyesFloatVal[i][0]+" g: "+dyesFloatVal[i][1]+" b: "+dyesFloatVal[i][2]);
		}
		MinecraftForge.EVENT_BUS.register(new WallPaintEventHandler());
		proxy.doRegister();
    }
	
	public static int sideConvert(int side){
		if(side==0){
			return 1;
		} 
		else if(side==1){
			return 2;
		}
		else if(side==2){
			return 4;
		}
		else if(side==3){
			return 8;
		}
		else if(side==4){
			return 16;
		}
		return 32;
	}
	public static int unSign(int color){
		if(color<0){
			color+=256;
		}
		return color;
	}
}
