package rafradek.wallpaint;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;

public class ItemPaintBucket extends Item {
	public IIcon grassOverlay;
	public ItemPaintBucket(){
		//this.setMaxDamage(type.durablity);
		this.setMaxStackSize(1);
		this.setCreativeTab(WallPaint.tabPaintAll);
		this.setUnlocalizedName("paintbucket");
		this.setMaxDamage(80);
		this.setTextureName("wallpaint:bucket_overlay");
		this.setNoRepair();
	}
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister)
    {
		grassOverlay=par1IconRegister.registerIcon("wallpaint:bucket_grass_overlay");
        super.registerIcons(par1IconRegister);
    }
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
    public void getSubItems(Item p_150895_1_, CreativeTabs p_150895_2_, List p_150895_3_)
    {
		if(p_150895_2_!=WallPaint.tabPaintAll){
	        for (int i = 0; i < 16; ++i)
	        {
	        	ItemStack stack=new ItemStack(p_150895_1_);
	        	stack.setTagCompound(new NBTTagCompound());
	        	stack.stackTagCompound.setByte("color", (byte) (i+(i<<4)));
	        	stack.stackTagCompound.setBoolean("cnv", true);
	            p_150895_3_.add(stack);
	        }
		}
		else{
			for (int i = 0; i < 256; ++i)
	        {
				int a=i>127?i-256:i;
				if(!(((a&15)>=0 && (WallPaint.unSign(a)>>4)>0 && (a&15)<(WallPaint.unSign(a)>>4)))){
						//||((a&15)>7 && (WallPaint.unSign(a)>>4)<8/*&&(i&15)<(WallPaint.unSign(i)>>4)*/)
						//||((a&15)<7 && (WallPaint.unSign(a)>>4)<8 && (a&15)<(WallPaint.unSign(a)>>4)))){
		        	ItemStack stack=new ItemStack(p_150895_1_);
		        	stack.setTagCompound(new NBTTagCompound());
		        	stack.stackTagCompound.setByte("color", (byte) a);
		        	stack.stackTagCompound.setBoolean("cnv", true);
		            p_150895_3_.add(stack);
				}
	        }
			for (int i = 1; i < 16; ++i)
	        {
	       		ItemStack stack=new ItemStack(p_150895_1_);
	        	stack.setTagCompound(new NBTTagCompound());
	        	stack.stackTagCompound.setByte("color", (byte) (i<<4));
	        	stack.stackTagCompound.setBoolean("cnv", true);
	            p_150895_3_.add(stack);
	        }
		}
    }
	public CreativeTabs[] getCreativeTabs(){
		return new CreativeTabs[]{WallPaint.tabPaint,WallPaint.tabPaintAll};
	}
	@SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }
	@SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
		if(par1ItemStack.hasTagCompound()&&(par1ItemStack.getTagCompound().getByte("color")&15)==(par1ItemStack.getTagCompound().getByte("color")>>4)){
			return par2 == 0 ? 16777215 : ItemDye.field_150922_c[par1ItemStack.getTagCompound().getByte("color")&15];
		}
		else if(par1ItemStack.hasTagCompound()){
			byte color=par1ItemStack.getTagCompound().getByte("color");
			int color1=ItemDye.field_150922_c[color&15];
			int color2=ItemDye.field_150922_c[WallPaint.unSign(color)>>4];
			int r1=color1>>16&255;
			int g1=color1>>8&255;
			int b1=color1&255;
			int max1=Math.max(r1, Math.max(g1, b1));
			int r2=color2>>16&255;
			int g2=color2>>8&255;
			int b2=color2&255;
			int max2=Math.max(r2, Math.max(g2, b2));
			r1=(r1+r2)/2;
			g1=(g1+g2)/2;
			b1=(b1+b2)/2;
			int maxsr=(max1+max2)/2;
			max1=Math.max(r1, Math.max(g1, b1));
			r1=r1*maxsr/max1;
			g1=g1*maxsr/max1;
			b1=b1*maxsr/max1;
			int endcolor=(r1<<16)+(g1<<8)+b1;
			//System.out.println("1: "+r1+" "+g1+" "+b1+" "+"2: "+r2+" "+g2+" "+b2+" en "+endcolor);
			return par2 == 0 ? 16777215 : endcolor;
		}
		else{
			return 16777215;
		}
    }
	@SuppressWarnings("rawtypes")
	@SideOnly(Side.CLIENT)
    public void addInformation(ItemStack p_77624_1_, EntityPlayer p_77624_2_, List p_77624_3_, boolean p_77624_4_) {
		if(p_77624_1_.hasTagCompound()){
			int color1=p_77624_1_.stackTagCompound.getByte("color")&15;
			int color2=WallPaint.unSign(p_77624_1_.stackTagCompound.getByte("color"))>>4;
			if(color2<=color1){
				p_77624_3_.add(StatCollector.translateToLocal("item.fireworksCharge." + ItemDye.field_150923_a[color1]));
				if(color1!=color2){
					p_77624_3_.add(StatCollector.translateToLocal("item.fireworksCharge." + ItemDye.field_150923_a[color2]));
				}
			}
			else{
				p_77624_3_.add(StatCollector.translateToLocal("item.fireworksCharge." + ItemDye.field_150923_a[color2]));
				p_77624_3_.add(StatCollector.translateToLocal("tile.tallgrass.name"));
			}
		}
	}
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(ItemStack stack, int pass)
    {
		if(stack.hasTagCompound()&&(stack.getTagCompound().getByte("color")&15)<(WallPaint.unSign(stack.getTagCompound().getByte("color"))>>4)){
			return pass > 0 ? grassOverlay : Items.bucket.getIcon(stack, pass);
		}
        return pass > 0 ? super.getIcon(stack, pass) : Items.bucket.getIcon(stack, pass);
    }
}
