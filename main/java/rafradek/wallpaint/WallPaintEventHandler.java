package rafradek.wallpaint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import rafradek.wallpaint.WallPaintMessage.UpdateSend;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagIntArray;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class WallPaintEventHandler {
	@SubscribeEvent
	public void blockBreak(BlockEvent.BreakEvent event){
		Chunk chunk=event.world.getChunkFromBlockCoords(event.x, event.z);
    	if(CommonProxy.map.containsKey(chunk)){
    		Iterator<int[]> iterator=CommonProxy.map.get(chunk).iterator();
	    	while(iterator.hasNext()){
	    		int[] array=iterator.next();
	        	if(array[0]==event.x&&array[1]==event.y&&array[2]==event.z){
	        		array[3]=0;
	        		WallPaint.network.sendToDimension(new UpdateSend(array,-1), event.world.provider.dimensionId);
	        		iterator.remove();
	        		break;
	        	}
	        }
    	}
	}
	@SubscribeEvent
	public void ChunkLoad(ChunkEvent.Load event){
		if(event.world.isRemote){
			WallPaint.network.sendToServer(new WallPaintMessage.ArrayRequest(event.getChunk().xPosition,event.getChunk().zPosition));
		}
	}
	@SubscribeEvent
	public void ChunkUnLoad(ChunkEvent.Unload event){
		if(event.world.isRemote){
			ClientProxy.map.remove(event.getChunk());
		}
		else{
			//if(CommonProxy.map.containsKey(event.getChunk()))
				//WallPaint.logger.info("Unloading chunk with paint");
			CommonProxy.map.remove(event.getChunk());
		}
	}
	
	@SubscribeEvent
	public void ChunkDataLoad(ChunkDataEvent.Load event){
		//WallPaint.logger.info("chunk load");
		NBTTagCompound levelTag=event.getData().getCompoundTag("Paint");
		if(event.getData().hasKey("arrays")){
			levelTag.setTag("paintarrays", event.getData().getTag("arrays").copy());
			event.getData().removeTag("arrays");
		}
		if(levelTag.hasKey("paintarrays")){
			//System.out.println("fsfdsfdsfzaladowano");
			NBTTagList taglist=(NBTTagList) levelTag.getTag("paintarrays");
			ArrayList<int[]> list=new ArrayList<int[]>();
			/*boolean cnv=levelTag.hasKey("cnv");
			boolean cnv2=levelTag.hasKey("cnv2");*/
			//WallPaint.logger.info("found chunk with paint: "+taglist.tagCount());
			for(int i=0; i<taglist.tagCount();i++){
				int[] array=taglist.func_150306_c(i);
				/*if(array.length<5){
					array=Arrays.copyOf(array, 5);
					array[4]=0;
				}
				if(!cnv){
					array[4]+=(array[3]>>7)<<7;
				}
				if(!cnv2){
					array[3]=(array[3]&63)+((array[3]>>7)<<6);
					array[4]=(array[4]&63)+((array[4]>>7)<<6);
				}*/
				list.add(array);
			}
			CommonProxy.map.put(event.getChunk(), list);
		}
		
	}
	@SubscribeEvent
	public void ChunkDataSave(ChunkDataEvent.Save event){
		if(CommonProxy.map.containsKey(event.getChunk())){
			//System.out.println("zapisano");
			NBTTagCompound levelTag=event.getData().getCompoundTag("Paint");
			NBTTagList taglist=new NBTTagList();
			ArrayList<int[]> list=CommonProxy.map.get(event.getChunk());
			//WallPaint.logger.info("saving chunk with paint:"+list.size());
			for(int i=0; i<list.size();i++){
				if(event.world.getBlock(list.get(i)[0], list.get(i)[1], list.get(i)[2])!=Blocks.air){
					taglist.appendTag(new NBTTagIntArray(list.get(i)));
				}
			}
			
			levelTag.setTag("paintarrays",taglist);
			event.getData().setTag("Paint", levelTag);
			/*event.getData().setBoolean("cnv", true);
			event.getData().setBoolean("cnv2", true);*/
		}
	}
	@SideOnly(value = Side.CLIENT)
	@SubscribeEvent
	public void TextureMake(TextureStitchEvent.Pre event){
		if(event.map.getTextureType()==0){
			ClientProxy.wallicon=event.map.registerIcon("wallpaint:wall");
			ClientProxy.replaced.put("dirt",event.map.registerIcon("wallpaint:dirt_white"));
			ClientProxy.replaced.put("planks",event.map.registerIcon("wallpaint:planks_white"));
			ClientProxy.replaced.put("log",event.map.registerIcon("wallpaint:log_oak_white"));
			ClientProxy.replaced.put("log_jungle",event.map.registerIcon("wallpaint:log_jungle_white"));
			ClientProxy.replaced.put("log_top",event.map.registerIcon("wallpaint:log_top_white"));
			ClientProxy.replaced.put("door_wood_upper",event.map.registerIcon("wallpaint:door_wood_upper_white"));
			ClientProxy.replaced.put("door_wood_lower",event.map.registerIcon("wallpaint:door_wood_lower_white"));
			ClientProxy.replaced.put("sand",event.map.registerIcon("wallpaint:sand_white"));
			ClientProxy.replaced.put("brick",event.map.registerIcon("wallpaint:brick_white"));
			ClientProxy.replaced.put("sandstone_bottom",event.map.registerIcon("wallpaint:sandstone_bottom_white"));
			ClientProxy.replaced.put("sandstone_top",event.map.registerIcon("wallpaint:sandstone_top_white"));
			ClientProxy.replaced.put("sandstone_normal",event.map.registerIcon("wallpaint:sandstone_normal_white"));
			ClientProxy.replaced.put("sandstone_smooth",event.map.registerIcon("wallpaint:sandstone_smooth_white"));
			ClientProxy.replaced.put("sandstone_carved",event.map.registerIcon("wallpaint:sandstone_carved_white"));
		}
	}
	@SubscribeEvent
	public void ItemCrafted(PlayerEvent.ItemCraftedEvent event){
		if(event.crafting != null && event.crafting.getItem() instanceof ItemPaintBucket){
			event.crafting.getTagCompound().setBoolean("cnv", true);
			if(event.craftMatrix.getSizeInventory()>4){
				event.craftMatrix.setInventorySlotContents(7, null);
			}
			if(event.crafting.stackSize>1){
				ItemStack stack=event.crafting.splitStack(1);
				if(!event.player.inventory.addItemStackToInventory(stack)){
					event.player.dropPlayerItemWithRandomChoice(stack, false);
				}
			}
		}
		else if(event.crafting != null&&event.crafting.hasTagCompound()&&event.crafting.stackTagCompound.hasKey("RColorFilter")){
			for(int i=0;i<event.craftMatrix.getSizeInventory();i++){
				ItemStack stack=event.craftMatrix.getStackInSlot(i);
				if(stack!=null&&stack.getItem() instanceof ItemPaintBucket){
					stack.stackSize++;
					stack.damageItem(1, event.player);
					/*if(!event.player.inventory.addItemStackToInventory(stack)){
						event.player.dropPlayerItemWithRandomChoice(stack, false);
					}*/
				}
			}
		}
	}
	
}
