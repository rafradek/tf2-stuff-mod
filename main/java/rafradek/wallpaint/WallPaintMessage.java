package rafradek.wallpaint;

import java.util.ArrayList;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;

public abstract class WallPaintMessage implements IMessage{
	
	public static class ArrayRequest extends WallPaintMessage{

		public int x;
		public int y;
		public ArrayRequest(){
			
		}
		public ArrayRequest(int x,int y){
			this.x=x;
			this.y=y;
		}
		@Override
		public void fromBytes(ByteBuf buf) {
			this.x=buf.readInt();
			this.y=buf.readInt();
		}

		@Override
		public void toBytes(ByteBuf buf) {
			buf.writeInt(x);
			buf.writeInt(y);
		}
	}
	public static class UpdateSend extends WallPaintMessage{

		public int[] data;
		public int currColor;
		public UpdateSend(){
			
		}
		public UpdateSend(int[] data,int currColor){
			this.data=data;
			this.currColor=currColor;
		}
		@Override
		public void fromBytes(ByteBuf buf) {
			data=new int[5];
			this.currColor=buf.readByte();
			int index=0;
			while(buf.readableBytes()>0){
				data[index]=buf.readInt();
				index++;
			}
		}

		@Override
		public void toBytes(ByteBuf buf) {
			buf.writeByte(currColor);
			for(int value:data){
				buf.writeInt(value);
			}
		}
	}
	public static class ArrayReturn extends WallPaintMessage{

		public int x;
		public int y;
		public ArrayList<int[]> list;
		public ArrayReturn(){
			
		}
		public ArrayReturn(int x,int y, ArrayList<int[]> arrayList){
			this.x=x;
			this.y=y;
			this.list=arrayList;
		}
		@Override
		public void fromBytes(ByteBuf buf) {
			this.x=buf.readInt();
			this.y=buf.readInt();
			this.list=new ArrayList<int[]>();
			int index=0;
			int[] array=new int[5];
			while(buf.readableBytes()>0){
				array[index]=buf.readInt();
				index++;
				if(index>4){
					index=0;
					this.list.add(array);
					array=new int[5];
				}
			}
		}

		@Override
		public void toBytes(ByteBuf buf) {
			buf.writeInt(x);
			buf.writeInt(y);
			for(int[] array:list){
				for(int value: array){
					buf.writeInt(value);
				}
				//buf.writeByte(0);
			}
		}
	}
}
