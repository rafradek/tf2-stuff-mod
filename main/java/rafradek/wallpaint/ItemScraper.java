package rafradek.wallpaint;

import java.util.Iterator;
import java.util.List;

import rafradek.wallpaint.WallPaintMessage.UpdateSend;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ItemScraper extends Item {

	public ItemScraper(){
		this.setMaxStackSize(1);
		this.setCreativeTab(WallPaint.tabPaint);
		this.setUnlocalizedName("scraper");
		this.setMaxDamage(500);
		this.setTextureName("wallpaint:scraper");
	}
	@SuppressWarnings("unchecked")
	public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_, World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {
		if(p_77648_3_.isRemote){
			Chunk chunk=p_77648_3_.getChunkFromBlockCoords(p_77648_4_, p_77648_6_);
	    	if(WallPaint.proxy.getSidedMap().containsKey(chunk)){
	    		Iterator<int[]> iterator=WallPaint.proxy.getSidedMap().get(chunk).iterator();
		    	while(iterator.hasNext()){
		    		int[] array=iterator.next();
		        	if(array[0]==p_77648_4_&&array[1]==p_77648_5_&&array[2]==p_77648_6_){
		        		if((array[3]&WallPaint.sideConvert(p_77648_7_))!=0){
							array[3]-=WallPaint.sideConvert(p_77648_7_);
							if((array[4]&WallPaint.sideConvert(p_77648_7_))!=0){
								array[4]-=WallPaint.sideConvert(p_77648_7_);
							}
							array[3]-=((array[3]>>(6+4*p_77648_7_))&15)<<(6+4*p_77648_7_);
							array[4]-=((array[4]>>(6+4*p_77648_7_))&15)<<(6+4*p_77648_7_);
							//System.out.println("dodano: "+WallPaint.sideConvert(p_77648_7_)+", teraz: "+array[3]);
							//WallPaint.network.sendToDimension(new UpdateSend(array), p_77648_3_.provider.dimensionId);
							WallPaint.network.sendToServer(new UpdateSend(array,-1));
							if(array[3]==0){
								//System.out.println("wyzerowano");
								iterator.remove();
							}
							Minecraft.getMinecraft().theWorld.markBlockForUpdate(p_77648_4_, p_77648_5_, p_77648_6_);
							break;
						}
		        	}
		        }
	    	}
		}
		return true;
    }
}
