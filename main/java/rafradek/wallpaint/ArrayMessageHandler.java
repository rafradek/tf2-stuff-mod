package rafradek.wallpaint;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.chunk.Chunk;
import rafradek.wallpaint.WallPaintMessage.ArrayRequest;
import rafradek.wallpaint.WallPaintMessage.ArrayReturn;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class ArrayMessageHandler implements IMessageHandler<WallPaintMessage.ArrayRequest, WallPaintMessage.ArrayReturn> {

	@Override
	public ArrayReturn onMessage(ArrayRequest message, MessageContext ctx) {
		EntityPlayerMP player=ctx.getServerHandler().playerEntity;
		Chunk chunk=player.worldObj.getChunkFromChunkCoords(message.x, message.y);
		if(CommonProxy.map.get(chunk)==null) return null;
		//WallPaint.logger.info("Sending painted chunk to: "+player.getDisplayName()+" "+message.x+" "+message.y);
		return new ArrayReturn(message.x, message.y,CommonProxy.map.get(chunk));
	}
	public static class ArrayReturnMessageHandler implements IMessageHandler<ArrayReturn, IMessage> {

		@Override
		public IMessage onMessage(ArrayReturn message, MessageContext ctx) {
			//System.out.println("doszlo");
			Chunk chunk=Minecraft.getMinecraft().theWorld.getChunkFromChunkCoords(message.x, message.y);
			ClientProxy.map.put(chunk, message.list);
			int[] array=message.list.get(0);
			Minecraft.getMinecraft().theWorld.markBlockForUpdate(array[0],array[1],array[2]);
			return null;
		}

	}
}
