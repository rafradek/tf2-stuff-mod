package rafradek.wallpaint;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecipeGrassPaint implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting p_77569_1_, World p_77569_2_) {
		boolean bucketFound=false;
		int seedsFound=0;
		int color=-1;
		for(int i=0;i<p_77569_1_.getSizeInventory();i++){
			if(p_77569_1_.getStackInSlot(i) != null){
				if(p_77569_1_.getStackInSlot(i).getItem() instanceof ItemPaintBucket &&p_77569_1_.getStackInSlot(i).hasTagCompound()){
					color=p_77569_1_.getStackInSlot(i).stackTagCompound.getByte("color");
					if(!bucketFound&&(WallPaint.unSign(color)>>4) == (color&15)&&color!=0){
						bucketFound=true;
					}
					else{
						return false;
					}
				}
				else if(p_77569_1_.getStackInSlot(i).getItem() == Items.wheat_seeds){
					seedsFound++;
				}
				else{
					return false;
				}
			}
		}
		return seedsFound==3;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting p_77572_1_) {
		int bucketColor=-1;
		for(int i=0;i<p_77572_1_.getSizeInventory();i++){
			if(p_77572_1_.getStackInSlot(i) != null){
				if(p_77572_1_.getStackInSlot(i).getItem() instanceof ItemPaintBucket){
					bucketColor=p_77572_1_.getStackInSlot(i).stackTagCompound.getByte("color")&15;
				}
			}
		}
		if(bucketColor>-1){
			ItemStack stack=new ItemStack(WallPaint.paintBucket);
			stack.stackTagCompound=new NBTTagCompound();
			stack.stackTagCompound.setByte("color",(byte) (bucketColor<<4));
			return stack;
		}
		return null;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public ItemStack getRecipeOutput() {
		// TODO Auto-generated method stub
		return null;
	}

}
