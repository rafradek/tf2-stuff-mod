package rafradek.wallpaint.core;

import java.util.Arrays;
import java.util.Map;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import rafradek.wallpaint.ClientProxy;
import rafradek.wallpaint.CommonProxy;
import rafradek.wallpaint.WallPaint;
import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ILanguageAdapter;
import cpw.mods.fml.common.LoadController;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.ProxyInjector;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLConstructionEvent;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.Side;

public class WallPaintLoadingPlugin implements IFMLLoadingPlugin {

	@Override
	public String[] getASMTransformerClass() {
		// TODO Auto-generated method stub
		return new String[]{WallPaintClassTransformer.class.getName()};
	}

	@Override
	public String getModContainerClass() {
		
		return null;//WallPaintModContainer.class.getName();
	}

	@Override
	public String getSetupClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAccessTransformerClass() {
		// TODO Auto-generated method stub
		return null;
	}
	/*public static class WallPaintModContainer extends DummyModContainer{
		@SidedProxy(modId="rafradek_wallpaint_core",clientSide="rafradek.wallpaint.ClientProxy", serverSide="rafradek.wallpaint.CommonProxy")
		public static CommonProxy proxy;
		public WallPaint mod=new WallPaint();
		public WallPaintModContainer(){
			super(new ModMetadata());
			ModMetadata meta = getMetadata();
			meta.modId = "rafradek_wallpaint_core";
			meta.name = "Wall Paint";
			meta.version = "@VERSION@";
			meta.authorList = Arrays.asList("rafradek");
			meta.description = "";
			meta.url = "";
			meta.updateUrl = "";
			meta.screenshots = new String[0];
			meta.logoFile = "";
		}
		@Override
	    public Object getMod()
	    {
	        return mod;
	    }
		@Override
	    public boolean matches(Object mod)
	    {
	        return mod instanceof WallPaint;
	    }
		@Override
		public boolean registerBus(EventBus bus, LoadController controller) {
			//bus.register(this);
			return false;
		}

		@Subscribe
		public void modConstruction(FMLConstructionEvent evt){
			//WallPaint.proxy=FMLCommonHandler.instance().getSide()==Side.CLIENT?new ClientProxy():new CommonProxy();
		}

		@Subscribe
		public void preInit(FMLPreInitializationEvent evt) {
			//mod.init(evt);
		}
	}*/
}
