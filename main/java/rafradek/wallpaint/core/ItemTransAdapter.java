package rafradek.wallpaint.core;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class ItemTransAdapter extends ClassVisitor implements Opcodes{
	public String owner;
	public boolean obf;
	public ItemTransAdapter(ClassVisitor cv,boolean obf) {
		super(ASM4, cv);
		this.obf=obf;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void visit(int version, int access, String name,
	String signature, String superName, String[] interfaces) {
		cv.visit(version, access, name, signature, superName, interfaces);
		owner = name;
	}
	
	@Override 
	public MethodVisitor visitMethod(int access, String name,
	String desc, String signature, String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
		exceptions);
		//System.out.println(name);
		//&&desc.equals("(Lnet/minecraft/block/Block;IIIFFF)Z")
		if(!obf){
			if (name.equals("getColorFromItemStack")) {
				mv = new ReplaceMethodVisitorColor(mv,"getColorFromItemStack",false);
			}
		}
		else if(desc.equals("(Ladd;I)I")){//(Lahu;DDDLps;)V
			System.out.println("znalazl metod"+ name);
			if (name.equals("a")) {
				mv =new ReplaceMethodVisitorColor(mv,"getColorFromItemStack",true);
			}
		}
		return mv;
	}
	
	class ReplaceMethodVisitorColor extends MethodVisitor{
		String linkToMethod="getColorFromItemStack";
		String itemPath;
		String itemStackPath;
		public boolean obf;
		public ReplaceMethodVisitorColor( MethodVisitor mv, String link,boolean obf) {
			super(ASM4, mv);
			this.linkToMethod=link;
			this.obf=obf;
			itemPath=obf?/*ahu*/"adb":"net/minecraft/item/Item";
			itemStackPath=obf?/*ble*/"add":"net/minecraft/item/ItemStack";
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void visitCode(){
			mv.visitCode();
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitLineNumber(672, l0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitVarInsn(ILOAD, 2);
			mv.visitMethodInsn(INVOKESTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods", this.linkToMethod, "(L"+itemStackPath+";I)I");
			mv.visitInsn(IRETURN);
			Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitLocalVariable("this", "L"+itemPath+";", null, l0, l1, 0);
			mv.visitLocalVariable("p_82790_1_", "L"+itemStackPath+";", null, l0, l1, 1);
			mv.visitLocalVariable("p_82790_2_", "I", null, l0, l1, 2);
			mv.visitMaxs(2, 3);
			mv.visitEnd();
		}
	}
}
