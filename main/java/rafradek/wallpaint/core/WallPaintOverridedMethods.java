package rafradek.wallpaint.core;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import rafradek.wallpaint.ClientProxy;
import rafradek.wallpaint.WallPaint;
import rafradek.wallpaint.WallPaintEventHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockBeacon;
import net.minecraft.block.BlockBrewingStand;
import net.minecraft.block.BlockCauldron;
import net.minecraft.block.BlockCocoa;
import net.minecraft.block.BlockDoublePlant;
import net.minecraft.block.BlockDragonEgg;
import net.minecraft.block.BlockEndPortalFrame;
import net.minecraft.block.BlockFence;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockFire;
import net.minecraft.block.BlockFlowerPot;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockHopper;
import net.minecraft.block.BlockPane;
import net.minecraft.block.BlockRailBase;
import net.minecraft.block.BlockRedstoneComparator;
import net.minecraft.block.BlockRedstoneDiode;
import net.minecraft.block.BlockRedstoneRepeater;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.BlockWall;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.src.FMLRenderAccessLibrary;
import net.minecraft.util.IIcon;
import net.minecraft.world.chunk.Chunk;

public class WallPaintOverridedMethods {

	public static boolean enabledForBlock;
	public static int[] colorTable;
	public static boolean changed0;
	public static boolean changed1;
	public static boolean changed2;
	public static boolean changed3;
	public static boolean changed4;
	public static boolean changed5;
	public static boolean filter0;
	public static boolean filter1;
	public static boolean filter2;
	public static boolean filter3;
	public static boolean filter4;
	public static boolean filter5;
	public static IIcon overrided;
	public static int x;
	public static int y;
	public static int z;
	public static int[] colorTable2;
	public static void resetTexture0(RenderBlocks render){
		render.overrideBlockTexture=overrided;
	}
	public static void renderBlockByRenderType(RenderBlocks render,Block p_147805_1_, int p_147805_2_, int p_147805_3_, int p_147805_4_)
    {
		//overrided=render.overrideBlockTexture;
        enabledForBlock=true;
        if(render.hasOverrideBlockTexture()||(!render.renderAllFaces && !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_, p_147805_3_ - 1, p_147805_4_, 0)
        		&& !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_, p_147805_3_+1, p_147805_4_, 1)
        		&& !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_, p_147805_3_, p_147805_4_- 1, 2 )
        		&& !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_, p_147805_3_, p_147805_4_+1, 3)
        		&& !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_ - 1, p_147805_3_, p_147805_4_, 4)
        		&& !p_147805_1_.shouldSideBeRendered(render.blockAccess, p_147805_2_+1, p_147805_3_, p_147805_4_, 5))){
        	enabledForBlock=false;
        }
        //overrided=render.overrideBlockTexture;
        if(enabledForBlock){
        	colorTable=new int[6];
        	colorTable2=new int[6];
        	x=p_147805_2_;
        	y=p_147805_3_;
        	z=p_147805_4_;
        	changed0=false;
        	changed1=false;
        	changed2=false;
        	changed3=false;
        	changed4=false;
        	changed5=false;
        	filter0=false;
        	filter1=false;
        	filter2=false;
        	filter3=false;
        	filter4=false;
        	filter5=false;
        	//overrided=render.overrideBlockTexture;
	    	Chunk chunk=Minecraft.getMinecraft().theWorld.getChunkFromBlockCoords(p_147805_2_, p_147805_4_);
	    	if(ClientProxy.map.containsKey(chunk)){
		    	for(int[] array:ClientProxy.map.get(chunk)){
		        	if(array[0]==p_147805_2_&&array[1]==p_147805_3_&&array[2]==p_147805_4_){
		        		//System.out.println("wyszlo: "+((array[3]>>7)&15)+" : "+((array[3]>>11)&15)+" : "+((array[3]>>15)&15));
		        		changed0=(array[3]&1)!=0;
		        		changed1=(array[3]&2)!=0;
		        		changed2=(array[3]&4)!=0;
		        		changed3=(array[3]&8)!=0;
		        		changed4=(array[3]&16)!=0;
		        		changed5=(array[3]&32)!=0;
		        		filter0=(array[4]&1)!=0;
		        		filter1=(array[4]&2)!=0;
		        		filter2=(array[4]&4)!=0;
		        		filter3=(array[4]&8)!=0;
		        		filter4=(array[4]&16)!=0;
		        		filter5=(array[4]&32)!=0;
		        		colorTable[0]=(array[3]>>6)&15;
		        		colorTable[1]=(array[3]>>10)&15;
		        		colorTable[2]=(array[3]>>14)&15;
		        		colorTable[3]=(array[3]>>18)&15;
		        		colorTable[4]=(array[3]>>22)&15;
		        		colorTable[5]=(array[3]>>26)&15;
		        		colorTable2[0]=(array[4]>>6)&15;
		        		colorTable2[1]=(array[4]>>10)&15;
		        		colorTable2[2]=(array[4]>>14)&15;
		        		colorTable2[3]=(array[4]>>18)&15;
		        		colorTable2[4]=(array[4]>>22)&15;
		        		colorTable2[5]=(array[4]>>26)&15;
		        		break;
		        	}
		        }
	    	}
        }
        /*if (l == -1)
        {
            return false;
        }
        else
        {
            p_147805_1_.setBlockBoundsBasedOnState(render.blockAccess, p_147805_2_, p_147805_3_, p_147805_4_);
            render.setRenderBoundsFromBlock(p_147805_1_);

            switch (l)
            {
            //regex: ' : \(l == ([\d]+) \?' replace: ';\ncase \1: return' ::: IMPORTANT: REMEMBER render ON FIRST line!
            case 0 : return render.renderStandardBlock(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 4: return render.renderBlockLiquid(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 31: return render.renderBlockLog(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 1: return render.renderCrossedSquares(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 40: return render.renderBlockDoublePlant((BlockDoublePlant)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 2: return render.renderBlockTorch(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 20: return render.renderBlockVine(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 11: return render.renderBlockFence((BlockFence)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 39: return render.renderBlockQuartz(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 5: return render.renderBlockRedstoneWire(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 13: return render.renderBlockCactus(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 9: return render.renderBlockMinecartTrack((BlockRailBase)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 19: return render.renderBlockStem(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 23: return render.renderBlockLilyPad(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 6: return render.renderBlockCrops(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 3: return render.renderBlockFire((BlockFire)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 8: return render.renderBlockLadder(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 7: return render.renderBlockDoor(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 10: return render.renderBlockStairs((BlockStairs)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 27: return render.renderBlockDragonEgg((BlockDragonEgg)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 32: return render.renderBlockWall((BlockWall)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 12: return render.renderBlockLever(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 29: return render.renderBlockTripWireSource(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 30: return render.renderBlockTripWire(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 14: return render.renderBlockBed(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 15: return render.renderBlockRepeater((BlockRedstoneRepeater)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 36: return render.renderBlockRedstoneDiode((BlockRedstoneDiode)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 37: return render.renderBlockRedstoneComparator((BlockRedstoneComparator)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 16: return render.renderPistonBase(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_, false) ;
            case 17: return render.renderPistonExtension(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_, true) ;
            case 18: return render.renderBlockPane((BlockPane)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 41: return render.renderBlockStainedGlassPane(p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 21: return render.renderBlockFenceGate((BlockFenceGate)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 24: return render.renderBlockCauldron((BlockCauldron)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 33: return render.renderBlockFlowerpot((BlockFlowerPot)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 35: return render.renderBlockAnvil((BlockAnvil)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 25: return render.renderBlockBrewingStand((BlockBrewingStand)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 26: return render.renderBlockEndPortalFrame((BlockEndPortalFrame)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 28: return render.renderBlockCocoa((BlockCocoa)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 34: return render.renderBlockBeacon((BlockBeacon)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_) ;
            case 38: return render.renderBlockHopper((BlockHopper)p_147805_1_, p_147805_2_, p_147805_3_, p_147805_4_);
            default: return FMLRenderAccessLibrary.renderWorldBlock(render, render.blockAccess, p_147805_2_, p_147805_3_, p_147805_4_, p_147805_1_, l);
            }
        }*/
    }
	 public static IIcon renderFaceYNeg(RenderBlocks render, Block p_147768_1_, double p_147768_2_, double p_147768_4_, double p_147768_6_, IIcon p_147768_8_)
	    {
		 	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147768_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed0){
	        	boolean override=false;
	        	if(colorTable2[0]<colorTable[0]){
	        		colorTable[0]=colorTable2[0];
	        		override=true;
	        		render.overrideBlockTexture=p_147768_8_ = Blocks.grass.getBlockTextureFromSide(1);
        		}
	        	float r=WallPaint.dyesFloatVal[colorTable[0]][0]*0.5f;
	        	float g=WallPaint.dyesFloatVal[colorTable[0]][1]*0.5f;
	        	float b=WallPaint.dyesFloatVal[colorTable[0]][2]*0.5f;
	        	if(colorTable2[0]!=colorTable[0]){
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[0]][0]*0.5f;
		        	float g2=WallPaint.dyesFloatVal[colorTable2[0]][1]*0.5f;
		        	float b2=WallPaint.dyesFloatVal[colorTable2[0]][2]*0.5f;
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter0){
	        		if(override){
	        			int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f*0.5f;
		        		g=(float)((color>>8)&255)/255f*0.5f;
		        		b=(float)(color&255)/255f*0.5f;
	        		}
	        		else{
	        			render.overrideBlockTexture=p_147768_8_ = ClientProxy.wallicon;
	        		}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147768_8_);
	        		if(newIcon!=p_147768_8_){
	        			render.overrideBlockTexture=p_147768_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[0]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x , y- 1, z).getAmbientOcclusionLightValue();
	        		float f3 = (render.aoLightValueScratchXYZNNP + render.aoLightValueScratchXYNN + render.aoLightValueScratchYZNP + f7) / 4.0F;
	                float f6 = (render.aoLightValueScratchYZNP + f7 + render.aoLightValueScratchXYZPNP + render.aoLightValueScratchXYPN) / 4.0F;
	                float f5 = (f7 + render.aoLightValueScratchYZNN + render.aoLightValueScratchXYPN + render.aoLightValueScratchXYZPNN) / 4.0F;
	                float f4 = (render.aoLightValueScratchXYNN + render.aoLightValueScratchXYZNNN + f7 + render.aoLightValueScratchYZNN) / 4.0F;

	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }
	        return p_147768_8_;
	    }

	    /**
	     * Renders the given texture to the top face of the block. Args: block, x, y, z, texture
	     */
	    public static IIcon renderFaceYPos(RenderBlocks render, Block p_147806_1_, double p_147806_2_, double p_147806_4_, double p_147806_6_, IIcon p_147806_8_)
	    {
	    	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147806_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed1){
	        	boolean override=false;
	        	if(colorTable2[1]<colorTable[1]){
	        		colorTable[1]=colorTable2[1];
	        		override=true;
	        		render.overrideBlockTexture=p_147806_8_ = Blocks.grass.getBlockTextureFromSide(1);
        		}
	        	float r=WallPaint.dyesFloatVal[colorTable[1]][0];
	        	float g=WallPaint.dyesFloatVal[colorTable[1]][1];
	        	float b=WallPaint.dyesFloatVal[colorTable[1]][2];
	        	if(colorTable2[1]!=colorTable[1]){
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[1]][0];
		        	float g2=WallPaint.dyesFloatVal[colorTable2[1]][1];
		        	float b2=WallPaint.dyesFloatVal[colorTable2[1]][2];
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter1){
	        		if(override){
		        		int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f;
		        		g=(float)((color>>8)&255)/255f;
		        		b=(float)(color&255)/255f;
	        		}
	        		else{
	        			render.overrideBlockTexture=p_147806_8_ = ClientProxy.wallicon;
	        		}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147806_8_);
	        		if(newIcon!=p_147806_8_){
	        			render.overrideBlockTexture=p_147806_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[1]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x, y + 1, z).getAmbientOcclusionLightValue();
	        		float f6 = (render.aoLightValueScratchXYZNPP + render.aoLightValueScratchXYNP + render.aoLightValueScratchYZPP + f7) / 4.0F;
	        		float f3 = (render.aoLightValueScratchYZPP + f7 + render.aoLightValueScratchXYZPPP + render.aoLightValueScratchXYPP) / 4.0F;
	        		float f4 = (f7 + render.aoLightValueScratchYZPN + render.aoLightValueScratchXYPP + render.aoLightValueScratchXYZPPN) / 4.0F;
	        		float f5 = (render.aoLightValueScratchXYNP + render.aoLightValueScratchXYZNPN + f7 + render.aoLightValueScratchYZPN) / 4.0F;

	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }

	        return p_147806_8_;
	    }

	    /**
	     * Renders the given texture to the north (z-negative) face of the block.  Args: block, x, y, z, texture
	     */
	    public static IIcon renderFaceZNeg(RenderBlocks render, Block p_147761_1_, double p_147761_2_, double p_147761_4_, double p_147761_6_, IIcon p_147761_8_)
	    {
	    	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147761_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed2){
	        	boolean override=false;
		        if(colorTable2[2]<colorTable[2]){
		        	colorTable[2]=colorTable2[2];
		        	override=true;
		        	render.overrideBlockTexture=p_147761_8_ = Blocks.grass.getBlockTextureFromSide(1);
	        	}
	        	float r=WallPaint.dyesFloatVal[colorTable[2]][0]*0.8f;
	        	float g=WallPaint.dyesFloatVal[colorTable[2]][1]*0.8f;
	        	float b=WallPaint.dyesFloatVal[colorTable[2]][2]*0.8f;
	        	if(colorTable2[2]!=colorTable[2]){
	        		
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[2]][0]*0.8f;
		        	float g2=WallPaint.dyesFloatVal[colorTable2[2]][1]*0.8f;
		        	float b2=WallPaint.dyesFloatVal[colorTable2[2]][2]*0.8f;
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter2){
	        		if(override){
		        		int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f*0.8f;
		        		g=(float)((color>>8)&255)/255f*0.8f;
		        		b=(float)(color&255)/255f*0.8f;
	        		}
	        		else{
	        			render.overrideBlockTexture=p_147761_8_ = ClientProxy.wallicon;
	        		}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147761_8_);
	        		if(newIcon!=p_147761_8_){
	        			render.overrideBlockTexture=p_147761_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[2]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x, y, z - 1).getAmbientOcclusionLightValue();
	        		float f3 = (render.aoLightValueScratchXZNN + render.aoLightValueScratchXYZNPN + f7 + render.aoLightValueScratchYZPN) / 4.0F;
	        		float f4 = (f7 + render.aoLightValueScratchYZPN + render.aoLightValueScratchXZPN + render.aoLightValueScratchXYZPPN) / 4.0F;
	        		float f5 = (render.aoLightValueScratchYZNN + f7 + render.aoLightValueScratchXYZPNN + render.aoLightValueScratchXZPN) / 4.0F;
	        		float f6 = (render.aoLightValueScratchXYZNNN + render.aoLightValueScratchXZNN + render.aoLightValueScratchYZNN + f7) / 4.0F;

	        		if(render.partialRenderBounds){
	        			float f8 = (float)((double)f3 * render.renderMaxY * (1.0D - render.renderMinX) + (double)f4 * render.renderMaxY * render.renderMinX + (double)f5 * (1.0D - render.renderMaxY) * render.renderMinX + (double)f6 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinX));
	        			float f9 = (float)((double)f3 * render.renderMaxY * (1.0D - render.renderMaxX) + (double)f4 * render.renderMaxY * render.renderMaxX + (double)f5 * (1.0D - render.renderMaxY) * render.renderMaxX + (double)f6 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxX));
	        			float f10 = (float)((double)f3 * render.renderMinY * (1.0D - render.renderMaxX) + (double)f4 * render.renderMinY * render.renderMaxX + (double)f5 * (1.0D - render.renderMinY) * render.renderMaxX + (double)f6 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxX));
	        			float f11 = (float)((double)f3 * render.renderMinY * (1.0D - render.renderMinX) + (double)f4 * render.renderMinY * render.renderMinX + (double)f5 * (1.0D - render.renderMinY) * render.renderMinX + (double)f6 * (1.0D - render.renderMinY) * (1.0D - render.renderMinX));
	        			f3=f8;
	        			f4=f9;
	        			f5=f10;
	        			f6=f11;
	                }
	        		
	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }

	        return p_147761_8_;
	    }

	    /**
	     * Renders the given texture to the south (z-positive) face of the block.  Args: block, x, y, z, texture
	     */
	    public static IIcon renderFaceZPos(RenderBlocks render, Block p_147734_1_, double p_147734_2_, double p_147734_4_, double p_147734_6_, IIcon p_147734_8_)
	    {
	    	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147734_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed3){
	        	boolean override=false;
		        if(colorTable2[3]<colorTable[3]){
		        	colorTable[3]=colorTable2[3];
		        	override=true;
		        	render.overrideBlockTexture=p_147734_8_ = Blocks.grass.getBlockTextureFromSide(1);
	        	}
	        	float r=WallPaint.dyesFloatVal[colorTable[3]][0]*0.8f;
	        	float g=WallPaint.dyesFloatVal[colorTable[3]][1]*0.8f;
	        	float b=WallPaint.dyesFloatVal[colorTable[3]][2]*0.8f;
	        	if(colorTable2[3]!=colorTable[3]){
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[3]][0]*0.8f;
		        	float g2=WallPaint.dyesFloatVal[colorTable2[3]][1]*0.8f;
		        	float b2=WallPaint.dyesFloatVal[colorTable2[3]][2]*0.8f;
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter3){
	        		
	        		if(override){
		        		int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f*0.8f;
		        		g=(float)((color>>8)&255)/255f*0.8f;
		        		b=(float)(color&255)/255f*0.8f;
	        		}
	        		else{
	        			render.overrideBlockTexture=p_147734_8_ = ClientProxy.wallicon;
	        		}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147734_8_);
	        		if(newIcon!=p_147734_8_){
	        			render.overrideBlockTexture=p_147734_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[3]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x, y, z + 1).getAmbientOcclusionLightValue();
	        		float f3 = (render.aoLightValueScratchXZNP + render.aoLightValueScratchXYZNPP + f7 + render.aoLightValueScratchYZPP) / 4.0F;
	        		float f6 = (f7 + render.aoLightValueScratchYZPP + render.aoLightValueScratchXZPP + render.aoLightValueScratchXYZPPP) / 4.0F;
	        		float f5 = (render.aoLightValueScratchYZNP + f7 + render.aoLightValueScratchXYZPNP + render.aoLightValueScratchXZPP) / 4.0F;
	        		float f4 = (render.aoLightValueScratchXYZNNP + render.aoLightValueScratchXZNP + render.aoLightValueScratchYZNP + f7) / 4.0F;

	        		if(render.partialRenderBounds){
	        			/*float f8 = (float)((double)f3 * render.renderMaxY * (1.0D - render.renderMinX) + (double)f6 * render.renderMaxY * render.renderMinX + (double)f4 * (1.0D - render.renderMaxY) * render.renderMinX + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinX));
	                    float f9 = (float)((double)f3 * render.renderMinY * (1.0D - render.renderMinX) + (double)f6 * render.renderMinY * render.renderMinX + (double)f4 * (1.0D - render.renderMinY) * render.renderMinX + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMinX));
	                    float f10 = (float)((double)f3 * render.renderMinY * (1.0D - render.renderMaxX) + (double)f6 * render.renderMinY * render.renderMaxX + (double)f4 * (1.0D - render.renderMinY) * render.renderMaxX + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxX));
	                    float f11 = (float)((double)f3 * render.renderMaxY * (1.0D - render.renderMaxX) + (double)f6 * render.renderMaxY * render.renderMaxX + (double)f4 * (1.0D - render.renderMaxY) * render.renderMaxX + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxX));
	                    f3=f8;
	        			f4=f9;
	        			f5=f10;
	        			f6=f11;*/
	        			float f8 = (render.aoLightValueScratchXZNP + render.aoLightValueScratchXYZNPP + f7 + render.aoLightValueScratchYZPP) / 4.0F;
	                    float f9 = (f7 + render.aoLightValueScratchYZPP + render.aoLightValueScratchXZPP + render.aoLightValueScratchXYZPPP) / 4.0F;
	                    float f10 = (render.aoLightValueScratchYZNP + f7 + render.aoLightValueScratchXYZPNP + render.aoLightValueScratchXZPP) / 4.0F;
	                    float f11 = (render.aoLightValueScratchXYZNNP + render.aoLightValueScratchXZNP + render.aoLightValueScratchYZNP + f7) / 4.0F;
	                    f3 = (float)((double)f8 * render.renderMaxY * (1.0D - render.renderMinX) + (double)f9 * render.renderMaxY * render.renderMinX + (double)f10 * (1.0D - render.renderMaxY) * render.renderMinX + (double)f11 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinX));
	                    f4 = (float)((double)f8 * render.renderMinY * (1.0D - render.renderMinX) + (double)f9 * render.renderMinY * render.renderMinX + (double)f10 * (1.0D - render.renderMinY) * render.renderMinX + (double)f11 * (1.0D - render.renderMinY) * (1.0D - render.renderMinX));
	                    f5 = (float)((double)f8 * render.renderMinY * (1.0D - render.renderMaxX) + (double)f9 * render.renderMinY * render.renderMaxX + (double)f10 * (1.0D - render.renderMinY) * render.renderMaxX + (double)f11 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxX));
	                    f6 = (float)((double)f8 * render.renderMaxY * (1.0D - render.renderMaxX) + (double)f9 * render.renderMaxY * render.renderMaxX + (double)f10 * (1.0D - render.renderMaxY) * render.renderMaxX + (double)f11 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxX));
	        		}
	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }

	        return p_147734_8_;
	    }

	    /**
	     * Renders the given texture to the west (x-negative) face of the block.  Args: block, x, y, z, texture
	     */
	    public static IIcon renderFaceXNeg(RenderBlocks render, Block p_147798_1_, double p_147798_2_, double p_147798_4_, double p_147798_6_, IIcon p_147798_8_)
	    {
	    	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147798_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed4){
	        	boolean override=false;
		        if(colorTable2[4]<colorTable[4]){
		        	colorTable[4]=colorTable2[4];
		        	override=true;
		        	render.overrideBlockTexture=p_147798_8_ = Blocks.grass.getBlockTextureFromSide(1);
	        	}
	        	float r=WallPaint.dyesFloatVal[colorTable[4]][0]*0.6f;
	        	float g=WallPaint.dyesFloatVal[colorTable[4]][1]*0.6f;
	        	float b=WallPaint.dyesFloatVal[colorTable[4]][2]*0.6f;
	        	if(colorTable2[4]!=colorTable[4]){
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[4]][0]*0.6f;
		        	float g2=WallPaint.dyesFloatVal[colorTable2[4]][1]*0.6f;
		        	float b2=WallPaint.dyesFloatVal[colorTable2[4]][2]*0.6f;
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter4){
	        	
	        		if(override){
		        		int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f*0.6f;
		        		g=(float)((color>>8)&255)/255f*0.6f;
		        		b=(float)(color&255)/255f*0.6f;
	        		}
	        		else{
	        			render.overrideBlockTexture=p_147798_8_ = ClientProxy.wallicon;
	        		}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147798_8_);
	        		if(newIcon!=p_147798_8_){
	        			render.overrideBlockTexture=p_147798_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[4]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x - 1, y, z).getAmbientOcclusionLightValue();
	                float f6 = (render.aoLightValueScratchXYNN + render.aoLightValueScratchXYZNNP + f7 + render.aoLightValueScratchXZNP) / 4.0F;
	                float f3 = (f7 + render.aoLightValueScratchXZNP + render.aoLightValueScratchXYNP + render.aoLightValueScratchXYZNPP) / 4.0F;
	                float f4 = (render.aoLightValueScratchXZNN + f7 + render.aoLightValueScratchXYZNPN + render.aoLightValueScratchXYNP) / 4.0F;
	                float f5 = (render.aoLightValueScratchXYZNNN + render.aoLightValueScratchXYNN + render.aoLightValueScratchXZNN + f7) / 4.0F;
	                
	                if(render.partialRenderBounds){
	                	float f8 = (float)((double)f3 * render.renderMaxY * render.renderMaxZ + (double)f4 * render.renderMaxY * (1.0D - render.renderMaxZ) + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxZ) + (double)f6 * (1.0D - render.renderMaxY) * render.renderMaxZ);
	                	float f9 = (float)((double)f3 * render.renderMaxY * render.renderMinZ + (double)f4 * render.renderMaxY * (1.0D - render.renderMinZ) + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinZ) + (double)f6 * (1.0D - render.renderMaxY) * render.renderMinZ);
	                	float f10 = (float)((double)f3 * render.renderMinY * render.renderMinZ + (double)f4 * render.renderMinY * (1.0D - render.renderMinZ) + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMinZ) + (double)f6 * (1.0D - render.renderMinY) * render.renderMinZ);
	                	float f11 = (float)((double)f3 * render.renderMinY * render.renderMaxZ + (double)f4 * render.renderMinY * (1.0D - render.renderMaxZ) + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxZ) + (double)f6 * (1.0D - render.renderMinY) * render.renderMaxZ);
	                	f3=f8;
	        			f4=f9;
	        			f5=f10;
	        			f6=f11;
	                }
	                	
	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }

	        return p_147798_8_;
	    }

	    /**
	     * Renders the given texture to the east (x-positive) face of the block.  Args: block, x, y, z, texture
	     */
	    public static IIcon renderFaceXPos(RenderBlocks render, Block p_147764_1_, double p_147764_2_, double p_147764_4_, double p_147764_6_, IIcon p_147764_8_)
	    {
	    	overrided=render.overrideBlockTexture;
	        Tessellator tessellator = Tessellator.instance;
	        if (render.hasOverrideBlockTexture())
	        {
	            p_147764_8_ = render.overrideBlockTexture;
	        } else if(enabledForBlock&&changed5){
	        	boolean override=false;
		        if(colorTable2[5]<colorTable[5]){
		        	colorTable[5]=colorTable2[5];
		        	override=true;
		        	render.overrideBlockTexture=p_147764_8_ = Blocks.grass.getBlockTextureFromSide(1);
	        	}
	        	float r=WallPaint.dyesFloatVal[colorTable[5]][0]*0.6f;
	        	float g=WallPaint.dyesFloatVal[colorTable[5]][1]*0.6f;
	        	float b=WallPaint.dyesFloatVal[colorTable[5]][2]*0.6f;
	        	if(colorTable2[5]!=colorTable[5]){
		        	float max1=Math.max(r, Math.max(g, b));
		        	float r2=WallPaint.dyesFloatVal[colorTable2[5]][0]*0.6f;
		        	float g2=WallPaint.dyesFloatVal[colorTable2[5]][1]*0.6f;
		        	float b2=WallPaint.dyesFloatVal[colorTable2[5]][2]*0.6f;
		        	float max2=Math.max(r2, Math.max(g2, b2));
		        	r=(r+r2)/2f;
		        	g=(g+g2)/2f;
		        	b=(b+b2)/2f;
		        	float maxsr=(max1+max2)/2f;
					max1=Math.max(r, Math.max(g, b));
					r=r*maxsr/max1;
					g=g*maxsr/max1;
					b=b*maxsr/max1;
	        	}
	        	if(!filter5){
		        	
		        	if(override){
		        		int color=render.blockAccess.getBiomeGenForCoords(x, z).getBiomeGrassColor(x, y, z);
		        		r=(float)((color>>16)&255)/255f*0.6f;
		        		g=(float)((color>>8)&255)/255f*0.6f;
		        		b=(float)(color&255)/255f*0.6f;
	        		}
		        	else{
		        		render.overrideBlockTexture=p_147764_8_ = ClientProxy.wallicon;
		        	}
	        	}
	        	else if(!override){
	        		IIcon newIcon= findTextureReplacement(p_147764_8_);
	        		if(newIcon!=p_147764_8_){
	        			render.overrideBlockTexture=p_147764_8_ =newIcon;
	        		}
	        	}
	        	if(override){
	        		colorTable[5]=15;
        		}
	        	if(!render.enableAO){
	        		tessellator.setColorOpaque_F(r,g,b);
	        	}
	        	else{
	        		float f7 = render.blockAccess.getBlock(x + 1, y, z).getAmbientOcclusionLightValue();
	                float f3 = (render.aoLightValueScratchXYPN + render.aoLightValueScratchXYZPNP + f7 + render.aoLightValueScratchXZPP) / 4.0F;
	                float f4 = (render.aoLightValueScratchXYZPNN + render.aoLightValueScratchXYPN + render.aoLightValueScratchXZPN + f7) / 4.0F;
	                float f5 = (render.aoLightValueScratchXZPN + f7 + render.aoLightValueScratchXYZPPN + render.aoLightValueScratchXYPP) / 4.0F;
	                float f6 = (f7 + render.aoLightValueScratchXZPP + render.aoLightValueScratchXYPP + render.aoLightValueScratchXYZPPP) / 4.0F;

	                if(render.partialRenderBounds){
	                	/*float f8 = (float)((double)f3 * render.renderMinY * render.renderMaxZ + (double)f4 * render.renderMinY * (1.0D - render.renderMaxZ) + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxZ) + (double)f6 * (1.0D - render.renderMinY) * render.renderMaxZ);
	                    float f9 = (float)((double)f3 * render.renderMinY * render.renderMinZ + (double)f4 * render.renderMinY * (1.0D - render.renderMinZ) + (double)f5 * (1.0D - render.renderMinY) * (1.0D - render.renderMinZ) + (double)f6 * (1.0D - render.renderMinY) * render.renderMinZ);
	                    float f10 = (float)((double)f3 * render.renderMaxY * render.renderMinZ + (double)f4 * render.renderMaxY * (1.0D - render.renderMinZ) + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinZ) + (double)f6 * (1.0D - render.renderMaxY) * render.renderMinZ);
	                    float f11 = (float)((double)f3 * render.renderMaxY * render.renderMaxZ + (double)f4 * render.renderMaxY * (1.0D - render.renderMaxZ) + (double)f5 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxZ) + (double)f6 * (1.0D - render.renderMaxY) * render.renderMaxZ);
	                    f3=f10;
	        			f4=f9;
	        			f5=f8;
	        			f6=f11;*/
	                	float f8 = (render.aoLightValueScratchXYPN + render.aoLightValueScratchXYZPNP + f7 + render.aoLightValueScratchXZPP) / 4.0F;
	                	float f9 = (render.aoLightValueScratchXYZPNN + render.aoLightValueScratchXYPN + render.aoLightValueScratchXZPN + f7) / 4.0F;
	                	float f10 = (render.aoLightValueScratchXZPN + f7 + render.aoLightValueScratchXYZPPN + render.aoLightValueScratchXYPP) / 4.0F;
	                	float f11 = (f7 + render.aoLightValueScratchXZPP + render.aoLightValueScratchXYPP + render.aoLightValueScratchXYZPPP) / 4.0F;
	                    f3 = (float)((double)f8 * (1.0D - render.renderMinY) * render.renderMaxZ + (double)f9 * (1.0D - render.renderMinY) * (1.0D - render.renderMaxZ) + (double)f10 * render.renderMinY * (1.0D - render.renderMaxZ) + (double)f11 * render.renderMinY * render.renderMaxZ);
	                    f4 = (float)((double)f8 * (1.0D - render.renderMinY) * render.renderMinZ + (double)f9 * (1.0D - render.renderMinY) * (1.0D - render.renderMinZ) + (double)f10 * render.renderMinY * (1.0D - render.renderMinZ) + (double)f11 * render.renderMinY * render.renderMinZ);
	                    f5 = (float)((double)f8 * (1.0D - render.renderMaxY) * render.renderMinZ + (double)f9 * (1.0D - render.renderMaxY) * (1.0D - render.renderMinZ) + (double)f10 * render.renderMaxY * (1.0D - render.renderMinZ) + (double)f11 * render.renderMaxY * render.renderMinZ);
	                    f6 = (float)((double)f8 * (1.0D - render.renderMaxY) * render.renderMaxZ + (double)f9 * (1.0D - render.renderMaxY) * (1.0D - render.renderMaxZ) + (double)f10 * render.renderMaxY * (1.0D - render.renderMaxZ) + (double)f11 * render.renderMaxY * render.renderMaxZ);
	                }
	                
	                render.colorRedTopLeft = render.colorRedBottomLeft = render.colorRedBottomRight = render.colorRedTopRight = r;
	                render.colorGreenTopLeft = render.colorGreenBottomLeft = render.colorGreenBottomRight = render.colorGreenTopRight = g;
	                render.colorBlueTopLeft = render.colorBlueBottomLeft = render.colorBlueBottomRight = render.colorBlueTopRight = b;

	                render.colorRedTopLeft *= f3;
	                render.colorGreenTopLeft *= f3;
	                render.colorBlueTopLeft *= f3;
	                render.colorRedBottomLeft *= f4;
	                render.colorGreenBottomLeft *= f4;
	                render.colorBlueBottomLeft *= f4;
	                render.colorRedBottomRight *= f5;
	                render.colorGreenBottomRight *= f5;
	                render.colorBlueBottomRight *= f5;
	                render.colorRedTopRight *= f6;
	                render.colorGreenTopRight *= f6;
	                render.colorBlueTopRight *= f6;
	        	}
	        }
	        
	        return p_147764_8_;
	    }
	public static void renderBlockAsItem(Block p_147800_1_, int p_147800_2_, float p_147800_3_)
	{
		enabledForBlock=false;
	}
	public static IIcon findTextureReplacement(IIcon icon){
		if(icon.getIconName().contains("planks")){
			return ClientProxy.replaced.get("planks");
		} 
		if(icon.getIconName().contains("log")&&!icon.getIconName().contains("top")&&(icon.getIconName().contains("oak")||icon.getIconName().contains("spruce")||icon.getIconName().contains("acacia"))){
			return ClientProxy.replaced.get("log");
		} 
		if(icon.getIconName().contains("log")&&icon.getIconName().contains("top")){
			return ClientProxy.replaced.get("log_top");
		} 
		if(ClientProxy.replaced.containsKey(icon.getIconName())){
			icon=ClientProxy.replaced.get(icon.getIconName());
		}
		return icon;
	}
    public static int getColorFromItemStack(ItemStack p_82790_1_, int p_82790_2_)
    {
    	if(p_82790_1_.hasTagCompound()&&p_82790_1_.stackTagCompound.hasKey("RColorFilter")){
    		byte color=p_82790_1_.getTagCompound().getByte("RColorFilter");
			int color1=ItemDye.field_150922_c[color&15];
			int color2=ItemDye.field_150922_c[WallPaint.unSign(color)>>4];
			int r1=color1>>16&255;
			int g1=color1>>8&255;
			int b1=color1&255;
			int max1=Math.max(r1, Math.max(g1, b1));
			int r2=color2>>16&255;
			int g2=color2>>8&255;
			int b2=color2&255;
			int max2=Math.max(r2, Math.max(g2, b2));
			r1=(r1+r2)/2;
			g1=(g1+g2)/2;
			b1=(b1+b2)/2;
			int maxsr=(max1+max2)/2;
			max1=Math.max(r1, Math.max(g1, b1));
			r1=r1*maxsr/max1;
			g1=g1*maxsr/max1;
			b1=b1*maxsr/max1;
			int endcolor=(r1<<16)+(g1<<8)+b1;
			//System.out.println("1: "+r1+" "+g1+" "+b1+" "+"2: "+r2+" "+g2+" "+b2+" en "+endcolor);
			return endcolor;
    	}
        return 16777215;
    }
}
