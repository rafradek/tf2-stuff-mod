package rafradek.wallpaint.core;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class RenderBlockTransAdapter extends ClassVisitor implements Opcodes{
	public String owner;
	public boolean obf;
	public RenderBlockTransAdapter(ClassVisitor cv,boolean obf) {
		super(ASM4, cv);
		this.obf=obf;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void visit(int version, int access, String name,
	String signature, String superName, String[] interfaces) {
		cv.visit(version, access, name, signature, superName, interfaces);
		owner = name;
	}
	
	@Override 
	public MethodVisitor visitMethod(int access, String name,
	String desc, String signature, String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
		exceptions);
		//System.out.println(name);
		//&&desc.equals("(Lnet/minecraft/block/Block;IIIFFF)Z")
		if(!obf){
			if (name.equals("renderBlockByRenderType")) {
				mv = new ReplaceMethodVisitorBlockType(mv,"renderBlockByRenderType",false);
			} else if (name.equals("renderFaceYNeg")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceYNeg",false,0);
			} else if (name.equals("renderFaceYPos")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceYPos",false,1);
			} else if (name.equals("renderFaceZNeg")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceZNeg",false,2);
			} else if (name.equals("renderFaceZPos")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceZPos",false,3);
			} else if (name.equals("renderFaceXNeg")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceXNeg",false,4);
			} else if (name.equals("renderFaceXPos")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceXPos",false,5);
			} else if (name.equals("renderBlockAsItem")) {
				mv = new ReplaceMethodVisitorItemRender(mv);
			}
			/*if (name.equals("renderStandardBlockWithAmbientOcclusion")) {
				mv = new ReplaceMethodVisitor(mv,"renderStandardBlockWithAmbientOcclusion",false);
			}
			else if (name.equals("renderStandardBlockWithAmbientOcclusionPartial")) {
				mv = new ReplaceMethodVisitor(mv,"renderStandardBlockWithAmbientOcclusionPartial",false);
			}
			else if (name.equals("renderStandardBlockWithColorMultiplier")) {
				mv = new ReplaceMethodVisitor(mv,"renderStandardBlockWithColorMultiplier",false);
			}*/
		}
		else if(desc.equals("(Laji;DDDLrf;)V")){//(Lahu;DDDLps;)V
			//System.out.println("znalazl face "+ name);
			if (name.equals("a")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceYNeg",true,0);
			} 
			else if (name.equals("b")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceYPos",true,1);
			}
			else if (name.equals("c")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceZNeg",true,2);
			}
			else if (name.equals("d")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceZPos",true,3);
			}
			else if (name.equals("e")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceXNeg",true,4);
			}
			else if (name.equals("f")) {
				mv = new ReplaceMethodVisitorFace(mv,"renderFaceXPos",true,5);
			}
		}
		else if(desc.equals("(Laji;III)Z")&&name.equals("b")){//desc.equals("(Lahu;III)Z")
			//System.out.println("znalazlmetode byrendertype");
			mv = new ReplaceMethodVisitorBlockType(mv,"renderBlockByRenderType",true);
		}
		else if(desc.equals("(Laji;IF)V")&&name.equals("a")){//desc.equals("(Lahu;III)Z")
			//System.out.println("znalazlmetode byrendertype");
			mv = new ReplaceMethodVisitorItemRender(mv);
		}
		return mv;
	}
	
	class ReplaceMethodVisitor extends MethodVisitor{
		String linkToMethod="renderStandardBlockWithAmbientOcclusion";
		String blockPath;
		String renderBlockPath;
		public boolean obf;
		public ReplaceMethodVisitor( MethodVisitor mv, String link,boolean obf) {
			super(ASM4, mv);
			this.linkToMethod=link;
			this.obf=obf;
			blockPath=obf?/*ahu*/"aji":"net/minecraft/block/Block";
			renderBlockPath=obf?/*ble*/"blm":"net/minecraft/client/renderer/RenderBlocks";
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void visitCode(){
			mv.visitCode();
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitLineNumber(4540, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitVarInsn(ILOAD, 2);
			mv.visitVarInsn(ILOAD, 3);
			mv.visitVarInsn(ILOAD, 4);
			mv.visitVarInsn(FLOAD, 5);
			mv.visitVarInsn(FLOAD, 6);
			mv.visitVarInsn(FLOAD, 7);
			mv.visitMethodInsn(INVOKESTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods",this.linkToMethod, "(L"+renderBlockPath+";L"+blockPath+";IIIFFF)Z");
			mv.visitInsn(IRETURN);
			Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitLocalVariable("this", "L"+blockPath+";", null, l0, l1, 0);
			mv.visitLocalVariable("p_147751_1_", "L+blockPath+;", null, l0, l1, 1);
			mv.visitLocalVariable("p_147751_2_", "I", null, l0, l1, 2);
			mv.visitLocalVariable("p_147751_3_", "I", null, l0, l1, 3);
			mv.visitLocalVariable("p_147751_4_", "I", null, l0, l1, 4);
			mv.visitLocalVariable("p_147751_5_", "F", null, l0, l1, 5);
			mv.visitLocalVariable("p_147751_6_", "F", null, l0, l1, 6);
			mv.visitLocalVariable("p_147751_7_", "F", null, l0, l1, 7);
			mv.visitMaxs(8, 8);
			mv.visitEnd();
		}
	}
	class ReplaceMethodVisitorBlockType extends MethodVisitor{
		String linkToMethod="renderStandardBlockWithAmbientOcclusion";
		String blockPath;
		String renderBlockPath;
		public boolean obf;
		public ReplaceMethodVisitorBlockType( MethodVisitor mv, String link,boolean obf) {
			super(ASM4, mv);
			this.linkToMethod=link;
			this.obf=obf;
			blockPath=obf?/*ahu*/"aji":"net/minecraft/block/Block";
			renderBlockPath=obf?/*ble*/"blm":"net/minecraft/client/renderer/RenderBlocks";
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void visitCode(){
			mv.visitCode();
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitLineNumber(382, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitVarInsn(ILOAD, 2);
			mv.visitVarInsn(ILOAD, 3);
			mv.visitVarInsn(ILOAD, 4);
			mv.visitMethodInsn(INVOKESTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods", this.linkToMethod, "(L"+renderBlockPath+";L"+blockPath+";III)V");
			/*mv.visitInsn(IRETURN);
			Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitLocalVariable("this", "L"+renderBlockPath+";", null, l0, l1, 0);
			mv.visitLocalVariable("p_147805_1_", "L"+blockPath+";", null, l0, l1, 1);
			mv.visitLocalVariable("p_147805_2_", "I", null, l0, l1, 2);
			mv.visitLocalVariable("p_147805_3_", "I", null, l0, l1, 3);
			mv.visitLocalVariable("p_147805_4_", "I", null, l0, l1, 4);
			mv.visitMaxs(5, 5);
			mv.visitEnd();*/
		}
	}
	class ReplaceMethodVisitorFace extends MethodVisitor{
		String linkToMethod="renderStandardBlockWithAmbientOcclusion";
		String blockPath;
		String iconPath;
		String renderBlockPath;
		public int face;
		public boolean firstMethod;
		public boolean MethodRemoved;
		public boolean firstAload;
		public boolean obf;
		public String overridePath;
		public ReplaceMethodVisitorFace( MethodVisitor mv, String link,boolean obf,int face) {
			super(ASM4, mv);
			this.linkToMethod=link;
			this.obf=obf;
			this.face=face;
			blockPath=obf?/*ahu*/"aji":"net/minecraft/block/Block";
			renderBlockPath=obf?/*ble*/"blm":"net/minecraft/client/renderer/RenderBlocks";
			iconPath=obf?/*"ps"*/"rf":"net/minecraft/util/IIcon";
			overridePath=obf?"d":"overrideBlockTexture";
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void visitCode(){
			mv.visitCode();
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitLineNumber(7720, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitVarInsn(DLOAD, 2);
			mv.visitVarInsn(DLOAD, 4);
			mv.visitVarInsn(DLOAD, 6);
			mv.visitVarInsn(ALOAD, 8);
			mv.visitMethodInsn(INVOKESTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods", this.linkToMethod, "(L"+renderBlockPath+";L"+blockPath+";DDDL"+iconPath+";)L"+iconPath+";");
			mv.visitVarInsn(ASTORE, 8);
			/*Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitLineNumber(7830, l1);
			mv.visitInsn(RETURN);
			Label l2 = new Label();
			mv.visitLabel(l2);
			mv.visitLocalVariable("this", "L"+renderBlockPath+";", null, l0, l2, 0);
			mv.visitLocalVariable("p_147798_1_", "L"+blockPath+";", null, l0, l2, 1);
			mv.visitLocalVariable("p_147798_2_", "D", null, l0, l2, 2);
			mv.visitLocalVariable("p_147798_4_", "D", null, l0, l2, 4);
			mv.visitLocalVariable("p_147798_6_", "D", null, l0, l2, 6);
			mv.visitLocalVariable("p_147798_8_", "L"+iconPath+";", null, l0, l2, 8);
			mv.visitMaxs(9, 9);
			mv.visitEnd();*/
		}
		/*@Override
		public void visitVarInsn(int opcode, int var){
			if(opcode==ALOAD&&var==0&&!this.MethodRemoved){
				this.firstAload=true;
			}
			else{
				mv.visitVarInsn(opcode, var);
			}
		}*/
		@Override
		public void visitInsn(int opcode){
			if(opcode==RETURN){
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETSTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods", "overrided", "L"+iconPath+";");
				mv.visitFieldInsn(PUTFIELD, renderBlockPath, overridePath, "L"+iconPath+";");
			}
			mv.visitInsn(opcode);
		}
		/*@Override
		public void visitMethodInsn(int opcode, String owner, String name,String desc){
			
			if(!this.MethodRemoved&&opcode==INVOKEVIRTUAL&&owner.equals(renderBlockPath)&&name.equals(overridePath)&&desc.equals("()Z")){
				mv.visitInsn(ICONST_0);
				//System.out.println("znalazl i usunol");
				this.MethodRemoved=true;
				
			}else{
				mv.visitMethodInsn(opcode, owner, name, desc);
			}
		}*/
	}
	class ReplaceMethodVisitorItemRender extends MethodVisitor{
		public ReplaceMethodVisitorItemRender( MethodVisitor mv) {
			super(ASM4, mv);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void visitCode(){
			mv.visitCode();
			mv.visitInsn(ICONST_0);
			mv.visitFieldInsn(PUTSTATIC, "rafradek/wallpaint/core/WallPaintOverridedMethods", "enabledForBlock", "Z");
		}
	}
}
