package rafradek.wallpaint.core;

import java.util.Iterator;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;

import net.minecraft.launchwrapper.IClassTransformer;

public class WallPaintClassTransformer implements IClassTransformer {

	@Override
	public byte[] transform(String name, String transformedName,
			byte[] basicClass) {
		boolean obf=false;
		if(name.equals("blm")){
			obf=true;
		}
		boolean obf2=false;
		if(name.equals("adb")){
			obf2=true;
			//System.out.println("znalazl klase");
		}
		/*if(name.equals("ble")){
			obf=true;
			//System.out.println("znalazlklase");
		}*/
		if(obf||name.equals("net.minecraft.client.renderer.RenderBlocks")){
			ClassReader cr = new ClassReader(basicClass);
			ClassWriter cw = new ClassWriter(cr,0);
			RenderBlockTransAdapter ca = new RenderBlockTransAdapter(cw,obf);
			cr.accept(ca, 0);
			return cw.toByteArray();
		}
		else if(obf2||name.equals("net.minecraft.item.Item")){
			ClassReader cr = new ClassReader(basicClass);
			ClassWriter cw = new ClassWriter(cr,0);
			ItemTransAdapter ca = new ItemTransAdapter(cw,obf2);
			cr.accept(ca, 0);
			return cw.toByteArray();
		}
		return basicClass;
	}

}
