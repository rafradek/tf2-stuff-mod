package rafradek.wallpaint;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecipeMixPaint implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting p_77569_1_, World p_77569_2_) {
		int previousBucketColor=-9999;
		int bucketFound=0;
		int color;
		for(int i=0;i<p_77569_1_.getSizeInventory();i++){
			if(p_77569_1_.getStackInSlot(i) != null){
				if(p_77569_1_.getStackInSlot(i).getItem() instanceof ItemPaintBucket &&p_77569_1_.getStackInSlot(i).hasTagCompound()){
					color=p_77569_1_.getStackInSlot(i).stackTagCompound.getByte("color");
					if((WallPaint.unSign(color)>>4) == (color&15) && previousBucketColor!=color){
						previousBucketColor=color;
						bucketFound++;
					}
					else{
						return false;
					}
				}
				else{
					return false;
				}
			}
		}
		return bucketFound==2;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting p_77572_1_) {
		int bucketColor1=-1;
		int bucketColor2=-1;
		for(int i=0;i<p_77572_1_.getSizeInventory();i++){
			if(p_77572_1_.getStackInSlot(i) != null){
				if(p_77572_1_.getStackInSlot(i).getItem() instanceof ItemPaintBucket){
					if(bucketColor1==-1)
						bucketColor1=p_77572_1_.getStackInSlot(i).stackTagCompound.getByte("color")&15;
					else
						bucketColor2=p_77572_1_.getStackInSlot(i).stackTagCompound.getByte("color")&15;
				}
			}
		}
		if(bucketColor1>-1&&bucketColor2>-1){
			ItemStack stack=new ItemStack(WallPaint.paintBucket,2);
			stack.stackTagCompound=new NBTTagCompound();
			int bucketColorMax=Math.max(bucketColor1, bucketColor2);
			int bucketColorMin=Math.min(bucketColor1, bucketColor2);
			stack.stackTagCompound.setByte("color",(byte) (bucketColorMax+(bucketColorMin<<4)));
			return stack;
		}
		return null;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public ItemStack getRecipeOutput() {
		// TODO Auto-generated method stub
		return null;
	}

}
