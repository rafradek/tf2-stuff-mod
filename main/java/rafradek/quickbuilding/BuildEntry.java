package rafradek.quickbuilding;

import net.minecraft.item.Item;

public class BuildEntry {

	public int x;
	public int y;
	public int z;
	public int face;
	public int tickTime;
	public Item item;
	public int metadata;

	public BuildEntry(int x, int y, int z, int face,Item item, int metadata,int tickTime){
		this.x=x;
		this.y=y;
		this.z=z;
		this.face=face;
		this.tickTime=tickTime;
		this.metadata=metadata;
		this.item=item;
	}
}
