package rafradek.quickbuilding;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.tileentity.TileEntityCommandBlock;

public class QBGui extends GuiScreen {

    private GuiButton doneBtn;
    private GuiButton cancelBtn;

    public QBGui()
    {

    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    @SuppressWarnings("unchecked")
	public void initGui()
    {
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(this.doneBtn = new GuiButton(0, this.width / 2 - 100, this.height / 4 + 125 + 12, I18n.format("gui.done")));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 2,20,20, I18n.format("+")));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 100, this.height / 4 + 10 + 12,20,20, I18n.format("-")));
        this.buttonList.add(new GuiButton(3, this.width / 2 - 65, this.height / 4 + 2,20,20, I18n.format("+")));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 65, this.height / 4 + 10 + 12,20,20, I18n.format("-")));
        this.buttonList.add(new GuiButton(5, this.width / 2 - 30, this.height / 4 + 2,20,20, I18n.format("+")));
        this.buttonList.add(new GuiButton(6, this.width / 2 - 30, this.height / 4 + 10 + 12,20,20, I18n.format("-")));
        this.buttonList.add(new GuiButton(7, this.width / 2 - 100, this.height / 4 + 50 + 12,100,20, I18n.format("Lava remover: off")));
        this.buttonList.add(new GuiButton(8, this.width / 2 , this.height / 4 + 50 + 12,100,20, I18n.format("Water remover: off")));
        this.buttonList.add(new GuiButton(9, this.width / 2 - 100, this.height / 4 + 75 + 12,200,20, I18n.format("Quick build mode: player")));
        this.buttonList.add(new GuiButton(10, this.width / 2 - 100, this.height / 4 + 100 + 12,200,20, I18n.format("Use only blocks")));
        this.buttonList.add(new GuiButton(11, this.width / 2 + 5, this.height / 4 + 2,20,20, I18n.format("+")));
        this.buttonList.add(new GuiButton(12, this.width / 2 + 5, this.height / 4 + 10 + 12,20,20, I18n.format("-")));
        this.buttonList.add(new GuiButton(13, this.width / 2 + 40, this.height / 4 + 2,20,20, I18n.format("+")));
        this.buttonList.add(new GuiButton(14, this.width / 2 + 40, this.height / 4 + 10 + 12,20,20, I18n.format("-")));
        this.setButtonNames();
    }

    /**
     * Called when the screen is unloaded. Used to disable keyboard repeat events
     */
    public void onGuiClosed()
    {
        Keyboard.enableRepeatEvents(false);
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton par1GuiButton)
    {
        if (par1GuiButton.enabled)
        {
            if (par1GuiButton.id == 0)
            {
                this.mc.displayGuiScreen((GuiScreen)null);
            }
            else if(par1GuiButton.id == 1) {
            	if(QuickBuilding.buildDepth<3){
            	QuickBuilding.buildDepth++;
            	}
            	if(QuickBuilding.buildDepth==3){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(2)).enabled=true;
            }
            else if(par1GuiButton.id == 2) {
            	if(QuickBuilding.buildDepth>1){
            	QuickBuilding.buildDepth--;
            	}
            	if(QuickBuilding.buildDepth==1){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(1)).enabled=true;
            }
            else if(par1GuiButton.id == 3) {
            	if(QuickBuilding.buildWidth<10){
            	QuickBuilding.buildWidth+=1;
            	}
            	if(QuickBuilding.buildWidth==10){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(4)).enabled=true;
            }
            else if(par1GuiButton.id == 4) {
            	if(QuickBuilding.buildWidth>2){
                	QuickBuilding.buildWidth-=1;
                }
            	if(QuickBuilding.buildWidth==2){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(3)).enabled=true;
            }
            else if(par1GuiButton.id == 5) {
            	if(QuickBuilding.buildType<3){
            	QuickBuilding.buildType++;
            	}
            	if(QuickBuilding.buildType==3){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(6)).enabled=true;
            }
            else if(par1GuiButton.id == 6) {
            	if(QuickBuilding.buildType>0){
                	QuickBuilding.buildType--;
                }
            	if(QuickBuilding.buildType==0){
            		par1GuiButton.enabled=false;
                }
            	((GuiButton)this.buttonList.get(5)).enabled=true;
            }
            else if(par1GuiButton.id == 7) {
            	QuickBuilding.lavaRemover=!QuickBuilding.lavaRemover;
            	Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
            }
            else if(par1GuiButton.id == 8) {
            	QuickBuilding.waterRemover=!QuickBuilding.waterRemover;
            	Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
            }
            else if(par1GuiButton.id == 9) {
            	QuickBuilding.dangerBuildMode=!QuickBuilding.dangerBuildMode;
            }
            else if(par1GuiButton.id == 10) {
            	QuickBuilding.allowAny=!QuickBuilding.allowAny;
            }
            else if(par1GuiButton.id == 12) {
            	if(QuickBuilding.maxDistance>4){
                	QuickBuilding.maxDistance--;
                }
            	((GuiButton)this.buttonList.get(11)).enabled=true;
            }
            else if(par1GuiButton.id == 11) {
            	if(QuickBuilding.maxDistance<10){
                	QuickBuilding.maxDistance++;
                }
            	((GuiButton)this.buttonList.get(12)).enabled=true;
            }
            else if(par1GuiButton.id == 14) {
            	if(QuickBuilding.instaBuild>0){
                	QuickBuilding.instaBuild--;
                }
            	((GuiButton)this.buttonList.get(13)).enabled=true;
            }
            else if(par1GuiButton.id == 13) {
            	if(QuickBuilding.instaBuild<10){
                	QuickBuilding.instaBuild++;
                }
            	((GuiButton)this.buttonList.get(14)).enabled=true;
            }
            QuickBuilding.ignoreMaxDistance=QuickBuilding.instaBuild>0 || QuickBuilding.maxDistance==10;
            this.setButtonNames();
            QuickBuilding.saveOptions();
        }
    }
    
    public void setButtonNames(){
    	for(int i=0; i<this.buttonList.size() ; i++){
    		if(i == 7){
    			if(QuickBuilding.lavaRemover){
            		((GuiButton)this.buttonList.get(i)).displayString="Lava remover: on";
            	}
            	else {
            		((GuiButton)this.buttonList.get(i)).displayString="Lava remover: off";
            	}
    		}
    		else if (i==8){
    			if(QuickBuilding.waterRemover){
            		((GuiButton)this.buttonList.get(i)).displayString="Water remover: on";
            	}
            	else {
            		((GuiButton)this.buttonList.get(i)).displayString="Water remover: off";
            	}
    		}
    		else if (i==9){
    			if(QuickBuilding.dangerBuildMode){
            		((GuiButton)this.buttonList.get(i)).displayString="Shealth mode: other players";
            	}
            	else { 
            		((GuiButton)this.buttonList.get(i)).displayString="Shealth mode: player";
            	}
    		}
    		else if (i==10){
    			if(QuickBuilding.allowAny){
            		((GuiButton)this.buttonList.get(i)).displayString="Use also items";
            	}
            	else {
            		((GuiButton)this.buttonList.get(i)).displayString="Use only blocks";
            	}
    		}
    		else if(i==11){
    			if(QuickBuilding.instaBuild==0){
            		((GuiButton)this.buttonList.get(i)).enabled=true;
            	}
            	else {
            		((GuiButton)this.buttonList.get(i)).enabled=false;
            	}
    		}
    		else if(i==12){
    			if(QuickBuilding.instaBuild==0){
            		((GuiButton)this.buttonList.get(i)).enabled=true;
            	}
            	else {
            		((GuiButton)this.buttonList.get(i)).enabled=false;
            	}
    		}
    		else if (i==13){
    			if(QuickBuilding.instaBuild==10){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
                }
    		}
    		else if (i==14){
    			if(QuickBuilding.instaBuild==0){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
    			}
    		}
    		else if (i==1){
    			if(QuickBuilding.buildDepth==3){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
                }
    		}
    		else if (i==2){
    			if(QuickBuilding.buildDepth==0){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
    			}
    		}
    		else if (i==3){
    			if(QuickBuilding.buildWidth==10){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
                }
    		}
    		else if (i==4){
    			if(QuickBuilding.buildWidth==2){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
    			}
    		}
    		else if (i==5){
    			if(QuickBuilding.buildType==3){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
                }
    		}
    		else if (i==6){
    			if(QuickBuilding.buildType==0){
    				((GuiButton)this.buttonList.get(i)).enabled=false;
    			}
    		}
    	}
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    protected void keyTyped(char par1, int par2)
    {
    	super.keyTyped(par1, par2);
    }

    /**
     * Called when the mouse is clicked.
     */
    protected void mouseClicked(int par1, int par2, int par3)
    {
        super.mouseClicked(par1, par2, par3);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRendererObj, "Quick building mod configuration", this.width / 2, 20, 16777215);
        this.drawCenteredString(this.fontRendererObj, Integer.toString(QuickBuilding.buildDepth), this.width / 2 - 90, this.height / 4 - 10, 16777215);
        this.drawCenteredString(this.fontRendererObj, Integer.toString(QuickBuilding.buildWidth), this.width / 2 - 55, this.height / 4 - 10, 16777215);
        if(QuickBuilding.buildType==0){
        	this.drawCenteredString(this.fontRendererObj, "Point", this.width / 2 - 20, this.height / 4 - 10, 16777215);
        }
        else if (QuickBuilding.buildType==1) {
        	this.drawCenteredString(this.fontRendererObj, "Wall", this.width / 2 - 20, this.height / 4 - 10, 16777215);
        }
        else if (QuickBuilding.buildType==2) {
        	this.drawCenteredString(this.fontRendererObj, "Pillar", this.width / 2 - 20, this.height / 4 - 10, 16777215);
        }
        else if (QuickBuilding.buildType==3) {
        	this.drawCenteredString(this.fontRendererObj, "Special", this.width / 2 - 20, this.height / 4 - 10, 16777215);
        }
        if(QuickBuilding.maxDistance<10 && QuickBuilding.instaBuild==0){
        	this.drawCenteredString(this.fontRendererObj, Integer.toString(QuickBuilding.maxDistance), this.width / 2 + 15, this.height / 4 - 10, 16777215);
        }
        else {
        	this.drawCenteredString(this.fontRendererObj, "Ignore", this.width / 2 + 15, this.height / 4 - 10, 16777215);
        }
        if (QuickBuilding.instaBuild>0){
        	this.drawCenteredString(this.fontRendererObj, Integer.toString(QuickBuilding.instaBuild), this.width / 2 + 50, this.height / 4 - 10, 16777215);
        }
        else {
        	this.drawCenteredString(this.fontRendererObj, "Instant", this.width / 2 + 50, this.height / 4 - 10, 16777215);
        }
        this.drawCenteredString(this.fontRendererObj, "Thick", this.width / 2 - 90, this.height / 4 - 25, 10526880);
        this.drawCenteredString(this.fontRendererObj, "Width", this.width / 2 - 55, this.height / 4 -25, 10526880);
        this.drawCenteredString(this.fontRendererObj, "Type", this.width / 2 - 20, this.height / 4 - 25, 10526880);
        this.drawCenteredString(this.fontRendererObj, "Distance", this.width / 2 + 15, this.height / 4 - 25, 10526880);
        this.drawCenteredString(this.fontRendererObj, "Speed", this.width / 2 + 50, this.height / 4 - 25, 10526880);
        this.drawString(this.fontRendererObj, I18n.format("advMode.allPlayers"), this.width / 2 - 540, 119, 10526880);
        super.drawScreen(par1, par2, par3);
    }
}
