package rafradek.quickbuilding;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

public class QBEventBusListener {
	public int index=0;
	public int ticks=0;
	public static ArrayList<TickTimer> timerList=new ArrayList<TickTimer>();
	@SubscribeEvent
	public void lolz(PlayerInteractEvent event)
	{
		if(event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK && event.entityPlayer.getHeldItem() != null && !(QuickBuilding.buildType==0 && QuickBuilding.buildDepth==1)&& event.entityPlayer instanceof EntityClientPlayerMP && QuickBuilding.isItemValid(event.entityPlayer.getHeldItem().getItem())){
			this.build(QuickBuilding.buildType, MathHelper.floor_double((double)(event.entityPlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3, event);
		}
		else if(QuickBuilding.quickDestroy&&event.action == PlayerInteractEvent.Action.LEFT_CLICK_BLOCK && !(QuickBuilding.buildType==0 && QuickBuilding.buildDepth==1)&& event.entityPlayer instanceof EntityClientPlayerMP){
			this.destroy(QuickBuilding.buildType, MathHelper.floor_double((double)(event.entityPlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3, event);
		}
	}
	public static void build(int type,EntityClientPlayerMP player, int side, ItemStack stack, int iX, int iY, int iZ, int face) {
		int w=QuickBuilding.buildWidth;
		int h=QuickBuilding.buildWidth;
		int d=QuickBuilding.buildWidth;
		if(type==0){
			w=QuickBuilding.buildDepth;
			d=QuickBuilding.buildDepth;
			h=QuickBuilding.buildDepth;
		}
		else if(type==1){
			if((MathHelper.floor_double(player.posX)==iX && MathHelper.floor_double(player.posZ)==iZ) || player.isSneaking()){
				h=QuickBuilding.buildDepth;
			}
			else{
				if(side==0 || side == 2) {
					d=QuickBuilding.buildDepth;
				}
				else if(side==1 || side == 3) {
					w=QuickBuilding.buildDepth;
				}
			}
			
		}
		else if(type==2){
			if((MathHelper.floor_double(player.posX)==iX && MathHelper.floor_double(player.posZ)==iZ) || player.isSneaking()){
				h=QuickBuilding.buildDepth;
				if(side==0 || side == 2) {
					w=QuickBuilding.buildDepth;
				}
				else if(side==1 || side == 3) {
					d=QuickBuilding.buildDepth;
				}	
			}
			else{
				w=QuickBuilding.buildDepth;
				d=QuickBuilding.buildDepth;
			}
			
		}
		Item item=player.getHeldItem().getItem();
		int minX=-w/2;
		int minY=-h/2;
		int minZ=-d/2;
		
		if(face==1){
			minY=0;
		}
		else if(face==0){
			minY=-h;
		}
		else if(face==5){
			minX=0;
		}
		else if(face==4){
			minX=-w;
		}
		else if(face==3){
			minZ=0;
		}
		else if(face==2){
			minZ=-d;
		}
		int i=player.getHeldItem().stackSize;
		for(int x=0; x<w; x++){
			for (int y=0; y<h; y++){
				for (int z=0; z<d; z++){
					//int validSide=QuickBuilding.getValidSide(player.worldObj, iX+minX+x+QuickBuilding.offsetXfromSide(face), iY+minY+y+QuickBuilding.offsetYfromSide(face), iZ+minZ+z+QuickBuilding.offsetZfromSide(face), face);
					int validSide=face;
					if(QuickBuilding.canPlayerBuildAtNoDistance(iX+minX+x, iY+minY+y, iZ+minZ+z, player, validSide)){
						QuickBuilding.blocksToPlace.add(new BuildEntry(iX+minX+x, iY+minY+y, iZ+minZ+z, validSide,item, player.getHeldItem().getItemDamage(), 200));
						//player.worldObj.spawnParticle("heart", iX-w/2+x, iY-h/2+y, iZ-d/2+z, 0, 0, 0);
					}
				}
			}
		}
		
	}
	public static void destroy(int type,final EntityClientPlayerMP player, int side, ItemStack stack, int iX, int iY, int iZ, int face) {
		int w=QuickBuilding.buildWidth;
		int h=QuickBuilding.buildWidth;
		int d=QuickBuilding.buildWidth;
		if(type==0){
			w=QuickBuilding.buildDepth;
			d=QuickBuilding.buildDepth;
			h=QuickBuilding.buildDepth;
		}
		else if(type==1){
			if((MathHelper.floor_double(player.posX)==iX && MathHelper.floor_double(player.posZ)==iZ) || player.isSneaking()){
				h=QuickBuilding.buildDepth;
			}
			else{
				if(side==0 || side == 2) {
					d=QuickBuilding.buildDepth;
				}
				else if(side==1 || side == 3) {
					w=QuickBuilding.buildDepth;
				}
			}
			
		}
		else if(type==2){
			if((MathHelper.floor_double(player.posX)==iX && MathHelper.floor_double(player.posZ)==iZ) || player.isSneaking()){
				h=QuickBuilding.buildDepth;
				if(side==0 || side == 2) {
					w=QuickBuilding.buildDepth;
				}
				else if(side==1 || side == 3) {
					d=QuickBuilding.buildDepth;
				}	
			}
			else{
				w=QuickBuilding.buildDepth;
				d=QuickBuilding.buildDepth;
			}
		}
		int minX=-w/2;
		int minY=-h/2;
		int minZ=-d/2;
		if(type==3){
			minY=0;
		}
		for(int x=0; x<w; x++){
			for (int y=0; y<h; y++){
				for (int z=0; z<d; z++){
					//int validSide=QuickBuilding.getValidSide(player.worldObj, iX+minX+x+QuickBuilding.offsetXfromSide(face), iY+minY+y+QuickBuilding.offsetYfromSide(face), iZ+minZ+z+QuickBuilding.offsetZfromSide(face), face);
					final int validSide=QuickBuilding.getValidSideForDestroy(player.worldObj, iX+minX+x, iY+minY+y, iZ+minZ+z, face);
					if(QuickBuilding.isProperDistance(iX+minX+x, iY+minY+y, iZ+minZ+z, player)&&!player.worldObj.isAirBlock(iX+minX+x, iY+minY+y, iZ+minZ+z)){
						player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, iX+minX+x, iY+minY+y, iZ+minZ+z, validSide));
						
						/*QBTickHandler.timerList.add(new TickTimer(1,50,iX+minX+x, iY+minY+y, iZ+minZ+z){
							@Override
							public void runLoop() {
								player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(1, (Integer)this.params[0],(Integer)this.params[1],(Integer)this.params[2], validSide));
								if(this.repeatsLeft==0){
									player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(2, (Integer)this.params[0],(Integer)this.params[1],(Integer)this.params[2], validSide));
								}
							}
							
						});*/
					
					}
				}
			}
		}
		
	}
	public static void build(int type, int side, PlayerInteractEvent event){
		build(type, (EntityClientPlayerMP) event.entityPlayer, side, event.entityPlayer.getHeldItem(), event.x, event.y, event.z,event.face);
	}
	public static void destroy(int type, int side, PlayerInteractEvent event){
		destroy(type, (EntityClientPlayerMP) event.entityPlayer, side, event.entityPlayer.getHeldItem(), event.x, event.y, event.z,event.face);
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void keyPressed(InputEvent.KeyInputEvent event){
		if (Keyboard.getEventKeyState()&&Minecraft.getMinecraft().theWorld != null && Minecraft.getMinecraft().currentScreen == null)
        {
			if(Keyboard.getEventKey()==QuickBuilding.buildTypeKB.getKeyCode()){
				if(!Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
					QuickBuilding.buildType++;
					if(QuickBuilding.buildType>2){
						QuickBuilding.buildType=0;
					}
					Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Build Type: "+ (QuickBuilding.buildType ==0? "point" :( QuickBuilding.buildType ==1 ? "wall": "pillar"))));
				}
				else{
					QuickBuilding.quickDestroy=!QuickBuilding.quickDestroy;
					Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Quick Destroy: "+ QuickBuilding.quickDestroy));
				}
				
			}
			if(Keyboard.getEventKey()==QuickBuilding.buildHeightKB.getKeyCode()){
				Minecraft.getMinecraft().displayGuiScreen(new QBGui());
			}
			if(Keyboard.getEventKey()==QuickBuilding.buildWidthKB.getKeyCode()){
				if(Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
				QuickBuilding.buildWidth+=1;
				if(QuickBuilding.buildWidth>10){
					QuickBuilding.buildWidth=2;
				}
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Build Width: "+ QuickBuilding.buildWidth));
				}
				else{
					QuickBuilding.buildDepth++;
				if(QuickBuilding.buildDepth>3){
					QuickBuilding.buildDepth=1;
				}
					
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Build Thickness: "+ QuickBuilding.buildDepth));
				}
			}
			if(Keyboard.getEventKey()==QuickBuilding.dangerBuildKB.getKeyCode()){
				if(!Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
				QuickBuilding.dangerBuild=!QuickBuilding.dangerBuild;
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Shealth Build: "+ QuickBuilding.dangerBuild));
				Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
				}
				else{
					//Minecraft.getMinecraft().timer.ticksPerSecond*=1.5;
					//Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Speed: "+ Minecraft.getMinecraft().timer.ticksPerSecond);
				}
			}
			if(Keyboard.getEventKey()==QuickBuilding.uselessButtonKB.getKeyCode()){
				if(Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
					QuickBuilding.waterRemover=!QuickBuilding.waterRemover;
					Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Water Remover: "+ QuickBuilding.waterRemover));
					Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
				}
				else {
				QuickBuilding.lavaRemover=!QuickBuilding.lavaRemover;
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Lava Remover: "+ QuickBuilding.lavaRemover));
				Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
				}
			}
			if(Keyboard.getEventKey()==QuickBuilding.autoFireKB.getKeyCode()){
				if(Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
					QuickBuilding.autoPlace=!QuickBuilding.autoPlace;
					Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Auto Place: "+ QuickBuilding.autoPlace));
					KeyBinding.setKeyBindState(-99, QuickBuilding.autoPlace);
				}
				else {
				QuickBuilding.autoMine=!QuickBuilding.autoMine;
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Auto Mine: "+ QuickBuilding.autoMine));
				KeyBinding.setKeyBindState(-100, QuickBuilding.autoMine);
				}
			}
			if(Keyboard.getEventKey()==QuickBuilding.instaBuildKB.getKeyCode()){
				if(Keyboard.isKeyDown(Keyboard.KEY_RMENU)){
					//Minecraft.getMinecraft().timer.ticksPerSecond/=1.5;
					//Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Speed: "+ Minecraft.getMinecraft()));
				}
				else{
				EntityClientPlayerMP player =Minecraft.getMinecraft().thePlayer;
				ItemStack stack=player.getHeldItem();
				if((stack == null || !QuickBuilding.isItemValid(stack.getItem())) && QuickBuilding.getBlockInInventory(player,true,1) != -1) {
					player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(QuickBuilding.getBlockInInventory(player,true,1)));
					stack=player.inventory.getStackInSlot(QuickBuilding.getBlockInInventory(player,true,1));
				}
				System.out.println(stack);
				if(stack != null){
				int bh=QuickBuilding.buildDepth;
				int bw=QuickBuilding.buildWidth;
				QuickBuilding.buildDepth=2;
				QuickBuilding.buildWidth=6;
				QBEventBusListener.build(1, player, MathHelper.floor_double((double)(player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3, stack, MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY-7),MathHelper.floor_double(player.posZ), 1);
				QuickBuilding.buildDepth=bh;
				QuickBuilding.buildWidth=bw;
				}
				}
			}
        }
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void clientTickStart(TickEvent.ClientTickEvent eventa){
		if(Minecraft.getMinecraft().theWorld != null && eventa.phase==Phase.START && Minecraft.getMinecraft().currentScreen == null){
			EntityClientPlayerMP player=Minecraft.getMinecraft().thePlayer;
			WorldClient world=Minecraft.getMinecraft().theWorld;
			if(QuickBuilding.dangerBuild){
				ItemStack stack=player.getHeldItem();
				if((stack == null || !QuickBuilding.isItemValid(stack.getItem())) && QuickBuilding.getBlockInInventory(player,true,1) != -1) {
					player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(QuickBuilding.getBlockInInventory(player,true,1)));
					stack=player.inventory.getStackInSlot(QuickBuilding.getBlockInInventory(player,true,1));
				}
				if(stack != null){
				if(QuickBuilding.dangerBuildMode) {
					List playerList=world.getEntitiesWithinAABBExcludingEntity(player, AxisAlignedBB.getBoundingBox(player.posX-4, player.posY-4, player.posZ-4, player.posX+4, player.posY+4, player.posZ+4));
					for(int i=0; i<playerList.size(); i++){
						if(playerList.get(i) instanceof EntityPlayer){
							Entity enemyPlayer=(Entity) playerList.get(i);
							for(int x=-6; x<6; x++){
								for(int y=-6; y<6; y++){
									for (int z=-6; z<6; z++){
										int posX=MathHelper.floor_double(enemyPlayer.posX+x);
										int posY=MathHelper.floor_double(enemyPlayer.posY-0.5+y);
										int posZ=MathHelper.floor_double(enemyPlayer.posZ+z);
										if(enemyPlayer.getDistanceSq(enemyPlayer.posX+x, enemyPlayer.posY-0.5+y+1, enemyPlayer.posZ+z) < QuickBuilding.buildDepth*4 && !(player.worldObj.getBlock(posX, posY, posZ)!= null &&player.worldObj.getBlock(posX, posY, posZ).onBlockActivated(player.worldObj, posX, posY, posZ, player, 1, 0, 0, 0))){
											player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(posX, posY ,posZ , 1, stack,(float)enemyPlayer.posX, (float)enemyPlayer.posY, (float)enemyPlayer.posZ));
										}
									}
								}
							}
						}
					}
				}
				
				else {
					for(int x=-6; x<6; x++){
						for(int y=-6; y<6; y++){
							for (int z=-6; z<6; z++){
								int posX=MathHelper.floor_double(player.posX+x);
								int posY=MathHelper.floor_double(player.posY-0.5+y);
								int posZ=MathHelper.floor_double(player.posZ+z);
								AxisAlignedBB axisalignedbb = Block.getBlockFromItem(stack.getItem()).getCollisionBoundingBoxFromPool(Minecraft.getMinecraft().theWorld, posX, posY, posZ);
								if(((axisalignedbb == null || world.checkNoEntityCollision(axisalignedbb))&&player.getDistanceSq(player.posX+x, player.posY+0.5+y+1, player.posZ+z) < QuickBuilding.buildDepth*4)&& !(player.worldObj.getBlock(posX, posY, posZ)!= null &&player.worldObj.getBlock(posX, posY, posZ).onBlockActivated(player.worldObj, posX, posY, posZ, player, 1, 0, 0, 0))){
								player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(posX, posY, posZ, 1, stack,(float)player.posX, (float)player.posY, (float)player.posZ));
								}
							}
						}
					}
				}
				}
			}
			if(timerList.size()>0){
				for(int i=0;i<timerList.size();i++){
					timerList.get(i).updateTimer();
					if(timerList.get(i).dead){
						timerList.remove(i);
					}
				}
			}
			if(QuickBuilding.lavaRemover || QuickBuilding.waterRemover){
				int used=3;
				for(int x=-used+1; x < used; x++){
					for(int y=-3; y < 2; y++){
						for(int z=-used+1; z < used; z++){
							int posX=MathHelper.floor_double(player.posX+x);
							int posY=MathHelper.floor_double(player.posY+y);
							int posZ=MathHelper.floor_double(player.posZ+z);
							if(QuickBuilding.lavaRemover &&world.getBlock(posX,posY,posZ).getMaterial()== Material.lava || world.getBlock(posX,posY,posZ).getMaterial()== Material.fire){
								player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0,posX,posY,posZ,QuickBuilding.getValidSideForDestroy(world, posX, posY, z, 1)));
							}
							else if(QuickBuilding.waterRemover &&world.getBlock(posX,posY,posZ).getMaterial()== Material.water){
								player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0,posX,posY,posZ,QuickBuilding.getValidSideForDestroy(world, posX, posY, z, 1)));
							}
						}
					}
				}
			}
			if(QuickBuilding.blocksToPlace.size()>0){
				if((QuickBuilding.instaBuild>0 && this.ticks<=0) || QuickBuilding.instaBuild==0) {
					for(int i=0;i<(QuickBuilding.instaBuild==0 ?QuickBuilding.blocksToPlace.size():1);i++){
						if(index<QuickBuilding.blocksToPlace.size()){
							BuildEntry event =QuickBuilding.blocksToPlace.get(index);
							Item itemID=event.item;
							int metadata=event.metadata;
							if(player.getHeldItem() != null && QuickBuilding.canPlayerBuildAt(event.x, event.y, event.z, player, event.face) && player.getHeldItem().stackSize > 0 && player.getHeldItem().getItem() == event.item && player.getHeldItem().getItemDamage() == event.metadata){
								player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(event.x, event.y, event.z, event.face, player.getHeldItem(),(float)player.posX, (float)player.posY, (float)player.posZ));
								event.tickTime-=QuickBuilding.instaBuild==0 ? 1 : QuickBuilding.instaBuild*5;
								this.ticks=QuickBuilding.instaBuild-1;
								if(event.tickTime<0){
									QuickBuilding.blocksToPlace.remove(index);
								}
							} else if ((player.getHeldItem() != null && QuickBuilding.canPlayerBuildAtNoDistance(event.x, event.y, event.z, player, event.face)) || player.getHeldItem() == null || player.getHeldItem().stackSize <= 0) {
								QuickBuilding.sheludedblocksToPlace.add(event);
								QuickBuilding.tryChangeItemStack(player,itemID, metadata);
								QuickBuilding.blocksToPlace.remove(index);
							}
							else {
								QuickBuilding.blocksToPlace.remove(index);
							}
							this.index++;
						}
						if(index>=QuickBuilding.blocksToPlace.size()){
							index=0;
						}
						
					
					}
				}
				else if(QuickBuilding.instaBuild>0){
					this.ticks--;
				}
				//System.out.println(QuickBuilding.blocksToPlace.size()+" "+QuickBuilding.ignoreMaxDistance);
			}
			if(QuickBuilding.sheludedblocksToPlace.size()>0){
				Iterator<BuildEntry> iterator = QuickBuilding.sheludedblocksToPlace.iterator();
				while(iterator.hasNext()){
					BuildEntry event =iterator.next();
					if(QuickBuilding.isProperDistance(event.x, event.y, event.z, player) && player.getHeldItem() != null && player.getHeldItem().stackSize > 0 && player.getHeldItem().getItem() == event.item && player.getHeldItem().getItemDamage() == event.metadata){
						QuickBuilding.blocksToPlace.add(0, event);
						iterator.remove();
					} else if (event.tickTime <= 0) {
						iterator.remove();
					}
					else {
						event.tickTime--;
					}
				}
				//System.out.println(QuickBuilding.sheludedblocksToPlace.size());
			}
		}

	}
}
