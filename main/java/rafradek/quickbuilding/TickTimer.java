package rafradek.quickbuilding;

public abstract class TickTimer {
	public int tickMax;
	public int tick;
	public int repeatsLeft;
	public int loops;
	public boolean dead;
	public Object[] params;
	
	public TickTimer(int ticktime, int repeat, Object... objects){
		this.tickMax=ticktime;
		this.tick=ticktime;
		this.repeatsLeft=repeat;
		this.params=objects;
	}
	
	public void updateTimer(){
		if(this.tick--<=0){
			if(this.repeatsLeft-->=0){
				this.runLoop();
				this.tick=this.tickMax;
			}
			if(this.repeatsLeft<=0){
				this.dead=true;
			}
		}
	}

	public abstract void runLoop();
}
