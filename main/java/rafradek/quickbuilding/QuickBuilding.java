package rafradek.quickbuilding;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.util.Facing;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = "rafradek_quick_building", name =  "Quick Building", version = "1.0")
public class QuickBuilding {
	
	public static List<BuildEntry> blocksToPlace;
	public static List<BuildEntry> sheludedblocksToPlace;
	
	public static File optionsFile;
	
	public static Configuration conf;

	public static KeyBinding buildTypeKB=new KeyBinding("key.buildtype", Keyboard.KEY_NUMPAD7,"QuickBuild");
	public static KeyBinding buildHeightKB=new KeyBinding("key.builddepth", Keyboard.KEY_NUMPAD8,"QuickBuild");
	public static KeyBinding buildWidthKB=new KeyBinding("key.buildwidth", Keyboard.KEY_NUMPAD9,"QuickBuild");
	public static KeyBinding instaBuildKB=new KeyBinding("key.instabuild", Keyboard.KEY_NUMPAD5,"QuickBuild");
	public static KeyBinding dangerBuildKB=new KeyBinding("key.dangerbuild", Keyboard.KEY_NUMPAD4,"QuickBuild");
	public static KeyBinding uselessButtonKB=new KeyBinding("key.uselessbutton", Keyboard.KEY_NUMPAD6,"QuickBuild");
	public static KeyBinding autoFireKB=new KeyBinding("key.instabuild", Keyboard.KEY_NUMPAD3,"QuickBuild");
	
	public static int tickLeft=20;
	public static int buildType=0;
	public static int buildDepth=1;
	public static int buildWidth=10;
	public static boolean dangerBuild;
	public static int instaBuild=0;
	public static boolean lavaRemover;
	public static int maxDistance=7;
	public static boolean waterRemover;
	public static boolean allowAny=false;
	public static boolean ignoreMaxDistance=false;
	public static boolean dangerBuildMode=false;
	public static boolean autoPlace;
	public static boolean autoMine;
	public static boolean specialDestroyMode;
	public static boolean quickDestroy;
    
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		optionsFile=new File(Minecraft.getMinecraft().mcDataDir, "options.txt");
		conf=new Configuration(event.getSuggestedConfigurationFile());
		MinecraftForge.EVENT_BUS.register(new QBEventBusListener());
		blocksToPlace = new ArrayList();
		sheludedblocksToPlace = new ArrayList();
		FMLCommonHandler.instance().bus().register(new QBEventBusListener());
		loadOptions();
	}
	public static int getBlockInInventory(EntityPlayer player, boolean hot, int index) {
		int a=0;
		for(int i=0; i<(hot?player.inventory.getHotbarSize():player.inventory.mainInventory.length); i++){
			if(player.inventory.mainInventory[i] != null && isItemValid(player.inventory.mainInventory[i].getItem())){
				a++;
				
			}
			if(a == index){
				return i;
			}
		}
		return -1;
	}
	public static int getFirstItemInInventory(EntityPlayer player, boolean hot, Item itemID, int metaData) {
		for(int i=0; i<(hot?player.inventory.getHotbarSize():player.inventory.mainInventory.length); i++){
			if(player.inventory.mainInventory[i] != null && player.inventory.mainInventory[i].getItem() == itemID && player.inventory.mainInventory[i].getItemDamage() == metaData){
				return i;
			}
		}
		return -1;
	}
	public static int getTotalItemCount(EntityPlayer player, boolean hot, Item itemID, int metadata) {
		int c=0;
		for(int i=0; i<(hot?player.inventory.getHotbarSize():player.inventory.mainInventory.length); i++){
			if(player.inventory.mainInventory[i] != null && player.inventory.mainInventory[i].getItem() == itemID && player.inventory.mainInventory[i].getItemDamage() == metadata ){
				c+=player.inventory.mainInventory[i].stackSize;
			}
		}
		return c;
	}
	public static int getTotalBlockCount(EntityPlayer player, boolean hot) {
		int c=0;
		for(int i=0; i<(hot?player.inventory.getHotbarSize():player.inventory.mainInventory.length); i++){
			if(player.inventory.mainInventory[i] != null && isItemValid(player.inventory.mainInventory[i].getItem()) ){
				c+=player.inventory.mainInventory[i].stackSize;
			}
		}
		return c;
	}
	public static int getTotalBlockStackCount(EntityPlayer player, boolean hot) {
		int c=0;
		for(int i=0; i<(hot?player.inventory.getHotbarSize():player.inventory.mainInventory.length); i++){
			if(player.inventory.mainInventory[i] != null && isItemValid(player.inventory.mainInventory[i].getItem()) ){
				c+=player.inventory.mainInventory[i].stackSize;
			}
		}
		return c;
	}
	public static void tryChangeItemStack(EntityClientPlayerMP player, Item itemID, int metadata) {
		if(player.getHeldItem() == null || player.getHeldItem().stackSize <= 0 || player.getHeldItem().getItem() != itemID || player.getHeldItem().getMaxDamage() !=metadata){
		int index=-1;
		for(int i=0; i<player.inventory.getHotbarSize(); i++){
			if(player.inventory.mainInventory[i] != null && player.inventory.mainInventory[i].getItem() == itemID && player.inventory.mainInventory[i].getItemDamage() == metadata){
				index=i;
				break;
			}
		}
		if(index==-1){
			return;
		}
		player.inventory.currentItem=index;
		player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(index));
		}
	}
	public static boolean canPlayerBuildAt(int x, int y, int z, EntityPlayer player, int side){
		double dist = player.capabilities.isCreativeMode ? maxDistance : maxDistance-0.5;
        dist *= dist;
		if(!(player.worldObj.getBlock(x, y, z)!= null &&player.worldObj.getBlock(x, y, z).onBlockActivated(player.worldObj, x, y, z, player, side, 0, 0, 0)) &&(ignoreMaxDistance || player.getDistanceSq((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D) < dist)){
			/*if(side==0){
				y--;
			}
			if(side==1){
				y++;
			}
			if(side==2){
				z--;
			}
			if(side==3){
				z++;
			}
			if(side==4){
				x--;
			}
			if(side==5){
				x++;
			}*/
			x+=offsetXfromSide(side);
			y+=offsetYfromSide(side);
			z+=offsetZfromSide(side);
			return y>0 && (player.worldObj.getBlock(x, y, z)== null || (!Block.getBlockFromItem(player.getHeldItem().getItem()).isReplaceable(player.worldObj, x, y, z) && player.worldObj.getBlock(x, y, z).isReplaceable(player.worldObj, x, y, z)) && Block.getBlockFromItem(player.getHeldItem().getItem()).canPlaceBlockAt(player.worldObj, x, y, z));
		}
		return false;
	}
	public static boolean isItemValid(Item item){
		return item instanceof ItemBlock ? true : allowAny;
	}
	public static boolean canPlayerBuildAtNoDistance(int x, int y, int z, EntityPlayer player, int side){
		if(!(player.worldObj.getBlock(x, y, z)!= null &&player.worldObj.getBlock(x, y, z).onBlockActivated(player.worldObj, x, y, z, player, side, 0, 0, 0))){
			/*if(side==0){
				y--;
			}
			if(side==1){
				y++;
			}
			if(side==2){
				z--;
			}
			if(side==3){
				z++;
			}
			if(side==4){
				x--;
			}
			if(side==5){
				x++;
			}*/
			x+=offsetXfromSide(side);
			y+=offsetYfromSide(side);
			z+=offsetZfromSide(side);
			return y>0 && (player.worldObj.getBlock(x, y, z)== null || (!Block.getBlockFromItem(player.getHeldItem().getItem()).isReplaceable(player.worldObj, x, y, z) && player.worldObj.getBlock(x, y, z).isReplaceable(player.worldObj, x, y, z)) && Block.getBlockFromItem(player.getHeldItem().getItem()).canPlaceBlockAt(player.worldObj, x, y, z));
		}
		return false;
	}
	public static boolean isProperDistance(int x, int y, int z, EntityPlayer player){
		double dist = player.capabilities.isCreativeMode ? maxDistance : maxDistance-0.5;
        dist *= dist;
        return ignoreMaxDistance || player.getDistanceSq((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D) < dist;
	}
	public static String getValueFromOptions(String name,String defaul){
		try
        {
            if (!optionsFile.exists())
            {
                return defaul;
            }

            BufferedReader bufferedreader = new BufferedReader(new FileReader(optionsFile));
            String s = "";

            while ((s = bufferedreader.readLine()) != null)
            {
            	String[] property=s.split(":");
            	if(property[0].equals(name) && property.length==2){
            		bufferedreader.close();
            		return property[1];
            	}
            }
            bufferedreader.close(); 
        }
		catch(Exception e){
			
		}
		
		return defaul;
	}
	public static void loadOptions() {
		conf.load();
		maxDistance=conf.get("config", "max distance", 7).getInt(7);
		buildDepth=conf.get("config", "build thickness",1).getInt(1);
		buildWidth=conf.get("config", "build height",10).getInt(10);
		dangerBuildMode=conf.get("config", "shealth mode", false).getBoolean(false);
		allowAny=conf.get("config", "allow items", false).getBoolean(false);
		instaBuild=conf.get("config", "build speed",0).getInt(0);
		if(maxDistance==10 || instaBuild>0){
			ignoreMaxDistance=true;
		}
		conf.save();
	}
	public static void saveOptions() {
		conf.get("config", "max distance", 7).set(maxDistance);
		conf.get("config", "build thickness",1).set(buildDepth);
		conf.get("config", "build height",10).set(buildWidth);
		conf.get("config", "shealth mode", false).set(dangerBuildMode);
		conf.get("config", "allow items", false).set(allowAny);
		conf.get("config", "build speed",0).set(instaBuild);
		conf.save();
	}
	public static int getValidSide(World world, int x, int y, int z,int def){
		if(!world.isAirBlock(x+offsetXfromSide(def),y+offsetYfromSide(def),z+offsetZfromSide(def))){
			return def;
		}
		for(int i=0; i<6;i++){
			if(!world.isAirBlock(x+offsetXfromSide(i),y+offsetYfromSide(i),z+offsetZfromSide(i))){
				return i;
			}
		}
		return def;
	}
	
	public static int getValidSideForDestroy(World world, int x, int y, int z,int def){
		if(!world.isBlockNormalCubeDefault(x+offsetXfromSide(def),y+offsetYfromSide(def),z+offsetZfromSide(def), true)){
			return def;
		}
		for(int i=0; i<6;i++){
			if(!world.isBlockNormalCubeDefault(x+offsetXfromSide(i),y+offsetYfromSide(i),z+offsetZfromSide(i),true)){
				return i;
			}
		}
		return def;
	}
	
	public static int offsetXfromSide(int side){
		if(side==4){
			return -1;
		}
		if(side==5){
			return 1;
		}
		return 0;
	}
	public static int offsetYfromSide(int side){
		if(side==0){
			return -1;
		}
		if(side==1){
			return 1;
		}
		return 0;
	}
	public static int offsetZfromSide(int side){
		if(side==2){
			return -1;
		}
		if(side==3){
			return 1;
		}
		return 0;
	}
}
