package rafradek.TF2weapons;

import java.util.HashMap;

import net.minecraft.entity.EntityLivingBase;

public class ThreadLocalPlayerAction extends ThreadLocal<HashMap<EntityLivingBase,Integer>> {
	
	@Override
	public HashMap<EntityLivingBase,Integer> initialValue(){
		return new HashMap<EntityLivingBase,Integer>();
	}
	
}
