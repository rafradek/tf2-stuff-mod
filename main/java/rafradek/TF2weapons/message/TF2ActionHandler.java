package rafradek.TF2weapons.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.ThreadLocalPlayerAction;
import rafradek.TF2weapons.weapons.ItemUsable;
import rafradek.TF2weapons.weapons.MinigunLoopSound;
import rafradek.wallpaint.WallPaintMessage.ArrayReturn;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class TF2ActionHandler implements IMessageHandler<TF2Message.ActionMessage, IMessage> {
	
	/*public static Map<EntityLivingBase,Integer> playerAction=new HashMap<EntityLivingBase,Integer>();
	public static Map<EntityLivingBase,Integer> playerActionClient=new HashMap<EntityLivingBase,Integer>();*/
	public static ThreadLocalPlayerAction playerAction=new ThreadLocalPlayerAction();
	@Override
	public ArrayReturn onMessage(TF2Message.ActionMessage message, MessageContext ctx) {
		EntityPlayerMP player=ctx.getServerHandler().playerEntity;
		handleMessage(message, player);
		message.entity=player.getEntityId();
		TF2weapons.network.sendToDimension(message, player.dimension);
		return null;
	}
	public static class TF2ActionHandlerReturn implements IMessageHandler<TF2Message.ActionMessage, IMessage> {

		@Override
		public IMessage onMessage(TF2Message.ActionMessage message, MessageContext ctx) {
			EntityLivingBase player=(EntityLivingBase) Minecraft.getMinecraft().theWorld.getEntityByID(message.entity);
			handleMessage(message, player);
			return null;
		}

	}
	public static void handleMessage(TF2Message.ActionMessage message,EntityLivingBase player){
		if(player!=null){
			/*int oldValue=playerAction.get().containsKey(player)?playerAction.get().get(player):0;
			if(player.getHeldItem() != null && player.getHeldItem().getItem() instanceof ItemUsable){
				if((oldValue&1)==0&&(message.value&1)!=0){
					((ItemUsable)player.getHeldItem().getItem()).startUse(player.getHeldItem(), player, player.worldObj);
				}
				if((oldValue&1)==0&&(message.value&1)!=0){
					((ItemUsable)player.getHeldItem().getItem()).endUse(player.getHeldItem(), player, player.worldObj);
				}
			}*/
			playerAction.get().put(player, message.value+(playerAction.get().containsKey(player)?playerAction.get().get(player)&8:0));
			//System.out.println("dostal: "+message.value);
		}
	}
	public static ArrayList<EnumAction> value=new ArrayList<EnumAction>();
	public static enum EnumAction{
		
		IDLE,
		FIRE,
		ALTFIRE,
		RELOAD;

		private EnumAction(){
			value.add(this);
		}
		
		/*public boolean leftClick(){
			return this==FIRE||this==DOUBLE;
		}
		public boolean rightClick(){
			return this==ALTFIRE||this==DOUBLE;
		}*/
	}
}
