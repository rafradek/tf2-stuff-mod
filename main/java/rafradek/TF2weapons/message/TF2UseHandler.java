package rafradek.TF2weapons.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.ThreadLocalPlayerAction;
import rafradek.TF2weapons.weapons.ItemRangedWeapon;
import rafradek.TF2weapons.weapons.ItemUsable;
import rafradek.TF2weapons.weapons.MinigunLoopSound;
import rafradek.wallpaint.WallPaintMessage.ArrayReturn;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class TF2UseHandler implements IMessageHandler<TF2Message.UseMessage, IMessage> {
	
	@Override
	public ArrayReturn onMessage(TF2Message.UseMessage message, MessageContext ctx) {
		EntityPlayer player=Minecraft.getMinecraft().thePlayer;
		ItemStack stack=player.getHeldItem();
		if(stack!=null&&stack.getItem() instanceof ItemRangedWeapon){
			stack.setItemDamage(message.value);
			if(message.reload){
				/*if(stack.getItemDamage()==0&&TF2ActionHandler.playerAction.get().get(player)!=null&&(TF2ActionHandler.playerAction.get().get(player)&8)==0){
					TF2ActionHandler.playerAction.get().put(player, arg1)
				}*/
				TF2weapons.proxy.playReloadSound(player,stack);
			}
		}
		return null;
	}
}
