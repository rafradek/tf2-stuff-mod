package rafradek.TF2weapons.message;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import rafradek.TF2weapons.message.TF2ActionHandler.EnumAction;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;

public abstract class TF2Message implements IMessage{
	
	public static class ActionMessage extends TF2Message{
		int value;
		int entity;
		public ActionMessage(){
			
		}
		public ActionMessage(int value,EntityLivingBase entity){
			this.value=value;
			this.entity=entity.getEntityId();
		}
		public ActionMessage(int value){
			this.value=value;
		}
		@Override
		public void fromBytes(ByteBuf buf) {
			this.value=buf.readByte();
			if(buf.readableBytes()>0){
				this.entity=buf.readInt();
			}
		}

		@Override
		public void toBytes(ByteBuf buf) {
			buf.writeByte(this.value);
			if(this.entity!=0){
				buf.writeInt(this.entity);
			}
		}
	}
	public static class UseMessage extends TF2Message{
		int value;
		boolean reload;
		public UseMessage(){
		}
		public UseMessage(int value,boolean reload){
			this.value=value;
			this.reload=reload;
		}
		@Override
		public void fromBytes(ByteBuf buf) {
			this.value=buf.readShort();
			this.reload=buf.readBoolean();
		}

		@Override
		public void toBytes(ByteBuf buf) {
			buf.writeShort(value);
			buf.writeBoolean(reload);
		}
		
	}
}
