package rafradek.TF2weapons;

import java.util.Iterator;

import rafradek.TF2weapons.characters.IRangedWeaponAttackMob;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;

public class TF2Attribute {

	public static TF2Attribute[] attributes=new TF2Attribute[25];
	
	public int id;
	public String name;
	public String typeOfValue;
	public String effect;
	public float defaultValue;
	public int state;
	
	/**
	 * Tworzy
	 * @param id
	 * @param name
	 * @param effect
	 * @param typeOfValue
	 * @param defaultValue
	 * @param state 1 dodatni, 0 neutralny, -1 ujemny
	 */
	
	
	public TF2Attribute(int id, String name, String effect, String typeOfValue, float defaultValue, int state) {
		this.id=id;
		attributes[id]=this;
		MapList.nameToAttribute.put(name, this);
		this.name=name;
		this.effect=effect;
		this.typeOfValue=typeOfValue;
		this.defaultValue=defaultValue;
		this.state=state;
		
	}
	public static void initAttributes(){
		new TF2Attribute(0, "DamageBonus", "Damage", "Percentage", 1f, 1);
		new TF2Attribute(1, "DamagePenalty", "Damage", "Percentage", 1f, -1);
		new TF2Attribute(2, "ClipSizeBonus", "Clip Size", "Percentage", 1f, 1);
		new TF2Attribute(3, "ClipSizePenalty", "Clip Size", "Percentage", 1f, -1);
		new TF2Attribute(4, "MinigunSpinBonus", "Minigun Spinup", "Inverted_Percentage", 1f, 1);
		new TF2Attribute(5, "MinigunSpinPenalty", "Minigun Spinup", "Percentage", 1f, -1);
		new TF2Attribute(6, "FireRateBonus", "Fire Rate", "Inverted_Percentage", 1f, 1);
		new TF2Attribute(7, "FireRatePenalty", "Fire Rate", "Inverted_Percentage", 1f, -1);
		new TF2Attribute(8, "SpreadBonus", "Spread", "Inverted_Percentage", 1f, 1);
		new TF2Attribute(9, "SpreadPenalty", "Spread", "Percentage", 1f, -1);
		new TF2Attribute(10, "PelletBonus", "Pellet Count", "Percentage", 1f, 1);
		new TF2Attribute(11, "PelletPenalty", "Pellet Count", "Percentage", 1f, -1);
		new TF2Attribute(12, "ReloadRateBonus", "Reload Time", "Inverted_Percentage", 1f, 1);
		new TF2Attribute(13, "ReloadRatePenalty", "Reload Time", "Percentage", 1f, -1);
		new TF2Attribute(14, "KnockbackBonus", "Knockback", "Percentage", 1f, 1);
		new TF2Attribute(15, "KnockbackPenalty", "Knockback", "Percentage", 1f, -1);
		new TF2Attribute(16, "ChargeBonus", "Charge", "Percentage", 1f, 1);
		new TF2Attribute(17, "ChargePenalty", "Charge", "Inverted_Percentage", 1f, -1);
	}
	public static float getModifier(String effect, ItemStack stack, float initial,EntityLivingBase entity) {
		float value=initial;
		if(stack.getTagCompound() != null){
			NBTTagCompound attributeList=stack.getTagCompound().getCompoundTag("Attributes");
			Iterator iterator=attributeList.func_150296_c().iterator();
			while(iterator.hasNext()){
				String name=(String) iterator.next();
				NBTBase tag=attributeList.getTag(name);
				if(tag instanceof NBTTagFloat){
					TF2Attribute attribute=attributes[Integer.parseInt(name)];
					if(attribute.effect==effect)
						if(attribute.typeOfValue.equals("Additive"))
							value+=((NBTTagFloat)tag).func_150288_h();
						else
							value*=((NBTTagFloat)tag).func_150288_h();
				}
			}
		}
		if(entity!=null&&entity instanceof IRangedWeaponAttackMob){
			value*=((IRangedWeaponAttackMob)entity).getAttributeModifier(effect);
		}
		return value;
	}
}
