package rafradek.TF2weapons.projectiles;

import rafradek.TF2weapons.TF2EventBusListener;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.World;

public class EntityRocketEffect extends EntityFX {
	
	private int duration;
	private boolean nextDead;

	public EntityRocketEffect(World par1World, double x, double y, double z) {
		super(par1World, x, y, z);
		this.noClip=true;
		this.particleScale=1;
		this.duration=8;
		this.particleMaxAge=200;
		this.setSize(0.1f, 0.1f);
		this.setParticleTextureIndex(65);
		//this.setParticleIcon(TF2EventBusListener.pelletIcon);
		//this.setParticleTextureIndex(81);
		this.multipleParticleScaleBy(2);
		// TODO Auto-generated constructor stub
		this.setRBGColorF(1f, 1f,1f);
		this.particleTextureJitterX = this.rand.nextFloat();
        this.particleTextureJitterY = this.rand.nextFloat();
	}
	
	public void onUpdate(){
		super.onUpdate();
		
		if(duration > 0){
			duration--;
			this.particleScale=this.duration/8*3;
			if(duration==0)
				this.setDead();
		}
	}

	/*public int getFXLayer()
    {
        return 2;
    }*/
	public void renderParticle(Tessellator par1Tessellator, float par2, float par3, float par4, float par5, float par6, float par7)
    {
		//System.out.println("update: "+duration);
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
    }
	@SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float p_70070_1_)
    {
		return 15728880;
    }

    public float getBrightness(float p_70013_1_)
    {
        return 1.0F;
    }
}
