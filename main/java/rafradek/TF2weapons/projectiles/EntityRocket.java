package rafradek.TF2weapons.projectiles;

import java.util.Iterator;

import rafradek.TF2weapons.ClientProxy;
import rafradek.TF2weapons.TF2weapons;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.S27PacketExplosion;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class EntityRocket extends EntityProjectileBase {

	public EntityRocket(World p_i1756_1_) {
		super(p_i1756_1_);
	}
	
	public EntityRocket(World p_i1756_1_, EntityLivingBase p_i1756_2_) {
		super(p_i1756_1_, p_i1756_2_);
	}

	@Override
	public void onHitGround(int x, int y, int z, MovingObjectPosition mop) {
		this.explode(mop.hitVec.xCoord, mop.hitVec.yCoord, mop.hitVec.zCoord,null);
	}

	@Override
	public void onHitMob(Entity entityHit, MovingObjectPosition mop) {
		this.explode(mop.hitVec.xCoord, mop.hitVec.yCoord, mop.hitVec.zCoord,mop.entityHit);
	}
	public double maxMotion(){
		return Math.max(this.motionX, Math.max(this.motionY, this.motionZ));
	}
	public void onUpdate()
    {
		super.onUpdate();
		if(this.worldObj.isRemote){
			for (int i = 0; i < 4; ++i)
            {
				double pX=this.posX - this.motionX * (double)i / 4.0D - this.motionX;
				double pY=this.posY - this.motionY * (double)i / 4.0D- this.motionY;
				double pZ=this.posZ - this.motionZ * (double)i / 4.0D- this.motionZ;
				ClientProxy.spawnParticle(this.worldObj,new EntityRocketEffect(worldObj, pX, pY, pZ));
                this.worldObj.spawnParticle("smoke", pX, pY, pZ, 0, 0, 0);
            }
			ClientProxy.spawnParticle(this.worldObj,new EntityRocketEffect(worldObj, this.posX, this.posY, this.posZ));
		}
    }
	
	protected float getSpeed()
    {
        return 1.04f;
    }
    
    protected double getGravity()
    {
        return 0;
    }

}
