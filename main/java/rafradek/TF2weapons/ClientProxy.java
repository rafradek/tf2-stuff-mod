package rafradek.TF2weapons;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.input.Keyboard;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import rafradek.TF2weapons.characters.EntityTF2Character;
import rafradek.TF2weapons.characters.RenderTF2Character;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.message.TF2Message;
import rafradek.TF2weapons.message.TF2UseHandler;
import rafradek.TF2weapons.projectiles.EntityRocket;
import rafradek.TF2weapons.projectiles.RenderRocket;
import rafradek.TF2weapons.weapons.EntityBulletTracer;
import rafradek.TF2weapons.weapons.ItemUsable;
import rafradek.TF2weapons.weapons.MinigunLoopSound;
import rafradek.TF2weapons.weapons.MinigunSound;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;

public class ClientProxy extends CommonProxy
{
	public static TextureMap particleMap;
	public static KeyBinding reload= new KeyBinding("key.reload", Keyboard.KEY_R,"lol");
	public static ResourceLocation scopeTexture=new ResourceLocation("tf2weapons:textures/misc/scope.png");
	//public static Map<MinigunLoopSound, EntityLivingBase > spinSounds;
	public static BiMap<EntityLivingBase, MinigunSound > fireSounds;
	public static Map<EntityLivingBase, ISound> reloadSounds;
	public static ResourceLocation blackTexture=new ResourceLocation("tf2weapons:textures/misc/black.png");
	public static ResourceLocation chargeTexture=new ResourceLocation("tf2weapons:textures/misc/charge.png");
	
    @Override
	public void registerRenderInformation()
    {
    	reloadSounds=new HashMap<EntityLivingBase,ISound>();
		fireSounds=HashBiMap.create();
    	ClientRegistry.registerKeyBinding(ClientProxy.reload);
    	RenderingRegistry.registerEntityRenderingHandler(EntityTF2Character.class, new RenderTF2Character());
    	RenderingRegistry.registerEntityRenderingHandler(EntityRocket.class, new RenderRocket());
    	TF2weapons.network.registerMessage(TF2ActionHandler.TF2ActionHandlerReturn.class, TF2Message.ActionMessage.class, 1, Side.CLIENT);
    	TF2weapons.network.registerMessage(TF2UseHandler.class, TF2Message.UseMessage.class, 2, Side.CLIENT);
    	//RenderingRegistry.registerEntityRenderingHandler(EntityScout.class, new RenderTF2Character());	
    	//Minecraft.getMinecraft().renderEngine.loadTextureMap(new ResourceLocation("textures/tfatlas/particles.png"), particleMap=new TF2TextureMap("textures/particle"));
    }
    @Override
	public void registerTicks()
    {
    }
	public static void spawnBulletParticle(World world, double startX, double startY, double startZ, double endX, double endY, double endZ, int j,int crits){
		EntityFX entity=new EntityBulletTracer(world, startX, startY, startZ, endX, endY, endZ, j,crits);
		spawnParticle(world,entity);
	}
	public static void spawnParticle(World world, EntityFX entity){
		if(Minecraft.getMinecraft() != null && Minecraft.getMinecraft().renderViewEntity != null && Minecraft.getMinecraft().effectRenderer != null){
			int i = Minecraft.getMinecraft().gameSettings.particleSetting;
	
	        if (i == 1 && entity.worldObj.rand.nextInt(3) == 0)
	            i = 2;
	        if (i > 1){
	        	entity.setDead();
	            return;
	        }
	        Minecraft.getMinecraft().effectRenderer.addEffect(entity);
		}
	}
	@Override
	public void playReloadSound(EntityLivingBase player,ItemStack stack){
		ResourceLocation soundName=new ResourceLocation(ItemUsable.getData(stack).get("Reload Sound").getString());
		PositionedSoundRecord sound=new PositionedSoundRecord(soundName, 0.6f, 1, (float)player.posX, (float)player.posY, (float)player.posZ);
		Minecraft.getMinecraft().getSoundHandler().stopSound(ClientProxy.reloadSounds.get(player));
		ClientProxy.reloadSounds.put(player, sound);
		Minecraft.getMinecraft().getSoundHandler().playSound(sound);
	}
	public static void playMinigunSound(EntityLivingBase living,ResourceLocation playSound, boolean loop, int type,ItemStack stack){
		//System.out.println(sound.type);
		MinigunSound sound;
		if(loop)
			sound=new MinigunLoopSound(playSound, living, type<2,ItemUsable.getData(stack),type==1,type);
		else
			sound=new MinigunSound(playSound, living, type,ItemUsable.getData(stack));
		if(fireSounds.get(living)!=null){
			Minecraft.getMinecraft().getSoundHandler().stopSound(fireSounds.get(living));
			fireSounds.get(living).setDone();
		}
		
		Minecraft.getMinecraft().getSoundHandler().playSound(sound);
		fireSounds.put(living, sound);
	}
	public static void removeReloadSound(EntityLivingBase entity) {
		if(reloadSounds.get(entity)!=null)
		Minecraft.getMinecraft().getSoundHandler().stopSound(reloadSounds.remove(entity));
	}
}
