package rafradek.TF2weapons;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rafradek.TF2weapons.weapons.ItemBulletWeapon;
import rafradek.TF2weapons.weapons.ItemMeeleWeapon;
import rafradek.TF2weapons.weapons.ItemMinigun;
import rafradek.TF2weapons.weapons.ItemProjectileWeapon;
import rafradek.TF2weapons.weapons.ItemSniperRifle;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;
import net.minecraft.world.biome.BiomeGenBase.SpawnListEntry;
import net.minecraftforge.common.config.ConfigCategory;

public class MapList {
	
	public static Map<String, String> attributesMap;
	public static Map<Integer, String> integerToMob;
	public static Map<String, Item> weaponClasses;
	public static Map<String, Class> projectileClasses;
	public static Map<String, ConfigCategory> nameToCC;
	public static Map<String, TF2Attribute> nameToAttribute;
	public static Map<String, ItemUsable> specialWeapons;
	public static Map<String, IIcon> nameToIcon;
	public static Map<String, NBTTagCompound> buildInAttributes;
	//public static Map<MinigunLoopSound, EntityLivingBase > fireCritSounds;
	public static Map<List<SpawnListEntry>, SpawnListEntry> scoutSpawn;
	
	public static void initMaps(){
		attributesMap = new HashMap<String, String>();
		weaponClasses = new HashMap<String, Item>();
		projectileClasses = new HashMap<String, Class>();
		nameToCC = new HashMap<String, ConfigCategory>();
		nameToIcon = new HashMap<String, IIcon>();
		nameToAttribute = new HashMap<String, TF2Attribute>();
		buildInAttributes = new HashMap<String, NBTTagCompound>();
		specialWeapons = new HashMap<String, ItemUsable>();
		integerToMob = new HashMap<Integer,String>();
		scoutSpawn=new HashMap<List<SpawnListEntry>, SpawnListEntry>();
		attributesMap.put("DamageBonus", "Percentage");
		attributesMap.put("DamagePenalty", "Percentage");
		attributesMap.put("ClipSizeBonus", "Percentage");
		attributesMap.put("ClipSizePenalty", "Percentage");
		attributesMap.put("MinigunSpinBonus", "Inverted_Percentage");
		attributesMap.put("MinigunSpinPenalty", "Percentage");
		weaponClasses.put("SniperRifle", new ItemSniperRifle());
		weaponClasses.put("Bullet", new ItemBulletWeapon());
		weaponClasses.put("Meele", new ItemMeeleWeapon());
		weaponClasses.put("Minigun", new ItemMinigun());
		weaponClasses.put("Projectile", new ItemProjectileWeapon());
	}
}
