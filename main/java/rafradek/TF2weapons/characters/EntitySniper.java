package rafradek.TF2weapons.characters;

import rafradek.TF2weapons.weapons.ItemRangedWeapon;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIFleeSun;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIRestrictSun;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntitySniper extends EntityTF2Character implements IRangedWeaponAttackMob {

	public EntitySniper(World par1World) {
		super(par1World);
		this.ammoLeft=18;
		this.experienceValue=15;
		this.rotation=3;
		if(this.attack !=null){
			attack.setRange(50);
		}
		//this.setCurrentItemOrArmor(0, ItemUsable.getNewStack("Minigun"));
		
	}
	protected void addRandomArmor()
    {
        this.setCurrentItemOrArmor(0, ItemUsable.getNewStack("SniperRifle"));
    }
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(60.0D);
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(15.0D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(0.25D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.33D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(6.0D);
    }
	public void onLivingUpdate()
    {
		if(this.getHeldItem()!=null&&!this.getHeldItem().getTagCompound().getBoolean("Zoomed")){
			this.rotation=15;
		}
		else{
			this.rotation=3;
		}
		/*if(this.getAttackTarget()!=null&&this.getEntitySenses().canSee(this.getAttackTarget())&&){
			((ItemRangedWeapon)this.getHeldItem().getItem()).altUse(getHeldItem(), this, worldObj);
		}
		else if(this.getHeldItem().getTagCompound().getBoolean("Zoomed")){
			((ItemRangedWeapon)this.getHeldItem().getItem()).altUse(getHeldItem(), this, worldObj);
		}*/
        super.onLivingUpdate();

    }
	protected String getLivingSound()
    {
        return "mob.zombie.say";
    }
	

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.zombie.hurt";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.zombie.death";
    }
    /**
     * Plays step sound at given x, y, z for the entity
     */
    protected void func_145780_a(int par1, int par2, int par3, Block par4)
    {
        this.playSound("mob.zombie.step", 0.15F, 1.0F);
    }

    /**
     * Get this Entity's EnumCreatureAttribute
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
    	if(this.rand.nextFloat()<0.15f+p_70628_2_*0.075f){
    		this.entityDropItem(ItemUsable.getNewStack("SMG"), 0);
    	}
    	if(this.rand.nextFloat()<0.05f+p_70628_2_*0.025f){
    		this.entityDropItem(ItemUsable.getNewStack("SniperRifle"), 0);
    	}
    }
    /*@Override
	public float getAttributeModifier(String attribute) {
		if(attribute.equals("Fire Rate")){
			return super.getAttributeModifier(attribute)*4f;
		}
		return super.getAttributeModifier(attribute);
	}*/
}
