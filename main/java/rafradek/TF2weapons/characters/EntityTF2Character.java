package rafradek.TF2weapons.characters;

import java.util.List;

import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.command.IEntitySelector;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIFleeSun;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIRestrictSun;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.EntityLookHelper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.scoreboard.Team;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.Chunk;

public class EntityTF2Character extends EntityCreature implements IRangedWeaponAttackMob,IMob {
	public boolean jump;
	public EntityAIUseRangedWeapon attack;
	public EntityAINearestChecked findplayer =new EntityAINearestChecked(this, EntityLivingBase.class, true,false,new Selector());
	private EntityAIAttackOnCollide attackMeele = new EntityAIAttackOnCollide(this, 1.1F, false);
	public EntityLookHelper lookHelper;
	public int ammoLeft;
	public boolean unlimitedAmmo;
	private boolean natural;
	private boolean noAmmo;
	public double motionSensitivity;
	public float rotation;
	public EntityTF2Character(World p_i1738_1_) {
		super(p_i1738_1_);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(5, new EntityAIWander(this, 1.0D));
		this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(6, new EntityAILookIdle(this));
		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true));
		this.targetTasks.addTask(2, findplayer);
		//this.lookHelper=new 
		//this.motionSensitivity=4;
		this.rotation=17;
		this.motionSensitivity=0.03;
		if(p_i1738_1_ != null){
		//this.getHeldItem().stackTagCompound.setTag("Attributes", (NBTTagCompound) ((ItemUsable)this.getHeldItem().getItem()).buildInAttributes.copy());
			this.attack =new EntityAIUseRangedWeapon(this, 1.0F, 20.0F);
			this.tasks.addTask(4, attack);
		}
		
		for (int i = 0; i < this.equipmentDropChances.length; ++i)
        {
            this.equipmentDropChances[i] = 0.0F;
        }
		// TODO Auto-generated constructor stub
	}
	protected boolean isAIEnabled()
    {
        return true;
    }
	/*public EntityLookHelper getLookHelper()
    {
        return this.lookHelper;
    }*/
	@Override
	public void attackEntityWithRangedAttack(EntityLivingBase p_82196_1_,
			float p_82196_2_) {
		// TODO Auto-generated method stub
	}
	protected void entityInit()
	{
		super.entityInit();
		this.getDataWatcher().addObject(12, Byte.valueOf((byte)this.rand.nextInt(2)));
		this.getDataWatcher().addObject(13, Byte.valueOf((byte)0));
	}
	public int getEntTeam(){
		return this.getDataWatcher().getWatchableObjectByte(12);
	}
	public int getDiff(){
		return this.getDataWatcher().getWatchableObjectByte(13);
	}
	public void setEntTeam(int team){
		this.getDataWatcher().updateObject(12,(byte) team);
	}
	public void setDiff(int diff){
		this.getDataWatcher().updateObject(13,(byte) diff);
	}
	@Override
	public int getAmmo() {
		// TODO Auto-generated method stub
		return ammoLeft;
	}
	@Override
	public void useAmmo(int amount) {
		if(!this.unlimitedAmmo)
			this.ammoLeft -= amount;
		
	}
	@Override
	public float getAttributeModifier(String attribute) {
		if(attribute.equals("Knockback"))
			return this.getDiff()==1 ? 0.55f : (this.getDiff()==3 ? 0.85f : 0.7f);
		else if(attribute.equals("Fire Rate")||attribute.equals("Minigun Spinup"))
			return this.getDiff()==1 ? 1.9f : (this.getDiff()==3 ? 1.2f : 1.55f);
		else if(attribute.equals("Spread")){
			float base=this.getDiff()==1 ? 1.9f : (this.getDiff()==3 ? 1.2f : 1.55f);
			/*if(this.getAttackTarget()!=null){
				double mX=this.getAttackTarget().motionX;
				double mY=this.getAttackTarget().motionY;
				double mZ=this.getAttackTarget().motionZ;
				double totalMotion=Math.sqrt(mX*mX+mY*mY+mZ*mZ);
				base+=totalMotion*motionSensitivity;
			}*/
			return base;
		}
		else if(attribute.equals("Damage")){
			return 0.58f;
		}
		return 1;
	}
	@Override
	public void onLivingUpdate()
    {
		super.onLivingUpdate();
		this.updateArmSwingProgress();
		if(this.getAttackTarget() instanceof EntityTF2Character&&((EntityTF2Character)this.getAttackTarget()).getEntTeam()==getEntTeam()){
			this.setAttackTarget(null);
		}
		if(this.jump&&this.onGround){
			this.jump();
		}
		if(!TF2ActionHandler.playerAction.get().containsKey(this)||(TF2ActionHandler.playerAction.get().get(this)&4)==0){
    		TF2ActionHandler.playerAction.get().put(this, TF2ActionHandler.playerAction.get().containsKey(this)?TF2ActionHandler.playerAction.get().get(this)+4:4);
    	}
		if(!this.noAmmo&&this.getAttackTarget()!=null/*TF2ActionHandler.playerAction.get().get(this)!=null&&(TF2ActionHandler.playerAction.get().get(this)&3)>0*/){
			this.rotationYaw=this.rotationYawHead;
		}
		if(!this.worldObj.isRemote){
			this.setDiff(this.worldObj.difficultySetting.getDifficultyId());
		}
		if(this.getHeldItem()!=null&&this.getHeldItem().getItem() instanceof ItemUsable){
			this.getHeldItem().getItem().onUpdate(getHeldItem(), worldObj, this, 0, true);
		}
		if(this.ammoLeft<=0&&!this.noAmmo){
			this.setCombatTask();
			this.noAmmo=true;
		}
    }
	@SuppressWarnings("unchecked")
	public IEntityLivingData onSpawnWithEgg(IEntityLivingData p_110161_1_)
    {
		super.onSpawnWithEgg(p_110161_1_);
		if(p_110161_1_==null){
			p_110161_1_=new TF2CharacterAdditionalData();
			((TF2CharacterAdditionalData)p_110161_1_).natural=true;
			List<EntityTF2Character> list = this.worldObj.selectEntitiesWithinAABB(EntityTF2Character.class, this.boundingBox.expand(40, 4.0D, 40), null);
			if(list.isEmpty()){
				((TF2CharacterAdditionalData)p_110161_1_).team=this.rand.nextInt(2);
			}
			else{
				((TF2CharacterAdditionalData)p_110161_1_).team=list.get(0).getEntTeam();
			}
		}
		if(p_110161_1_ instanceof TF2CharacterAdditionalData){
			this.natural=(((TF2CharacterAdditionalData)p_110161_1_).natural);
			this.setEntTeam(((TF2CharacterAdditionalData)p_110161_1_).team);
		}
		this.addRandomArmor();
		return p_110161_1_;
    }
	public void setCombatTask()
    {
        this.tasks.removeTask(this.attack);
        this.tasks.removeTask(this.attackMeele);
        TF2ActionHandler.playerAction.get().put(this, 0);

        if (this.ammoLeft>0)
        {
            this.tasks.addTask(4, this.attack);
        }
        else
        {
            this.tasks.addTask(4, this.attackMeele);
        }
    }
	public class Selector implements IEntitySelector{
		@Override
		public boolean isEntityApplicable(Entity p_82704_1_) {
			// TODO Auto-generated method stub
			return p_82704_1_ instanceof EntityPlayer||(p_82704_1_ instanceof EntityTF2Character&&((EntityTF2Character)p_82704_1_).getEntTeam()!=getEntTeam()&&(!((EntityTF2Character)p_82704_1_).natural||!natural));
		}
	}
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readEntityFromNBT(par1NBTTagCompound);

        this.ammoLeft=par1NBTTagCompound.getShort("Ammo");
        this.unlimitedAmmo=par1NBTTagCompound.getBoolean("UnlimitedAmmo");
        this.setEntTeam(par1NBTTagCompound.getByte("Team"));
        this.natural=par1NBTTagCompound.getBoolean("Natural");
    }
	
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeEntityToNBT(par1NBTTagCompound);
        
        par1NBTTagCompound.setShort("Ammo", (short) this.ammoLeft);
        par1NBTTagCompound.setBoolean("UnlimitedAmmo", this.unlimitedAmmo);
        par1NBTTagCompound.setByte("Team", (byte) this.getEntTeam());
        par1NBTTagCompound.setBoolean("Natural", this.natural);
    }
    public boolean getCanSpawnHere()
    {
    	boolean validLight=this.isValidLightLevel();
    	boolean spawnDay=this.rand.nextInt(64)==0;
    	//System.out.println("ust: "+spawnDay+" "+validLight);
    	if(!spawnDay&&!validLight) return false;
    	int time=(int) Math.min((this.worldObj.getWorldInfo().getWorldTime()/24000),16);
    	/*if(!this.isValidLightLevel()){
    		time/=8;
    		System.out.println("incorrect light");
    	}*/
    	//System.out.println("Timelevel "+time);
        Chunk chunk = this.worldObj.getChunkFromBlockCoords(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posZ));
        if (this.rand.nextInt(15) < time && chunk.getRandomWithSeed(987234911L).nextInt(16*(validLight?1:8)-1) < time && this.posY < 40.0D)
        {
        	//System.out.println(super.getCanSpawnHere());
            return this.worldObj.difficultySetting != EnumDifficulty.PEACEFUL&&super.getCanSpawnHere();
        }
        return false;
    }
    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        super.onUpdate();

        if (!this.worldObj.isRemote && this.worldObj.difficultySetting == EnumDifficulty.PEACEFUL)
        {
            this.setDead();
        }
    }

    protected String getSwimSound()
    {
        return "game.hostile.swim";
    }

    protected String getSplashSound()
    {
        return "game.hostile.swim.splash";
    }

    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource p_70097_1_, float p_70097_2_)
    {
        if (this.isEntityInvulnerable())
        {
            return false;
        }
        else if (super.attackEntityFrom(p_70097_1_, p_70097_2_))
        {
            Entity entity = p_70097_1_.getEntity();

            if (this.riddenByEntity != entity && this.ridingEntity != entity)
            {
                if (entity != this)
                {
                    this.entityToAttack = entity;
                }

                return true;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "game.hostile.hurt";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "game.hostile.die";
    }

    protected String func_146067_o(int p_146067_1_)
    {
        return p_146067_1_ > 4 ? "game.hostile.hurt.fall.big" : "game.hostile.hurt.fall.small";
    }

    public boolean attackEntityAsMob(Entity p_70652_1_)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.attackDamage).getAttributeValue();
        int i = 0;

        if (p_70652_1_ instanceof EntityLivingBase)
        {
            f += EnchantmentHelper.getEnchantmentModifierLiving(this, (EntityLivingBase)p_70652_1_);
            i += EnchantmentHelper.getKnockbackModifier(this, (EntityLivingBase)p_70652_1_);
        }

        boolean flag = p_70652_1_.attackEntityFrom(DamageSource.causeMobDamage(this), f);

        if (flag)
        {
            if (i > 0)
            {
                p_70652_1_.addVelocity((double)(-MathHelper.sin(this.rotationYaw * (float)Math.PI / 180.0F) * (float)i * 0.5F), 0.1D, (double)(MathHelper.cos(this.rotationYaw * (float)Math.PI / 180.0F) * (float)i * 0.5F));
                this.motionX *= 0.6D;
                this.motionZ *= 0.6D;
            }

            int j = EnchantmentHelper.getFireAspectModifier(this);

            if (j > 0)
            {
                p_70652_1_.setFire(j * 4);
            }

            if (p_70652_1_ instanceof EntityLivingBase)
            {
                EnchantmentHelper.func_151384_a((EntityLivingBase)p_70652_1_, this);
            }

            EnchantmentHelper.func_151385_b(this, p_70652_1_);
        }

        return flag;
    }

    protected boolean isValidLightLevel()
    {
        int i = MathHelper.floor_double(this.posX);
        int j = MathHelper.floor_double(this.boundingBox.minY);
        int k = MathHelper.floor_double(this.posZ);

        if (this.worldObj.getSavedLightValue(EnumSkyBlock.Sky, i, j, k) > this.rand.nextInt(32))
        {
            return false;
        }
        else
        {
            int l = this.worldObj.getBlockLightValue(i, j, k);

            if (this.worldObj.isThundering())
            {
                int i1 = this.worldObj.skylightSubtracted;
                this.worldObj.skylightSubtracted = 10;
                l = this.worldObj.getBlockLightValue(i, j, k);
                this.worldObj.skylightSubtracted = i1;
            }

            return l <= 4+this.rand.nextInt(4);
        }
    }

    /**
     * Checks if the entity's current position is a valid location to spawn this entity.
     */

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.attackDamage);
    }

    protected boolean func_146066_aG()
    {
        return true;
    }
}
