package rafradek.TF2weapons.characters;

import net.minecraft.entity.IRangedAttackMob;

public interface IRangedWeaponAttackMob extends IRangedAttackMob {
 
	int getAmmo();
	void useAmmo(int amount);

	float getAttributeModifier(String effect);
}
