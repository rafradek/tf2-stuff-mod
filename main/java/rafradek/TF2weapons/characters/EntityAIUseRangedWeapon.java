package rafradek.TF2weapons.characters;

import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.message.TF2Message;
import rafradek.TF2weapons.weapons.ItemRangedWeapon;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;

public class EntityAIUseRangedWeapon extends EntityAIBase {

	/** The entity the AI instance has been applied to */
    private final EntityTF2Character entityHost;

    /**
     * The entity (as a RangedAttackMob) the AI instance has been applied to.
     */
    private final IRangedWeaponAttackMob rangedAttackEntityHost;
    private EntityLivingBase attackTarget;
    public boolean reloading;

    /**
     * A decrementing tick that spawns a ranged attack once this value reaches 0. It is then set back to the
     * maxRangedAttackTime.
     */
    private int rangedAttackTime;
    private float entityMoveSpeed;
    private int field_75318_f;

    /**
     * The maximum time the AI has to wait before peforming another ranged attack.
     */
    private float attackRange;
    private float attackRangeSquared;
    
    private boolean pressed;
    private boolean altpreesed;
    private boolean dodging;
	private boolean dodge;
	public boolean jump;
	public float dodgeSpeed=1f;
	public int jumprange;

	private boolean firstTick;


    public EntityAIUseRangedWeapon(IRangedWeaponAttackMob par1IRangedAttackMob, float par2, float par5)
    {
        this.rangedAttackTime = -1;
        this.field_75318_f = 0;

        if (!(par1IRangedAttackMob instanceof EntityTF2Character))
        {
            throw new IllegalArgumentException("UseRangedWeapon requires Mob implements RangedWeaponAttackMob");
        }
        else
        {
            this.rangedAttackEntityHost = par1IRangedAttackMob;
            this.entityHost = (EntityTF2Character)par1IRangedAttackMob;
            this.entityMoveSpeed = par2;
            this.attackRange = par5;
            this.attackRangeSquared = par5 * par5;
            
            this.setMutexBits(3);
        }
    }
    public void setRange(float range){
    	this.attackRange = range;
        this.attackRangeSquared = range * range;
    }
    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        EntityLivingBase EntityLivingBase = this.entityHost.getAttackTarget();

        if (EntityLivingBase == null)
        {
            return false;
        }
        else
        {
            this.attackTarget = EntityLivingBase;
            return true;
        }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        return this.shouldExecute() || !this.entityHost.getNavigator().noPath();
    }

    /**
     * Resets the task
     */
    public void resetTask()
    {
    	if(TF2ActionHandler.playerAction.get().containsKey(this.entityHost)&&TF2ActionHandler.playerAction.get().get(entityHost)!=0){
	    	pressed=false;
	    	TF2ActionHandler.playerAction.get().put(entityHost, 0);
	    	TF2weapons.network.sendToDimension(new TF2Message.ActionMessage(0, entityHost), entityHost.dimension);
    	}
    	if(this.jump){
			this.entityHost.jump=false;
    	}
    	this.entityHost.getNavigator().clearPathEntity();
        this.attackTarget = null;
        this.field_75318_f = 0;
        this.rangedAttackTime = -1;
    }

    /**
     * Updates the task
     */
    public boolean lookingAt(double max){
    	if(this.attackTarget==null) return false;
    	double d0 = this.attackTarget.posX - this.entityHost.posX;
        double d1 = (this.attackTarget.posY + this.attackTarget.getEyeHeight()) - (this.entityHost.posY + (double)this.entityHost.getEyeHeight());
        double d2 = this.attackTarget.posZ - this.entityHost.posZ;
        double d3 = (double)MathHelper.sqrt_double(d0 * d0 + d2 * d2);
        float f = (float)(Math.atan2(d2, d0) * 180.0D / Math.PI) - 90.0F;
        float f1 = (float)(-(Math.atan2(d1, d3) * 180.0D / Math.PI));
        float compareyaw=Math.abs( 180 - Math.abs(Math.abs(f - MathHelper.wrapAngleTo180_float(this.entityHost.rotationYawHead)) - 180)); 
        float comparepitch=Math.abs( 180 - Math.abs(Math.abs(f1 - this.entityHost.rotationPitch) - 180)); 
        //System.out.println("Angl: "+compareyaw+" "+comparepitch);
        return compareyaw<max&&comparepitch<max;
    }
    public double lookingAtMax(){
    	if(this.attackTarget==null) return 0;
    	double d0 = this.attackTarget.posX - this.entityHost.posX;
        double d1 = (this.attackTarget.posY + this.attackTarget.getEyeHeight()) - (this.entityHost.posY + (double)this.entityHost.getEyeHeight());
        double d2 = this.attackTarget.posZ - this.entityHost.posZ;
        double d3 = (double)MathHelper.sqrt_double(d0 * d0 + d2 * d2);
        float f = (float)(Math.atan2(d2, d0) * 180.0D / Math.PI) - 90.0F;
        float f1 = (float)(-(Math.atan2(d1, d3) * 180.0D / Math.PI));
        float compareyaw=Math.abs( 180 - Math.abs(Math.abs(f - MathHelper.wrapAngleTo180_float(this.entityHost.rotationYawHead)) - 180)); 
        float comparepitch=Math.abs( 180 - Math.abs(Math.abs(f1 - this.entityHost.rotationPitch) - 180)); 
        //System.out.println("Angled: "+compareyaw+" "+comparepitch);
        return Math.max(compareyaw, comparepitch);
    }
    public void updateTask()
    {
    	if((this.attackTarget != null && this.attackTarget.deathTime>0) || this.entityHost.deathTime>0){
    		this.resetTask();
    		return;
    	}
    	if(this.attackTarget == null){
    		return;
    	}
        double d0 = this.entityHost.getDistanceSq(this.attackTarget.posX, this.attackTarget.boundingBox.minY, this.attackTarget.posZ);
        boolean flag = this.entityHost.getEntitySenses().canSee(this.attackTarget)&&lookingAt(((ItemRangedWeapon)this.entityHost.getHeldItem().getItem()).getWeaponSpreadBase(this.entityHost.getHeldItem(), this.entityHost)*100+1);
        if(!this.reloading&&this.entityHost.getHeldItem().getItemDamage()==this.entityHost.getHeldItem().getMaxDamage()&&((ItemRangedWeapon)this.entityHost.getHeldItem().getItem()).hasClip(this.entityHost.getHeldItem())){
        	this.reloading=true;
        }
        else if(this.reloading&&this.entityHost.getHeldItem().getItemDamage()==0){
        	this.reloading=false;
        }
        this.entityHost.setJumping(true);
        if (flag){
            ++this.field_75318_f;
            if(d0 <= (double)this.attackRangeSquared/4){
            	this.field_75318_f=20;
            }
        }
        else
            this.field_75318_f = 0;

        if (d0 <= (double)this.attackRangeSquared && this.field_75318_f >= 20){
        	if(!this.dodging){
        		this.entityHost.getNavigator().clearPathEntity();
        		this.dodging=true;
        	}
        }
        else {
        	this.dodging=false;
            this.entityHost.getNavigator().tryMoveToEntityLiving(this.attackTarget, this.entityMoveSpeed);
        }

        this.entityHost.getLookHelper().setLookPosition(this.attackTarget.lastTickPosX,this.attackTarget.lastTickPosY+this.attackTarget.getEyeHeight(),this.attackTarget.lastTickPosZ, this.entityHost.rotation, 90.0F);
        //this.entityHost.getLookHelper().setLookPositionWithEntity(this.attackTarget, 1.0F, 90.0F);
        if(!reloading&&flag && d0 <= (double)this.attackRangeSquared && this.rangedAttackEntityHost.getAmmo() > 0){
        	
        	if(!pressed){
        		pressed=true;
        		TF2ActionHandler.playerAction.get().put(entityHost, 1);
        		TF2weapons.network.sendToDimension(new TF2Message.ActionMessage(1, entityHost), entityHost.dimension);
        		//System.out.println("co�do");
        	}
        	
    	}
        else{
        	if(pressed){
        		if(this.jump){
        			this.entityHost.jump=false;
            	}
        		int valuedef=this.entityHost instanceof EntityHeavy?2:0;
		    	TF2ActionHandler.playerAction.get().put(entityHost,valuedef);
		    	TF2weapons.network.sendToDimension(new TF2Message.ActionMessage(valuedef, entityHost), entityHost.dimension);
		    	//System.out.println("co�z");
        	}
        	pressed=false;
        }
        //if(){
        	if(this.jump&&d0<this.jumprange){
        		this.entityHost.jump=true;
        	}
        	else if(this.jump){
        		this.entityHost.jump=false;
        	}
        	if(this.dodge&&this.entityHost.getNavigator().noPath()){
	        	Vec3 vec3 = RandomPositionGenerator.findRandomTarget((EntityCreature) this.entityHost, 3, 2);
	
	            if (vec3 != null)
	            {
	                this.entityHost.getNavigator().tryMoveToXYZ(vec3.xCoord, vec3.yCoord, vec3.zCoord, this.entityMoveSpeed*this.dodgeSpeed);
	            }
        	}
       // }
    }

	public void setDodge(boolean i) {
		this.dodge=i;
	}

}
