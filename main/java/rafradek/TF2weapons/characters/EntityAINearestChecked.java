package rafradek.TF2weapons.characters;

import java.util.Collections;
import java.util.List;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget.Sorter;
import net.minecraft.entity.ai.EntityAITarget;



public class EntityAINearestChecked extends EntityAITarget {
	public int targetChoosen=0;
	private Class targetClass;
	private int targetChance;
	private Sorter theNearestAttackableTargetSorter;
	private IEntitySelector targetEntitySelector;
	private EntityLivingBase targetEntity;
	public EntityAINearestChecked(EntityCreature p_i1665_1_, Class p_i1665_2_, boolean p_i1665_4_, boolean p_i1665_5_, final IEntitySelector p_i1665_6_)
    {
        super(p_i1665_1_, p_i1665_4_, p_i1665_5_);
        this.targetClass = p_i1665_2_;
        this.targetChance = 0;
        this.theNearestAttackableTargetSorter = new EntityAINearestAttackableTarget.Sorter(p_i1665_1_);
        this.setMutexBits(1);
        this.targetEntitySelector = new IEntitySelector()
        {
            /**
             * Return whether the specified entity is applicable to this filter.
             */
            public boolean isEntityApplicable(Entity p_82704_1_)
            {
                return !(p_82704_1_ instanceof EntityLivingBase) ? false : (p_i1665_6_ != null && !p_i1665_6_.isEntityApplicable(p_82704_1_) ? false : EntityAINearestChecked.this.isSuitableTarget((EntityLivingBase)p_82704_1_, false));
            }
        };
    }
	public boolean shouldExecute()
    {
		double d0 = this.getTargetDistance()/2;
		if(this.taskOwner.getAttackTarget()==null||this.taskOwner.getAttackTarget().getDistanceSqToEntity(taskOwner)>d0*d0){
			this.targetChoosen++;
		}
        if((this.taskOwner.getAttackTarget()==null&&this.targetChoosen>1)||this.targetChoosen>15){
        	//System.out.println("execute");
        	this.targetChoosen=0;
        	if (this.targetChance > 0 && this.taskOwner.getRNG().nextInt(this.targetChance) != 0)
            {
                return false;
            }
            else
            {
                double d1 = this.getTargetDistance();
                List list = this.taskOwner.worldObj.selectEntitiesWithinAABB(this.targetClass, this.taskOwner.boundingBox.expand(d1, d0, d1), this.targetEntitySelector);
                Collections.sort(list, this.theNearestAttackableTargetSorter);

                if (list.isEmpty())
                {
                    return false;
                }
                else
                {
                    this.targetEntity = (EntityLivingBase)list.get(0);
                    return true;
                }
            }
        }
        return false;
    }
	public void startExecuting()
    {
        this.taskOwner.setAttackTarget(this.targetEntity);
        super.startExecuting();
    }
}
