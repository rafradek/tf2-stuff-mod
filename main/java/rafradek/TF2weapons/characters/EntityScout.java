package rafradek.TF2weapons.characters;

import rafradek.TF2weapons.weapons.ItemRangedWeapon;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIFleeSun;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIRestrictSun;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityScout extends EntityTF2Character implements IRangedWeaponAttackMob {
	public boolean doubleJumped;
	private int jumpDelay;
	public EntityScout(World par1World) {
		super(par1World);
		if(this.attack !=null){
			this.attack.setDodge(true);
			this.attack.jump=true;
			this.attack.jumprange=40;
			this.attack.dodgeSpeed=1.25f;
		}
		this.ammoLeft=24;
		this.experienceValue=15;
			
		//this.setCurrentItemOrArmor(0, ItemUsable.getNewStack("Minigun"));
		
	}
	protected void addRandomArmor()
    {
        this.setCurrentItemOrArmor(0, ItemUsable.getNewStack("Scattergun"));
    }
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(40.0D);
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(15.0D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(0.25D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.4D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(3.0D);
    }
	
	public void onLivingUpdate()
	{
        super.onLivingUpdate();
        if(jumpDelay>0&&--jumpDelay ==0){
        	this.jump();
        }
        if(this.onGround){
        	this.doubleJumped=false;
        }
        
    }
	protected void jump()
    {
		super.jump();
		/*double speed=Math.sqrt(motionX*motionX+motionZ*motionZ);
		if(speed!=0){
			
			double speedMultiply=this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getAttributeValue()/speed;
			this.motionX*=speedMultiply;
			this.motionZ*=speedMultiply;
		}*/
		this.motionX = (double)(-MathHelper.sin(this.rotationYaw / 180.0F * (float)Math.PI));
        this.motionZ = (double)(MathHelper.cos(this.rotationYaw / 180.0F * (float)Math.PI));
        float f2 = (float) (MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ)*this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getAttributeValue());
        this.motionX *= f2;
        this.motionZ *= f2;
        this.fallDistance = -3.0F;
		if(!this.doubleJumped&&this.jump){
			this.doubleJumped=true;
			this.jumpDelay=8;
		}
    }
	protected String getLivingSound()
    {
        return "mob.zombie.say";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.zombie.hurt";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.zombie.death";
    }
    
    /*public int getAttackStrength(Entity par1Entity)
    {
        ItemStack itemstack = this.getHeldItem();
        float f = (float)(this.getMaxHealth() - this.getHealth()) / (float)this.getMaxHealth();
        int i = 4 + MathHelper.floor_float(f * 4.0F);

        if (itemstack != null)
        {
            i += itemstack.getDamageVsEntity(this);
        }

        return i;
    }*/

    /**
     * Plays step sound at given x, y, z for the entity
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
    	if(this.rand.nextFloat()<0.12f+p_70628_2_*0.075f){
    		this.entityDropItem(ItemUsable.getNewStack("Scattergun"), 0);
    	}
    	if(this.rand.nextFloat()<0.15f+p_70628_2_*0.075f){
    		this.entityDropItem(ItemUsable.getNewStack("Pistol"), 0);
    	}
    }
    protected void func_145780_a(int par1, int par2, int par3, Block par4)
    {
        this.playSound("mob.zombie.step", 0.15F, 1.0F);
    }

    /**
     * Get this Entity's EnumCreatureAttribute
     */
}
