package rafradek.TF2weapons.characters;

import org.lwjgl.opengl.GL11;

import rafradek.TF2weapons.MapList;
import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelSkeleton;
import net.minecraft.client.model.ModelZombie;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderTF2Character extends RenderBiped {

	private static final ResourceLocation HEAVY_RED = new ResourceLocation("tf2weapons:textures/entity/tf2/red/Heavy.png");
	private static final ResourceLocation HEAVY_BLU = new ResourceLocation("tf2weapons:textures/entity/tf2/blu/Heavy.png");
	private static final ResourceLocation SCOUT_RED = new ResourceLocation("tf2weapons:textures/entity/tf2/red/Scout.png");
	private static final ResourceLocation SCOUT_BLU = new ResourceLocation("tf2weapons:textures/entity/tf2/blu/Scout.png");
	private static final ResourceLocation SNIPER_RED = new ResourceLocation("tf2weapons:textures/entity/tf2/red/Sniper.png");
	private static final ResourceLocation SNIPER_BLU = new ResourceLocation("tf2weapons:textures/entity/tf2/blu/Sniper.png");
	public ModelBiped modelHeavy=new ModelHeavy();
	public ModelBiped modelMain;
	public RenderTF2Character() {
		super(new ModelBiped(), 0.5F, 1.0F);
		this.modelMain=this.modelBipedMain;
	}
	
	protected ResourceLocation getEntityTexture(EntityLiving par1EntityLiving)
    {
		if(par1EntityLiving instanceof EntityTF2Character && ((EntityTF2Character)par1EntityLiving).getEntTeam()==0){
			if(par1EntityLiving instanceof EntityHeavy)
				return HEAVY_RED;
			if(par1EntityLiving instanceof EntityScout)
				return SCOUT_RED;
			if(par1EntityLiving instanceof EntitySniper)
				return SNIPER_RED;
		}
		else{
			if(par1EntityLiving instanceof EntityHeavy)
				return HEAVY_BLU;
			if(par1EntityLiving instanceof EntityScout)
				return SCOUT_BLU;
			if(par1EntityLiving instanceof EntitySniper)
				return SNIPER_BLU;
		}
		return HEAVY_BLU;
    }
	public void doRender(EntityLiving p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_)
    {
		this.setModel(p_76986_1_);
		boolean sniperZoomed=p_76986_1_.getHeldItem()!=null&&p_76986_1_.getHeldItem().stackTagCompound.getBoolean("Zoomed");
		this.field_82423_g.aimedBow = this.field_82425_h.aimedBow = this.modelBipedMain.aimedBow =(TF2ActionHandler.playerAction.get().containsKey(p_76986_1_) && (TF2ActionHandler.playerAction.get().get(p_76986_1_)&3)>0)||sniperZoomed;
		super.doRender(p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
    }
	private void setModel(EntityLivingBase p_76986_1_) {
		if(p_76986_1_ instanceof EntityHeavy){
			this.mainModel=this.modelHeavy;
			this.modelBipedMain=this.modelHeavy;
		}
		else{
			this.mainModel=this.modelMain;
			this.modelBipedMain=this.modelMain;
		}
	}
	protected int shouldRenderPass(EntityLivingBase p_77032_1_, int p_77032_2_, float p_77032_3_)
    {
		this.setModel(p_77032_1_);
        return super.shouldRenderPass(p_77032_1_, p_77032_2_, p_77032_3_);
    }
	protected void renderEquippedItems(EntityLivingBase p_77029_1_, float p_77029_2_)
    {
		this.setModel(p_77029_1_);
		//ItemStack oldStack=p_77029_1_.getHeldItem();
		//p_77029_1_.setCurrentItemOrArmor(0, new ItemStack(/*MapList.weaponClasses.get("Bullet")*/TF2weapons.itemPlacer));
		super.renderEquippedItems(p_77029_1_, p_77029_2_);
		//p_77029_1_.setCurrentItemOrArmor(0, oldStack);
    }
}
