package rafradek.TF2weapons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import rafradek.TF2weapons.characters.EntityTF2Character;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.message.TF2Message;
import rafradek.TF2weapons.weapons.ItemMinigun;
import rafradek.TF2weapons.weapons.ItemSniperRifle;
import rafradek.TF2weapons.weapons.ItemUsable;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.WorldEvent;

public class TF2EventBusListener {
	public int tickleft;
	public static IIcon pelletIcon;
    boolean alreadypressed;
	private boolean alreadypressedalt;
	private boolean alreadypressedreload;
	private HashMap eligibleChunksForSpawning = new HashMap();
	/*@SubscribeEvent
	public void spawn(WorldEvent.PotentialSpawns event){
		int time=(int) (event.world.getWorldInfo().getWorldTotalTime()/24000);
		if(MapList.scoutSpawn.containsKey(event.list)){
			MapList.scoutSpawn.get(event.list).itemWeight=time;
		}
		else{
			System.out.println("add");
			SpawnListEntry entry=new SpawnListEntry(EntityScout.class, time, 1, 3);
			event.list.add(entry);
			MapList.scoutSpawn.put(event.list,entry);
		}
	}*/
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void registerIcons(TextureStitchEvent.Pre event){
		if(event.map.getTextureType()==1){
			pelletIcon=event.map.registerIcon("tf2weapons:pellet3");
		}
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void clientTickEnd(TickEvent.ClientTickEvent event){
		Minecraft minecraft=Minecraft.getMinecraft();
		if(event.phase==TickEvent.Phase.END){
			if (minecraft.currentScreen == null && minecraft.thePlayer.getCurrentEquippedItem() != null)
	        {
	            if ( minecraft.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemUsable)
	            {
	            	
	            	boolean changed=false;
	                ItemStack item = minecraft.thePlayer.getCurrentEquippedItem();

	                if (minecraft.gameSettings.keyBindAttack.getIsKeyPressed()&&!this.alreadypressed)
	                {
	                	changed=true;
	                    this.alreadypressed=true;
	                }if(!minecraft.gameSettings.keyBindAttack.getIsKeyPressed()&&this.alreadypressed)
	                {
	                	changed=true;
	                	this.alreadypressed=false;
	                }
	                if (minecraft.gameSettings.keyBindUseItem.getIsKeyPressed()&&!this.alreadypressedalt)
	                {
	                	changed=true;
	                    this.alreadypressedalt=true;
	                }
	                if(!minecraft.gameSettings.keyBindUseItem.getIsKeyPressed()&&this.alreadypressedalt)
	                {
	                	changed=true;
	                	this.alreadypressedalt=false;
	                }
	                if (ClientProxy.reload.getIsKeyPressed()&&!this.alreadypressedreload)
	                {
	                	changed=true;
	                    this.alreadypressedreload=true;
	                }
	                if(!ClientProxy.reload.getIsKeyPressed()&&this.alreadypressedreload)
	                {
	                	changed=true;
	                	this.alreadypressedreload=false;
	                }
	                if(changed){
	                	int plus=TF2ActionHandler.playerAction.get().containsKey(minecraft.thePlayer)?TF2ActionHandler.playerAction.get().get(minecraft.thePlayer)&8:0;
	                	TF2ActionHandler.playerAction.get().put(minecraft.thePlayer, this.getActionType()+plus);
	                	TF2weapons.network.sendToServer(new TF2Message.ActionMessage(this.getActionType()));
	                }
	            }
	        }
	        ItemUsable.tick(true);
		}
	}
	@SideOnly(Side.CLIENT)
	public int getActionType(){
		Minecraft minecraft=Minecraft.getMinecraft();
		int value=0;
		if(minecraft.gameSettings.keyBindAttack.getIsKeyPressed()){
			value++;
		}
		if(minecraft.gameSettings.keyBindUseItem.getIsKeyPressed()){
			value+=2;
		}
		if(ClientProxy.reload.getIsKeyPressed()){
			value+=4;
		}
		return value;
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void getFov(FOVUpdateEvent event){
		if(event.entity.getHeldItem()!=null&&event.entity.getHeldItem().getItem() instanceof ItemUsable){
			if(event.entity.getHeldItem().stackTagCompound.getBoolean("Zoomed")){
				event.newfov*=0.55f;
			}
			else if(event.entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getModifier(ItemMinigun.slowdownUUID)!=null){
				event.newfov*=1.4f;
			}
		}
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void renderOverlay(RenderGameOverlayEvent.Pre event){
		EntityPlayer player=Minecraft.getMinecraft().thePlayer;
		if(event.type==ElementType.HELMET&&player!=null&&player.getHeldItem()!=null&&player.getHeldItem().getItem() instanceof ItemUsable && player.getHeldItem().stackTagCompound.getBoolean("Zoomed")){
			GL11.glDisable(GL11.GL_DEPTH_TEST);
	        GL11.glDepthMask(false);
	        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
	        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	        GL11.glDisable(GL11.GL_ALPHA_TEST);
	        Minecraft.getMinecraft().getTextureManager().bindTexture(ClientProxy.scopeTexture);
	        double widthDrawStart=(double)(event.resolution.getScaledWidth()-event.resolution.getScaledHeight())/2;
	        double widthDrawEnd=widthDrawStart+event.resolution.getScaledHeight();
	        Tessellator tessellator = Tessellator.instance;
	        tessellator.startDrawingQuads();
	        tessellator.addVertexWithUV(widthDrawStart, (double)event.resolution.getScaledHeight(), -90.0D, 0.0D, 1.0D);
	        tessellator.addVertexWithUV(widthDrawEnd, (double)event.resolution.getScaledHeight(), -90.0D, 1.0D, 1.0D);
	        tessellator.addVertexWithUV(widthDrawEnd, 0.0D, -90.0D, 1.0D, 0.0D);
	        tessellator.addVertexWithUV(widthDrawStart, 0.0D, -90.0D, 0.0D, 0.0D);
	        tessellator.draw();
	        Minecraft.getMinecraft().getTextureManager().bindTexture(ClientProxy.blackTexture);
	        tessellator.startDrawingQuads();
	        tessellator.addVertexWithUV(0, (double)event.resolution.getScaledHeight(), -90.0D,0d,1d);
	        tessellator.addVertexWithUV(widthDrawStart, (double)event.resolution.getScaledHeight(), -90.0D,1d,1d);
	        tessellator.addVertexWithUV(widthDrawStart, 0.0D, -90.0D,1d,0d);
	        tessellator.addVertexWithUV(0, 0.0D, -90.0D,0d,0d);
	        tessellator.draw();
	        tessellator.startDrawingQuads();
	        tessellator.addVertexWithUV(widthDrawEnd, (double)event.resolution.getScaledHeight(), -90.0D,0d,1d);
	        tessellator.addVertexWithUV(event.resolution.getScaledWidth(), (double)event.resolution.getScaledHeight(), -90.0D,1d,1d);
	        tessellator.addVertexWithUV(event.resolution.getScaledWidth(), 0.0D, -90.0D,1d,0d);
	        tessellator.addVertexWithUV(widthDrawEnd, 0.0D, -90.0D,0d,0d);
	        tessellator.draw();
	        Minecraft.getMinecraft().getTextureManager().bindTexture(ClientProxy.chargeTexture);
	        GL11.glColor4f(0.5F, 0.5F, 0.5F, 0.7F);
	        tessellator.startDrawingQuads();
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50, (double)event.resolution.getScaledHeight()/2+15, -90.0D,0d,0.25d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+100, (double)event.resolution.getScaledHeight()/2+15, -90.0D,0.508d,0.25d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+100, (double)event.resolution.getScaledHeight()/2, -90.0D,0.508d,0d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50, (double)event.resolution.getScaledHeight()/2, -90.0D,0d,0d);
	        tessellator.draw();
	        if(player.getHeldItem().stackTagCompound.getInteger("ZoomTime")>=20){
		        tessellator.startDrawingQuads();
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+110, (double)event.resolution.getScaledHeight()/2+18, -90.0D,0d,0.57d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+121, (double)event.resolution.getScaledHeight()/2+18, -90.0D,0.125d,0.57d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+121, (double)event.resolution.getScaledHeight()/2-3, -90.0D,0.125d,0.25d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+110, (double)event.resolution.getScaledHeight()/2-3, -90.0D,0d,0.25d);
		        tessellator.draw();
	        }
	        double progress=player.getHeldItem().stackTagCompound.getInteger("ZoomTime")/ItemSniperRifle.getChargeTime(player.getHeldItem(), player);
	        GL11.glColor4f(1F, 1F, 1F, 1F);
	        tessellator.startDrawingQuads();
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50, (double)event.resolution.getScaledHeight()/2+15, -90.0D,0d,0.25d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50+progress*50, (double)event.resolution.getScaledHeight()/2+15, -90.0D,progress*0.508d,0.25d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50+progress*50, (double)event.resolution.getScaledHeight()/2, -90.0D,progress*0.508d,0d);
	        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+50, (double)event.resolution.getScaledHeight()/2, -90.0D,0d,0d);
	        tessellator.draw();
	        if(progress==1d){
		        tessellator.startDrawingQuads();
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+110, (double)event.resolution.getScaledHeight()/2+18, -90.0D,0d,0.57d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+121, (double)event.resolution.getScaledHeight()/2+18, -90.0D,0.125d,0.57d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+121, (double)event.resolution.getScaledHeight()/2-3, -90.0D,0.125d,0.25d);
		        tessellator.addVertexWithUV((double)event.resolution.getScaledWidth()/2+110, (double)event.resolution.getScaledHeight()/2-3, -90.0D,0d,0.25d);
		        tessellator.draw();
	        }
	        GL11.glDepthMask(true);
	        GL11.glEnable(GL11.GL_DEPTH_TEST);
	        GL11.glEnable(GL11.GL_ALPHA_TEST);
	        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
	@SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
        if(eventArgs.modID.equals("rafradek_tf2_weapons"))
            TF2weapons.syncConfig();
    }
	@SubscribeEvent
	public void serverTickEnd(TickEvent.ServerTickEvent event){
		if(event.phase==TickEvent.Phase.END){
			ItemUsable.tick(false);
	    	if(tickleft<=0){
				tickleft=20;
				Object[] entArray=ItemUsable.lastDamage.keySet().toArray();
				for(int x=0; x<entArray.length;x++){
					Entity entity=(Entity) entArray[x];
					float[] dmg=ItemUsable.lastDamage.get(entArray[x]);
					for(int i=19;i>=0;i--){
						if(i>0){
							dmg[i]=dmg[i-1];
						}
						else{
							dmg[0]=0;
						}
					}
				}
			}
	    	else
				tickleft--;
		}
	}
	/*@SubscribeEvent
	public void spawnCharacters(TickEvent.WorldTickEvent event){
		if(!event.world.isRemote && event.phase==TickEvent.Phase.END){
			
			//if(time!=0&&event.world.rand.nextInt(2500/time)!=0) return;
			this.eligibleChunksForSpawning.clear();
            int i;
            int k;

            for (i = 0; i < event.world.playerEntities.size(); ++i)
            {
                EntityPlayer entityplayer = (EntityPlayer)event.world.playerEntities.get(i);
                int j = MathHelper.floor_double(entityplayer.posX / 16.0D);
                k = MathHelper.floor_double(entityplayer.posZ / 16.0D);
                byte b0 = 8;

                for (int l = -b0; l <= b0; ++l)
                {
                    for (int i1 = -b0; i1 <= b0; ++i1)
                    {
                        boolean flag3 = l == -b0 || l == b0 || i1 == -b0 || i1 == b0;
                        ChunkCoordIntPair chunkcoordintpair = new ChunkCoordIntPair(l + j, i1 + k);

                        if (!flag3)
                        {
                            this.eligibleChunksForSpawning.put(chunkcoordintpair, Boolean.valueOf(false));
                        }
                        else if (!this.eligibleChunksForSpawning.containsKey(chunkcoordintpair))
                        {
                            this.eligibleChunksForSpawning.put(chunkcoordintpair, Boolean.valueOf(true));
                        }
                    }
                }
            }

            i = 0;
            ChunkCoordinates chunkcoordinates = event.world.getSpawnPoint();
            Iterator iterator = this.eligibleChunksForSpawning.keySet().iterator();
            ArrayList<ChunkCoordIntPair> tmp = new ArrayList(eligibleChunksForSpawning.keySet());
            Collections.shuffle(tmp);
            iterator = tmp.iterator();
            label110:

            while (iterator.hasNext())
            {
                ChunkCoordIntPair chunkcoordintpair1 = (ChunkCoordIntPair)iterator.next();

                if (!((Boolean)this.eligibleChunksForSpawning.get(chunkcoordintpair1)).booleanValue())
                {
                	Chunk chunk = event.world.getChunkFromChunkCoords(chunkcoordintpair1.chunkXPos, chunkcoordintpair1.chunkZPos);
                    int x = chunkcoordintpair1.chunkXPos * 16 + event.world.rand.nextInt(16);
                    int z = chunkcoordintpair1.chunkZPos * 16 + event.world.rand.nextInt(16);
                    int y = event.world.rand.nextInt(chunk == null ? event.world.getActualHeight() : chunk.getTopFilledSegment() + 16 - 1);
                    System.out.println("tick2");
                    if (!event.world.getBlock(x, y, z).isNormalCube() && event.world.getBlock(x, y, z).getMaterial() == Material.air)
                    {
                        int i2 = 0;
                        int j2 = 0;
                        int team=event.world.rand.nextInt(2);
                        System.out.println("tick3");
                        while (j2 < 1)
                        {
                            int k2 = x;
                            int l2 = y;
                            int i3 = z;
                            byte b1 = 6;
                            IEntityLivingData ientitylivingdata = null;
                            int j3 = 0;
                            System.out.println("tick4");
                            while (true)
                            {
                            	System.out.println("tick5");
                                if (j3 < 4)
                                {
                                    label103:
                                    {
                                        k2 += event.world.rand.nextInt(b1) - event.world.rand.nextInt(b1);
                                        l2 += event.world.rand.nextInt(1) - event.world.rand.nextInt(1);
                                        i3 += event.world.rand.nextInt(b1) - event.world.rand.nextInt(b1);

                                        if (canCreatureTypeSpawnAtLocation(EnumCreatureType.monster, event.world, k2, l2, i3))
                                        {System.out.println("tick6");
                                            float f = (float)k2 + 0.5F;
                                            float f1 = (float)l2;
                                            float f2 = (float)i3 + 0.5F;

                                            if (event.world.getClosestPlayer((double)f, (double)f1, (double)f2, 24.0D) == null)
                                            {
                                            	System.out.println("tick7");
                                                float f3 = f - (float)chunkcoordinates.posX;
                                                float f4 = f1 - (float)chunkcoordinates.posY;
                                                float f5 = f2 - (float)chunkcoordinates.posZ;
                                                float f6 = f3 * f3 + f4 * f4 + f5 * f5;

                                                if (f6 >= 576.0F)
                                                {
                                                    EntityTF2Character entityliving=null;

                                                    System.out.println("tick8");
                                                    try
                                                    {
                                                    	switch (event.world.rand.nextInt(2)){
                                                    	case 1:entityliving = new EntityScout(event.world);
                                                    	default:entityliving = new EntityHeavy(event.world);
                                                    	}
                                                        entityliving.setEntTeam(team);
                                                    }
                                                    catch (Exception exception)
                                                    {
                                                        exception.printStackTrace();
                                                    }

                                                    entityliving.setLocationAndAngles((double)f, (double)f1, (double)f2, event.world.rand.nextFloat() * 360.0F, 0.0F);

                                                    Result canSpawn = ForgeEventFactory.canEntitySpawn(entityliving, event.world, f, f1, f2);
                                                    if (canSpawn == Result.ALLOW || (canSpawn == Result.DEFAULT && entityliving.getCanSpawnHere()))
                                                    {
                                                    	System.out.println("tick9");
                                                        ++i2;
                                                        event.world.spawnEntityInWorld(entityliving);
                                                        if (!ForgeEventFactory.doSpecialSpawn(entityliving, event.world, f, f1, f2))
                                                        {
                                                            ientitylivingdata = entityliving.onSpawnWithEgg(ientitylivingdata);
                                                        }

                                                        if (j2 >= ForgeEventFactory.getMaxSpawnPackSize(entityliving))
                                                        {
                                                            continue label110;
                                                        }
                                                    }

                                                    i += i2;
                                                }
                                            }
                                        }

                                        ++j3;
                                        continue;
                                    }
                                }
                                j2++;
                                break;
                            }
                        }
                    }
                }
            }
        }
	}
	public static boolean canCreatureTypeSpawnAtLocation(EnumCreatureType p_77190_0_, World p_77190_1_, int p_77190_2_, int p_77190_3_, int p_77190_4_)
    {
        if (!World.doesBlockHaveSolidTopSurface(p_77190_1_, p_77190_2_, p_77190_3_ - 1, p_77190_4_))
        {
            return false;
        }
        else
        {
            Block block = p_77190_1_.getBlock(p_77190_2_, p_77190_3_ - 1, p_77190_4_);
            boolean spawnBlock = block.canCreatureSpawn(p_77190_0_, p_77190_1_, p_77190_2_, p_77190_3_ - 1, p_77190_4_);
            return spawnBlock && block != Blocks.bedrock && !p_77190_1_.getBlock(p_77190_2_, p_77190_3_, p_77190_4_).isNormalCube() && !p_77190_1_.getBlock(p_77190_2_, p_77190_3_, p_77190_4_).getMaterial().isLiquid() && !p_77190_1_.getBlock(p_77190_2_, p_77190_3_ + 1, p_77190_4_).isNormalCube();
        }
    }*/
	@SubscribeEvent
	public void stopHurt(LivingAttackEvent event){
		if(event.entityLiving instanceof EntityTF2Character && event.source.getSourceOfDamage()!=null && event.source.getSourceOfDamage() instanceof EntityTF2Character 
				&& ((EntityTF2Character)event.source.getSourceOfDamage()).getEntTeam()==((EntityTF2Character)event.entityLiving).getEntTeam()){
			event.setCanceled(true);
		}
	}
	@SubscribeEvent
	public void stopBreak(BlockEvent.BreakEvent event){
		if(event.getPlayer().getHeldItem() !=null&&event.getPlayer().getHeldItem().getItem() instanceof ItemUsable){
			event.setCanceled(true);
		}
	}
	@SubscribeEvent
	public void unloadPlayers(WorldEvent.Unload event){
		Map<EntityLivingBase,Integer> map=TF2ActionHandler.playerAction.get();
		if(map.isEmpty()) return;
		Iterator<EntityLivingBase> iterator=map.keySet().iterator();
		while(iterator.hasNext())
    	{
			if(iterator.next().worldObj==event.world){
				iterator.remove();
			}
    	}
	}
	
}
