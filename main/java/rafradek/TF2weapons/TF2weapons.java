package rafradek.TF2weapons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.minecraft.block.BlockDispenser;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.dispenser.BehaviorDefaultDispenseItem;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.util.EnumHelper;

import org.apache.commons.lang3.ArrayUtils;

import rafradek.TF2weapons.characters.EntityHeavy;
import rafradek.TF2weapons.characters.EntityScout;
import rafradek.TF2weapons.characters.EntitySniper;
import rafradek.TF2weapons.characters.EntityTF2Character;
import rafradek.TF2weapons.characters.ItemMonsterPlacerPlus;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.message.TF2Message;
import rafradek.TF2weapons.projectiles.EntityProjectileBase;
import rafradek.TF2weapons.projectiles.EntityRocket;
import rafradek.TF2weapons.weapons.ItemRangedWeapon;
import rafradek.TF2weapons.weapons.ItemUsable;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.Metadata;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = "rafradek_tf2_weapons", name =  "TF2 Stuff Mod", version = "0.2",guiFactory = "rafradek.TF2weapons.TF2GuiFactory")
public class TF2weapons
{
	@Metadata("rafradek_tf2_weapons")
	public static ModMetadata metadata;
	
    public int[] itemid = new int[9];
    public static Configuration conf;
	public static CreativeTabs tabtf2;
    public static final ArmorMaterial OPARMOR = EnumHelper.addArmorMaterial("OPARMOR", 1000, new int[] {24,0,0,0}, 100);
    public static SimpleNetworkWrapper network;
    public static Item itemPlacer;
    public static Item mobHeldItem;
    public static boolean destTerrain;
    public File weaponDir;
    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event)
    {
    	this.weaponDir=new File(event.getModConfigurationDirectory(),"TF2WeaponsLists");
    	if(!this.weaponDir.exists()){
    		this.weaponDir.mkdirs();
    	}
    	metadata.autogenerated=false;
    	File outputFile=new File(this.weaponDir,"Weapons.cfg");
    	File file=event.getSourceFile();
    	//System.out.println("LOLOLOLOLOLOL "+file.getAbsolutePath());
    	//System.out.println("LOLOLOLOLOLOL2 "+event.getModConfigurationDirectory());
    	//System.out.println("Istnieje? "+outputFile.exists());
    	if(!outputFile.exists()){
    		if(file.isFile()){
				try {	
			    	ZipFile zip = new ZipFile(file);
			    	ZipEntry entry = zip.getEntry("Weapons.cfg");
			
			    	if(entry!=null) {
					
				    	InputStream zin = zip.getInputStream(entry);
				    	byte[] bytes=new byte[(int) entry.getSize()];
				    	zin.read(bytes);
				    	FileOutputStream str=new FileOutputStream(outputFile);
				    	str.write(bytes);
				    	str.close();
				    	zin.close();
				    	zip.close();
			    	
					}
		    	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	else{
	    		try {	
	    			File inputFile=new File(file,"Weapons.cfg");
			    	FileInputStream istr=new FileInputStream(inputFile);
			
			    	byte[] bytes=new byte[(int) inputFile.length()];
			    	istr.read(bytes);
			    	FileOutputStream str=new FileOutputStream(outputFile);
			    	str.write(bytes);
			    	str.close();
			    	istr.close();
			    	
		    	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
    	}
    	
    	MapList.initMaps();
    	TF2Attribute.initAttributes();
        conf = new Configuration(event.getSuggestedConfigurationFile());
        syncConfig();
        //conf.save();
    }
    public static void syncConfig(){
    	destTerrain=conf.getBoolean("gameplay", "Destructible Terrain", false,"Explosions can destroy blocks");
        if(conf.hasChanged())
            conf.save();
    }
    @SidedProxy(clientSide = "rafradek.TF2weapons.ClientProxy", serverSide = "rafradek.TF2weapons.CommonProxy")
    public static CommonProxy proxy;
    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
    	tabtf2=new CreativeTabs("tf2"){
			@Override
			public Item getTabIconItem() {
				return MapList.weaponClasses.get("Bullet");
			}
		};
    	//EntityRegistry.registerModEntity(EntityBullet.class, "bullet", 1, this, 256, 100, false);
    	EntityRegistry.registerModEntity(EntityHeavy.class, "heavy", 2, this, 80,3, true);
    	EntityRegistry.registerModEntity(EntityScout.class, "scout", 3, this, 80,3, true);
    	EntityRegistry.registerModEntity(EntitySniper.class, "sniper", 4, this, 80,3, true);
    	EntityRegistry.registerModEntity(EntityRocket.class, "rocket", 5, this, 64,20, false);
    	//GameRegistry.registerItem(new ItemArmor(TF2weapons.OPARMOR, 3, 0).setUnlocalizedName("oparmor").setTextureName("diamond_helmet").setCreativeTab(tabtf2),"oparmor");
    	GameRegistry.registerItem(itemPlacer=new ItemMonsterPlacerPlus().setUnlocalizedName("monsterPlacer").setTextureName("spawn_egg"),"placer");
    	EntityRegistry.addSpawn(EntitySniper.class, 46, 1, 3, EnumCreatureType.monster,ArrayUtils.subarray(BiomeGenBase.getBiomeGenArray(), 0, ArrayUtils.indexOf(BiomeGenBase.getBiomeGenArray(), null, 0)));
    	EntityRegistry.addSpawn(EntityHeavy.class, 46, 1, 3, EnumCreatureType.monster,ArrayUtils.subarray(BiomeGenBase.getBiomeGenArray(), 0, ArrayUtils.indexOf(BiomeGenBase.getBiomeGenArray(), null, 0)));
    	EntityRegistry.addSpawn(EntityScout.class, 46, 1, 3, EnumCreatureType.monster,ArrayUtils.subarray(BiomeGenBase.getBiomeGenArray(), 0, ArrayUtils.indexOf(BiomeGenBase.getBiomeGenArray(), null, 0)));
    	//new Item(2498).setUnlocalizedName("FakeItem").setTextureName("tf2weapons:saw").setCreativeTab(CreativeTabs.tabBlock);
    	BlockDispenser.dispenseBehaviorRegistry.putObject(itemPlacer, new BehaviorDefaultDispenseItem()
    	{
            @Override
			public ItemStack dispenseStack(IBlockSource p_82487_1_, ItemStack p_82487_2_)
            {
                EnumFacing enumfacing = BlockDispenser.func_149937_b(p_82487_1_.getBlockMetadata());
                double d0 = p_82487_1_.getX() + enumfacing.getFrontOffsetX();
                double d1 = p_82487_1_.getYInt() + 0.2F;
                double d2 = p_82487_1_.getZ() + enumfacing.getFrontOffsetZ();
                Entity entity = ItemMonsterPlacerPlus.spawnCreature(p_82487_1_.getWorld(), p_82487_2_.getItemDamage(), d0, d1, d2);

                if (entity instanceof EntityLivingBase && p_82487_2_.hasDisplayName())
                {
                    ((EntityLiving)entity).setCustomNameTag(p_82487_2_.getDisplayName());
                }

                p_82487_2_.splitStack(1);
                return p_82487_2_;
            }
        });
    	loadWeapons();
        network=NetworkRegistry.INSTANCE.newSimpleChannel("rafradek_tf2_weapons");
		network.registerMessage(TF2ActionHandler.class, TF2Message.ActionMessage.class, 0, Side.SERVER);
		
        //TickRegistry.registerTickHandler(new CommonTickHandler(), Side.SERVER);
        MinecraftForge.EVENT_BUS.register(new TF2EventBusListener());
        FMLCommonHandler.instance().bus().register(new TF2EventBusListener());
        proxy.registerRenderInformation();
        
    }
    public void loadWeapons() {
    	Iterator<String> iterator= MapList.weaponClasses.keySet().iterator();
    	while(iterator.hasNext()){
    		String name=iterator.next();
    		GameRegistry.registerItem(MapList.weaponClasses.get(name), "rafradek_"+name);
    	}
    	this.loadConfig(new File(this.weaponDir,"Weapons.cfg"));
    	File[] files=this.weaponDir.listFiles(new FilenameFilter(){

			@Override
			public boolean accept(File arg0, String arg1) {
				return !arg1.equalsIgnoreCase("Weapons.cfg");
			}
    		
    	});
    	for(File file:files){
    		this.loadConfig(file);
    	}
    }
    public void loadConfig(File file){
    	Configuration weaponsFile= new Configuration(file);
        weaponsFile.load();
    	Iterator<String> icategories=  weaponsFile.getCategoryNames().iterator();
    	while(icategories.hasNext()){
    		String weaponEntry= icategories.next();
    		ConfigCategory weaponData=weaponsFile.getCategory(weaponEntry);
    		//Class<?> weaponClass = MapList.weaponClasses.get(weaponData.get("Class").getString());
    		try {
    			if(weaponData.containsKey("Based on")&&MapList.nameToCC.containsKey(weaponData.get("Based on").getString())) {
    				weaponData=attach(MapList.nameToCC.get(weaponData.get("Based on").getString()),weaponData);
    				//weaponList[Integer.parseInt(weaponEntry)] =(ItemUsable) weaponClass.getConstructor(new Class[] {ConfigCategory.class, ConfigCategory.class}).newInstance(new Object[] {weaponPreData, weaponData});
    			}
    			/*else{
    				weaponList[Integer.parseInt(weaponEntry)] =(ItemUsable) weaponClass.getConstructor(new Class[] {ConfigCategory.class, ConfigCategory.class}).newInstance(new Object[] {weaponData, null});
    			}*/
    			//GameRegistry.registerItem(weaponList[Integer.parseInt(weaponEntry)], "weapon"+Integer.parseInt(weaponEntry));
    			MapList.nameToCC.put(weaponEntry, weaponData);
    			//LanguageRegistry.instance().addStringLocalization(weaponData.get("Name").getString()+".name", weaponData.get("Name").getString());
    			NBTTagCompound tag=new NBTTagCompound();
    			tag.setString("Type", weaponEntry);
    			NBTTagCompound tag2=new NBTTagCompound();
    			if(weaponData.containsKey("Attributes")){
    	        	String[] attributes=weaponData.get("Attributes").getStringList();
    	        	for(int i=0; i < attributes.length; i++) {
    	        		String attributeName=attributes[i].split(":")[0];
    	        		String attributeValue=attributes[i].split(":")[1];
    	        		
    	        		Iterator<String> iterator2 = MapList.nameToAttribute.keySet().iterator();
    	        		//System.out.println("to je"+attributeName+" "+attributeValue);
    	        		boolean has=false;
    	        		while(iterator2.hasNext()) {
    	        			if(iterator2.next().equals(attributeName)){
    	        				tag2.setFloat(String.valueOf(MapList.nameToAttribute.get(attributeName).id), Float.parseFloat(attributeValue));
    	        				has=true;
    	        			}
    	        		}
    	        		if(has==false){
    	        			tag2.setFloat(attributeName, Float.parseFloat(attributeValue));
    	        		}
    	        	}
    	        	
    	        }
    			tag.setTag("Attributes", tag2);
    			MapList.buildInAttributes.put(weaponEntry, tag);
    		}catch (Exception var4){
                var4.printStackTrace();
            }
    	}
    }
    public static ConfigCategory attach(ConfigCategory old, ConfigCategory additional){
    	Set<String> dataKeys = old.keySet();
    	Iterator<String> iterator=dataKeys.iterator();
    	while(iterator.hasNext()){
    		String key=iterator.next();
    		if(!additional.containsKey(key))
    			additional.put(key, old.get(key));
    	}
    	Set<String> data2Keys = old.keySet();
    	Iterator<String> iterator2=data2Keys.iterator();
    	new ConfigCategory(null, additional);
    	return additional;
    }
    public static DamageSource causeBulletDamage(String weapon, Entity shooter, boolean crititical)
    {
        return (new DamageSourceBullet(weapon, shooter)).setProjectile();
    }
    public static int RoundUp(double value, int zero)
    {
        if (value > (int)value)
        {
            return (int)value + 1;
        }

        return (int)value;
    }
    public static double[] radiusRandom3D(float radius,Random random){
    	double x=Double.MAX_VALUE,y=Double.MAX_VALUE,z=Double.MAX_VALUE;
    	while (Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)+Math.pow(z, 2))>radius){
    		x=random.nextDouble()*radius*2-radius;
    		y=random.nextDouble()*radius*2-radius;
    		z=random.nextDouble()*radius*2-radius;
    	}
    	return new double[]{x,y,z};
    	
    }
    public static double[] radiusRandom2D(float radius,Random random){
    	/*double x=random.nextDouble()*radius*2-radius;
    	radius -= Math.abs(x);
    	double y=random.nextDouble()*radius*2-radius;*/
    	
    	/*double t = 4*Math.PI*random.nextDouble()*radius-radius;
    	double u = (random.nextDouble()*radius*2-radius)+(random.nextDouble()*radius*2-raddddddddius);
    	double r = u>1?2-u:u;*/
    	float a=random.nextFloat(),b=random.nextFloat();
    	return new double[]{Math.max(a, b)*radius*Math.cos(2*Math.PI*Math.min(a, b)/Math.max(a, b)),Math.max(a, b)*radius*Math.sin(2*Math.PI*Math.min(a, b)/Math.max(a, b))};
    }
    public static MovingObjectPosition pierce(World world,EntityLivingBase living, double startX, double startY,double startZ,double endX,double endY,double endZ,boolean headshot){
    	Vec3 var17 = Vec3.createVectorHelper(startX, startY, startZ);
        Vec3 var3 = Vec3.createVectorHelper(endX, endY, endZ);
        MovingObjectPosition var4 = world.func_147447_a(var17, var3, false, true,false);
        var17 = Vec3.createVectorHelper(startX, startY, startZ);
        var3 = Vec3.createVectorHelper(endX, endY, endZ);

        
        if (var4 != null)
        {
            var3 = Vec3.createVectorHelper(var4.hitVec.xCoord, var4.hitVec.yCoord, var4.hitVec.zCoord);
        }

        Entity var5 = null;
        AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(startX-0.5, startY-0.5, startZ-0.5, startX+0.5, startY+0.5, startZ+0.5);
        List var6 = world.getEntitiesWithinAABBExcludingEntity(living, living.boundingBox.addCoord(endX - startX, endY - startY, endZ - startZ).expand(1.0D, 1.0D, 1.0D));
        //System.out.println("shoot: "+startX+","+startY+","+startZ+", do: "+endX+","+endY+","+endZ+" Count: "+var6.size());
        double var7 = 0.0D;
        Iterator iterator = var6.iterator();
        float var11;
        Vec3 collideVec=Vec3.createVectorHelper(0, 0, 0);
        while (iterator.hasNext())
        {
            Entity var10 = (Entity)iterator.next();

            if (var10.canBeCollidedWith() && (!(var10 instanceof EntityLivingBase) || (var10 instanceof EntityLivingBase && ((EntityLivingBase)var10).deathTime <= 0)))
            {
                var11 = 0.08F;
                AxisAlignedBB var12 = var10.boundingBox.expand(var11, (double)var11+0.16f, var11);
                MovingObjectPosition var13 = var12.calculateIntercept(var17, var3);

                if (var13 != null)
                {
                    double var14 = var17.distanceTo(var13.hitVec);

                    if (var14 < var7 || var7 == 0.0D)
                    {
                        var5 = var10;
                        var7 = var14;
                        collideVec=var13.hitVec;
                    }
                }
            }
        }

        if (var5 != null && !(var5 instanceof EntityLivingBase && ((EntityLivingBase)var5).getHealth()<=0))
        {
            var4 = new MovingObjectPosition(var5,collideVec);
            if(headshot){
	            double ymax=var5.boundingBox.maxY;
	    		AxisAlignedBB head=AxisAlignedBB.getBoundingBox(var5.posX-0.32, ymax-0.24, var5.posZ-0.32,var5.posX+0.32, ymax+0.48, var5.posZ+0.32);
	    		if(var5 instanceof EntityCreeper||var5 instanceof EntityEnderman||var5 instanceof EntityIronGolem){
	    			head.offset(0, -0.24, 0);
	    		}
	    		if(var5.width>var5.height*0.65){
	    			double offsetX=(double)(MathHelper.cos(living.rotationYaw / 180.0F * (float)Math.PI) * var5.width/2);
	    			double offsetZ=-(double)(MathHelper.sin(living.rotationYaw / 180.0F * (float)Math.PI) * var5.width/2);//cos
	    			//double offsetX2=- (double)(MathHelper.sin(living.rotationYaw / 180.0F * (float)Math.PI) * var5.width/2);
	    			//double offsetZ2=(double)(MathHelper.cos(living.rotationYaw / 180.0F * (float)Math.PI) * var5.width/2);//cos
	    			//System.out.println("Offsets: "+offsetX+" "+offsetZ+" "+offsetX2+" "+offsetZ2);
	    			head.offset(offsetX, 0, offsetZ);
	    		}
	    		MovingObjectPosition var13 = head.calculateIntercept(var17, var3);
	            var4.hitInfo=var13;
            }
        }
        return var4;
    }
    public static float calculateDamage(Entity target,World world, EntityLivingBase living, ItemStack stack,boolean critical,float distance){
    	ItemRangedWeapon weapon=(ItemRangedWeapon) stack.getItem();
    	float calculateddamage = weapon.getWeaponDamage(stack,living);
    	if(calculateddamage==0){
    		return 0f;
    	}
        if(!critical||target==living){
        	if(distance <= weapon.getWeaponDamageFalloff(stack))
        		//calculateddamage *=weapon.maxDamage - ((distance / (float)weapon.damageFalloff) * (weapon.maxDamage-weapon.damage));
        		calculateddamage *= (weapon.getWeaponMaxDamage(stack,living)/weapon.getWeaponDamage(stack,living)) - ((distance / weapon.getWeaponDamageFalloff(stack)) * ((weapon.getWeaponMaxDamage(stack,living)/weapon.getWeaponDamage(stack,living))-1));
        	else {
        		calculateddamage *=Math.max(weapon.getWeaponMinDamage(stack,living)/weapon.getWeaponDamage(stack,living), ((weapon.getWeaponDamage(stack,living)) - (((distance-weapon.getWeaponDamageFalloff(stack)) / ((float)weapon.getWeaponDamageFalloff(stack)*2)) * (weapon.getWeaponDamage(stack,living)-weapon.getWeaponMinDamage(stack,living))))/weapon.getWeaponDamage(stack,living));
        		//System.out.println((distance-weapon.getWeaponDamageFalloff(stack))-(weapon.getWeaponDamageFalloff(stack)*2));
        	}
        }
        else
        	calculateddamage*=3;
        
        /*if (living instanceof IRangedWeaponAttackMob)
        	calculateddamage*=((IRangedWeaponAttackMob)living).getAttributeModifier("Damage");*/
        return calculateddamage;
    }
    public static int getTeam(EntityLivingBase living){
    	if(living instanceof EntityTF2Character){
    		return ((EntityTF2Character)living).getEntTeam();
    	}
    	return 0;
    }
    public static void dealDamage(Entity entity,World world,EntityLivingBase living, ItemStack stack, ItemRangedWeapon weapon, boolean critical,float damage,DamageSource source,float distance){
    	double lvelocityX = entity.motionX;
        double lvelocityY = entity.motionY;
        double lvelocityZ = entity.motionZ;

        if (!world.isRemote && entity.attackEntityFrom(source,damage))
        {
        	if(!ItemUsable.lastDamage.containsKey(living))
        		ItemUsable.lastDamage.put(living, new float[20]);
        	ItemUsable.lastDamage.get(living)[0]+=damage;
        	if (entity instanceof EntityLivingBase)
	        {
	            EntityLivingBase livingTarget = (EntityLivingBase)entity;
	            // System.out.println("Scaled"+source.isDifficultyScaled()+" "+damage);
	            livingTarget.hurtResistantTime = 0;
	            if(critical){
	            	world.playSoundAtEntity(entity, "tf2weapons:misc.crit", 0.7F, 1.2F / (world.rand.nextFloat() * 0.2F + 0.9F));
	            }
	            world.playSoundAtEntity(entity, "tf2weapons:misc.pain", 0.6F, 1.2F / (world.rand.nextFloat() * 0.2F + 0.9F));
	            if (living instanceof EntityPlayer && !world.isRemote)
	            {
	                EntityPlayer player = (EntityPlayer) living;
	                String string="";
	                for(int i=0; i<20;i++){
	                	string+=ItemUsable.lastDamage.get(living)[i]+" ";
	                }
	                
	                //player.addChatMessage(string);
	               // player.addChatMessage("Health: " + livingTarget.getHealth() + "/" + livingTarget.getMaxHealth() + " Armor: " + livingTarget.getTotalArmorValue()*4+ "% Critical: "+critical+" Distance: "+distance);
	            }
	            livingTarget.motionX=lvelocityX;
	            livingTarget.motionY=lvelocityY;
	            livingTarget.motionZ=lvelocityZ;
	        }
        }
    }
}
