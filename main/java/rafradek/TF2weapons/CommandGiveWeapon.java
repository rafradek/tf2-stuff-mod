package rafradek.TF2weapons;

import rafradek.TF2weapons.weapons.ItemUsable;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CommandGiveWeapon extends CommandBase {

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return "giveweapon";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		// TODO Auto-generated method stub
		return "command.giveweapon.usage";
	}

	@Override
	public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
		if(p_71515_2_.length<1){
			throw new WrongUsageException("commands.giveweapon.usage", new Object[0]);
		}
		try{
			ItemStack item=ItemUsable.getNewStack(p_71515_2_[0]);
			EntityPlayerMP entityplayermp = p_71515_2_.length >= 1 ? getPlayer(p_71515_1_, p_71515_2_[1]) : getCommandSenderAsPlayer(p_71515_1_);
			EntityItem entityitem = entityplayermp.dropPlayerItemWithRandomChoice(item, false);
	        entityitem.delayBeforeCanPickup = 0;
	        entityitem.func_145797_a(entityplayermp.getCommandSenderName());
	        func_152373_a(p_71515_1_, this, "commands.giveweapon.success", new Object[] {item.func_151000_E(), entityplayermp.getCommandSenderName()});
		}
		catch(Exception e){
			func_152373_a(p_71515_1_, this, "commands.giveweapon.notfound", new Object[] {p_71515_2_[0]});
		}
	}

}
