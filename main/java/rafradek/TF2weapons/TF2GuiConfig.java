package rafradek.TF2weapons;

import java.util.List;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import cpw.mods.fml.client.config.GuiConfig;
import cpw.mods.fml.client.config.IConfigElement;

public class TF2GuiConfig extends GuiConfig {

	public TF2GuiConfig(GuiScreen parentScreen) {
		super(parentScreen,
                new ConfigElement(TF2weapons.conf.getCategory("gameplay")).getChildElements(),
                "rafradet_tf2_weapons", false, false, GuiConfig.getAbridgedConfigPath(TF2weapons.conf.toString()));
	}

}
