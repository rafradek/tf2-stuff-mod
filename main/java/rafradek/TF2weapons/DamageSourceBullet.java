package rafradek.TF2weapons;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;

public class DamageSourceBullet extends EntityDamageSource
{
    public String weapon;
    public Entity entity;
    public Entity shooter;
    public DamageSourceBullet(String weapon, Entity shooter)
    {
        super("bullet", shooter);
        this.shooter = shooter;
        this.weapon = weapon;
    }
    @Override
	public Entity getSourceOfDamage()
    {
        return this.shooter;
    }

    @Override
	public Entity getEntity()
    {
        return this.shooter;
    }
   /* public DamageSource bypassesArmor()
    {
        this.setDamageBypassesArmor();
        return this;
    }*/
    /**
     * Returns the message to be displayed on player death.
     */
    public IChatComponent func_151519_b(EntityLivingBase p_151519_1_)
    {
        ItemStack itemstack = this.damageSourceEntity instanceof EntityLivingBase ? ((EntityLivingBase)this.damageSourceEntity).getHeldItem() : null;
        String s = "death.attack." + this.damageType;
        String s1 = s+ ".item";
        return itemstack != null && StatCollector.canTranslate(s1) ? new ChatComponentTranslation(s1, new Object[] {p_151519_1_.func_145748_c_(), this.damageSourceEntity.func_145748_c_(), itemstack.func_151000_E()}): new ChatComponentTranslation(s, new Object[] {p_151519_1_.func_145748_c_(), this.damageSourceEntity.func_145748_c_()});
    }
    /*public String getDeathMessage(EntityPlayer par1EntityPlayer)
    {
        return StatCollector.translateToLocalFormatted("death." + this.damageType, new Object[] {par1EntityPlayer.getDisplayName(), this.shooter.getCommandSenderName(), StatCollector.translateToLocal(this.weapon)});
    }*/
}
