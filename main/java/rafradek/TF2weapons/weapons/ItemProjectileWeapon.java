package rafradek.TF2weapons.weapons;

import rafradek.TF2weapons.projectiles.EntityProjectileBase;
import rafradek.TF2weapons.projectiles.EntityRocket;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemProjectileWeapon extends ItemRangedWeapon {

	@Override
	public void shoot(ItemStack stack, EntityLivingBase living, World world,
			EntityLivingBase target, boolean thisCritical) {
		if(!world.isRemote){
			EntityProjectileBase proj=new EntityRocket(world, living);
			proj.setIsCritical(thisCritical);
			world.spawnEntityInWorld(proj);
			
		}
	}

	@Override
	public void usingItemFirst(ItemStack stack, World world,
			EntityLivingBase entity) {
		// TODO Auto-generated method stub

	}

}
