package rafradek.TF2weapons.weapons;

import java.util.List;
import java.util.UUID;

import rafradek.TF2weapons.ClientProxy;
import rafradek.TF2weapons.TF2Attribute;
import rafradek.TF2weapons.message.TF2ActionHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.config.ConfigCategory;

public class ItemMinigun extends ItemBulletWeapon {

	public static UUID slowdownUUID=UUID.fromString("12843092-A5D6-BBCD-3D4F-A3DD4D8C94C8");
	public static AttributeModifier slowdown = new AttributeModifier(slowdownUUID, "minigun slowdown", -0.58D, 2);

	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
	{
		super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
		if(par1ItemStack.stackTagCompound !=null){
			if((par3Entity instanceof EntityPlayer&&par4 !=((EntityPlayer)par3Entity).inventory.currentItem) ||
					(TF2ActionHandler.playerAction.get().get(par3Entity)!=null&&(TF2ActionHandler.playerAction.get().get(par3Entity)==0||TF2ActionHandler.playerAction.get().get(par3Entity)==4)))
			{
				if(par1ItemStack.stackTagCompound.getShort("minigunticks")>0){
					par1ItemStack.stackTagCompound.setShort("minigunticks", (short) (par1ItemStack.stackTagCompound.getShort("minigunticks")-1));
					((EntityLivingBase)par3Entity).getEntityAttribute(SharedMonsterAttributes.movementSpeed).removeModifier(slowdown);
				}
			}
		}
	}
	@Override
	public void usingItemFirst(ItemStack stack, World world,
			EntityLivingBase entity) {
		if(!world.isRemote) return;
		/*System.out.println("play");
		ResourceLocation playSound=new ResourceLocation(getData(stack).get("Minigun Sound").getString());
		ResourceLocation spinSound=new ResourceLocation(getData(stack).get("Spin Sound").getString());	
		Minecraft.getMinecraft().getSoundHandler().playSound(new MinigunLoopSound(playSound, (EntityLivingBase) entity, true, ((EntityLivingBase) entity).getHeldItem()));
		Minecraft.getMinecraft().getSoundHandler().playSound(new MinigunLoopSound(spinSound, (EntityLivingBase) entity, false, ((EntityLivingBase) entity).getHeldItem()));*/
	}
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par2List, boolean par4)
    {
		super.addInformation(par1ItemStack, par2EntityPlayer, par2List, par4);
        if (par1ItemStack.hasTagCompound())
        {
            par2List.add("minigun: "+Integer.toString(par1ItemStack.stackTagCompound.getShort("minigunticks")));
        }
    }
	public boolean startUse(ItemStack stack, EntityLivingBase living, World world) {
		if(world.isRemote&&(!ClientProxy.fireSounds.containsKey(living)||ClientProxy.fireSounds.get(living).type!=3)){
			ResourceLocation playSound=new ResourceLocation(getData(stack).get("Wind Up Sound").getString());
			ClientProxy.playMinigunSound(living, playSound,false,3,stack);
		}
		return false;
	}
	public boolean endUse(ItemStack stack, EntityLivingBase living, World world) {
		if(world.isRemote&&(!ClientProxy.fireSounds.containsKey(living)||ClientProxy.fireSounds.get(living).type!=4)){
			ResourceLocation playSound=new ResourceLocation(getData(stack).get("Wind Down Sound").getString());
			ClientProxy.playMinigunSound(living, playSound,false,4,stack);
		}
		return false;
	}
	@Override
	public boolean fireTick(ItemStack stack, EntityLivingBase living, World world) {
		if(world.isRemote&&this.canFire(world, living, stack)){
			if(stack.stackTagCompound.getShort("crittime")<=0&&(!ClientProxy.fireSounds.containsKey(living)||ClientProxy.fireSounds.get(living).type!=0)){
				ResourceLocation playSound=new ResourceLocation(getData(stack).get("Minigun Sound").getString());
				
				ClientProxy.playMinigunSound(living, playSound,true,0,stack);
			}
			else if(stack.stackTagCompound.getShort("crittime")>0&&(!ClientProxy.fireSounds.containsKey(living)||ClientProxy.fireSounds.get(living).type!=1)){
				ResourceLocation playSoundCrit=new ResourceLocation(getData(stack).get("Minigun Sound").getString()+".crit");
				
				ClientProxy.playMinigunSound(living, playSoundCrit,true,1,stack);
			}
		}
		//System.out.println("nie");
		this.spinMinigun(stack, living, world);
		return false;
	}
	@Override
	public boolean altFireTick(ItemStack stack, EntityLivingBase living, World world) {
		if(world.isRemote&&this.canFire(world, living, stack)&&(!ClientProxy.fireSounds.containsKey(living)||ClientProxy.fireSounds.get(living).type>2)){
			ResourceLocation playSound=new ResourceLocation(getData(stack).get("Spin Sound").getString());
			ClientProxy.playMinigunSound(living, playSound,true,2,stack);
		}
			/*System.out.println("start");
			ResourceLocation playSound=new ResourceLocation(getData(stack).get("Spin Sound").getString());
			MinigunLoopSound sound=new MinigunLoopSound(playSound, living, false, getData(stack),false);
			Minecraft.getMinecraft().getSoundHandler().playSound(sound);
			MapList.spinSounds.put(sound, living);
		}*/
		this.spinMinigun(stack, living, world);
		return false;
	}
	public void spinMinigun(ItemStack stack, EntityLivingBase living, World world) {
		if(living.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getModifier(slowdownUUID)==null)
			living.getEntityAttribute(SharedMonsterAttributes.movementSpeed).applyModifier(slowdown);
		if(stack.stackTagCompound !=null && stack.stackTagCompound.getShort("reload")<= 0 && stack.stackTagCompound.getShort("minigunticks")<17*TF2Attribute.getModifier("Minigun Spinup", stack, 1,living)){
			stack.stackTagCompound.setShort("minigunticks", (short) (stack.stackTagCompound.getShort("minigunticks")+1));
			//System.out.println("tak");
		}
	}
	@Override
    public boolean canFire(World world, EntityLivingBase living, ItemStack stack)
    {
        return (stack.getItemDamage()<stack.getMaxDamage() && stack.stackTagCompound.getShort("minigunticks") >= 17*TF2Attribute.getModifier("Minigun Spinup", stack, 1,living)) || (living instanceof EntityPlayer && ((EntityPlayer)living).capabilities.isCreativeMode) ? true : false;
    }

}
