package rafradek.TF2weapons.weapons;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import rafradek.TF2weapons.ClientProxy;
import rafradek.TF2weapons.IWeaponItem;
import rafradek.TF2weapons.TF2Attribute;
import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.characters.EntitySniper;
import rafradek.TF2weapons.characters.EntityTF2Character;
import rafradek.TF2weapons.characters.IRangedWeaponAttackMob;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.config.ConfigCategory;

public abstract class ItemRangedWeapon extends ItemUsable implements IWeaponItem
{
    /*public float damage;
    public float scatter;
    public int pellets;
    public float maxDamage;
    public int damageFalloff;
    public float minDamage;
    public int reload;
    public int clipSize;
    public boolean hasClip;
    public boolean clipType;
	public boolean randomCrits;
	public float criticalDamage;
	public int firstReload;
	public int knockback;*/
	
	public static boolean critical;
    public ItemRangedWeapon()
    {
    	super();
        
    }

    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
	{
    	super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
		if(par1ItemStack.stackTagCompound !=null){
			if((par3Entity instanceof EntityPlayer&&par4 !=((EntityPlayer)par3Entity).inventory.currentItem) && par1ItemStack.stackTagCompound.getShort("reloadd")>0)
			{
				par1ItemStack.stackTagCompound.setShort("reloadd",(short) 0);
			}
		
			if(!par2World.isRemote&&par1ItemStack.stackTagCompound.getShort("timeleft")<=0){
				par1ItemStack.stackTagCompound.setShort("timeleft", (short) 20);
				if(this.rapidFireCrits(par1ItemStack) && this.hasRandomCrits(par1ItemStack,par3Entity) && Math.random()<this.critChance(par1ItemStack, par3Entity)){
        			par1ItemStack.stackTagCompound.setShort("crittime", (short) 40);
        		}
			}
			if(par1ItemStack.stackTagCompound.getShort("crittime")>0){
				par1ItemStack.stackTagCompound.setShort("crittime", (short) (par1ItemStack.stackTagCompound.getShort("crittime") -1));
			}
			par1ItemStack.stackTagCompound.setShort("timeleft", (short) (par1ItemStack.stackTagCompound.getShort("timeleft") - 1));
			/*if(par3Entity instanceof EntityTF2Character&&((EntityTF2Character) par3Entity).getAttackTarget()!=null){
				System.out.println(this.getWeaponSpreadBase(par1ItemStack, (EntityLivingBase) par3Entity));
				if(par1ItemStack.getTagCompound().getInteger("reload")<=100&&!((EntityTF2Character)par3Entity).attack.lookingAt(this.getWeaponSpreadBase(par1ItemStack, (EntityLivingBase) par3Entity)*100+1)){
					par1ItemStack.getTagCompound().setInteger("reload", 100);
				}
				//par1ItemStack.getTagCompound().setBoolean("WaitProper", true);
			}*/
		}
	}
    
    @Override
    public boolean use(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target)
    {
        if (stack.getItemDamage() != stack.getMaxDamage())
            if (this.hasClip(stack))
            {
                stack.damageItem(1, living);
            }
        boolean thisCritical=false;
        if((!this.rapidFireCrits(stack)&&this.hasRandomCrits(stack,living) && Math.random()<this.critChance(stack, living))||stack.stackTagCompound.getShort("crittime")>0){
            thisCritical=true;
        }
        critical=thisCritical;
        if(living instanceof IRangedWeaponAttackMob&&((IRangedWeaponAttackMob)living).getAmmo()>=0){
        	((IRangedWeaponAttackMob)living).useAmmo(1);
    	}
        if(getData(stack).containsKey("Fire Sound")){
        	String soundToPlay=getData(stack).get("Fire Sound").getString()+(thisCritical?".crit":"");
        	world.playSoundAtEntity(living, soundToPlay, 0.45f, 1f);
        	if(world.isRemote){
        		ClientProxy.removeReloadSound(living);
        	}
        }
        
        for (int x = 0; x < this.getWeaponPelletCount(stack,living); x++)
        {
        	/*EntityBullet bullet;
        	if(target==null){
        		bullet = new EntityBullet(world, living, this.scatter);
        	}
        	else{
        		bullet = new EntityBullet(world, living, target, this.scatter);
        	}
            bullet.stack = stack;
            bullet.setDamage(this.damage*damagemult);
            bullet.damageM = this.maxDamage;
            bullet.damageMM = this.damageFalloff;
            bullet.minDamage = this.minDamage;
            if(thisCritical)
            	bullet.critical = true;
            	bullet.setDamage(this.damage*3);
            }
            world.spawnEntityInWorld(bullet);*/
        	this.shoot(stack,living,world,target,thisCritical);
        }
        return true;
    }
    public abstract void shoot(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target, boolean thisCritical);
    @Override
    public boolean canFire(World world, EntityLivingBase living, ItemStack stack)
    {
    	boolean flag=true;
    	if(living instanceof IRangedWeaponAttackMob&&((IRangedWeaponAttackMob)living).getAmmo()<=0){
    		flag=false;
    	}
        return stack.getItemDamage()<stack.getMaxDamage() || (living instanceof EntityPlayer && ((EntityPlayer)living).capabilities.isCreativeMode) ? true : false;
    }
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par2List, boolean par4)
    {
    	super.addInformation(par1ItemStack, par2EntityPlayer, par2List, par4);
        if (par1ItemStack.hasTagCompound())
        {
            par2List.add("Firing: "+Integer.toString(par1ItemStack.stackTagCompound.getShort("reload")));
            par2List.add("Reload: "+Integer.toString(par1ItemStack.stackTagCompound.getShort("reloadd")));
            par2List.add("Crit: "+Integer.toString(par1ItemStack.stackTagCompound.getShort("crittime")));
        }

        par2List.add("Clip: "+Integer.toString(par1ItemStack.getItemDamage()));
    }

    public float critChance(ItemStack stack, Entity entity){
    	float chance=0.02f;
    	if(ItemUsable.lastDamage.containsKey(entity)){
			for(int i=0;i<20;i++){
				chance+=ItemUsable.lastDamage.get(entity)[i]/800;
			}
    	}
    	return Math.min(chance,0.12f);
    }
	@Override
	public boolean fireTick(ItemStack stack, EntityLivingBase living, World world) {
		// TODO Auto-generated method stub
		return false;
	}
	@SideOnly(Side.CLIENT)
    public boolean isFull3D()
    {
        return true;
    }
	@Override
	public boolean altFireTick(ItemStack stack, EntityLivingBase living, World world) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public int getMaxDamage(ItemStack stack)
    {
		return stack.hasTagCompound()?this.getWeaponClipSize(stack,null):0;
    }
	
	public float getWeaponDamage(ItemStack stack,EntityLivingBase living){
		return (float) getData(stack).get("Damage").getDouble(1) * TF2Attribute.getModifier("Damage", stack, 1,living);
	}
	
	public float getWeaponMaxDamage(ItemStack stack,EntityLivingBase living) {
		return (float) getData(stack).get("Max damage").getDouble(1) * TF2Attribute.getModifier("Damage", stack, 1,living);
	}
	
	public float getWeaponMinDamage(ItemStack stack,EntityLivingBase living){
		return (float) getData(stack).get("Min damage").getDouble(1) * TF2Attribute.getModifier("Damage", stack, 1,living);
	}
	
	public float getWeaponSpread(ItemStack stack,EntityLivingBase living){
		float base=this.getWeaponSpreadBase(stack, living);
		if(living instanceof EntityTF2Character&&((EntityTF2Character)living).getAttackTarget()!=null){
			base+=((EntityTF2Character)living).attack.lookingAtMax()*((EntityTF2Character)living).motionSensitivity;
		}
		return Math.abs(base);
	}
	public float getWeaponSpreadBase(ItemStack stack,EntityLivingBase living){
		return (float) (getData(stack).get("Spread Recovery").getBoolean(false)&&stack.stackTagCompound.getShort("lastfire")<=0?0:getData(stack).get("Spread").getDouble(1) * TF2Attribute.getModifier("Spread", stack, 1,living));
	}
	public int getWeaponPelletCount(ItemStack stack,EntityLivingBase living){
		return (int) (getData(stack).get("Pellets").getInt(1) * TF2Attribute.getModifier("Pellet Count", stack, 1,living));
	}
	
	public int getWeaponDamageFalloff(ItemStack stack){
		return getData(stack).get("Damage falloff").getInt(10);
	}
	
	public int getWeaponReloadTime(ItemStack stack,EntityLivingBase living){
		return (int) (getData(stack).get("Reload time").getInt(1000) * TF2Attribute.getModifier("Reload Time", stack, 1,living));
	}
	
	public int getWeaponFirstReloadTime(ItemStack stack,EntityLivingBase living){
		return (int) (getData(stack).get("Reload time first").getInt(1000) * TF2Attribute.getModifier("Reload Time", stack, 1,living));
	}
	
	public boolean hasClip(ItemStack stack){
		return getData(stack).get("Reloads clip").getBoolean(true);
	}
	
	public int getWeaponClipSize(ItemStack stack,EntityLivingBase living){
		return (int) (getData(stack).get("Clip size").getInt(1) * TF2Attribute.getModifier("Clip Size", stack, 1,living));
	}
	
	public boolean IsReloadingFullClip(ItemStack stack){
		return getData(stack).get("Reloads full clip").getBoolean(false);
	}
	
	public boolean hasRandomCrits(ItemStack stack,Entity par3Entity){
		return par3Entity instanceof EntityPlayer&&getData(stack).get("Random crits").getBoolean(true);
	}
	
	public double getWeaponKnockback(ItemStack stack,EntityLivingBase living){
		return getData(stack).get("Knockback").getInt(0) * TF2Attribute.getModifier("Knockback", stack, 1,living);
	}
	public boolean rapidFireCrits(ItemStack stack){
		return getData(stack).get("Rapidfire crits").getBoolean(false);
	}
}
