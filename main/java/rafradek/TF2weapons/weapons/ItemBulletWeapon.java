package rafradek.TF2weapons.weapons;

import java.util.HashMap;
import java.util.Iterator;

import rafradek.TF2weapons.ClientProxy;
import rafradek.TF2weapons.TF2weapons;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemBulletWeapon extends ItemRangedWeapon {
	public static double lastStartX=90;
	public static double lastStartY=90;
	public static double lastStartZ=90;
	public static double lastEndX=990;
	public static double lastEndY=900;
	public static double lastEndZ=900;
	public static int maxRange=300;
	public static HashMap<Entity, float[]> lastShot= new HashMap<Entity, float[]>();
	
	public boolean use(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target)
    {
		if(super.use(stack, living, world, target)){
			
			DamageSource var22 = null;

            if(!world.isRemote && living != null)
            {
            	var22 = TF2weapons.causeBulletDamage(stack.getUnlocalizedName(), living, critical);
            	Iterator<Entity> iterator=lastShot.keySet().iterator();
	            while(iterator.hasNext()){
	            	Entity entity=(Entity) iterator.next();
	            	float distance = (float) Vec3.createVectorHelper(living.posX, living.posY, living.posZ).distanceTo(Vec3.createVectorHelper(entity.posX, entity.posY, entity.posZ));
	            	if(lastShot.get(entity) != null && lastShot.get(entity)[1] != 0){
	            		TF2weapons.dealDamage(entity, world, living, stack, this, critical, lastShot.get(entity)[1],var22,distance);
		            	distance=maxRange/distance;
		            	double distX=(living.posX-entity.posX)*distance;
		            	double distY=(living.posY-entity.posY)*distance;
		            	double distZ=(living.posZ-entity.posZ)*distance;
		            	if(entity != null && stack != null && lastShot.get(entity) != null){
		            		double knockbackAmount=(double)this.getWeaponKnockback(stack,living) *lastShot.get(entity)[1]* 0.016875D / this.maxRange;
			            	if (knockbackAmount > 0)
			                {
			                    if (distance > 0.0F)
			                    {
			                        entity.addVelocity(-(distX * knockbackAmount),-(distY * knockbackAmount), -(distZ * knockbackAmount));
			                    }
			                }
		            	}
	            	}
	            }
	            lastShot.clear();
            }
            
            return true;
		}
		return false;
    }
	public boolean showTracer(ItemStack stack){
		return true;
	}
	@Override
	public void shoot(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target, boolean critical) {
		
		double startX=0;
		double startY=0;
		double startZ=0;
		
		double pureX=0;
		double pureY=0;
		double pureZ=0;
		
		double endX=0;
		double endY=0;
		double endZ=0;
		
		double[] rand=TF2weapons.radiusRandom3D(this.getWeaponSpread(stack,living), world.rand);
		
		if(target==null){
			startX=living.posX;// - (double)(MathHelper.cos(living.rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
			startY=living.posY + living.getEyeHeight();
			startZ=living.posZ;// - (double)(MathHelper.sin(living.rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
			
			//double[] rand=TF2weapons.radiusRandom2D(this.getWeaponSpread(stack), world.rand);
			
			//float spreadPitch = (float) (living.rotationPitch / 180 + rand[1]);
			//float spreadYaw = (float) (living.rotationYaw / 180 + rand[0]*(90/Math.max(90-Math.abs(spreadPitch*180),0.0001f)));
			//System.out.println("Rot: "+living.rotationYawHead+" "+living.rotationPitch);
			float spreadPitch = living.rotationPitch / 180;
			float spreadYaw = living.rotationYawHead / 180;
		
			endX=(double)(-MathHelper.sin(spreadYaw * (float)Math.PI) * MathHelper.cos(spreadPitch * (float)Math.PI));
			endY=(double)(-MathHelper.sin(spreadPitch * (float)Math.PI));
			endZ=(double)(MathHelper.cos(spreadYaw * (float)Math.PI) * MathHelper.cos(spreadPitch * (float)Math.PI));
			
			float var9 = MathHelper.sqrt_double(endX * endX + endY * endY + endZ * endZ);
			//float[] ratioX= this.calculateRatioX(living.rotationYaw, living.rotationPitch);
			//float[] ratioY= this.calculateRatioY(living.rotationYaw, living.rotationPitch);
			//float wrapAngledYaw=MathHelper.wrapAngleTo180_float(living.rotationYaw);
			//float fixedYaw=Math.max(Math.abs(wrapAngledYaw),90)-Math.min(Math.abs(wrapAngledYaw),90);
			
			endX = (endX / (double)var9 + rand[0]) * maxRange /*+ (rand[0]*ratioX[0])((fixedYaw/90)+(1-fixedYaw/90)*(-living.rotationPitch/90))*this.positive(wrapAngledYaw)*40*/;
			endY = (endY / (double)var9 + rand[1]) * maxRange /*+ (rand[1]*ratioY[1])(0.5-Math.abs(spreadPitch))*80*40*/;
			endZ = (endZ / (double)var9 + rand[2]) * maxRange /*+ ((ratioX[2]>ratioY[2]?rand[0]:rand[1])*(ratioX[2]+ratioY[2]))(rand[0]*ratioX[2] + rand[1]*ratioY[2])((1-fixedYaw/90)+(fixedYaw/90)*(-living.rotationPitch/90))*this.positive(wrapAngledYaw)*40*/;
			double distance=maxRange/Math.sqrt(Math.pow(endX, 2)+Math.pow(endY, 2)+Math.pow(endZ,2));
			//System.out.println(ratioX[0]+" "+ratioX[1]+" "+ratioX[2]+" "+ratioY[0]+" "+ratioY[1]+" "+ratioY[2]);
			pureX = endX-rand[0]*maxRange;
			pureY = endY-rand[1]*maxRange;
			pureZ = endZ-rand[2]*maxRange;
			
			endX *= distance;
			endY *= distance;
			endZ *= distance;
			endX += startX;
			endY += startY;
			endZ += startZ;
			
			
			
		} else {
			startY = living.posY + (double)living.getEyeHeight() - 0.10000000149011612D;
			endX = target.posX - living.posX;
	        double var8 = target.posY + (double)target.getEyeHeight() - 0.699999988079071D - startY;
	        endZ = target.posZ - living.posZ;
	        double var12 = (double)MathHelper.sqrt_double(endX * endX + endZ * endZ);

	        if (var12 >= 1.0E-7D)
	        {
	            float var14 = (float)(Math.atan2(endZ, endX) * 180.0D / Math.PI) - 90.0F;
	            float var15 = (float)(-(Math.atan2(var8, var12) * 180.0D / Math.PI));
	            double var16 = endX / var12;
	            double var18 = endZ / var12;
	            startX=living.posX + var16;
	            startZ=living.posZ + var18;
	            float var20 = (float)var12 * 0.2F;
	            
	            endY=var8 + (double)var20;
	            
	            float var9 = MathHelper.sqrt_double(endX * endX + endY * endY + endZ * endZ);
	            endX = (endX / (double)var9 + rand[0]) * maxRange;
				endY = (endY / (double)var9 + rand[1]) * maxRange;
				endZ = (endZ / (double)var9 + rand[2]) * maxRange;
				
				double distance=maxRange/Math.sqrt(Math.pow(endX, 2)+Math.pow(endY, 2)+Math.pow(endZ,2));
				pureX = endX-rand[0]*maxRange;
				pureY = endY-rand[1]*maxRange;
				pureZ = endZ-rand[2]*maxRange;
				
				endX *= distance;
				endY *= distance;
				endZ *= distance;
				endX += startX;
				endY += startY;
				endZ += startZ;
	        }
		}
		if(world.isRemote&&this.showTracer(stack)){
		ClientProxy.spawnBulletParticle(world,startX- (double)(MathHelper.cos(living.rotationYaw / 180.0F * (float)Math.PI) * 0.16F), startY, 
				startZ- (double)(MathHelper.sin(living.rotationYaw / 180.0F * (float)Math.PI) * 0.16F), endX, endY, endZ, 20,critical?TF2weapons.getTeam(living)+1:0);
		}
		
        MovingObjectPosition var4=TF2weapons.pierce(world, living, startX, startY, startZ, endX, endY, endZ,this.canHeadshot(stack));
        if (var4 != null)
        {	
        	
            if (var4.entityHit != null)
            {
                float distance = 0;

                if (living != null)
                {
                    distance = (float) Vec3.createVectorHelper(living.posX, living.posY, living.posZ).distanceTo(Vec3.createVectorHelper(var4.entityHit.posX, var4.entityHit.posY, var4.entityHit.posZ));
                    //System.out.println(distance);
                }
                
                if (!world.isRemote)
                {
                	if(!lastShot.containsKey(var4.entityHit)||lastShot.get(var4.entityHit)==null){
                		lastShot.put(var4.entityHit, new float[2]);
                	}
                	//System.out.println(var4.hitInfo);
                	if(var4.hitInfo!=null){
                		critical=true;
                		ItemRangedWeapon.critical=true;
                	}
                	float[] values=lastShot.get(var4.entityHit);
                	values[0]++;
                	values[1]+=TF2weapons.calculateDamage(var4.entityHit,world, living, stack, critical, distance);
                }
            }
        }
	}
	/*public boolean checkHeadshot(World world, Entity living,
			ItemStack stack, Vec3 hitVec) {
		double ymax=living.boundingBox.maxY;
		AxisAlignedBB head=AxisAlignedBB.getBoundingBox(living.posX-0.21, ymax-0.21, living.posZ-0.21,living.posX+0.21, ymax+0.21, living.posZ+0.21);
		System.out.println("Trafienie: "+Math.abs(ymax-hitVec.yCoord));
		
		return Math.abs(ymax-hitVec.yCoord)<0.205;
	}*/
	public boolean canHeadshot(ItemStack stack) {
		// TODO Auto-generated method stub
		return false;
	}
	public int positive(float value){
		if(value>0)
			return 1;
		return -1;
	}
	public float[] calculateRatioX(float yaw,float pitch){
		float[] result=new float[3];
		float angledYaw=Math.abs(MathHelper.wrapAngleTo180_float(yaw));
		float distanceYaw=Math.max(angledYaw,90)-Math.min(angledYaw,90);
		result[0]=(distanceYaw/90)+(1-distanceYaw/90)*(-pitch/90);
		result[2]=(1-distanceYaw/90);//+(1-distanceYaw/90)*(-pitch/90);
		result[1]=0;
		return result;
	}
	public float[] calculateRatioY(float yaw,float pitch){
		float[] result=new float[3];
		float angledYaw=Math.abs(MathHelper.wrapAngleTo180_float(yaw));
		float distanceYaw=Math.max(angledYaw,90)-Math.min(angledYaw,90);
		result[0]=0;
		result[2]=(distanceYaw/90)*(-pitch/90);
		result[1]=1-Math.abs(pitch)/90;
		return result;
	}

	@Override
	public void usingItemFirst(ItemStack stack, World world,
			EntityLivingBase entity) {
		// TODO Auto-generated method stub
		
	}

}
