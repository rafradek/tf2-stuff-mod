package rafradek.TF2weapons.weapons;

import java.util.UUID;

import rafradek.TF2weapons.TF2Attribute;
import rafradek.TF2weapons.characters.EntitySniper;
import rafradek.TF2weapons.characters.EntityTF2Character;
import rafradek.TF2weapons.message.TF2ActionHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemSniperRifle extends ItemBulletWeapon {
	public static UUID slowdownUUID=UUID.fromString("12843092-A5D6-BBCD-3D4F-A3DD4D8C65A9");
	public static AttributeModifier slowdown = new AttributeModifier(slowdownUUID, "sniper slowdown", -0.73D, 2);
	public float oldSensitive;
	public boolean canAltFire(World worldObj, EntityLivingBase player,
			ItemStack item) {
		return item.stackTagCompound.getShort("reload") <= 0;
	}
	public boolean use(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target){
		if(living instanceof EntityPlayer||stack.getTagCompound().getBoolean("WaitProper")){
			super.use(stack, living, world, target);
			this.disableZoom(stack, living);
			stack.getTagCompound().setBoolean("WaitProper", false);
		}
		else{
			stack.getTagCompound().setBoolean("WaitProper", true);
			this.altUse(stack, living, world);
			stack.stackTagCompound.setInteger("reload", 2500);
		}
		return true;
	}
	public void altUse(ItemStack stack, EntityLivingBase living,
			World world) {
		if(!stack.getTagCompound().getBoolean("Zoomed")){
			stack.getTagCompound().setBoolean("Zoomed", true);
			if(living instanceof EntityPlayer&&world.isRemote&&this.oldSensitive==0){
				this.oldSensitive=Minecraft.getMinecraft().gameSettings.mouseSensitivity;
				Minecraft.getMinecraft().gameSettings.mouseSensitivity*=0.4f;
			}
			if(living.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getModifier(slowdownUUID)==null)
				living.getEntityAttribute(SharedMonsterAttributes.movementSpeed).applyModifier(slowdown);
		}
		else{
			this.disableZoom(stack, living);
		}
	}
	public void disableZoom(ItemStack stack,EntityLivingBase living){
		if(living instanceof EntityPlayer&&living.worldObj.isRemote&&this.oldSensitive!=0&&stack.getTagCompound().getBoolean("Zoomed")){
			Minecraft.getMinecraft().gameSettings.mouseSensitivity=this.oldSensitive;
			this.oldSensitive=0;
		}
		stack.getTagCompound().setInteger("ZoomTime", 0);
		stack.getTagCompound().setBoolean("Zoomed", false);
		living.getEntityAttribute(SharedMonsterAttributes.movementSpeed).removeModifier(slowdown);
	}
	public boolean canHeadshot(ItemStack stack) {
		// TODO Auto-generated method stub
		return stack.getTagCompound().getInteger("ZoomTime")>4;
	}
	public boolean showTracer(ItemStack stack){
		return false;
	}
	public float getWeaponDamage(ItemStack stack,EntityLivingBase living){
		return super.getWeaponDamage(stack, living)*this.getZoomBonus(stack,living);
	}
	
	public float getWeaponMaxDamage(ItemStack stack,EntityLivingBase living) {
		return super.getWeaponMaxDamage(stack, living)*this.getZoomBonus(stack,living);
	}
	
	public float getWeaponMinDamage(ItemStack stack,EntityLivingBase living){
		return super.getWeaponMinDamage(stack, living)*this.getZoomBonus(stack,living);
	}
	public float getZoomBonus(ItemStack stack,EntityLivingBase living){
		return 1+Math.max(0,(stack.stackTagCompound.getInteger("ZoomTime")-20)/((getChargeTime(stack,living)-20)/2));
	}
	public static float getChargeTime(ItemStack stack,EntityLivingBase living){
		return 66 * TF2Attribute.getModifier("Charge", stack, 1,living);
	}
	public short getAltFiringSpeed(ItemStack item, EntityLivingBase player) {
		return 400;
	}
	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
	{
		super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
		if(par1ItemStack.stackTagCompound.getBoolean("Zoomed")){
			if(!par5){
				this.disableZoom(par1ItemStack,(EntityLivingBase) par3Entity);
			}
			else if(par1ItemStack.getTagCompound().getInteger("ZoomTime")<66){
				par1ItemStack.getTagCompound().setInteger("ZoomTime", par1ItemStack.getTagCompound().getInteger("ZoomTime")+1);
			}
		}
		if(par3Entity instanceof EntitySniper&&((EntitySniper) par3Entity).getAttackTarget()!=null&&par1ItemStack.getTagCompound().getBoolean("WaitProper")){
			if(((EntitySniper) par3Entity).getHealth()<8&&par1ItemStack.getTagCompound().getInteger("reload")>250){
				par1ItemStack.getTagCompound().setInteger("reload", 250);
			}
			/*else if(par1ItemStack.getTagCompound().getInteger("reload")<=100&&!((EntitySniper)par3Entity).attack.lookingAt(1)){
				par1ItemStack.getTagCompound().setInteger("reload", 100);
			}*/
			//par1ItemStack.getTagCompound().setBoolean("WaitProper", true);
		}
	}
	/*public double getDiff(EntityTF2Character mob){
		 if(mob.getAttackTarget()!=null){
			mob.attack.lookingAt(mob.getAttackTarget(),2)
			double mX=mob.getAttackTarget().posX-mob.getAttackTarget().lastTickPosX;
			double mY=mob.getAttackTarget().posY-mob.getAttackTarget().lastTickPosY;
			double mZ=mob.getAttackTarget().posZ-mob.getAttackTarget().lastTickPosZ;
			double totalMotion=Math.sqrt(mX*mX+mY*mY+mZ*mZ);
			System.out.println("Odskok: "+totalMotion);
			return totalMotion;
		}
		 return 0;
	}*/
	public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player)
    {
		super.onDroppedByPlayer(item, player);
    	this.disableZoom(item, player);
        return true;
    }
}
