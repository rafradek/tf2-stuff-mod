package rafradek.TF2weapons.weapons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rafradek.TF2weapons.ClientProxy;
import rafradek.TF2weapons.MapList;
import rafradek.TF2weapons.TF2Attribute;
import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.message.TF2ActionHandler;
import rafradek.TF2weapons.message.TF2Message;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.common.config.ConfigCategory;

public abstract class ItemUsable extends Item
{
    //public ConfigCategory data;
    public String render;
    public static int sps;
    public static int tickleft;
    public static boolean addedIcons;
    public static HashMap<EntityLivingBase,float[]> lastDamage= new HashMap<EntityLivingBase,float[]>();
    public ItemUsable()
    {
    	this.setCreativeTab(TF2weapons.tabtf2);
        /*this.buildInAttributes=new NBTTagCompound();
        if(getData(stack).containsKey("Attributes")){
        	String[] attributes=getData(stack).get("Attributes").getStringList();
        	for(int i=0; i < attributes.length; i++) {
        		String attributeName=attributes[i].split(":")[0];
        		String attributeValue=attributes[i].split(":")[1];
        		
        		Iterator<String> iterator = MapList.nameToAttribute.keySet().iterator();
        		System.out.println(MapList.nameToAttribute.size());
        		boolean has=false;
        		while(iterator.hasNext()) {
        			if(iterator.next().equals(attributeName)){
        				buildInAttributes.setFloat(String.valueOf(MapList.nameToAttribute.get(attributeName).id), Float.parseFloat(attributeValue));
        				has=true;
        			}
        		}
        		if(has==false){
        			buildInAttributes.setFloat(attributeName, Float.parseFloat(attributeValue));
        		}
        	}
        }
        this.data=data;*/
        //this.setUnlocalizedName(getData(stack).get("Name").getString());
    	this.setTextureName("tf2weapons:minigun");
        this.setUnlocalizedName("tf2usable");
        this.setMaxStackSize(1);
        // TODO Auto-generated constructor stub
    }
    public abstract boolean use(ItemStack stack, EntityLivingBase living, World world, EntityLivingBase target);
    
    public boolean startUse(ItemStack stack, EntityLivingBase living, World world) {
		return false;
	}
    
    public boolean endUse(ItemStack stack, EntityLivingBase living, World world) {
		return false;
	}
    
    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
   	{
    	if(!par1ItemStack.hasTagCompound()&&par3Entity instanceof EntityPlayer){
    		((EntityPlayer)par3Entity).inventory.dropAllItems();
    		return;
    	}
    	if(!par1ItemStack.stackTagCompound.getBoolean("active")&&par5&&TF2ActionHandler.playerAction.get().containsKey(par3Entity)&&(TF2ActionHandler.playerAction.get().get(par3Entity)&3)>0){
			par1ItemStack.stackTagCompound.setBoolean("active", true);
			this.startUse(par1ItemStack,(EntityLivingBase) par3Entity,par2World);
		}
		else if(par1ItemStack.stackTagCompound.getBoolean("active")&&(!par5||(!TF2ActionHandler.playerAction.get().containsKey(par3Entity)||(TF2ActionHandler.playerAction.get().get(par3Entity)&3)==0))){
			par1ItemStack.stackTagCompound.setBoolean("active", false);
			this.endUse(par1ItemStack,(EntityLivingBase) par3Entity,par2World);
		}
   		if(par1ItemStack.hasTagCompound()&&par1ItemStack.stackTagCompound.getShort("lastfire")>0){
   			par1ItemStack.stackTagCompound.setShort("lastfire", (short) (par1ItemStack.stackTagCompound.getShort("lastfire") - 50));
   		}
   		if(!par5&&!(par3Entity instanceof EntityPlayer&&((EntityPlayer)par3Entity).capabilities.isCreativeMode)){
   			par1ItemStack.stackTagCompound.setShort("reload", (short) (800));
   		}
   	}
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player)
    {
    	item.stackTagCompound.removeTag("active");
        return true;
    }
    public abstract void usingItemFirst(ItemStack stack, World world,
			EntityLivingBase entity);
	public abstract boolean canFire(World world, EntityLivingBase living, ItemStack stack);
    public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player)
    {
        return true;
    }
	public abstract boolean fireTick(ItemStack stack, EntityLivingBase living, World world);
	public abstract boolean altFireTick(ItemStack stack, EntityLivingBase living, World world);
	
	@SideOnly(Side.CLIENT)
    public CreativeTabs getCreativeTab()
    {
        return TF2weapons.tabtf2;
    }
	
	public void registerIcons(IIconRegister par1IconRegister)
    {
		this.itemIcon = par1IconRegister.registerIcon(this.getIconString());
		if(addedIcons) return;
		Iterator<String> iterator=MapList.nameToCC.keySet().iterator();
		addedIcons=true;
		while(iterator.hasNext()){
			String name=iterator.next();
			//System.out.println(MapList.nameToCC.get(name).get("Render").getString()+" "+name);
			MapList.nameToIcon.put(name, par1IconRegister.registerIcon(MapList.nameToCC.get(name).get("Render").getString()));
		}
        //this.itemIcon = par1IconRegister.registerIcon(getData(stack).get("Render").getString());
    }
	/*@SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }
	public int getRenderPasses(int metadata)
    {
        return 1;
    }*/
	public String getItemStackDisplayName(ItemStack p_77653_1_)
    {
		return ItemUsable.getData(p_77653_1_).get("Name").getString();
    }
	@SideOnly(Side.CLIENT)
    public IIcon getIconIndex(ItemStack stack)
    {
		//System.out.println("trying: "+this.iconString+" "+stack.hasTagCompound());
		if(stack.stackTagCompound!=null) {
			IIcon icon=MapList.nameToIcon.get(stack.stackTagCompound.getString("Type"));
			if(icon!=null){
				return icon;
			}
		}
		return MapList.nameToIcon.get("Minigun");
    }
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(ItemStack stack,int pass)
    {
		//System.out.println("trying: "+this.iconString+" "+stack.hasTagCompound());
		if(stack.stackTagCompound!=null) {
			IIcon icon=MapList.nameToIcon.get(stack.stackTagCompound.getString("Type"));
			if(icon!=null){
				return icon;
			}
		}
		return MapList.nameToIcon.get("Minigun");
    }
	
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par2List, boolean par4)
    {	
		/*if (!par1ItemStack.hasTagCompound()) {
			par1ItemStack.stackTagCompound=new NBTTagCompound();
			par1ItemStack.stackTagCompound.setTag("Attributes", (NBTTagCompound) ((ItemUsable)par1ItemStack.getItem()).buildInAttributes.copy());
		}*/
        if (par1ItemStack.hasTagCompound())
        {
        	NBTTagCompound attributeList=par1ItemStack.getTagCompound().getCompoundTag("Attributes");
    		Iterator iterator=attributeList.func_150296_c().iterator();
    		while(iterator.hasNext()){
    			String name=(String) iterator.next();
    			NBTBase tag=attributeList.getTag(name);
    			if(tag instanceof NBTTagFloat){
    				NBTTagFloat tagFloat=(NBTTagFloat) tag;
    				TF2Attribute attribute=TF2Attribute.attributes[Integer.parseInt(name)];
    				String value=tagFloat.toString();
            	
    				if(attribute.typeOfValue.equals("Percentage")) 
    					value=Integer.toString((int)((tagFloat.func_150288_h()-1)*100))+"%";
    				else if(attribute.typeOfValue.equals("Inverted_Percentage")) 
    					value=Integer.toString((int)((1-tagFloat.func_150288_h())*100))+"%";
    				else if(attribute.typeOfValue.equals("Additive")) 
    					value=Integer.toString((int)(tagFloat.func_150288_h()));
    				EnumChatFormatting color=attribute.state == 1 ? EnumChatFormatting.AQUA : (attribute.state == -1 ? EnumChatFormatting.RED : EnumChatFormatting.WHITE);
    				par2List.add(color+StatCollector.translateToLocalFormatted("weaponAttribute."+attribute.name, new Object[] {value}));
    			}
            }
        }
    }
	
	public int getFiringSpeed(ItemStack stack,EntityLivingBase living){
		return (int) (getData(stack).get("Firing speed").getInt(1000) * TF2Attribute.getModifier("Fire Rate", stack, 1,living));
	}
	public static ConfigCategory getData(ItemStack stack){
		if(stack != null&& stack.hasTagCompound()){
			return MapList.nameToCC.get(stack.stackTagCompound.getString("Type"));
		}
		return null;
	}
	public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List)
    {
		//System.out.println(this.getCreativeTab());
		Iterator<String> iterator=MapList.nameToCC.keySet().iterator();
		while(iterator.hasNext()){
			String name=iterator.next();
			Item item=MapList.weaponClasses.get(MapList.nameToCC.get(name).get("Class").getString());
			if(item==this){
				par3List.add(getNewStack(name));
			}
		}
    }
	public static ItemStack getNewStack(String type){
		//System.out.println("Dano: "+type+" "+MapList.weaponClasses.get(MapList.nameToCC.get(type).get("Class").getString())+" "+Thread.currentThread().getName());
		ItemStack stack=new ItemStack(MapList.weaponClasses.get(MapList.nameToCC.get(type).get("Class").getString()));
		stack.stackTagCompound=(NBTTagCompound) MapList.buildInAttributes.get(type).copy();
		//System.out.println(stack.toString());
		return stack;
	}
	public static void tick(boolean client){
		Map<EntityLivingBase,Integer> map=TF2ActionHandler.playerAction.get();
		if(map.isEmpty()) return;
		Iterator<EntityLivingBase> iterator=map.keySet().iterator();
		while(iterator.hasNext())
    	{
    		EntityLivingBase player = iterator.next();
    		if(player.isDead||player.deathTime>0){
    			iterator.remove();
    			continue;
    		}
    		int state=map.get(player);
    		//System.out.println("graczin"+state);
    		ItemStack item = player.getHeldItem();
    		if (item != null&&item.getItem() instanceof ItemUsable)
            {
    			if (item.stackTagCompound.getShort("reloadd") > 0)
                {
                    item.stackTagCompound.setShort("reloadd", (short) (item.stackTagCompound.getShort("reloadd") - 50));
                }
    			if (item.stackTagCompound.getShort("reload") > 0)
	            {
	            	item.stackTagCompound.setShort("reload", (short) (item.stackTagCompound.getShort("reload") - 50));
	            }
    			if (item.stackTagCompound.getShort("reloadalt") > 0)
	            {
	            	item.stackTagCompound.setShort("reloadalt", (short) (item.stackTagCompound.getShort("reloadalt") - 50));
	            }
    			if(player instanceof EntityPlayerMP){
    				((EntityPlayerMP)player).isChangingQuantityOnly=state>0;
    			}
    			if((state&1)!=0){
    				//System.out.println("firin");
		            ((ItemUsable)item.getItem()).fireTick(item, player, player.worldObj);
		            /*if (!item.hasTagCompound())
		            {
		                item.stackTagCompound = new NBTTagCompound();
		                item.stackTagCompound.setTag("Attributes", (NBTTagCompound) ((ItemUsable)item.getItem()).buildInAttributes.copy());
		            }*/
                	while(item.stackTagCompound.getShort("reload") <= 0&&((ItemUsable)item.getItem()).canFire(player.worldObj, player, item)){
                	
                	//System.out.println("PLAJERRRR: "+player);
                		item.stackTagCompound.setShort("reload", (short) (((ItemUsable)item.getItem()).getFiringSpeed(item,player)+item.stackTagCompound.getShort("reload")));
                		((ItemUsable)item.getItem()).use(item, player, player.worldObj, null);
                		item.stackTagCompound.setShort("lastfire",(short) 1250);
                		if(!client)
                			sps++;
                		if (item.getItem() instanceof ItemRangedWeapon)
		                {
		                	item.stackTagCompound.setShort("reloadd", (short) 0);
		                	if((state&8)!=0){
		                	state-=8;
		                	}
		                }
                	}
    			}
    			if((state&2)!=0){
    				((ItemUsable)item.getItem()).altFireTick(item, player, player.worldObj);
    				while(item.stackTagCompound.getShort("reloadalt") <= 0&&((ItemUsable)item.getItem()).canAltFire(player.worldObj, player, item)){
                	
                	//System.out.println("PLAJERRRR: "+player);
                		item.stackTagCompound.setShort("reloadalt", (short) (((ItemUsable)item.getItem()).getAltFiringSpeed(item,player)+item.stackTagCompound.getShort("reloadalt")));
                		((ItemUsable)item.getItem()).altUse(item, player, player.worldObj);
                		if(!client)
                			sps++;
                		if (item.getItem() instanceof ItemRangedWeapon)
		                {
		                	item.stackTagCompound.setShort("reloadd", (short) 0);
		                	if((state&8)!=0){
		                	state-=8;
		                	}
		                }
                	}
    			}
    			
    			if((!client||player!=Minecraft.getMinecraft().thePlayer)&&item.getItem() instanceof ItemRangedWeapon && ((ItemRangedWeapon)item.getItem()).hasClip(item)){
    				boolean firstReload=false;
    				if((state&4)!=0&&(state&8)==0&&item.getItemDamage()!=0&& item.stackTagCompound.getShort("reloadd")<=0
    						&&(item.stackTagCompound.getShort("reload")<=0||((ItemRangedWeapon)item.getItem()).IsReloadingFullClip(item))){
    					state+=8;
    					firstReload=true;
    					item.stackTagCompound.setShort("reloadd", (short) ((ItemRangedWeapon)item.getItem()).getWeaponFirstReloadTime(item,player));
    					
    					if(client&&((ItemRangedWeapon)item.getItem()).IsReloadingFullClip(item)){
    						TF2weapons.proxy.playReloadSound(player,item);
                        }
    				}
    				else if(item.stackTagCompound.getShort("reload")<=0||((ItemRangedWeapon)item.getItem()).IsReloadingFullClip(item)){
                        while ((state&8)!=0&&item.stackTagCompound.getShort("reloadd") <= 0 && item.getItemDamage()!=0)
                        {
                            if(((ItemRangedWeapon)item.getItem()).IsReloadingFullClip(item)){
                            	item.setItemDamage(0);
                            } else {
                            	item.setItemDamage(item.getItemDamage() - 1); 
                            	TF2weapons.proxy.playReloadSound(player,item);
                            }
                            if(!client&&player instanceof EntityPlayerMP)
                            	TF2weapons.network.sendTo(new TF2Message.UseMessage(item.getItemDamage(), true), (EntityPlayerMP) player);
                            item.stackTagCompound.setShort("reloadd", (short) (((ItemRangedWeapon)item.getItem()).getWeaponReloadTime(item,player) + item.stackTagCompound.getShort("reloadd")));
                            if(item.getItemDamage()==0){
                            	state-=8;
                            	item.stackTagCompound.setShort("reloadd", (short) 0);
                            }
                        }
    				}
                }
            }
    		else {
    			player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).removeModifier(ItemMinigun.slowdown);
    			player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).removeModifier(ItemSniperRifle.slowdown);
    		}
    		map.put(player, state);
    	}
	}
	public boolean canAltFire(World worldObj, EntityLivingBase player,
			ItemStack item) {
		// TODO Auto-generated method stub
		return false;
	}
	public void altUse(ItemStack stack, EntityLivingBase living,
			World world) {
		
	}
	public short getAltFiringSpeed(ItemStack item, EntityLivingBase player) {
		return Short.MAX_VALUE;
	}
}
