package rafradek.TF2weapons.weapons;

import rafradek.TF2weapons.ClientProxy;
import net.minecraft.client.audio.MovingSound;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.ConfigCategory;

public class MinigunSound extends MovingSound {
	public EntityLivingBase entity;
	public int type;
	public ConfigCategory conf;
	public boolean endsnextTick;
	public MinigunSound(ResourceLocation p_i45104_1_,EntityLivingBase entity,int type,ConfigCategory conf) {
		super(p_i45104_1_);
		this.type=type;
		this.entity=entity;
		this.conf=conf;
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(this.endsnextTick){
			this.setDone();
		}
		this.xPosF=(float) entity.posX;
		this.yPosF=(float) entity.posY;
		this.zPosF=(float) entity.posZ;
		if(/*!(entity instanceof EntityPlayer && ((EntityPlayer)entity).inventory.currentItem != slot)*/ItemUsable.getData(entity.getHeldItem())!=conf || this.entity.isDead){
			this.setDone();
		}
	}
	public void setDone(){
		ClientProxy.fireSounds.remove(entity);
		this.donePlaying=true;
	}
}
