package rafradek.TF2weapons.weapons;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import rafradek.TF2weapons.TF2weapons;
import rafradek.TF2weapons.projectiles.EntityProjectileBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class TF2Explosion extends Explosion{

    /** whether or not the explosion sets fire to blocks around it */
    public boolean isFlaming;
    /** whether or not this explosion spawns smoke particles */
    public boolean isSmoking = true;
    private int field_77289_h = 16;
    private Random explosionRNG = new Random();
    public World worldObj;
    /** A list of ChunkPositions of blocks affected by this explosion */
    public List affectedBlockPositions = new ArrayList();
	private float dmg;
	private Entity directHit;
    
    public TF2Explosion(World p_i1948_1_, Entity p_i1948_2_,
			double p_i1948_3_, double p_i1948_5_, double p_i1948_7_,
			float p_i1948_9_, float dmg, Entity direct) {
    	super(p_i1948_1_, p_i1948_2_, p_i1948_3_, p_i1948_5_, p_i1948_7_, p_i1948_9_);
    	this.worldObj=p_i1948_1_;
    	this.dmg=dmg;
    	this.directHit=direct;
	}
    /**
     * Does the first part of the explosion (destroy blocks)
     */
    @Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void doExplosionA()
    {
        float f = this.explosionSize;
        HashSet hashset = new HashSet();
        int i;
        int j;
        int k;
        double d5;
        double d6;
        double d7;
        if(TF2weapons.destTerrain){
		    for (i = 0; i < this.field_77289_h; ++i)
		    {
		        for (j = 0; j < this.field_77289_h; ++j)
		        {
		            for (k = 0; k < this.field_77289_h; ++k)
		            {
		                if (i == 0 || i == this.field_77289_h - 1 || j == 0 || j == this.field_77289_h - 1 || k == 0 || k == this.field_77289_h - 1)
		                {
		                    double d0 = i / (this.field_77289_h - 1.0F) * 2.0F - 1.0F;
		                    double d1 = j / (this.field_77289_h - 1.0F) * 2.0F - 1.0F;
		                    double d2 = k / (this.field_77289_h - 1.0F) * 2.0F - 1.0F;
		                    double d3 = Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
		                    d0 /= d3;
		                    d1 /= d3;
		                    d2 /= d3;
		                    float f1 = this.explosionSize * (0.7F + this.worldObj.rand.nextFloat() * 0.6F);
		                    d5 = this.explosionX;
		                    d6 = this.explosionY;
		                    d7 = this.explosionZ;
		
		                    for (float f2 = 0.3F; f1 > 0.0F; f1 -= f2 * 0.75F)
		                    {
		                        int j1 = MathHelper.floor_double(d5);
		                        int k1 = MathHelper.floor_double(d6);
		                        int l1 = MathHelper.floor_double(d7);
		                        Block block = this.worldObj.getBlock(j1, k1, l1);
		
		                        if (block.getMaterial() != Material.air)
		                        {
		                            float f3 = this.exploder != null ? this.exploder.func_145772_a(this, this.worldObj, j1, k1, l1, block) : block.getExplosionResistance(this.exploder, worldObj, j1, k1, l1, explosionX, explosionY, explosionZ);
		                            f1 -= (f3 + 0.3F) * f2;
		                        }
		
		                        if (f1 > 0.0F && (this.exploder == null || this.exploder.func_145774_a(this, this.worldObj, j1, k1, l1, block, f1)))
		                        {
		                            hashset.add(new ChunkPosition(j1, k1, l1));
		                        }
		
		                        d5 += d0 * f2;
		                        d6 += d1 * f2;
		                        d7 += d2 * f2;
		                    }
		                }
		            }
		        }
		    }
        }
        this.affectedBlockPositions.addAll(hashset);
        this.explosionSize *= 2.0F;
        i = MathHelper.floor_double(this.explosionX - this.explosionSize - 1.0D);
        j = MathHelper.floor_double(this.explosionX + this.explosionSize + 1.0D);
        k = MathHelper.floor_double(this.explosionY - this.explosionSize - 1.0D);
        int i2 = MathHelper.floor_double(this.explosionY + this.explosionSize + 1.0D);
        int l = MathHelper.floor_double(this.explosionZ - this.explosionSize - 1.0D);
        int j2 = MathHelper.floor_double(this.explosionZ + this.explosionSize + 1.0D);
        List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this.exploder, AxisAlignedBB.getBoundingBox(i, k, l, j, i2, j2));
        Vec3 vec3 = Vec3.createVectorHelper(this.explosionX, this.explosionY, this.explosionZ);
        int livingEntities=0;
        for (Object obj:list){
        	if(obj instanceof EntityLivingBase){
        		livingEntities++;
        	}
        }
        for (int i1 = 0; i1 < list.size(); ++i1)
        {
            Entity entity = (Entity)list.get(i1);
            double d4 = entity.getDistance(this.explosionX, this.explosionY, this.explosionZ) / this.explosionSize*0.5;

            if (d4 <= 0.5D)
            {
            	boolean isExploder=this.getExplosivePlacedBy()==entity;
            	boolean expJump=isExploder&&!entity.onGround&&livingEntities==1;
            	System.out.println("jump: "+expJump);
                d5 = entity.posX - this.explosionX;
                d6 = entity.posY + (isExploder?entity.getEyeHeight():(entity.boundingBox.maxY-entity.boundingBox.minY)/2)- this.explosionY;
                d7 = entity.posZ - this.explosionZ;
                double d9 = MathHelper.sqrt_double(d5 * d5 + d6 * d6 + d7 * d7);

                if (d9 != 0.0D)
                {
                    d5 /= d9;
                    d6 /= d9;
                    d7 /= d9;
                    //float explMod=(this.explosionSize/4);
                    d5 *= 1.55;
                    d6 *= 1.55;
                    d7 *= 1.55;
                    double d10 = this.getBlockDensity(vec3, entity.boundingBox);
                    double d11 = (1D - d4) * d10;
                    //System.out.println("multiplier: "+d11+" damage: "+this.dmg);
                    if(entity==this.directHit){
                    	d11=1;
                    }
                    if(d11==0){
                    	continue;
                    }
                    if(expJump){
                    	entity.fallDistance-=d6*8-1;
                    }
                    entity.attackEntityFrom(DamageSource.setExplosionSource(this), (float) (this.dmg*d11/(expJump?2:1)));
                    double d8 = EnchantmentProtection.func_92092_a(entity, d11);
                    //System.out.println("d: "+ d5 * d8+" "+d6 * d8+" "+d7 * d8);
                    entity.addVelocity(d5, d6, d7);
                   // entity.addVelocity(2, 2, 2);
                    if (entity instanceof EntityPlayer)
                    {
                        this.func_77277_b().put(entity, Vec3.createVectorHelper(d5 * d11, d6 * d11, d7 * d11));
                    }
                }
            }
        }

        this.explosionSize = f;
    }

    /**
     * Does the second part of the explosion (sound, particles, drop spawn)
     */
    @Override
	public void doExplosionB(boolean p_77279_1_)
    {
        this.worldObj.playSoundEffect(this.explosionX, this.explosionY, this.explosionZ, "random.explode", 4.0F, (1.0F + (this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.2F) * 0.7F);

        if (this.explosionSize >= 2.0F && this.isSmoking)
        {
            this.worldObj.spawnParticle("hugeexplosion", this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D);
        }
        else
        {
            this.worldObj.spawnParticle("largeexplode", this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D);
        }

        Iterator iterator;
        ChunkPosition chunkposition;
        int i;
        int j;
        int k;
        Block block;

        if (this.isSmoking&&!TF2weapons.destTerrain)
        {
            iterator = this.affectedBlockPositions.iterator();

            while (iterator.hasNext())
            {
                chunkposition = (ChunkPosition)iterator.next();
                i = chunkposition.chunkPosX;
                j = chunkposition.chunkPosY;
                k = chunkposition.chunkPosZ;
                block = this.worldObj.getBlock(i, j, k);

                if (p_77279_1_)
                {
                    double d0 = i + this.worldObj.rand.nextFloat();
                    double d1 = j + this.worldObj.rand.nextFloat();
                    double d2 = k + this.worldObj.rand.nextFloat();
                    double d3 = d0 - this.explosionX;
                    double d4 = d1 - this.explosionY;
                    double d5 = d2 - this.explosionZ;
                    double d6 = MathHelper.sqrt_double(d3 * d3 + d4 * d4 + d5 * d5);
                    d3 /= d6;
                    d4 /= d6;
                    d5 /= d6;
                    double d7 = 0.5D / (d6 / this.explosionSize + 0.1D);
                    d7 *= this.worldObj.rand.nextFloat() * this.worldObj.rand.nextFloat() + 0.3F;
                    d3 *= d7;
                    d4 *= d7;
                    d5 *= d7;
                    this.worldObj.spawnParticle("explode", (d0 + this.explosionX * 1.0D) / 2.0D, (d1 + this.explosionY * 1.0D) / 2.0D, (d2 + this.explosionZ * 1.0D) / 2.0D, d3, d4, d5);
                    this.worldObj.spawnParticle("smoke", d0, d1, d2, d3, d4, d5);
                }

                if (block.getMaterial() != Material.air)
                {
                    if (block.canDropFromExplosion(this))
                    {
                        block.dropBlockAsItemWithChance(this.worldObj, i, j, k, this.worldObj.getBlockMetadata(i, j, k), 1.0F / this.explosionSize, 0);
                    }

                    block.onBlockExploded(this.worldObj, i, j, k, this);
                }
            }
        }

        if (this.isFlaming)
        {
            iterator = this.affectedBlockPositions.iterator();

            while (iterator.hasNext())
            {
                chunkposition = (ChunkPosition)iterator.next();
                i = chunkposition.chunkPosX;
                j = chunkposition.chunkPosY;
                k = chunkposition.chunkPosZ;
                block = this.worldObj.getBlock(i, j, k);
                Block block1 = this.worldObj.getBlock(i, j - 1, k);

                if (block.getMaterial() == Material.air && block1.func_149730_j() && this.explosionRNG.nextInt(3) == 0)
                {
                    this.worldObj.setBlock(i, j, k, Blocks.fire);
                }
            }
        }
    }
    public float getBlockDensity(Vec3 p_72842_1_, AxisAlignedBB p_72842_2_)
    {
        double d0 = 1.0D / ((p_72842_2_.maxX - p_72842_2_.minX) * 2.0D + 1.0D);
        double d1 = 1.0D / ((p_72842_2_.maxY - p_72842_2_.minY) * 2.0D + 1.0D);
        double d2 = 1.0D / ((p_72842_2_.maxZ - p_72842_2_.minZ) * 2.0D + 1.0D);

        if (d0 >= 0.0D && d1 >= 0.0D && d2 >= 0.0D)
        {
            int i = 0;
            int j = 0;

            for (float f = 0.0F; f <= 1.0F; f = (float)(f + d0))
            {
                for (float f1 = 0.0F; f1 <= 1.0F; f1 = (float)(f1 + d1))
                {
                    for (float f2 = 0.0F; f2 <= 1.0F; f2 = (float)(f2 + d2))
                    {
                        double d3 = p_72842_2_.minX + (p_72842_2_.maxX - p_72842_2_.minX) * f;
                        double d4 = p_72842_2_.minY + (p_72842_2_.maxY - p_72842_2_.minY) * f1;
                        double d5 = p_72842_2_.minZ + (p_72842_2_.maxZ - p_72842_2_.minZ) * f2;
                        MovingObjectPosition mop=this.worldObj.func_147447_a(Vec3.createVectorHelper(d3, d4, d5), p_72842_1_, false, true, false);
                        if (mop == null||this.worldObj.getBlock(mop.blockX, mop.blockY, mop.blockZ)==Blocks.snow_layer)
                        {
                            ++i;
                        }

                        ++j;
                    }
                }
            }

            return (float)i / (float)j;
        }
        else
        {
            return 0.0F;
        }
    }
    /**
     * Returns either the entity that placed the explosive block, the entity that caused the explosion or null.
     */
    @Override
	public EntityLivingBase getExplosivePlacedBy()
    {
        return (EntityLivingBase) ((EntityProjectileBase)this.exploder).shootingEntity;
    }
}