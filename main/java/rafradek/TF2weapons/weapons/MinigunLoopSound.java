package rafradek.TF2weapons.weapons;

import rafradek.TF2weapons.MapList;
import rafradek.TF2weapons.message.TF2ActionHandler;
import net.minecraft.client.audio.MovingSound;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.ConfigCategory;

public class MinigunLoopSound extends MinigunSound {

	
	private boolean firing;
	private boolean crit;

	public MinigunLoopSound(ResourceLocation p_i45104_1_,EntityLivingBase entity,boolean firing, ConfigCategory conf,boolean crit, int type) {
		super(p_i45104_1_, entity, type, conf);
		this.repeat=true;
		this.firing=firing;
		this.crit=crit;
	}

	@Override
	public void update() {
		super.update();
		if(this.endsnextTick||this.donePlaying){
			return;
		}
		ItemStack stack=this.entity.getHeldItem();
		boolean playThis=(stack.stackTagCompound.getShort("crittime")>0&&crit)||(stack.stackTagCompound.getShort("crittime")<=0&&!crit);
		if(TF2ActionHandler.playerAction.get().containsKey(entity)&&((ItemUsable)stack.getItem()).canFire(entity.worldObj, entity, stack)/*stack.stackTagCompound.getShort("minigunticks")>=17*TF2Attribute.getModifier("Minigun Spinup", stack, 1,entity)*/){
			int action=TF2ActionHandler.playerAction.get().get(entity);
			if(((action&1)!=0&&firing&&playThis)||(!firing&&(action&3)==2)){
				//this.volume=1.0f;
			}
			else{
				this.setDone();
			}
		}
		else{
			this.setDone();
		}
	}
	
}
