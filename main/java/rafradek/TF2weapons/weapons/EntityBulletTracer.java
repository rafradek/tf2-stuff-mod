package rafradek.TF2weapons.weapons;

import rafradek.TF2weapons.TF2EventBusListener;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityBulletTracer extends EntityFX {
	
	private int duration;
	private boolean nextDead;

	public EntityBulletTracer(World par1World, double startX, double startY, double startZ, double x, double y, double z, int duration,int crits) {
		super(par1World, startX, startY, startZ);
		this.noClip=true;
		this.particleScale=1;
		this.duration=duration;
		this.motionX=(x-startX)/duration;
		this.motionY=(y-startY)/duration;
		this.motionZ=(z-startZ)/duration;
		this.particleMaxAge=200;
		this.setSize(0.1f, 0.1f);
		//this.setParticleIcon(Item.itemsList[2498+256].getIconFromDamage(0));
		this.setParticleIcon(TF2EventBusListener.pelletIcon);
		//this.setParticleTextureIndex(81);
		this.multipleParticleScaleBy(2);
		// TODO Auto-generated constructor stub
		if(crits==0){
			this.setRBGColorF(0.97f, 0.76f,0.51f);
		}
		else if(crits==1){
			this.setRBGColorF(1f, 0.15f,0.15f);
		}
		else if(crits==2){
			this.setRBGColorF(1f, 0.2f,0.2f);
		}
		this.particleTextureJitterX = this.rand.nextFloat();
        this.particleTextureJitterY = this.rand.nextFloat();
	}
	
	public void onUpdate(){
		if(nextDead){
			this.setDead();
		}
		if(this.worldObj.rayTraceBlocks(Vec3.createVectorHelper(posX, posY, posZ), Vec3.createVectorHelper(posX+motionX, posY+motionY, posZ+motionZ)) != null){
			nextDead=true;
			//this.setVelocity(0, 0, 0);
		}
		super.onUpdate();
		if(duration > 0){
			duration--;
			if(duration==0)
				this.setVelocity(0, 0, 0);
		}
	}

	public int getFXLayer()
    {
        return 2;
    }
	public void renderParticle(Tessellator par1Tessellator, float par2, float par3, float par4, float par5, float par6, float par7)
    {
		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
    }
	@SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float p_70070_1_)
    {
		return 15728880;
    }

    public float getBrightness(float p_70013_1_)
    {
        return 1.0F;
    }
}
