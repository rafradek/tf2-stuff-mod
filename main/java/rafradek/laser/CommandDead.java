package rafradek.laser;

import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;

public class CommandDead extends CommandBase {

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return "setdead";
	}

	@Override
	public String getCommandUsage(ICommandSender var1) {
		// TODO Auto-generated method stub
		return "command.setdead.usage";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processCommand(ICommandSender var1, String[] var2) {
		List<Entity> list=var1.getEntityWorld().loadedEntityList;
		String id=var2.length>0?var2[0]:"all";
		boolean result=false;
		for(Entity entity : list){
			if(!(entity instanceof EntityPlayer) && (id.equals("all")||(EntityList.classToStringMapping.containsKey(entity.getClass())&&EntityList.classToStringMapping.get(entity.getClass()).equals(id)))){
				entity.setDead();
				result=true;
			}
		}
		/*if(result)
			notifyAdmins(var1, "commands.setdead.success", new Object[0]);
		else
			notifyAdmins(var1, "commands.setdead.failed", new Object[0]);*/
		
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
