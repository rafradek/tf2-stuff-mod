package rafradek.laser;

import rafradek.laser.EntityLaser.LaserType;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class ItemLaserEmitter extends Item {

	public LaserType type;
	public ItemLaserEmitter(LaserType type){
		this.setMaxDamage(type.durablity);
		this.setMaxStackSize(1);
		this.setCreativeTab(CreativeTabs.tabTools);
		this.type=type;
		this.setUnlocalizedName("laseremitter");
		this.setTextureName("gmod:toolgun");
	}
	public void onPlayerStoppedUsing(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer, int par4)
    {
		if(!par2World.isRemote)
			LaserMod.lasers.remove(par3EntityPlayer);
    }
	public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
		if(!par2World.isRemote)
			LaserMod.lasers.remove(par3EntityPlayer);
        return par1ItemStack;
    }

	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
		if(!par5&&!par2World.isRemote)
			LaserMod.lasers.remove(par3Entity);
	}
    /**
     * How long it takes to use or consume an item
     */
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 36000;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.bow;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par3EntityPlayer.capabilities.isCreativeMode || par3EntityPlayer.inventory.hasItem(Items.redstone))
        {
            if(!par2World.isRemote){
            	EntityLaser laser=new EntityLaser(par2World, this.type, new MovingObjectPosition(par3EntityPlayer));
            	LaserMod.lasers.put(par3EntityPlayer,laser);
            	par2World.spawnEntityInWorld(laser);
            	
            }
            par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        }
        return par1ItemStack;
    }
}
