package rafradek.laser;

import java.util.HashMap;
import java.util.Iterator;

import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.*;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import rafradek.laser.LaserMod.ILaserEmitter;
import io.netty.buffer.ByteBuf;

public class EntityLaser extends Entity implements IEntityAdditionalSpawnData{

	public MovingObjectPosition owner;
	public LaserType type;
	public Vec3[] vectors;
	public HashMap<Vec3,Float> blockDamage;
	public HashMap<Vec3,Float> tempHashMap;
	public Vec3[][] endTarget;
	public Vec3[][] startTarget;
	public EntityLaser(World par1World) {
		super(par1World);
		this.setSize(1, 1);
		this.vectors=new Vec3[8];
		for(int i =0;i<8;i++){
			this.vectors[i]=Vec3.createVectorHelper(0, 0, 0);
		}
		this.blockDamage=new HashMap<Vec3,Float>();
		this.tempHashMap=new HashMap<Vec3,Float>();
		// TODO Auto-generated constructor stub
	}
	public EntityLaser(World par1World,LaserType type,MovingObjectPosition owner) {
		this(par1World);
		this.type=type;
		this.owner=owner;
		this.setPositionAndRotation();
	}
	@Override
	protected void entityInit() {
		// TODO Auto-generated method stub

	}
	public int getBrightnessForRender(float value){
		return 15728880;
	}
	public void setPositionAndRotation(){
		if(owner != null){
			if(this.owner.entityHit!=null){
				this.setVector(this.owner.entityHit.posX, this.owner.entityHit.posY+this.owner.entityHit.getEyeHeight(), this.owner.entityHit.posZ, this.vectors[0]);
				LaserMod.getEndVector(worldObj, this.vectors[0], this.owner.entityHit.rotationYaw, this.owner.entityHit.rotationPitch, 1, this.vectors[1], this.vectors[2]);
				this.setPosition(this.vectors[1].xCoord,this.vectors[1].yCoord,this.vectors[1].zCoord);
				this.rotationPitch=this.owner.entityHit.rotationPitch;
				this.rotationYaw=this.owner.entityHit.rotationYaw;
			}
		}
		
	}
	@Override
	protected void readEntityFromNBT(NBTTagCompound var1) {
		this.setDead();

	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound var1) {
	}
	public void onUpdate()
    {
		super.onUpdate();
		if(this.shouldBeDestroyed()){
			this.setDead();
		}
		this.setPositionAndRotation();
		if(!this.worldObj.isRemote&&this.ticksExisted%2==0)
			this.damageObjects(this.getTargets());
		if(this.worldObj.isRemote)
			this.getClientTargets(endTarget);
    }
	public MovingObjectPosition[] getTargets(){
		int size=Math.max(1, Math.round(this.type.size));
		int targetPoints=(size+1)*(size+1)+size*size;
		MovingObjectPosition[] array=new MovingObjectPosition[targetPoints];
		int i=0;
		for(float x=-this.type.size/2;x<=this.type.size/2;x+=this.type.size/size){
			for(float y=-this.type.size/2;y<=this.type.size/2;y+=this.type.size/size){
				array[i]=this.target(x, y);
				System.out.println(array[i]);
				i++;
			}
		}
		for(float x=-this.type.size/2+this.type.size/size/2;x<=this.type.size/2-this.type.size/size/2;x+=this.type.size/size){
			for(float y=-this.type.size/2+this.type.size/size/2;y<=this.type.size/2-this.type.size/size/2;y+=this.type.size/size){
				array[i]=this.target(x, y);
				i++;
			}
		}
		//System.out.println(i);
		return array;
	}
	public void getClientTargets(Vec3[][] array){
		int size=Math.max(1, Math.round(this.type.size));
		int targetPoints=(size+1)*(size+1)+size*size;
		int xint=0;
		int yint=0;
		for(float x=-this.type.size/2;x<=this.type.size/2;x+=this.type.size/size){
			for(float y=-this.type.size/2;y<=this.type.size/2;y+=this.type.size/size){
				Vec3 targetVec=this.clientTarget(x, y);
				this.setVector(targetVec.xCoord, targetVec.yCoord, targetVec.zCoord,array[xint][yint]);
				this.copyVec(this.vectors[6],this.startTarget[xint][yint]);
				yint+=2;
			}
			xint+=2;
			yint=0;
		}
		xint=1;
		yint=1;
		for(float x=-this.type.size/2+this.type.size/size/2;x<=this.type.size/2-this.type.size/size/2;x+=this.type.size/size){
			for(float y=-this.type.size/2+this.type.size/size/2;y<=this.type.size/2-this.type.size/size/2;y+=this.type.size/size){
				Vec3 targetVec=this.clientTarget(x, y);
				this.setVector(targetVec.xCoord, targetVec.yCoord, targetVec.zCoord,array[xint][yint]);
				this.copyVec(this.vectors[6],this.startTarget[xint][yint]);
				yint+=2;
			}
			xint+=2;
			yint=1;
		}
		//System.out.println(i);
	}
	public MovingObjectPosition target(float x, float y){
		Vec3 changeVec=LaserMod.moveVector(worldObj, x, y, this.rotationYaw, this.rotationPitch,this.vectors[0]);
		this.setVector(this.posX+changeVec.xCoord,this.posY+changeVec.yCoord,this.posZ+changeVec.zCoord,this.vectors[1])/*.addVector(changeVec.xCoord, changeVec.yCoord, changeVec.zCoord)*/;
		LaserMod.getEndVector(worldObj, this.vectors[1], this.rotationYaw,this.rotationPitch , 80,this.vectors[2],this.vectors[4]);
		//EntityArrow arrow=new EntityArrow(this.worldObj,this.vectors[1].xCoord,this.vectors[1].yCoord,this.vectors[1].zCoord);
		//arrow.setThrowableHeading(0, 0, 1, 1, 1);
		//this.worldObj.spawnEntityInWorld(arrow);
		return LaserMod.tryGetObject(worldObj, this.vectors[1],this.vectors[2],this.vectors[3],this.vectors[5],this.owner.entityHit);
	}
	public Vec3 clientTarget(float x, float y){
		Vec3 changeVec=LaserMod.moveVector(worldObj, x, y, this.rotationYaw, this.rotationPitch,this.vectors[0]);
		this.setVector(this.posX+changeVec.xCoord,this.posY+changeVec.yCoord,this.posZ+changeVec.zCoord,this.vectors[1])/*.addVector(changeVec.xCoord, changeVec.yCoord, changeVec.zCoord)*/;
		this.copyVec(this.vectors[1], this.vectors[6]);
		LaserMod.getEndVector(worldObj, this.vectors[1], this.rotationYaw,this.rotationPitch , 80,this.vectors[2],this.vectors[4]);
		MovingObjectPosition target=LaserMod.tryGetObject(worldObj, this.vectors[1],this.vectors[2],this.vectors[3],this.vectors[5],this.owner.entityHit);
		Vec3 targetVec=null;
		if(target!=null){
			targetVec=target.hitVec;
		}
		else
			targetVec=this.vectors[2];
		return targetVec;
		
	}
	public Vec3 setVector(double x, double y, double z,Vec3 vector){
		vector.xCoord=x;
		vector.yCoord=y;
		vector.zCoord=z;
		return vector;
		
	}
	public Vec3 copyVec(Vec3 vector1,Vec3 vector2){
		return this.setVector(vector1.xCoord, vector1.yCoord, vector1.zCoord, vector2);
	}
	public void damageObjects(MovingObjectPosition[] array){
		for(int i=0;i<array.length;i++){
			if(array[i]!=null){
				if(array[i].entityHit !=null){
					DamageSource source=this.owner.entityHit!=null?DamageSource.causeThrownDamage(this, this.owner.entityHit):DamageSource.inFire;
					array[i].entityHit.attackEntityFrom(source, this.type.damage/array.length);
					array[i].entityHit.setFire((int) (this.type.damage*2.5));
					if(array[i].entityHit instanceof EntityLivingBase){
						((EntityLivingBase)array[i].entityHit).hurtResistantTime=0;
					}
				}
				else if(array[i].typeOfHit==MovingObjectType.BLOCK){
					Vec3 vecOld=this.getVectorAt(array[i].blockX, array[i].blockY, array[i].blockZ);
					float old=vecOld!=null?(this.tempHashMap.containsKey(vecOld)?this.tempHashMap.get(vecOld):this.blockDamage.get(vecOld)):0;
					if(vecOld==null){
						vecOld=Vec3.createVectorHelper(array[i].blockX, array[i].blockY, array[i].blockZ);
					}
					float add=this.worldObj.getBlock(array[i].blockX, array[i].blockY, array[i].blockZ).getPlayerRelativeBlockHardness((EntityPlayer) this.owner.entityHit, this.worldObj, array[i].blockX, array[i].blockY, array[i].blockZ)*this.type.power;
					if(old+add>=1){
						if(!(this.owner.entityHit instanceof EntityPlayer&&((EntityPlayer)this.owner.entityHit).capabilities.isCreativeMode))
							this.worldObj.getBlock(array[i].blockX, array[i].blockY, array[i].blockZ).dropBlockAsItem(worldObj, array[i].blockX, array[i].blockY, array[i].blockZ, this.worldObj.getBlockMetadata(array[i].blockX, array[i].blockY, array[i].blockZ), 0);
						this.worldObj.setBlockToAir(array[i].blockX, array[i].blockY, array[i].blockZ);
					}
					else
						this.tempHashMap.put(vecOld, old+add);
				}
			}
		}
		if(!this.tempHashMap.isEmpty()){
			this.blockDamage.clear();
			this.blockDamage.putAll(tempHashMap);
			this.tempHashMap.clear();
		}
	}
	public void setDead(){
		super.setDead();
		if(this.owner !=null && this.owner.entityHit !=null)
			LaserMod.lasers.remove(this.owner.entityHit);
	}
	public boolean shouldBeDestroyed(){
		if (this.worldObj.isRemote) return false;
		if (this.owner==null) return true;
		if (this.owner.typeOfHit==MovingObjectType.ENTITY){
			if(this.owner.entityHit==null) return true;
			else{
				if(this.owner.entityHit.isDead) return true;
				return !LaserMod.lasers.containsKey(this.owner.entityHit);
			}
		}
		else if(this.owner.typeOfHit==MovingObjectType.BLOCK){
			Block block=this.worldObj.getBlock(owner.blockX,owner.blockY,owner.blockZ);
			if(!(block instanceof ILaserEmitter)) return true;
			return ((ILaserEmitter)block).shouldEmitLaser(this.worldObj, owner.blockX,owner.blockY,owner.blockZ);
		}
		return false;
	}
	public Vec3 getVectorAt(int x,int y,int z){
		Iterator<Vec3> iterator=this.blockDamage.keySet().iterator();
		while(iterator.hasNext()){
			Vec3 vec=iterator.next();
			if(vec.xCoord==x&&vec.yCoord==y&&vec.zCoord==z)
				return vec;
		}
		Iterator<Vec3> iterator2=this.tempHashMap.keySet().iterator();
		while(iterator2.hasNext()){
			Vec3 vec=iterator2.next();
			if(vec.xCoord==x&&vec.yCoord==y&&vec.zCoord==z)
				return vec;
		}
		return null;
	}
	public static enum LaserType{
		REDCRYSTAL(0,10000f,4f,0.1f,0.3f,1f,0f,0f,60);
		
		public float power;
		public float size;
		public float damage;
		public float alpha;
		public float red;
		public float green;
		public float blue;
		public int durablity;
		public int id;

		private LaserType(int id,float power, float damage, float size, float alpha,float red, float green, float blue,int durablity)
        {
			this.id=id;
			this.power=power;
			this.damage=damage;
			this.size=size;
			this.alpha=alpha;
			this.red=red;
			this.green=green;
			this.blue=blue;
        }
	}
	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeByte(this.type.id);
		if(this.owner != null && this.owner.typeOfHit == MovingObjectType.BLOCK){
			buffer.writeInt(this.owner.blockX);
			buffer.writeInt(this.owner.blockY);
			buffer.writeInt(this.owner.blockZ);
			buffer.writeByte(this.owner.sideHit);
		}
		else if(this.owner != null && this.owner.entityHit != null){
			buffer.writeInt(this.owner.entityHit.getEntityId());
		}
	}
	@Override
	public void readSpawnData(ByteBuf additionalData) {
		switch(additionalData.readByte()){
		case 0:
			this.type=LaserType.REDCRYSTAL;
		default:
			this.type=LaserType.REDCRYSTAL;
		}
		if(additionalData.readableBytes()>4)
			this.owner=new MovingObjectPosition(additionalData.readInt(), additionalData.readInt(), additionalData.readInt(), additionalData.readByte(), Vec3.createVectorHelper(0, 0, 0));
		else if(additionalData.readableBytes()==4){
			Entity entity=this.worldObj.getEntityByID(additionalData.readInt());
			if(entity != null)
				this.owner=new MovingObjectPosition(entity);
		}
		int size=Math.max(1, Math.round(this.type.size));
		this.endTarget=new Vec3[size*2+1][size*2+1];
		for(int x =0;x<size*2+1;x++){
			for(int y =0;y<size*2+1;y++){
				this.endTarget[x][y]=Vec3.createVectorHelper(0, 0, 0);
			}
		}
		this.startTarget=new Vec3[size*2+1][size*2+1];
		for(int x =0;x<size*2+1;x++){
			for(int y =0;y<size*2+1;y++){
				this.startTarget[x][y]=Vec3.createVectorHelper(0, 0, 0);
			}
		}
		this.setPositionAndRotation();
	}
}
