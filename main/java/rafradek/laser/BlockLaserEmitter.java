package rafradek.laser;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import rafradek.laser.EntityLaser.LaserType;
import rafradek.laser.LaserMod.ILaserEmitter;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class BlockLaserEmitter extends Block implements ILaserEmitter {

	protected BlockLaserEmitter() {
		super(Material.iron);
		this.setCreativeTab(CreativeTabs.tabRedstone);
	}

	@Override
	public boolean shouldEmitLaser(World world, int x, int y, int z) {
		// TODO Auto-generated method stub
		return world.isBlockIndirectlyGettingPowered(x, y, z) || world.isBlockIndirectlyGettingPowered(x, y-1, z);
	}
	public void func_149695_a(World p_149726_1_, int p_149726_2_, int p_149726_3_, int p_149726_4_, Block p_149726_5_)
    {
        if (!p_149726_1_.isRemote)
        {
            EntityLaser laser=new EntityLaser(p_149726_1_, LaserType.REDCRYSTAL, new MovingObjectPosition(p_149726_2_, p_149726_3_, p_149726_4_, p_149726_1_.getBlockMetadata(p_149726_2_, p_149726_3_, p_149726_4_), Vec3.createVectorHelper(p_149726_2_, p_149726_3_, p_149726_4_)));
            laser.posX=p_149726_2_+1.5;
            laser.posY=p_149726_2_+0.5;
            laser.posZ=p_149726_2_+0.5;
            p_149726_1_.spawnEntityInWorld(laser);
        }
    }
	public void func_149726_b(World p_149726_1_, int p_149726_2_, int p_149726_3_, int p_149726_4_)
    {
		if (!p_149726_1_.isRemote)
        {
            EntityLaser laser=new EntityLaser(p_149726_1_, LaserType.REDCRYSTAL, new MovingObjectPosition(p_149726_2_, p_149726_3_, p_149726_4_, p_149726_1_.getBlockMetadata(p_149726_2_, p_149726_3_, p_149726_4_), Vec3.createVectorHelper(p_149726_2_, p_149726_3_, p_149726_4_)));
            laser.posX=p_149726_2_+1.5;
            laser.posY=p_149726_2_+0.5;
            laser.posZ=p_149726_2_+0.5;
            p_149726_1_.spawnEntityInWorld(laser);
        }
    }
	@SideOnly(Side.CLIENT)
    public String func_149702_O()
    {
        return "dispenser";
    }
}
