package rafradek.laser;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;

public class RenderLaser extends Render {

	@Override
	public void doRender(Entity var1, double var2, double var4, double var6,
			float var8, float var9) {
		EntityLaser laser=(EntityLaser) var1;
		/*for(int x=-4; x<=4; x++){
			for(int y=-4; y<=4; y++){
				Vec3 vector=LaserMod.getEndVector(var1.worldObj, var1.worldObj.getWorldVec3Pool().getVecFromPool(var1.posX, var1.posY, var1.posZ), var1.rotationYaw, var1.rotationPitch, 20, laser.vectors[0], laser.vectors[1]);
				Vec3 vector2=LaserMod.moveVector(var1.worldObj, x, y, var1.rotationYaw, var1.rotationPitch, laser.vectors[2]);
				Vec3 vec=vector.addVector(vector2.xCoord, vector2.yCoord, vector2.zCoord);
				//System.out.println(vector2.xCoord+" "+vector2.yCoord+" "+vector2.zCoord);
				GL11.glPushMatrix();
				renderOffsetAABB2(AxisAlignedBB.getAABBPool().getAABB(vec.xCoord-0.3,vec.yCoord-0.3,vec.zCoord-0.3,vec.xCoord+0.3,vec.yCoord+0.3,vec.zCoord+0.3),var2 - var1.lastTickPosX, var4 - var1.lastTickPosY, var6 - var1.lastTickPosZ,(var1.ticksExisted%200)/20);
				GL11.glPopMatrix();
			}
		}*/
		if(laser.endTarget!=null){
			GL11.glDisable(GL11.GL_TEXTURE_2D);
	        Tessellator tessellator = Tessellator.instance;
	        GL11.glColor4f(laser.type.red,laser.type.green,laser.type.blue,laser.type.alpha);
	        tessellator.startDrawingQuads();
	        tessellator.setTranslation(var2-var1.lastTickPosX, var4 - var1.lastTickPosY, var6 - var1.lastTickPosZ);
			for(int x=1;x<laser.endTarget.length;x++){
				for(int y=2;y<laser.endTarget.length;y+=2){
					if(x%2==0){
						tessellator.addVertex(laser.endTarget[x-2][y].xCoord, laser.endTarget[x-2][y].yCoord, laser.endTarget[x-2][y].zCoord);
						tessellator.addVertex(laser.endTarget[x-1][y-1].xCoord, laser.endTarget[x-1][y-1].yCoord, laser.endTarget[x-1][y-1].zCoord);
						tessellator.addVertex(laser.endTarget[x][y-2].xCoord, laser.endTarget[x][y-2].yCoord, laser.endTarget[x][y-2].zCoord);
						tessellator.addVertex(laser.endTarget[x][y].xCoord, laser.endTarget[x][y].yCoord, laser.endTarget[x][y].zCoord);
					}
					else {
						tessellator.addVertex(laser.endTarget[x-1][y].xCoord, laser.endTarget[x-1][y].yCoord, laser.endTarget[x-1][y].zCoord);
						tessellator.addVertex(laser.endTarget[x-1][y-2].xCoord, laser.endTarget[x-1][y-2].yCoord, laser.endTarget[x-1][y-2].zCoord);
						tessellator.addVertex(laser.endTarget[x+1][y-2].xCoord, laser.endTarget[x+1][y-2].yCoord, laser.endTarget[x+1][y-2].zCoord);
						tessellator.addVertex(laser.endTarget[x][y-1].xCoord, laser.endTarget[x][y-1].yCoord, laser.endTarget[x][y-1].zCoord);
					}
					
				}
			}
			int max=laser.startTarget.length-1;
			tessellator.addVertex(laser.startTarget[0][max].xCoord, laser.startTarget[0][max].yCoord, laser.startTarget[0][max].zCoord);
			tessellator.addVertex(laser.startTarget[max][max].xCoord, laser.startTarget[max][max].yCoord, laser.startTarget[max][max].zCoord);
			tessellator.addVertex(laser.startTarget[max][0].xCoord, laser.startTarget[max][0].yCoord, laser.startTarget[max][0].zCoord);
			tessellator.addVertex(laser.startTarget[0][0].xCoord, laser.startTarget[0][0].yCoord, laser.startTarget[0][0].zCoord);
			tessellator.setTranslation(0.0D, 0.0D, 0.0D);
			System.out.println(laser.startTarget[0][0].xCoord);
	        tessellator.draw();
	        GL11.glEnable(GL11.GL_TEXTURE_2D);
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity var1) {
		// TODO Auto-generated method stub
		return null;
	}
	public static void renderOffsetAABB2(AxisAlignedBB par0AxisAlignedBB, double par1, double par3, double par5,int add)
    {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        Tessellator tessellator = Tessellator.instance;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        tessellator.startDrawing(add);
        tessellator.setTranslation(par1, par3, par5);
        /*tessellator.setNormal(0.0F, 0.0F, -1.0F);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.setNormal(0.0F, -1.0F, 0.0F);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.setNormal(-1.0F, 0.0F, 0.0F);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.minX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.minZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.maxY, par0AxisAlignedBB.maxZ);
        tessellator.addVertex(par0AxisAlignedBB.maxX, par0AxisAlignedBB.minY, par0AxisAlignedBB.maxZ);*/
        tessellator.setTranslation(0.0D, 0.0D, 0.0D);
        tessellator.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }
	
}
