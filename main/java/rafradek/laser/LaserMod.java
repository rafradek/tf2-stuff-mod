package rafradek.laser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;




import rafradek.laser.EntityLaser.LaserType;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = "rafradek_laser", name =  "Laser", version = "1.0")
public class LaserMod{

	public static HashMap<Entity,EntityLaser> lasers;
	public static ItemLaserEmitter laserEmitter;
	public static BlockLaserEmitter blockLaserEmitter;

	@Mod.EventHandler
    public void init(FMLPreInitializationEvent event)
    {
		EntityRegistry.registerModEntity(EntityLaser.class, "laser_raf", 1, this, 80,Integer.MAX_VALUE, false);
		GameRegistry.registerItem(laserEmitter=new ItemLaserEmitter(LaserType.REDCRYSTAL),"laserraf");
		GameRegistry.registerBlock(blockLaserEmitter=(BlockLaserEmitter) new BlockLaserEmitter().setHardness(3.5F).setStepSound(Block.soundTypeStone).setBlockName("dispenser").setBlockTextureName("laseremitter"), "laserrafblock");
		//RenderingRegistry.registerEntityRenderingHandler(EntityLaser.class, new RenderLaser());	
    }
	public interface ILaserEmitter{
		public boolean shouldEmitLaser(World world,int x, int y, int z);
	}
	@Mod.EventHandler
    public void serverInit(FMLServerStartingEvent event)
    {
		lasers=new HashMap<Entity, EntityLaser>();
		event.registerServerCommand(new MathCommand());
		event.registerServerCommand(new CommandDead());
		event.registerServerCommand(new CommandWorldGen());
    }
	@SuppressWarnings("rawtypes")
	public static MovingObjectPosition tryGetObject(World world,Vec3 start, Vec3 end,Vec3 start2,Vec3 end2,Entity exlude){
		start2.xCoord=start.xCoord;
		start2.yCoord=start.yCoord;
		start2.zCoord=start.zCoord;
		end2.xCoord=end.xCoord;
		end2.yCoord=end.yCoord;
		end2.zCoord=end.zCoord;
        MovingObjectPosition var4 = world.func_147447_a(start, end, false, true, true);
        if (var4 != null)
        {
        	end2.xCoord=var4.hitVec.xCoord;
        	end2.yCoord=var4.hitVec.yCoord;
        	end2.zCoord=var4.hitVec.zCoord;
        }

        Entity var5 = null;
        AxisAlignedBB AABB=AxisAlignedBB.getBoundingBox(start2.xCoord-0.2, start2.yCoord-0.2, start2.zCoord-0.2, start2.xCoord+0.2, start2.yCoord+0.2, start2.zCoord+0.2).addCoord(end2.xCoord-start2.xCoord, end2.yCoord-start2.yCoord, end2.zCoord-start2.zCoord);
        List<?> var6 = world.getEntitiesWithinAABBExcludingEntity(exlude, AABB);
        double var7 = 0.0D;
        
        Iterator<?> iterator = var6.iterator();
        float var11;

        while (iterator.hasNext())
        {
            Entity var10 = (Entity)iterator.next();

            if (/*allowEveryEntity||*/var10.canBeCollidedWith()&&!(var10 instanceof EntityLivingBase&& ((EntityLivingBase)var10).deathTime>4))
            {
                var11 = 0.0F;
                AxisAlignedBB var12 = var10.boundingBox.expand((double)var11, (double)var11, (double)var11);
                MovingObjectPosition var13 = var12.calculateIntercept(start2, end2);

                if (var13 != null)
                {
                    double var14 = start2.distanceTo(var13.hitVec);

                    if (var14 < var7 || var7 == 0.0D)
                    {
                        var5 = var10;
                        var7 = var14;
                    }
                }
            }
        }
        //System.out.println(var6);
        if (var5 != null)
        {
        	/*if(var5 instanceof IEntityPart)
        		var5=((IEntityPart)var5).getMainEntity();*/
            return new MovingObjectPosition(var5);
        }
        else{
        	return var4;
       }
	}
	public static Vec3 getEndVector(World world,Vec3 start, float yaw, float pitch, double range,Vec3 vec,Vec3 vec2){
		float f1 = MathHelper.cos(-yaw * 0.017453292F - (float)Math.PI);
        float f2 = MathHelper.sin(-yaw * 0.017453292F - (float)Math.PI);
        float f3 = -MathHelper.cos(-pitch * 0.017453292F);
        float f4 = MathHelper.sin(-pitch * 0.017453292F);
        //System.out.println("pr�ba `: "+vec2);
        vec2.xCoord=(double)(f2 * f3);
        vec2.yCoord=(double)f4;
        vec2.zCoord=(double)(f1 * f3);
        vec.xCoord=start.xCoord+vec2.xCoord * range;
        vec.yCoord=start.yCoord+vec2.yCoord * range;
        vec.zCoord=start.zCoord+vec2.zCoord * range;
        //System.out.println("pr�ba 2: "+((f2 * f3)*range)+" "+f4*range+" "+(f1 * f3)*range);
        return vec;
	}
	public static Vec3 moveVector(World world, float x, float y,float yaw, float pitch,Vec3 vector){
		/*double xnew = -MathHelper.sin(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI);
        double znew = MathHelper.cos(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI);
        double ynew = -MathHelper.sin(yaw / 180.0F * (float)Math.PI);
        return world.getWorldVec3Pool().getVecFromPool(xnew, ynew, znew);*/
		/*double xnew = vector.xCoord=MathHelper.cos(yaw / 180.0F * (float)Math.PI) * x;
		double ynew = vector.yCoord=MathHelper.cos(pitch / 180.0F * (float)Math.PI) * y;
		double znew = vector.zCoord=MathHelper.sin(yaw / 180.0F * (float)Math.PI) * x;*/
		if(Math.abs(pitch)!=90){
			float absY=Math.abs(y);
			float absX=Math.abs(x);
			vector.xCoord=x;
			vector.yCoord=MathHelper.cos(pitch / 180.0F * (float)Math.PI) * y;
			vector.zCoord=0;
			vector.rotateAroundY(-yaw * 0.017453292F - (float)Math.PI);
			int modx=MathHelper.wrapAngleTo180_float(yaw)>0?-1:1;
			int mody=MathHelper.wrapAngleTo180_float(yaw)>-90&&MathHelper.wrapAngleTo180_float(yaw)<90?1:-1;
			//vector.xCoord*=Math.signum(x);
			//vector.yCoord*=Math.signum(y);
			//vector.zCoord*=Math.signum(x);
			float y2=y;
			float x2=x;
			if((x>0&&y<0)||(x<0&&y>0)){
				y2=-y2;
			}
			/*if(x2==0){
				x2=1f;
				y2=-1f;
			}*/
			double zchange=(MathHelper.sin(pitch / 180.0F * (float)Math.PI))*mody *(absY-Math.abs(vector.zCoord)*(y2/x2));
			double xchange=(MathHelper.sin(pitch / 180.0F * (float)Math.PI))*modx *(absY-Math.abs(vector.xCoord)*(y2/x2));
			if(y>0&&x!=0){
				vector.xCoord+=xchange;
				vector.zCoord+=zchange;
			}
			if(y<0&&x!=0){
				vector.xCoord-=xchange;
				vector.zCoord-=zchange;
			}
			
		}
		else{
			vector.xCoord=x;
			vector.yCoord=0;
			vector.zCoord=y;
			if(yaw % 90 != 0)
				vector.rotateAroundY(-yaw * 0.017453292F - (float)Math.PI);
		}
		return vector;
		/*float pitch2=y/(x+y)*90;
		float f1 = MathHelper.cos(-(yaw+90) * 0.017453292F - (float)Math.PI);
        float f2 = MathHelper.sin(-(yaw+90) * 0.017453292F - (float)Math.PI);
        float f3 = -MathHelper.cos(-(90-pitch) * 0.017453292F);
        float f4 = MathHelper.sin(-(90-pitch) * 0.017453292F);
        //float f5 = -MathHelper.cos(-pitch * 0.017453292F);*/
        //return world.getWorldVec3Pool().getVecFromPool((double)(f2 + f3), (double)f4, (double)(f1 + f3));
		
	}
}
