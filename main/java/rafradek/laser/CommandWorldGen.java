package rafradek.laser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.structure.MapGenVillage;

public class CommandWorldGen extends CommandBase {

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return "worldgen";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		// TODO Auto-generated method stub
		return "nope";
	}

	@Override
	public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
		if(p_71515_2_.length>0&&p_71515_2_[0].equals("village")){
			List<BiomeGenBase[]> biomes=MapGenVillage.villageSpawnBiomes;
			MapGenVillage.villageSpawnBiomes=Arrays.asList(BiomeGenBase.getBiomeGenArray());
			Map map=new HashMap<String,String>();
			map.put("distance", "32");
			MapGenVillage village=new MapGenVillage(map);
			//village.func_151539_a(wor, this.worldObj, p_73154_1_, p_73154_2_, ablock);
			village.func_151545_a(p_71515_1_.getEntityWorld(), p_71515_1_.getPlayerCoordinates().posX, p_71515_1_.getPlayerCoordinates().posY, p_71515_1_.getPlayerCoordinates().posZ);
			for(int x=-20;x<20;x++){
				for(int z=-20;z<20;z++){
					village.generateStructuresInChunk(p_71515_1_.getEntityWorld(), new Random(), (p_71515_1_.getPlayerCoordinates().posX>>4)+x, (p_71515_1_.getPlayerCoordinates().posZ>>4)+z);
				}
			}
			//func_152373_a(p_71515_1_, this, "Zadanie zakończone sukczesem", new Object[] {p_71515_2_[0]});
			
		}
	}

}
