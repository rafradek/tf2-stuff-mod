package rafradek.blocklauncher;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.util.ForgeDirection;

public class FakeBlockAccess implements IBlockAccess {

	public Block storedBlock;
	public int storedMetadata;
	public TileEntity storedTileEntity;
	public World realWorld;
	public int desiredX;
	public int desiredY;
	public int desiredZ;
	public void setPosition(int x,int y, int z){
		this.desiredX=x;
		this.desiredY=y;
		this.desiredZ=z;
	}


	@Override
	public int getLightBrightnessForSkyBlocks(int var1, int var2, int var3,
			int var4) {
		// TODO Auto-generated method stub
		return realWorld.getLightBrightnessForSkyBlocks(var1, var2, var3, var4);
	}

	@Override
	public int getBlockMetadata(int var1, int var2, int var3) {
		// TODO Auto-generated method stub
		return this.storedMetadata;
	}


	@Override
	public BiomeGenBase getBiomeGenForCoords(int var1, int var2) {
		// TODO Auto-generated method stub
		return realWorld.getBiomeGenForCoords(var1, var2);
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 255;
	}

	@Override
	public boolean extendedLevelsInChunkCache() {
		// TODO Auto-generated method stub
		return this.realWorld.extendedLevelsInChunkCache();
	}

	@Override
	public int isBlockProvidingPowerTo(int var1, int var2, int var3, int var4) {
		// TODO Auto-generated method stub
		return this.storedBlock.isProvidingStrongPower(this, var1, var2, var3, var4);
	}

	@Override
	public boolean isSideSolid(int x, int y, int z, ForgeDirection side,
			boolean _default) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Block getBlock(int p_147439_1_, int p_147439_2_, int p_147439_3_) {
		// TODO Auto-generated method stub
		if(p_147439_1_==this.desiredX&&p_147439_2_==this.desiredY&& p_147439_3_==this.desiredZ)
			return this.storedBlock;
		else
			return Blocks.air;
	}
	@Override
	public TileEntity getTileEntity(int p_147438_1_, int p_147438_2_,
			int p_147438_3_) {
		// TODO Auto-generated method stub
		return this.storedTileEntity;
	}
	@Override
	public boolean isAirBlock(int p_147437_1_, int p_147437_2_, int p_147437_3_) {
		if(p_147437_1_==this.desiredX&&p_147437_2_==this.desiredY&&p_147437_3_==this.desiredZ)
			return storedBlock != null? storedBlock.isAir(this.realWorld, p_147437_1_, p_147437_2_, p_147437_3_):true;
		else
			return true;
	}

}
