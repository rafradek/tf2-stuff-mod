package rafradek.blocklauncher;

import io.netty.buffer.ByteBuf;

import java.util.List;

import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityTNTPrimedBetter extends EntityTNTPrimed implements IEntityAdditionalSpawnData{

	public EntityLivingBase tntPlacedBy;
	public boolean sticky;
	public boolean impact;
	public float explosionRadius;
	public EntityTNTPrimedBetter(World par1World) {
		super(par1World);
	}
	
	public EntityTNTPrimedBetter(World par1World, double par2, double par4,
			double par6, EntityLivingBase par8EntityLivingBase, boolean sticky,int fuse,boolean impact, float explosionRadius) {
		super(par1World, par2, par4, par6, par8EntityLivingBase);
		this.fuse=fuse;
		this.sticky=sticky;
		this.impact=impact;
		this.tntPlacedBy=par8EntityLivingBase;
		this.explosionRadius=explosionRadius;
	}
	
	@Override
	public void onUpdate()
    { 
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        boolean vertical=false; 
        if(!(this.isSticky()&& this.isCollided)){
        this.motionY -= 0.03999999910593033D;
        vertical=motionY > 0;
        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        this.motionX *= 0.9800000190734863D;
        this.motionY *= 0.9800000190734863D;
        this.motionZ *= 0.9800000190734863D;
        }
        Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        MovingObjectPosition movingobjectposition = this.worldObj.func_147447_a(vec31, vec3, false, true, false);
        vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

        if (movingobjectposition != null)
        {
            vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
        }

        if (!this.worldObj.isRemote && this.impact && this.fuse>2)
        {
            Entity entity = null;
            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
            double d0 = 0.0D;
            EntityLivingBase entitylivingbase = this.tntPlacedBy;

            for (int j = 0; j < list.size(); ++j)
            {
                Entity entity1 = (Entity)list.get(j);

                if (entity1.canBeCollidedWith() && (entity1 != entitylivingbase))
                {
                    float f = 0.3F;
                    AxisAlignedBB axisalignedbb = entity1.boundingBox.expand(f, f, f);
                    MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vec3, vec31);

                    if (movingobjectposition1 != null)
                    {
                        double d1 = vec3.distanceTo(movingobjectposition1.hitVec);

                        if (d1 < d0 || d0 == 0.0D)
                        {
                            entity = entity1;
                            d0 = d1;
                        }
                    }
                }
            }

            if ((entity != null &&!(entity instanceof EntityTNTPrimedBetter)) ||this.isCollided){
            	this.fuse=2;
            	this.motionX *= 0.3D;
                this.motionZ *= 0.3D;
                this.motionY *= 0D;
            }
        }
        
		if (this.isSticky() ? this.isCollided: (/*this.worldObj.provider.terrainType instanceof ReversedWorldType ? (this.isCollidedVertically && vertical) :*/ this.onGround))
        {
			this.motionX *= 0.699999988079071D;
            this.motionZ *= 0.699999988079071D;
            this.motionY *= -0.5D;
        }
		
        if (this.fuse-- <= 0)
        {
            this.setDead();

            if (!this.worldObj.isRemote)
            {
                this.explode();
            }
        }
        else
        {
            this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
        }
        if(!this.worldObj.isRemote&&MinecraftServer.getServer().isSinglePlayer()&&Minecraft.getMinecraft().thePlayer!=null){
        	EntityTNTPrimedBetter fentity=(EntityTNTPrimedBetter) Minecraft.getMinecraft().theWorld.getEntityByID(this.getEntityId());
        	if(fentity!=null){
            	fentity.setVelocity(this.motionX,this.motionY,this.motionZ);
            	fentity.setPosition(this.posX, this.posY, this.posZ);
            	fentity.prevPosX=this.prevPosX;
            	fentity.prevPosY=this.prevPosY;
            	fentity.prevPosZ=this.prevPosZ;
        	}
        }
    }
	
	private void explode()
    {
		int i = MathHelper.floor_double(this.posX - this.explosionRadius - 1.0D);
        int j = MathHelper.floor_double(this.posX + this.explosionRadius + 1.0D);
        int k = MathHelper.floor_double(this.posY - this.explosionRadius - 1.0D);
        int l1 = MathHelper.floor_double(this.posY + this.explosionRadius + 1.0D);
        int i2 = MathHelper.floor_double(this.posZ - this.explosionRadius - 1.0D);
        int j2 = MathHelper.floor_double(this.posZ + this.explosionRadius + 1.0D);
        List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getBoundingBox(i, k, i2, j, l1, j2));
        Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);

        for (int k2 = 0; k2 < list.size(); ++k2)
        {
            Entity entity = (Entity)list.get(k2);
            if(entity instanceof EntityTNTPrimed){
            double d7 = entity.getDistance(this.posX, this.posY, this.posZ) / this.explosionRadius;

            if (d7 <= 0.5D)
            {
                entity.setDead();
                if(entity instanceof EntityTNTPrimedBetter){
                	this.explosionRadius+=((EntityTNTPrimedBetter)entity).explosionRadius/2.1f;
                }
                else{
                	this.explosionRadius+=2;
                }
            }
        }
        }
        this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, this.explosionRadius, true);
    }

	@Override
	protected void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        par1NBTTagCompound.setShort("Fuse", (short)this.getFuse());
        par1NBTTagCompound.setBoolean("Sticky", this.isSticky());
        par1NBTTagCompound.setBoolean("Impact", this.impact);
    }
	
	@Override
	protected void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
		this.fuse=par1NBTTagCompound.getShort("Fuse");
        this.sticky=par1NBTTagCompound.getBoolean("Sticky");
        this.impact=par1NBTTagCompound.getBoolean("Impact");
    }
	public boolean isSticky(){
		return this.sticky;
	}
	public int getFuse(){
		return this.fuse;
	}

	@Override
	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
		if(par1DamageSource.getDamageType().equals("explosion") || par1DamageSource.getDamageType().equals("explosion.player")){
			this.fuse/=4;
		}
        return super.attackEntityFrom(par1DamageSource, par2);
        
    }

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		// TODO Auto-generated method stub
		buffer.writeShort((short)this.fuse);
		buffer.writeBoolean(this.sticky);
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		this.fuse=additionalData.readShort();
		this.sticky=additionalData.readBoolean();
	}
}
