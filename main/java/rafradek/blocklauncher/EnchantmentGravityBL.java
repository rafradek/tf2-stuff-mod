package rafradek.blocklauncher;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class EnchantmentGravityBL extends Enchantment{

	protected EnchantmentGravityBL(int p_i1926_1_, int p_i1926_2_) {
		super(p_i1926_1_, p_i1926_2_, BlockLauncher.enchType);
		this.setName("gravity");
	}
	@Override
	public boolean canApply(ItemStack p_92089_1_)
    {
        return p_92089_1_.getItem() instanceof TNTCannon;
    }
	@Override
	public int getMaxLevel()
    {
        return 1;
    }
	@Override
	public int getMinEnchantability(int p_77321_1_)
    {
        return 30;
    }
	@Override
	public int getMaxEnchantability(int p_77321_1_)
    {
        return 50;
    }
}
