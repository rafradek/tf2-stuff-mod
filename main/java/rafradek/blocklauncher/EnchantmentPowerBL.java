package rafradek.blocklauncher;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class EnchantmentPowerBL extends Enchantment {

	protected EnchantmentPowerBL(int p_i1926_1_, int p_i1926_2_) {
		super(p_i1926_1_, p_i1926_2_, BlockLauncher.enchType);
	}
	@Override
	public boolean canApply(ItemStack p_92089_1_)
    {
        return p_92089_1_.getItem() instanceof TNTCannon;
    }
	@Override
	public int getMaxLevel()
    {
        return 4;
    }
	@Override
	public int getMinEnchantability(int p_77321_1_)
    {
        return 1 + (p_77321_1_ - 1) * 15;
    }
	@Override
	public int getMaxEnchantability(int p_77321_1_)
    {
        return this.getMinEnchantability(p_77321_1_)+15;
    }
	@Override
	public String getName()
	{
		return Enchantment.power.getName();
	}
}
