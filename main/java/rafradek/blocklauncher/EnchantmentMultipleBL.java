package rafradek.blocklauncher;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class EnchantmentMultipleBL extends Enchantment{

	protected EnchantmentMultipleBL(int p_i1926_1_, int p_i1926_2_) {
		super(p_i1926_1_, p_i1926_2_, BlockLauncher.enchType);
		this.setName("multiple");
	}
	@Override
	public boolean canApply(ItemStack p_92089_1_)
    {
        return p_92089_1_.getItem() instanceof TNTCannon&& BlockLauncher.cannon.isActivator(p_92089_1_);
    }
	@Override
	public int getMaxLevel()
    {
        return 3;
    }
	@Override
	public int getMinEnchantability(int p_77321_1_)
    {
        return 12 + (p_77321_1_ - 1) * 16;
    }
	@Override
	public int getMaxEnchantability(int p_77321_1_)
    {
        return this.getMinEnchantability(p_77321_1_)+16;
    }
}
