package rafradek.blocklauncher;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class BLClientProxy extends BLCommonProxy {
	@Override
	public void registerRender(){
		RenderingRegistry.registerEntityRenderingHandler(EntityFallingEnchantedBlock.class, new RenderFallingBlock());
		RenderingRegistry.registerEntityRenderingHandler(EntityTNTPrimedBetter.class, new RenderTNTPrimedBetter());
	}
}
