package rafradek.blocklauncher;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class RenderFallingBlock extends Render {

	private final RenderBlocks sandRenderBlocks = new RenderBlocks();
	private final FakeBlockAccess fakeWorld=new FakeBlockAccess();

    public RenderFallingBlock()
    {
        this.shadowSize = 0.0F;
    }

    /**
     * The actual render method that is used in doRender
     */
    public void doRenderFallingBlock(EntityFallingEnchantedBlock entity, double par2, double par4, double par6, float par8, float par9)
    {
    	World world = this.fakeWorld.realWorld=entity.worldObj;
        Block block = this.fakeWorld.storedBlock=entity.block;
        this.fakeWorld.storedMetadata=entity.metadata;
        if (world.getBlock(MathHelper.floor_double(entity.posX), MathHelper.floor_double(entity.posY), MathHelper.floor_double(entity.posZ)) != entity.block)
	        if(this.sandRenderBlocks.blockAccess==null){
	        	this.sandRenderBlocks.blockAccess=world;
	        }
	        int i = MathHelper.floor_double(entity.posX);
	        int j = MathHelper.floor_double(entity.posY);
	        int k = MathHelper.floor_double(entity.posZ);

	        if (block != null)
	        {
	        	float f2;
	        	Tessellator tessellator = Tessellator.instance;
	            GL11.glPushMatrix();
	            GL11.glTranslatef((float)par2, (float)par4, (float)par6);
	            if (entity.isPrimed&&entity.fuse - par9 + 1.0F < 10.0F)
	            {
	                f2 = 1.0F - (entity.fuse - par9 + 1.0F) / 10.0F;

	                if (f2 < 0.0F)
	                {
	                    f2 = 0.0F;
	                }

	                if (f2 > 1.0F)
	                {
	                    f2 = 1.0F;
	                }

	                f2 *= f2;
	                f2 *= f2;
	                float f3 = entity.scale + f2 * 0.3F;
	                GL11.glScalef(f3, f3, f3);
	            }
	            else{
	            	GL11.glScalef(entity.scale, entity.scale, entity.scale);
	            }
	            //GL11.glRotatef(entity.prevRotationYaw + (entity.rotationYaw - entity.prevRotationYaw) * par9, 0.0F, 1.0F, 0.0F);
	            //GL11.glRotatef(entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * par9, 0.0F, 0.0F, 1.0F);
	            this.bindEntityTexture(entity);
	            GL11.glDisable(GL11.GL_LIGHTING);
	            
	            

	            /*if (block instanceof BlockAnvil)
	            {
	                this.blockRender.field_147845_a = world;
	                tessellator.startDrawingQuads();
	                tessellator.setTranslation((double)((float)(-i) - 0.5F), (double)((float)(-j) - 0.5F), (double)((float)(-k) - 0.5F));
	                this.blockRender.func_147780_a((BlockAnvil)block, i, j, k, entity.metadata);
	                tessellator.setTranslation(0.0D, 0.0D, 0.0D);
	                tessellator.draw();
	            }
	            else if (block instanceof BlockDragonEgg)
	            {
	                this.blockRender.field_147845_a = world;
	                tessellator.startDrawingQuads();
	                tessellator.setTranslation((double)((float)(-i) - 0.5F), (double)((float)(-j) - 0.5F), (double)((float)(-k) - 0.5F));
	                this.blockRender.func_147802_a((BlockDragonEgg)block, i, j, k);
	                tessellator.setTranslation(0.0D, 0.0D, 0.0D);
	                tessellator.draw();
	            }
	            else
	            {*/
	                //this.blockRender.func_147775_a(block);
	            //int bright = entity.getBrightnessForRender(p_147918_9_);
	            //int bright1 = bright % 65536;
	            //int bright2 = bright / 65536;
	            //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)bright1 / 1.0F, (float)bright2 / 1.0F);
	            if(block.getRenderType()!=-1){
	            	tessellator.startDrawingQuads();
	            	tessellator.setTranslation((-i) - 0.5F, ((-j)), (-k) - 0.5F);
                	this.sandRenderBlocks.blockAccess=this.fakeWorld;
                	this.fakeWorld.setPosition(i, j, k);
	            	this.sandRenderBlocks.renderBlockByRenderType(block, i, j, k);
	            	tessellator.setTranslation(0.0D, 0.0D, 0.0D);
                	tessellator.draw();
	            	if (entity.isPrimed&&entity.fuse / 5 % 2 == 0)
                    {
	            		f2 = (1.0F - (entity.fuse - par9 + 1.0F) / 100.0F) * 0.8F;
	            		GL11.glTranslatef(0,0.5f,0);
                        GL11.glDisable(GL11.GL_TEXTURE_2D);
                        GL11.glDisable(GL11.GL_LIGHTING);
                        GL11.glEnable(GL11.GL_BLEND);
                        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_DST_ALPHA);
                        GL11.glColor4f(1.0F, 1.0F, 1.0F, f2);
                        this.sandRenderBlocks.renderBlockAsItem(block, 0, 1.0F);
                        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                        GL11.glDisable(GL11.GL_BLEND);
                        GL11.glEnable(GL11.GL_LIGHTING);
                        GL11.glEnable(GL11.GL_TEXTURE_2D);
                    }
                	this.sandRenderBlocks.blockAccess=world;
	            }
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glPopMatrix();
        }
    }
    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    @Override
	public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
    {
        this.doRenderFallingBlock((EntityFallingEnchantedBlock)par1Entity, par2, par4, par6, par8, par9);
    }

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return TextureMap.locationBlocksTexture;
	}
	
}
