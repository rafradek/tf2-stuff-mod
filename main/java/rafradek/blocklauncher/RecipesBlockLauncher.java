package rafradek.blocklauncher;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecipesBlockLauncher implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting inventorycrafting, World world) {
		int launchers=0;
		int slimeballs=0;
		int feathers=0;
		int ingots=0;
		boolean activator=false;
		boolean shotblock=false;
		/*int bows=0;
		int flintandsteel=0;
		int powder=0;
		int redstone=0;*/
		for(int i=0; i<inventorycrafting.getSizeInventory();i++){
			if(inventorycrafting.getStackInSlot(i) != null && inventorycrafting.getStackInSlot(i).stackSize !=0){
				if(inventorycrafting.getStackInSlot(i).getItem() instanceof TNTCannon){
					launchers++;
					activator=BlockLauncher.cannon.isActivator(inventorycrafting.getStackInSlot(i));
					shotblock=BlockLauncher.cannon.getType(inventorycrafting.getStackInSlot(i))==2;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.slime_ball){
					slimeballs++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.feather){
					feathers++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.iron_ingot){
					ingots++;
				}
				/*else if(inventorycrafting.getStackInSlot(i).getItem() == Items.flint_and_steel){
					flintandsteel++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.bow){
					bows++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Item.getItemFromBlock(Blocks.tnt)){
					powder++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Item.getItemFromBlock(Blocks.glowstone)){
					redstone++;
				}*/
				else{
					return false;
				}
			}
		}
		if(launchers==1 && (slimeballs==0 ||slimeballs==4 || slimeballs==8)&&(feathers==0||(feathers==4&&activator))&&(ingots==0||(ingots==8&&shotblock))){
			return true;
		}
		return false;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inventorycrafting) {
		int launchers=0;
		int launcherslot=-1;
		int slimeballs=0;
		int feathers=0;
		int ingots=0;
		/*int bows=0;
		int flintandsteel=0;
		int powder=0;
		int redstone=0;*/
		for(int i=0; i<inventorycrafting.getSizeInventory();i++){
			if(inventorycrafting.getStackInSlot(i) != null && inventorycrafting.getStackInSlot(i).stackSize !=0){
				if(inventorycrafting.getStackInSlot(i).getItem() instanceof TNTCannon){
					launchers++;
					launcherslot=i;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.slime_ball){
					slimeballs++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.feather){
					feathers++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.iron_ingot){
					ingots++;
				}
				/*else if(inventorycrafting.getStackInSlot(i).getItem() == Items.flint_and_steel){
					flintandsteel++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Items.bow){
					bows++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Item.getItemFromBlock(Blocks.tnt)){
					powder++;
				}
				else if(inventorycrafting.getStackInSlot(i).getItem() == Item.getItemFromBlock(Blocks.glowstone)){
					redstone++;
				}*/
				else{
					return null;
				}
			}
		}
		if(launchers==1){
			ItemStack stack=inventorycrafting.getStackInSlot(launcherslot).copy();
			if(stack.getTagCompound() == null){
				stack.stackTagCompound= new NBTTagCompound();
			}
			if(slimeballs==4){
				stack.stackTagCompound.setBoolean("Sticky",true);
				stack.stackTagCompound.removeTag("Bouncy");
			}
			else if(slimeballs==8){
				stack.stackTagCompound.setBoolean("Bouncy", true);
				stack.stackTagCompound.removeTag("Sticky");
			}
			if(feathers==4){
				stack.stackTagCompound.setBoolean("Harmless", true);
			}
			if(ingots==8){
				stack.stackTagCompound.setBoolean("Stack", true);
			}
			/*if(bows==1){
				stack.stackTagCompound.setBoolean("BowLike",true);
			}
			if(flintandsteel==1){
				stack.stackTagCompound.setBoolean("Activator",true);
			}
			if(powder==1){
				stack.stackTagCompound.setBoolean("Powder", true);
			}
			if(redstone==1){
				stack.stackTagCompound.setBoolean("Glowstone", true);
			}*/
			return stack;
		}
		return null;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public ItemStack getRecipeOutput() {
		// TODO Auto-generated method stub
		return null;
	}

}
