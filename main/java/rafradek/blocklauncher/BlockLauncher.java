package rafradek.blocklauncher;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.util.EnumHelper;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = "rafradek_blocklauncher", name =  "Block Launcher", version = "1.4")
public class BlockLauncher {
	
	public static TNTCannon cannon;
	public static Item launchpart;

	@SidedProxy(modId="rafradek_blocklauncher",clientSide="rafradek.blocklauncher.BLClientProxy", serverSide="rafradek.blocklauncher.BLCommonProxy")
	public static BLCommonProxy proxy;
	public static CreativeTabs tabblocklauncher;
	public static EnchantmentPowerBL enchPower;
	public static EnchantmentEfficiencyBL enchEff;
	public static EnchantmentDropBL enchLoot;
	public static EnchantmentShrinkBL enchShrink;
	public static EnchantmentHeavyBL enchHeavy;
	public static Block fireench;
	public static EnchantmentFireBL enchFire;
	public static EnumEnchantmentType enchType=EnumHelper.addEnchantmentType("BLOCK_LAUNCHER");
	public static EnchantmentGravityBL enchGravity;
	public static EnchantmentMultipleBL enchMultiple;
	
	@Mod.EventHandler
    public void init(FMLPreInitializationEvent event)
    {
		tabblocklauncher=new CreativeTabs("blocklauncher"){
			@Override
			public Item getTabIconItem() {
				return cannon;
			}
		}.func_111229_a(enchType);
		enchPower=new EnchantmentPowerBL(140, 7);
		enchEff=new EnchantmentEfficiencyBL(141, 3);
		enchLoot=new EnchantmentDropBL(142, 2);
		enchShrink=new EnchantmentShrinkBL(143, 1);
		enchHeavy=new EnchantmentHeavyBL(144, 1);
		enchFire=new EnchantmentFireBL(145, 2);
		enchGravity=new EnchantmentGravityBL(146, 2);
		enchMultiple=new EnchantmentMultipleBL(147, 2);
		EntityRegistry.registerModEntity(EntityFallingEnchantedBlock.class, "rafradek_block_enchanted", 0, this, 160, Integer.MAX_VALUE, true);
		EntityRegistry.registerModEntity(EntityTNTPrimedBetter.class, "rafradek_tnt_primed", 1, this, 160, 10, true);
		GameRegistry.registerBlock(fireench=new BlockFireEnchanted().setHardness(0.0F).setLightLevel(1.0F).setStepSound(Block.soundTypeWood).setBlockName("fire").setBlockTextureName("fire"), "rafradek_fire");
		GameRegistry.registerItem(cannon=new TNTCannon(), "rafradek_tntcannon");
		GameRegistry.registerItem(launchpart=new Item().setCreativeTab(tabblocklauncher).setUnlocalizedName("launchpart").setTextureName("blocklauncher:launchpart"), "rafradek_launchpart");
		GameRegistry.addRecipe(new RecipesBlockLauncher());
		ItemStack stack1=new ItemStack(cannon);
		stack1.stackTagCompound=new NBTTagCompound();
		stack1.stackTagCompound.setInteger("Type", 0);
		ItemStack stack2=stack1.copy();
		stack2.stackTagCompound.setInteger("Type", 1);
		ItemStack stack3=stack1.copy();
		stack3.stackTagCompound.setInteger("Type", 2);
		ItemStack stack6=stack1.copy();
		stack6.stackTagCompound.setInteger("Type", 3);
		ItemStack stack4=stack1.copy();
		stack4.stackTagCompound.setInteger("Type", 16);
		ItemStack stack5=stack1.copy();
		stack5.stackTagCompound.setInteger("Type", 17);
		GameRegistry.addRecipe(stack1, "ABC","  D", Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'),launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Blocks.planks);
		GameRegistry.addRecipe(stack2, "ABC"," D ", Character.valueOf('A'), Blocks.obsidian, Character.valueOf('B'), launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Items.iron_ingot);
		GameRegistry.addRecipe(stack3, "ABC","D E", Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'),launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Items.stick,Character.valueOf('E'),Items.leather);
		GameRegistry.addRecipe(stack4, "ABD"," CE", Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'),launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Items.flint_and_steel,Character.valueOf('E'),Blocks.planks);
		GameRegistry.addRecipe(stack5, "ABD","ECF", Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'),launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Items.flint_and_steel,Character.valueOf('E'),Blocks.planks,Character.valueOf('F'),Blocks.glass);
		GameRegistry.addRecipe(stack6, "DAB"," CA", Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'),launchpart,Character.valueOf('C'),Items.redstone,Character.valueOf('D'),Items.flint_and_steel,Character.valueOf('E'),Blocks.planks);
		GameRegistry.addRecipe(new ItemStack(launchpart), "BCB"," A ", Character.valueOf('A'), Blocks.iron_block, Character.valueOf('B'),Blocks.piston,Character.valueOf('C'),Items.redstone);
    }
	@Mod.EventHandler
    public void postInit(FMLInitializationEvent event){
		proxy.registerRender();
	}
	public static float getHardness(Block block, World world){
		if(block==Blocks.cobblestone){
			return 2.125f;
		}
		return Math.min(block.getMaterial().isToolNotRequired()?block.getBlockHardness(world, 0, 0, 0):block.getBlockHardness(world, 0, 0, 0)*1.9f,50f);
	}
	public static Vec3 normalize(Vec3 vec){
		double maxValue=Math.max(vec.xCoord, Math.max(vec.yCoord, vec.zCoord));
		vec.xCoord/=maxValue;
		vec.yCoord/=maxValue;
		vec.zCoord/=maxValue;
		return vec;
	}
}
