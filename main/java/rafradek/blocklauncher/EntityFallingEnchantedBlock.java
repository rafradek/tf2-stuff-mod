package rafradek.blocklauncher;

import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S27PacketExplosion;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Facing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.Vec3;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class EntityFallingEnchantedBlock extends Entity implements IEntityAdditionalSpawnData{

	private boolean isBreakingAnvil;
	public boolean isPrimed;
	public int sticky;
	public boolean attackOne;
	public Block block;
    public int metadata;
    public int field_145812_b;
    public boolean dropItems;
    private boolean field_145809_g;
    private int field_145815_h;
    private float field_145816_i;
    public NBTTagCompound dataTag;
	public EntityLivingBase owner;
	public float scale;
	private Entity lastAttacked;
	private float explosionSize;
	private boolean impact;
	public int fuse;
	private float dropChance;
	private boolean harmless;
	private int shrink;
	private float damage;
	public boolean isFired;
	public Block fireBlock=Blocks.fire;
	private boolean nogravity;
	private int tntamount;
    public EntityFallingEnchantedBlock(World p_i1706_1_)
    {
        super(p_i1706_1_);
        this.setSize(1, 1);
        this.dropItems = true;
        this.field_145815_h = 40;
        this.field_145816_i = 2.0F;
    }

    public EntityFallingEnchantedBlock(World p_i45318_1_, double p_i45318_2_, double p_i45318_4_, double p_i45318_6_, Block p_i45318_8_)
    {
        this(p_i45318_1_, p_i45318_2_, p_i45318_4_, p_i45318_6_, p_i45318_8_, 0);
    }
    public EntityFallingEnchantedBlock(World p_i45319_1_, double p_i45319_2_, double p_i45319_4_, double p_i45319_6_, Block p_i45319_8_, int p_i45319_9_)
    {
        super(p_i45319_1_);
        
        this.field_145815_h = 40;
        this.field_145816_i = 2.0F;
        this.block = p_i45319_8_;
        this.metadata = p_i45319_9_;
        this.preventEntitySpawning = true;
        this.setSize(0.98F, 0.98F);
        this.yOffset = this.height / 2.0F;
        this.setPosition(p_i45319_2_, p_i45319_4_, p_i45319_6_);
        this.motionX = 0.0D;
        this.motionY = 0.0D;
        this.motionZ = 0.0D;
        this.prevPosX = p_i45319_2_;
        this.prevPosY = p_i45319_4_;
        this.prevPosZ = p_i45319_6_;
        this.attackOne=p_i45319_8_.getMaterial()==Material.glass;
    }
    public EntityFallingEnchantedBlock(World par1World, double par2, double par4,double par6, int fuse,boolean impact, float explosionRadius,float dropchance, boolean harmless,int tntamount) {
    	this(par1World, par2, par4, par6, Blocks.tnt, 0);
    	this.fuse=fuse;
    	this.impact=impact;
    	this.explosionSize=explosionRadius;
    	this.isPrimed=true;
    	this.dropChance=dropchance;
    	this.harmless=harmless;
    	this.tntamount=tntamount;
    }
    public void setupEntity(int mode, float scale,boolean efficient,int shrink,float damage, EntityLivingBase owner,boolean nogravity) {
    	this.dropItems = !efficient;
    	this.nogravity=nogravity;
    	this.scale=scale;
        this.owner=owner;
        this.shrink=shrink;
        this.damage=damage;
        this.setSize(scale/(shrink*shrink), scale/(shrink*shrink));
    	this.sticky=mode;
	}
    /**
     * returns if this entity triggers Block.onEntityWalking on the blocks they walk on. used for spiders and wolves to
     * prevent them from trampling crops
     */
    @Override
	protected boolean canTriggerWalking()
    {
        return false;
    }

    @Override
	protected void entityInit() {}

    /**
     * Returns true if other Entities should be prevented from moving through this Entity.
     */
    @Override
	public boolean canBeCollidedWith()
    {
        return !this.isDead;
    }
	
	@Override
	@SuppressWarnings("unchecked")
	public void onUpdate()
    {
        if (this.block.getMaterial() == Material.air)
        {
            this.setDead();
        }
        else
        {
        	this.prevPosX = this.posX;
            this.prevPosY = this.posY;
            this.prevPosZ = this.posZ;
            double oldMotionX=this.motionX;
            double oldMotionY=this.motionY;
            double oldMotionZ=this.motionZ;
            float motion=(float) (this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
            int i = MathHelper.floor_double(this.posX);
            int j = MathHelper.floor_double(this.posY);
            int k = MathHelper.floor_double(this.posZ); 
           
	        if (!this.worldObj.isRemote/*&&this.block.getCollisionBoundingBoxFromPool(this.worldObj, i, j, k)!=null*/)
            {
	        	List list2 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(10.0D, 10.0D, 10.0D));
	        	List<Entity> resultList=new ArrayList<Entity>();
	        	Vec3[] startVecs=new Vec3[1];
	        	Entity entity = null;
	        	/*startVecs[0]=Vec3.createVectorHelper(this.boundingBox.minX, this.motionX<0||this.motionZ<0?this.boundingBox.minY:this.boundingBox.maxY, this.boundingBox.minZ);
	        	startVecs[1]=Vec3.createVectorHelper(this.boundingBox.maxX, this.motionX>0||this.motionZ<0?this.boundingBox.minY:this.boundingBox.maxY, this.boundingBox.minZ);
	        	startVecs[2]=Vec3.createVectorHelper(this.boundingBox.minX, this.motionX<0||this.motionZ>0?this.boundingBox.minY:this.boundingBox.maxY, this.boundingBox.maxZ);
	        	startVecs[3]=Vec3.createVectorHelper(this.boundingBox.maxX, this.motionX>0||this.motionZ>0?this.boundingBox.minY:this.boundingBox.maxY, this.boundingBox.maxZ);*/
	        	startVecs[0]=Vec3.createVectorHelper(this.posX,this.posY,this.posZ);
	        	for(int c=0;c<1;c++){
		            MovingObjectPosition movingobjectposition = this.worldObj.func_147447_a(startVecs[c], startVecs[c].addVector(this.motionX, this.motionY, this.motionZ), false, true, false);
		            Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
		            Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
		            if (movingobjectposition != null)
		            {
		                vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
		                if(this.impact){
		            		this.setPosition(vec31.xCoord, vec31.yCoord, vec31.zCoord);
		            		this.fuse=999;
		            		this.setDead();
		            		this.explode(oldMotionX, oldMotionY, oldMotionZ);
		            		return;
		            	}
		            }
		            
	                double d0 = 0.0D;
	                EntityLivingBase entitylivingbase = this.owner;
		
		            for (int h = 0; h < list2.size(); ++h)
		            {
		                Entity entity1 = (Entity)list2.get(h);
		
		                if (entity1.canBeCollidedWith() && (entity1 != entitylivingbase)&&!(entity1 instanceof EntityFallingEnchantedBlock))
		                {
		                	float f = this.scale/2;
		                	AxisAlignedBB axisalignedbb = entity1.boundingBox.expand(f, f, f).offset(0, -f, 0);
		                    MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vec3, vec31);
		
		                    if (movingobjectposition1 != null)
		                    {
		                        double d1 = vec3.distanceTo(movingobjectposition1.hitVec);

		                        if (this.attackOne&&(d1 < d0 || d0 == 0.0D))
		                        {
		                            entity = entity1;
		                            d0 = d1;
		                        }
		                        else if(!resultList.contains(entity1)){
		                        	resultList.add(entity1);
		                        }
		                        
		                    }
		                }
		            	
		            }
	        	}
	            if (this.attackOne)
                {
	            	this.attackEntity(entity,motion);
                }
	            else{
	            	for(Entity entityr:resultList){
	            		this.attackEntity(entityr,motion);
	            	}
	            }
	            
            }
            int g = -1;
            ++this.field_145812_b;
            if(!(this.sticky==1&& this.isCollided)){
	            if(!this.nogravity){
		            this.motionY += 0.03999999910593033D*g;
	            }
	            this.moveEntity(this.motionX, this.motionY, this.motionZ);
	            double mult=this.nogravity?0.99:0.98;
	            this.motionX *= mult;
		        this.motionY *= mult;
		        this.motionZ *= mult;
	            
            }
            else{
            	this.motionX *= 0D;
                this.motionZ *= 0D;
                this.motionY *= 0D;
            } 
            
            if ((this.sticky==1 && this.isCollided) || (this.sticky==0 && this.onGround))
            {
                this.motionX *= 0.699999988079071D;
                this.motionZ *= 0.699999988079071D;
                this.motionY *= -0.5D;
            }
            if (!this.worldObj.isRemote)
            {	
            	i = MathHelper.floor_double(this.posX);
            	j = MathHelper.floor_double(this.posY);
            	k = MathHelper.floor_double(this.posZ); 
            	if(this.isFired&&this.worldObj.getBlock(i, j, k).isReplaceable(worldObj, i, j, k)){
            		this.worldObj.setBlock(i, j, k, this.fireBlock);
            	}
            	if(this.isCollided){
                	if(this.impact){
                		this.fuse=0;
                	} else if(this.dropItems&&this.attackOne){
                		this.worldObj.playSoundAtEntity(this, this.block.stepSound.getBreakSound(), 1F, 1F);
                		for(int a=0;a<16;a++){
	                		EntityFallingEnchantedBlock blockF= new EntityFallingEnchantedBlock(worldObj, this.posX, this.posY+0.5, this.posZ, this.block, this.metadata);
	                		blockF.setupEntity(this.sticky,this.scale/2, true, this.shrink, this.damage, owner,this.nogravity);
	            			blockF.motionX=this.rand.nextFloat()*1-0.5;
	            			blockF.motionY=this.rand.nextFloat()*0.4;
	            			blockF.motionZ=this.rand.nextFloat()*1-0.5;
	            			this.worldObj.spawnEntityInWorld(blockF);
                		}
            			this.dropItems=false;
            			this.setDead();
                	}
                }
                if(!this.isPrimed){
	                if((this.sticky==1 && this.isCollided) || (this.sticky!=1 && this.onGround) || (this.nogravity&&motion<0.004d))
	                {
	                    if ((this.sticky==1||motion<0.004d)&&this.worldObj.getBlock(i, j, k) != Blocks.piston_extension)
	                    {
	                    	boolean placed=false;
	                    	if(this.dropItems)
	                    		placed=placeBlock(i,j,k)||placeBlock(i+1,j,k)||placeBlock(i-1,j,k)||placeBlock(i,j+1,k)||placeBlock(i,j-1,k)||placeBlock(i,j,k+1)||placeBlock(i,j,k-1);
	                    	else if(this.isFired){
                        		for(int a=0;a<6;a++){
                        			int x=i+Facing.offsetsXForSide[a];
                        			int y=j+Facing.offsetsYForSide[a];
                        			int z=k+Facing.offsetsZForSide[a];
                        			if(this.worldObj.getBlock(x, y, z).isReplaceable(worldObj, x, y, z)){
                        				this.worldObj.setBlock(x, y, z, this.fireBlock);
                        			}
                        		}
                    			if(this.worldObj.getBlock(i, j, k).isReplaceable(worldObj, i, j, k)){
                    				this.worldObj.setBlock(i, j, k, this.fireBlock);
                    			}
	                    	}
	                    	this.setDead();
	                        if (this.dropItems && !this.isBreakingAnvil && !placed)
	                        {
	                            this.entityDropItem(new ItemStack(this.block, 1, this.block.damageDropped(this.metadata)), 0.0F);
	                        }
	                    }
	                }
	                else if (j < 1 || j > 256)
	                {
	                    if (this.dropItems)
	                    {
	                        this.entityDropItem(new ItemStack(this.block, 1, this.block.damageDropped(this.metadata)), 0.0F);
	                    }
	
	                    this.setDead();
	                }
                }
                
               
                if(MinecraftServer.getServer().isSinglePlayer()&&Minecraft.getMinecraft().thePlayer!=null){
                	EntityFallingEnchantedBlock fentity=(EntityFallingEnchantedBlock) Minecraft.getMinecraft().theWorld.getEntityByID(this.getEntityId());
                	if(fentity!=null){
	                	fentity.setVelocity(this.motionX,this.motionY,this.motionZ);
	                	fentity.setPosition(this.posX, this.posY, this.posZ);
	                	fentity.prevPosX=this.prevPosX;
	                	fentity.prevPosY=this.prevPosY;
	                	fentity.prevPosZ=this.prevPosZ;
                	}
                }
            }
            if(this.isPrimed&&fuse--<=0){
            	this.setDead();
            	if (!this.worldObj.isRemote)
                {
            		this.explode(oldMotionX, oldMotionY, oldMotionZ);
                }
            }
        }
    }

	@SuppressWarnings("unchecked")
	private void attackEntity(Entity entity,float motion) {
		if (entity ==null){
        	List<Entity> list2=this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox);
        	for (int h = 0; h < list2.size(); ++h)
            {
                Entity entity1 = list2.get(h);
                if(entity1 != this.owner&&!(entity1 instanceof EntityFallingEnchantedBlock)){
            		entity=entity1;
            		break;
            	}
            }
        	
        }
        if (entity != null){
        	float hardness=BlockLauncher.getHardness(this.block,this.worldObj);
        	if(this.impact){
        		double maxPos=Math.max(this.motionX, Math.max(this.motionY,this.motionZ))*3;
        		this.setPosition(entity.posX-this.motionX/maxPos, entity.posY-this.motionY/maxPos, entity.posZ-this.motionZ/maxPos);
        		this.fuse=999;
        		this.setDead();
        		this.explode(this.motionX,this.motionY,this.motionZ);
        		return;
        	}
        	if(this.lastAttacked!=entity){
        		entity.hurtResistantTime=0;
            	float dmg=(float) Math.min((hardness*Math.sqrt(motion)*2.5f*this.damage),80);
            	if (this.owner == null)
                {
                    entity.attackEntityFrom(DamageSource.causeThrownDamage(this, this), dmg);
                }
                else
                {
                	entity.attackEntityFrom(DamageSource.causeThrownDamage(this, this.owner), dmg);
                }
            	if(this.isFired){
            		if(this.fireBlock==Blocks.fire){
            			entity.setFire(10);
            		}
            		else{
            			entity.setFire(15);
            		}
            	}
            	if(entity instanceof EntityLivingBase){
            		if(this.block instanceof BlockFalling&&this.block!=Blocks.anvil){
            			((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.blindness.id, 50, 1));
            			if(entity instanceof EntityLiving){
            				((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 50, 3));
            				((EntityLiving)entity).setAttackTarget(null);
            				((EntityLiving)entity).setLastAttacker(null);
            			}
            		}
            		else if(this.block==Blocks.web){
            			((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100, 2));
            		}
            		else if(this.block instanceof BlockBush){
            			((EntityLivingBase)entity).heal(2);
            		}
            		else if(this.block.getMaterial()==Material.glass){
            			((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 100, 1));
            		}
            	}
        	}
        	if(this.attackOne == true && this.dropItems == true){
        		this.worldObj.playSoundAtEntity(this, this.block.stepSound.getBreakSound(), 1F, 1F);
        		for(int i=0;i<16;i++){
        			EntityFallingEnchantedBlock blockF= new EntityFallingEnchantedBlock(worldObj, entity.posX, entity.posY+entity.getEyeHeight(), entity.posZ, this.block, this.metadata);
        			blockF.setupEntity(this.sticky,this.scale/2, true, this.shrink, this.damage/2, owner,this.nogravity);
        			blockF.motionX=this.rand.nextFloat()*1-0.5;
        			blockF.motionY=this.rand.nextFloat()*0.4;
        			blockF.motionZ=this.rand.nextFloat()*1-0.5;
        			this.worldObj.spawnEntityInWorld(blockF);
        		}
        		this.dropItems=false;
        		this.setDead();
        	}
        	this.lastAttacked=entity;
        	entity.addVelocity(this.motionX*Math.min(hardness/(this.isFired?10:4), 2), this.motionY*Math.min(hardness/(this.isFired?10:4), 2), this.motionZ*Math.min(hardness/(this.isFired?10:4), 2));
        }
	}

	public boolean canBuildAt(int posX, int posY, int posZ){
		return !BlockFalling.func_149831_e(this.worldObj, posX+1, posY, posZ) || !BlockFalling.func_149831_e(this.worldObj, posX-1, posY, posZ) || !BlockFalling.func_149831_e(this.worldObj, posX, posY+1, posZ)
			|| !BlockFalling.func_149831_e(this.worldObj, posX, posY-1, posZ) || !BlockFalling.func_149831_e(this.worldObj, posX, posY, posZ+1) || !BlockFalling.func_149831_e(this.worldObj, posX, posY, posZ-1);
		
	}
	public boolean placeBlock(int i, int j, int k){
		if (!this.isBreakingAnvil && this.worldObj.canPlaceEntityOnSide(this.block, i, j, k, true, 1, (Entity)null, (ItemStack)null) && (!BlockFalling.func_149831_e(this.worldObj, i, j - 1, k) || (this.sticky==1 && this.canBuildAt(i, j, k))||this.nogravity) && this.worldObj.setBlock(i, j, k, this.block, this.metadata, 3))
        {
			if(this.isFired){
        		for(int a=0;a<6;a++){
        			int x=i+Facing.offsetsXForSide[a];
        			int y=j+Facing.offsetsYForSide[a];
        			int z=k+Facing.offsetsZForSide[a];
        			if(this.worldObj.getBlock(x, y, z).isReplaceable(worldObj, x, y, z)){
        				this.worldObj.setBlock(x, y, z, this.fireBlock);
        			}
        		}
        	}
			if (this.block instanceof BlockFalling)
            {
                ((BlockFalling)this.block).func_149828_a(this.worldObj, i, j, k, this.metadata);
            }

            if (this.dataTag != null && this.block instanceof ITileEntityProvider)
            {
                TileEntity tileentity = this.worldObj.getTileEntity(i, j, k);

                if (tileentity != null)
                {
                    NBTTagCompound nbttagcompound = new NBTTagCompound();
                    tileentity.writeToNBT(nbttagcompound);
                    Iterator iterator = this.dataTag.func_150296_c().iterator();

                    while (iterator.hasNext())
                    {
                        String s = (String)iterator.next();
                        NBTBase nbtbase = this.dataTag.getTag(s);

                        if (!s.equals("x") && !s.equals("y") && !s.equals("z"))
                        {
                            nbttagcompound.setTag(s, nbtbase.copy());
                        }
                    }

                    tileentity.readFromNBT(nbttagcompound);
                    tileentity.markDirty();
                }
            }
            return true;
        }
		return false;
	}
	@Override
	protected void fall(float p_70069_1_)
    {
        if (this.field_145809_g)
        {
            int i = MathHelper.ceiling_float_int(p_70069_1_ - 1.0F);

            if (i > 0)
            {
                ArrayList arraylist = new ArrayList(this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox));
                boolean flag = this.block == Blocks.anvil;
                DamageSource damagesource = flag ? DamageSource.anvil : DamageSource.fallingBlock;
                Iterator iterator = arraylist.iterator();

                while (iterator.hasNext())
                {
                    Entity entity = (Entity)iterator.next();
                    entity.attackEntityFrom(damagesource, Math.min(MathHelper.floor_float(i * this.field_145816_i), this.field_145815_h));
                }

                if (flag && this.rand.nextFloat() < 0.05000000074505806D + i * 0.05D)
                {
                    int j = this.metadata >> 2;
                    int k = this.metadata & 3;
                    ++j;

                    if (j > 2)
                    {
                    }
                    else
                    {
                        this.metadata = k | j << 2;
                    }
                }
            }
        }
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	protected void writeEntityToNBT(NBTTagCompound p_70014_1_)
    {
        p_70014_1_.setByte("Tile", (byte)Block.getIdFromBlock(this.block));
        p_70014_1_.setInteger("TileID", Block.getIdFromBlock(this.block));
        p_70014_1_.setByte("Data", (byte)this.metadata);
        p_70014_1_.setByte("Time", (byte)this.field_145812_b);
        p_70014_1_.setBoolean("DropItem", this.dropItems);
        p_70014_1_.setBoolean("HurtEntities", this.field_145809_g);
        p_70014_1_.setFloat("FallHurtAmount", this.field_145816_i);
        p_70014_1_.setInteger("FallHurtMax", this.field_145815_h);
        p_70014_1_.setInteger("Sticky", this.sticky);
        p_70014_1_.setBoolean("NoGravity", this.nogravity);

        if (this.dataTag != null)
        {
            p_70014_1_.setTag("TileEntityData", this.dataTag);
        }
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	protected void readEntityFromNBT(NBTTagCompound p_70037_1_)
    {
        if (p_70037_1_.hasKey("TileID", 99))
        {
            this.block = Block.getBlockById(p_70037_1_.getInteger("TileID"));
        }
        else
        {
            this.block = Block.getBlockById(p_70037_1_.getByte("Tile") & 255);
        }
        this.sticky=p_70037_1_.getInteger("Sticky");
        this.nogravity=p_70037_1_.getBoolean("NoGravity");
        this.metadata = p_70037_1_.getByte("Data") & 255;
        this.field_145812_b = p_70037_1_.getByte("Time") & 255;

        if (p_70037_1_.hasKey("HurtEntities", 99))
        {
            this.field_145809_g = p_70037_1_.getBoolean("HurtEntities");
            this.field_145816_i = p_70037_1_.getFloat("FallHurtAmount");
            this.field_145815_h = p_70037_1_.getInteger("FallHurtMax");
        }
        else if (this.block == Blocks.anvil)
        {
            this.field_145809_g = true;
        }

        if (p_70037_1_.hasKey("DropItem", 99))
        {
            this.dropItems = p_70037_1_.getBoolean("DropItem");
        }

        if (p_70037_1_.hasKey("TileEntityData", 10))
        {
            this.dataTag = p_70037_1_.getCompoundTag("TileEntityData");
        }

        if (this.block.getMaterial() == Material.air)
        {
            this.block = Blocks.sand;
        }
    }

    public void func_145806_a(boolean p_145806_1_)
    {
        this.field_145809_g = p_145806_1_;
    }

    @Override
	public void addEntityCrashInfo(CrashReportCategory p_85029_1_)
    {
        super.addEntityCrashInfo(p_85029_1_);
        p_85029_1_.addCrashSection("Immitating block ID", Integer.valueOf(Block.getIdFromBlock(this.block)));
        p_85029_1_.addCrashSection("Immitating block data", Integer.valueOf(this.metadata));
    }

    @Override
	@SideOnly(Side.CLIENT)
    public float getShadowSize()
    {
        return 0.0F;
    }

    @SideOnly(Side.CLIENT)
    public World func_145807_e()
    {
        return this.worldObj;
    }
    @Override
	public void moveEntity(double p_70091_1_, double p_70091_3_, double p_70091_5_)
    {
        if (this.noClip)
        {
            this.boundingBox.offset(p_70091_1_, p_70091_3_, p_70091_5_);
            this.posX = (this.boundingBox.minX + this.boundingBox.maxX) / 2.0D;
            this.posY = this.boundingBox.minY + this.yOffset - this.ySize;
            this.posZ = (this.boundingBox.minZ + this.boundingBox.maxZ) / 2.0D;
        }
        else
        {
            this.worldObj.theProfiler.startSection("move");
            this.ySize *= 0.4F;
            double d3 = this.posX;
            double d4 = this.posY;
            double d5 = this.posZ;

            if (this.isInWeb)
            {
                this.isInWeb = false;
                p_70091_1_ *= 0.25D;
                p_70091_3_ *= 0.05000000074505806D;
                p_70091_5_ *= 0.25D;
                this.motionX = 0.0D;
                this.motionY = 0.0D;
                this.motionZ = 0.0D;
            }

            double d6 = p_70091_1_;
            double d7 = p_70091_3_;
            double d8 = p_70091_5_;
            AxisAlignedBB axisalignedbb = this.boundingBox.copy();

            List list = this.worldObj.getCollidingBoundingBoxes(this, this.boundingBox.addCoord(p_70091_1_, p_70091_3_, p_70091_5_));
            for (int i = 0; i < list.size(); ++i)
            {
                p_70091_3_ = ((AxisAlignedBB)list.get(i)).calculateYOffset(this.boundingBox, p_70091_3_);
            }
            
            this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);

            if (!this.field_70135_K && d7 != p_70091_3_)
            {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            boolean flag1 = this.onGround || d7 != p_70091_3_ && d7 < 0.0D;
            int j;

            for (j = 0; j < list.size(); ++j)
            {
                p_70091_1_ = ((AxisAlignedBB)list.get(j)).calculateXOffset(this.boundingBox, p_70091_1_);
            }

            this.boundingBox.offset(p_70091_1_, 0.0D, 0.0D);

            if (!this.field_70135_K && d6 != p_70091_1_)
            {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            for (j = 0; j < list.size(); ++j)
            {
                p_70091_5_ = ((AxisAlignedBB)list.get(j)).calculateZOffset(this.boundingBox, p_70091_5_);
            }

            this.boundingBox.offset(0.0D, 0.0D, p_70091_5_);

            if (!this.field_70135_K && d8 != p_70091_5_)
            {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            double d10;
            double d11;
            int k;
            double d12;

            if (this.stepHeight > 0.0F && flag1 && (this.ySize < 0.05F) && (d6 != p_70091_1_ || d8 != p_70091_5_))
            {
                d12 = p_70091_1_;
                d10 = p_70091_3_;
                d11 = p_70091_5_;
                p_70091_1_ = d6;
                p_70091_3_ = this.stepHeight;
                p_70091_5_ = d8;
                AxisAlignedBB axisalignedbb1 = this.boundingBox.copy();
                this.boundingBox.setBB(axisalignedbb);
                list = this.worldObj.getCollidingBoundingBoxes(this, this.boundingBox.addCoord(d6, p_70091_3_, d8));

                for (k = 0; k < list.size(); ++k)
                {
                    p_70091_3_ = ((AxisAlignedBB)list.get(k)).calculateYOffset(this.boundingBox, p_70091_3_);
                }

                this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);

                if (!this.field_70135_K && d7 != p_70091_3_)
                {
                    p_70091_5_ = 0.0D;
                    p_70091_3_ = 0.0D;
                    p_70091_1_ = 0.0D;
                }

                for (k = 0; k < list.size(); ++k)
                {
                    p_70091_1_ = ((AxisAlignedBB)list.get(k)).calculateXOffset(this.boundingBox, p_70091_1_);
                }

                this.boundingBox.offset(p_70091_1_, 0.0D, 0.0D);

                if (!this.field_70135_K && d6 != p_70091_1_)
                {
                    p_70091_5_ = 0.0D;
                    p_70091_3_ = 0.0D;
                    p_70091_1_ = 0.0D;
                }

                for (k = 0; k < list.size(); ++k)
                {
                    p_70091_5_ = ((AxisAlignedBB)list.get(k)).calculateZOffset(this.boundingBox, p_70091_5_);
                }

                this.boundingBox.offset(0.0D, 0.0D, p_70091_5_);

                if (!this.field_70135_K && d8 != p_70091_5_)
                {
                    p_70091_5_ = 0.0D;
                    p_70091_3_ = 0.0D;
                    p_70091_1_ = 0.0D;
                }

                if (!this.field_70135_K && d7 != p_70091_3_)
                {
                    p_70091_5_ = 0.0D;
                    p_70091_3_ = 0.0D;
                    p_70091_1_ = 0.0D;
                }
                else
                {
                    p_70091_3_ = (-this.stepHeight);

                    for (k = 0; k < list.size(); ++k)
                    {
                        p_70091_3_ = ((AxisAlignedBB)list.get(k)).calculateYOffset(this.boundingBox, p_70091_3_);
                    }

                    this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);
                }

                if (d12 * d12 + d11 * d11 >= p_70091_1_ * p_70091_1_ + p_70091_5_ * p_70091_5_)
                {
                    p_70091_1_ = d12;
                    p_70091_3_ = d10;
                    p_70091_5_ = d11;
                    this.boundingBox.setBB(axisalignedbb1);
                }
            }

            this.worldObj.theProfiler.endSection();
            this.worldObj.theProfiler.startSection("rest");
            this.posX = (this.boundingBox.minX + this.boundingBox.maxX) / 2.0D;
            this.posY = this.boundingBox.minY + this.yOffset - this.ySize;
            this.posZ = (this.boundingBox.minZ + this.boundingBox.maxZ) / 2.0D;
            this.isCollidedHorizontally = d6 != p_70091_1_ || d8 != p_70091_5_;
            this.isCollidedVertically = d7 != p_70091_3_;
            this.onGround = d7 != p_70091_3_ && d7 < 0.0D;
            this.isCollided = this.isCollidedHorizontally || this.isCollidedVertically;
            this.updateFallState(p_70091_3_, this.onGround);

            if (d6 != p_70091_1_)
            {
            	if(this.sticky!=2)
            		this.motionX = 0.0D;
            	else
            		this.motionX*=-1;
            }

            if (d7 != p_70091_3_)
            {
            	if(this.sticky!=2)
            		this.motionY = 0.0D;
            	else
            		this.motionY*=-1;
            }

            if (d8 != p_70091_5_)
            {
            	if(this.sticky!=2)
            		this.motionZ = 0.0D;
            	else
            		this.motionZ*=-1;
            }

            d12 = this.posX - d3;
            d10 = this.posY - d4;
            d11 = this.posZ - d5;

            try
            {
                this.func_145775_I();
            }
            catch (Throwable throwable)
            {
                CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Checking entity block collision");
                CrashReportCategory crashreportcategory = crashreport.makeCategory("Entity being checked for collision");
                this.addEntityCrashInfo(crashreportcategory);
                throw new ReportedException(crashreport);
            }

            this.isWet();
            
            this.worldObj.theProfiler.endSection();
        }
    }
    /*protected void func_145775_I()
    {
        int i = MathHelper.floor_double(this.boundingBox.minX + 0.001D);
        int j = MathHelper.floor_double(this.boundingBox.minY + 0.001D);
        int k = MathHelper.floor_double(this.boundingBox.minZ + 0.001D);
        int l = MathHelper.floor_double(this.boundingBox.maxX - 0.001D);
        int i1 = MathHelper.floor_double(this.boundingBox.maxY - 0.001D);
        int j1 = MathHelper.floor_double(this.boundingBox.maxZ - 0.001D);

        boolean first=false;
        if (this.worldObj.checkChunksExist(i, j, k, l, i1, j1))
        {
            for (int k1 = i; k1 <= l; ++k1)
            {
                for (int l1 = j; l1 <= i1; ++l1)
                {
                    for (int i2 = k; i2 <= j1; ++i2)
                    {
                        Block block = this.worldObj.getBlock(k1, l1, i2);
                        if(!first){
                        	first=true;
                        	this.placeBlock(k1, l1, i2);
                        }
                        try
                        {
                            block.onEntityCollidedWithBlock(this.worldObj, k1, l1, i2, this);
                        }
                        catch (Throwable throwable)
                        {
                            CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Colliding entity with block");
                            CrashReportCategory crashreportcategory = crashreport.makeCategory("Block being collided with");
                            CrashReportCategory.func_147153_a(crashreportcategory, k1, l1, i2, block, this.worldObj.getBlockMetadata(k1, l1, i2));
                            throw new ReportedException(crashreport);
                        }
                    }
                }
            }
        }
    }*/
    /**
     * Return whether this entity should be rendered as on fire.
     */
    private void explode(double motionX,double motionY,double motionZ)
    {
		int i = MathHelper.floor_double(this.posX - this.explosionSize - 1.0D);
        int j = MathHelper.floor_double(this.posX + this.explosionSize + 1.0D);
        int k = MathHelper.floor_double(this.posY - this.explosionSize - 1.0D);
        int l1 = MathHelper.floor_double(this.posY + this.explosionSize + 1.0D);
        int i2 = MathHelper.floor_double(this.posZ - this.explosionSize - 1.0D);
        int j2 = MathHelper.floor_double(this.posZ + this.explosionSize + 1.0D);
        List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getBoundingBox(i, k, i2, j, l1, j2));
        for (int k2 = 0; k2 < list.size(); ++k2)
        {
            Entity entity = (Entity)list.get(k2);
            if(entity instanceof EntityTNTPrimed||entity instanceof EntityFallingEnchantedBlock){
	            double d7 = entity.getDistance(this.posX, this.posY, this.posZ) / this.explosionSize;
	
	            if (this.explosionSize<50&&d7 <= 1D)
	            {
	                entity.setDead();
	                if(entity instanceof EntityFallingEnchantedBlock){
	                	this.explosionSize+=((EntityFallingEnchantedBlock)entity).explosionSize/2.1f;
	                }
	                else{
	                	this.explosionSize+=2;
	                }
	            }
	            else if(entity instanceof EntityFallingEnchantedBlock){
	            	((EntityFallingEnchantedBlock)entity).fuse*=0.3;
	            }
	        }
        }
        Explosion explosion = new EnchantedExplosion(this.worldObj, this, this.posX, this.posY, this.posZ, this.explosionSize, dropChance, this.harmless);
        explosion.isFlaming = false;
        explosion.isSmoking = true;
        explosion.doExplosionA();
        explosion.doExplosionB(true);
        Iterator iterator = this.worldObj.playerEntities.iterator();

        while (iterator.hasNext())
        {
            EntityPlayer entityplayer = (EntityPlayer)iterator.next();

            if (entityplayer.getDistanceSq(this.posX, this.posY, this.posZ) < 4096.0D)
            {
                ((EntityPlayerMP)entityplayer).playerNetServerHandler.sendPacket(new S27PacketExplosion(this.posX, this.posY, this.posZ, this.explosionSize, explosion.affectedBlockPositions, (Vec3)explosion.func_77277_b().get(entityplayer)));
            }
        }
        for(int a=0;a<this.tntamount;a++){
        	EntityFallingEnchantedBlock block=new EntityFallingEnchantedBlock(worldObj, this.posX, this.posY, this.posZ, 25+a, false, this.explosionSize/3, this.dropChance, this.harmless, 0);
        	block.setupEntity(this.sticky,this.scale/2, true, this.shrink, this.damage, owner,this.nogravity);
        	//double maxPos=Math.max(motionX, Math.max(motionY,motionZ))/*/this.explosionSize*/;	
        	if(this.impact){
        		block.motionY+=this.rand.nextFloat()*1;
        		/*block.fuse+=20;
        		block.motionX-=motionX*0.3/maxPos;
        		block.motionY-=motionY/maxPos;
        		block.motionZ-=motionZ*0.3/maxPos;
        		block.setPosition(block.posX+block.motionX*4, block.posY+block.motionY, block.posZ+block.motionZ*4);
        		block.motionX*=this.rand.nextFloat()*0.3+0.85;
	        	block.motionY*=this.rand.nextFloat()*0.3+0.85;
	        	block.motionZ*=this.rand.nextFloat()*0.3+0.85;*/
        		//block.nogravity=true;
        		/*block.setPosition(block.posX+this.rand.nextFloat()*this.explosionSize*4-this.explosionSize*2, 
        				block.posY+this.rand.nextFloat()*this.explosionSize*4-this.explosionSize*2, 
        				block.posZ+this.rand.nextFloat()*this.explosionSize*4-this.explosionSize*2);*/
        	}
        	else{
        		block.motionY+=this.rand.nextFloat()*0.5+0.5;
        		
        	}
        	block.motionX+=this.rand.nextFloat()*2-1;
        	block.motionZ+=this.rand.nextFloat()*2-1;
        	this.worldObj.spawnEntityInWorld(block);
        	
        }
    }
    @Override
	@SideOnly(Side.CLIENT)
    public boolean canRenderOnFire()
    {
        return this.isFired;
    }
	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeByte(this.sticky);
		buffer.writeInt(Block.getIdFromBlock(block));
		buffer.writeByte((byte)this.metadata);
		buffer.writeFloat(scale);
		buffer.writeShort(this.fuse);
		buffer.writeByte(this.shrink);
		buffer.writeBoolean(this.isPrimed);
		buffer.writeBoolean(this.isFired);
		buffer.writeBoolean(this.nogravity);
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		this.sticky=additionalData.readByte();
		this.block=Block.getBlockById(additionalData.readInt());
		this.metadata=additionalData.readByte();
		this.scale=additionalData.readFloat();
		this.fuse=additionalData.readShort();
		this.shrink=additionalData.readByte();
		this.isPrimed=additionalData.readBoolean();
		this.isFired=additionalData.readBoolean();
		this.nogravity=additionalData.readBoolean();
		this.setSize(scale,scale);
	}
}
