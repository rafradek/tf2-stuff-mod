package rafradek.blocklauncher;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class TNTCannon extends Item {

	private IIcon rifleIcon;
	private IIcon cannonIcon;
	public IIcon shotgunIcon;
	private IIcon tntlauncherIcon;
	private IIcon tntcannonIcon;
	private IIcon tntlauncherEmptyIcon;
	private IIcon throwerIcon;
	public TNTCannon() {
		this.setUnlocalizedName("blocklauncher");
		this.setCreativeTab(BlockLauncher.tabblocklauncher);
		this.setMaxStackSize(1);
		this.setMaxDamage(180);
		this.setNoRepair();
		// TODO Auto-generated constructor stub
	}
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World par2World, EntityPlayer player){
		if(!(stack.hasTagCompound()&&stack.getTagCompound().getInteger("wait")>0)&&this.allowShot(player,stack, par2World)){
			if (!this.usesBowAnimation(stack))
			{
        		this.use(stack, par2World, player, 1.8f,player.inventory.getStackInSlot(this.getSlotForUse(player,stack)),false);
			}
        	else{
        		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        	}
        }
		return stack;
	}
	@Override
	public void onUpdate(ItemStack p_77663_1_, World p_77663_2_, Entity p_77663_3_, int i,boolean p_77663_5_) {
		if(!p_77663_2_.isRemote&&p_77663_1_.hasTagCompound()){
			EntityPlayer player=(EntityPlayer) p_77663_3_;
			if(p_77663_1_.stackTagCompound.getInteger("wait")>0){
				p_77663_1_.stackTagCompound.setInteger("wait",p_77663_1_.stackTagCompound.getInteger("wait")-1);
			}
			if(this.isRepeatable(p_77663_1_)&&i==player.inventory.currentItem&&p_77663_1_.stackTagCompound.getInteger("repeat")>0){
				if(p_77663_2_.getWorldTime()%2==0&&this.allowShot(player,p_77663_1_, player.worldObj)){
					this.use(p_77663_1_,player.worldObj,player,1.8f,player.inventory.getStackInSlot(this.getSlotForUse(player,p_77663_1_)),true);
					p_77663_1_.stackTagCompound.setInteger("repeat",p_77663_1_.stackTagCompound.getInteger("repeat")-1);
				}
			}
		}
		
	}
	public boolean isRepeatable(ItemStack stack) {
		// TODO Auto-generated method stub
		return this.getType(stack)==3;
	}
	public int getSlotForUse(EntityPlayer player,ItemStack stack){
		if(player.inventory.getStackInSlot(player.inventory.currentItem+1) != null && player.inventory.getStackInSlot(player.inventory.currentItem+1).getItem() instanceof ItemBlock
				&& this.allowBlock(stack, Block.getBlockFromItem(player.inventory.getStackInSlot(player.inventory.currentItem+1).getItem()), player.worldObj)){
			return player.inventory.currentItem+1;
		}
		else{
			for(int i=0;i<player.inventory.getSizeInventory();i++){
				if(player.inventory.getStackInSlot(i) != null && player.inventory.getStackInSlot(i).getItem() instanceof ItemBlock
						&& this.allowBlock(stack, Block.getBlockFromItem(player.inventory.getStackInSlot(i).getItem()), player.worldObj)){
					return i;
				}
			}
		}
		return -1;
	}
	@Override
	public void onPlayerStoppedUsing(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer, int par4)
    {
		if(!this.isRepeatable(par1ItemStack)){
			int j = this.getMaxItemUseDuration(par1ItemStack) - par4;

            float f = j / 20.0F;
            f = (f * f + f * 2.0F) / 3.0F;

            if (f < 0.04D)
            {
                return;
            }

            if (f > 1.5F)
            {
                f = 1.5F;
            }
            f++;
            this.use(par1ItemStack,par2World,par3EntityPlayer,f*1.8f,par3EntityPlayer.inventory.getStackInSlot(this.getSlotForUse(par3EntityPlayer,par1ItemStack)),false);
		}
    }
	
	@Override
	public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        return par1ItemStack;
    }

    /**
     * How long it takes to use or consume an item
     */
    @Override
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 72000;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    @Override
	public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.bow;
    }
	public void use(ItemStack stack, World par2World, EntityPlayer player, float force,ItemStack stackToUse,boolean fEfficent){
		force*=this.speedMult(stack);
		EntityFallingEnchantedBlock entity;
		float rFloat=player.getRNG().nextFloat();
		boolean efficient=fEfficent || rFloat<=EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchEff.effectId, stack)*2.5f/10f || (this.isHarmless(stack)&&rFloat<0.8f);
		int stackSize=stackToUse.stackSize;
		int waitAmount;
		for(int i=0;i<(this.isSpreader(stack)?Math.min(stackSize,this.getSpreaderBlockCount(stack)):1);i++){
			if(Block.getBlockFromItem(stackToUse.getItem())==Blocks.tnt && this.isActivator(stack)){
				float radius=this.getExplosionSize(stack)*(this.biggerExplosion(stack)?1.4f:1);
				int fuse=Math.max(18,(int) (50*(this.biggerExplosion(stack)?1.4:1)))+i*2;
				float dropChance=EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchLoot.effectId, stack)+1;
				int tntAmount=EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchMultiple.effectId, stack)*8;
				waitAmount=(int) (10*this.fireRateMult(stack));
				entity=new EntityFallingEnchantedBlock(par2World, player.posX, player.posY, player.posZ,fuse, this.explodesOnImpact(stack),radius,
						dropChance, this.isHarmless(stack),tntAmount);
				//entity=new EntityMinecartTNT(par2World, player.posX, player.posY, player.posZ);
			}
			else if(!this.isActivator(stack)){
				if(!stack.hasTagCompound()){
					stack.stackTagCompound=new NBTTagCompound();
				}
				waitAmount=(int) Math.max(BlockLauncher.getHardness(Block.getBlockFromItem(stackToUse.getItem()),par2World)*this.fireRateMult(stack),this.fireRateMin(stack));
				entity=new EntityFallingEnchantedBlock(par2World, player.posX, player.posY, player.posZ, Block.getBlockFromItem(stackToUse.getItem()),stackToUse.getItemDamage());
				if(this.getType(stack)==3){
					entity.isFired=true;
					entity.fireBlock=EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchFire.effectId, stack)==0?Blocks.fire:BlockLauncher.fireench;
				}
				entity.preventEntitySpawning=false;
				//System.out.println(this.isSticky(stack)?1:(this.isBouncy(stack)?2:0));
			}
			else{
				return;
			}
			int shrink=EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchShrink.effectId, stack)+1;
			entity.setupEntity(this.isSticky(stack)?1:(this.isBouncy(stack)?2:0), this.getScale(stack),efficient,shrink,this.damageMult(stack),player,this.noGravity(stack));
			stack.stackTagCompound.setInteger("wait", waitAmount); 
			if(this.isRepeatable(stack)&&stack.stackTagCompound.getInteger("repeat")==0)
				stack.stackTagCompound.setInteger("repeat", 3);
			par2World.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
			
			if(!par2World.isRemote){
				/*if(reversed && this.getMovingObjectPositionFromPlayer(par2World, player, true) != null){
					double x=this.getMovingObjectPositionFromPlayer(par2World, player, true).blockX;
					double y=this.getMovingObjectPositionFromPlayer(par2World, player, true).blockY+0.8;
					double z=this.getMovingObjectPositionFromPlayer(par2World, player, true).blockZ;
					entity.setLocationAndAngles(x,y,z, player.rotationYaw, player.rotationPitch*-1);
				}
				else{*/
				entity.setLocationAndAngles(player.posX, player.posY + player.getEyeHeight(), player.posZ, player.rotationYaw, player.rotationPitch);
				entity.posX -= MathHelper.cos(entity.rotationYaw / 180.0F * (float)Math.PI) * 0.3F;
				entity.posY -= 0.5D;
				entity.posZ -= MathHelper.sin(entity.rotationYaw / 180.0F * (float)Math.PI) * 0.3F;
				entity.posX += -MathHelper.sin(entity.rotationYaw / 180.0F * (float)Math.PI)/* MathHelper.cos(0 / 180.0F * (float)Math.PI))*/*this.getScale(stack)/2;
				entity.posZ += MathHelper.cos(entity.rotationYaw / 180.0F * (float)Math.PI) /* MathHelper.cos(0 / 180.0F * (float)Math.PI))*/*this.getScale(stack)/2;
				entity.setPosition(entity.posX, entity.posY, entity.posZ);
				entity.yOffset = 0.0F;
				entity.motionX = -MathHelper.sin(entity.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(entity.rotationPitch / 180.0F * (float)Math.PI);
				entity.motionZ = MathHelper.cos(entity.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(entity.rotationPitch / 180.0F * (float)Math.PI);
				entity.motionY = (-MathHelper.sin(entity.rotationPitch / 180.0F * (float)Math.PI));
				float f2 = MathHelper.sqrt_double(entity.motionX * entity.motionX + entity.motionY * entity.motionY + entity.motionZ * entity.motionZ);
				Random random=new Random();
				entity.motionX /= f2;
				entity.motionY /= f2;
				entity.motionZ /= f2;
				double spread = this.isSpreader(stack)?(this.getType(stack)==2&&!this.firesEntireStack(stack)?0.012+0.01*Math.min(i,this.getSpreaderBlockCount(stack)):0.012+0.01*this.getSpreaderBlockCount(stack)):(this.isRepeatable(stack)?0.13:0.0075);
				entity.motionX += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * spread;
				entity.motionY += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * spread;
				entity.motionZ += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * spread;
				entity.motionX *= force;
				entity.motionY *= force;
				entity.motionZ *= force;
		        float f3 = MathHelper.sqrt_double(entity.motionX * entity.motionX + entity.motionZ * entity.motionZ);
		        entity.prevRotationYaw = entity.rotationYaw = (float)(Math.atan2(entity.motionX, entity.motionZ) * 180.0D / Math.PI);
		        entity.prevRotationPitch = entity.rotationPitch = (float)(Math.atan2(entity.motionY, f3) * 180.0D / Math.PI);
		        par2World.spawnEntityInWorld(entity);
		        //entity.setFire(99999);
		        if(!player.capabilities.isCreativeMode&&!efficient){
		        	stackToUse.stackSize--;
					if(stackToUse.stackSize<1){
						player.inventory.setInventorySlotContents(this.getSlotForUse(player,stack), null);//
					}
		        }
			}
        }
        stack.damageItem(1, player);
	}
	
	
	public float getScale(ItemStack stack) {
		float base=1;
		if(this.getType(stack)==0){
			base= 0.6f;
		}
		else if(this.getType(stack)==2||this.getType(stack)==3){
			base= 0.4f;
		}
		return base*(1+EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchShrink.effectId, stack)*0.33f);
	}
	@Override
	public void addInformation(ItemStack stack, EntityPlayer par2EntityPlayer, List par2List, boolean par4){
		if(this.isSticky(stack)){
			par2List.add("Sticky");
		}
		if(this.isBouncy(stack)){
			par2List.add("Bouncing");
		}
		if(this.isHarmless(stack)){
			par2List.add("Harmless");
		}
		if(this.firesEntireStack(stack)){
			par2List.add("Super Spread");
		}
		//par2List.add(""+stack.getTagCompound().getInteger("wait"));
		/*if(this.isActivator(stack)){
			par2List.add("Activates TNT");
		}
		if(this.usesBowAnimation(stack)){
			par2List.add("Uses bow animations");
		}
		if(this.biggerExplosion(stack)){
			par2List.add("Bigger TNT explosion");
		}
		if(this.explodesOnImpact(stack)){
			par2List.add("TNT explodes on impact");
		}*/
	}
	public boolean noGravity(ItemStack stack) {
		return EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchGravity.effectId, stack)>0;
	}
	public boolean isHarmless(ItemStack stack) {
		return stack.getTagCompound()!=null && stack.getTagCompound().getBoolean("Harmless");
	}
	public boolean firesEntireStack(ItemStack stack) {
		return stack.getTagCompound()!=null && stack.getTagCompound().getBoolean("Stack");
	}
	public boolean isSticky(ItemStack stack){
		return stack.getTagCompound()!=null && stack.getTagCompound().getBoolean("Sticky");
	}
	public boolean isBouncy(ItemStack stack){
		return stack.getTagCompound()!=null && stack.getTagCompound().getBoolean("Bouncy");
	}
	public boolean isActivator(ItemStack stack){
		return this.getType(stack)>15;
	}
	public boolean usesBowAnimation(ItemStack stack){
		return this.getType(stack)==1||this.getType(stack)==17;
	}
	public boolean biggerExplosion(ItemStack stack){
		return stack.getTagCompound()!=null && stack.getTagCompound().getBoolean("Powder");
	}
	public boolean explodesOnImpact(ItemStack stack){
		return this.getType(stack)==17;
	}
	public boolean isSpreader(ItemStack stack){
		return this.getType(stack)==2;
	}
	public int getSpreaderBlockCount(ItemStack stack){
		return this.getType(stack)==2&&!this.firesEntireStack(stack)?8:64;
	}
	public float getExplosionSize(ItemStack stack){
		float base=this.getType(stack)==17?3:4;
		return base+EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchPower.effectId, stack)*(this.explodesOnImpact(stack)?0.6f:1);
	}
	public int getType(ItemStack stack){
		return stack.getTagCompound()!=null ? stack.getTagCompound().getInteger("Type"):0;
	}
	public float fireRateMult(ItemStack stack){
		float base=1;
		if(this.getType(stack)==0){
			base= 2.4f;
		}
		if(this.getType(stack)==2){
			base= 10f;
		}
		if(this.getType(stack)==17){
			base= 3.7f;
		}
		return (base-EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchHeavy.effectId, stack)*0.2f)*(this.isHarmless(stack)?0.2f:1);
	}
	public float speedMult(ItemStack stack){
		float base=this.getType(stack)==17?1.5f:this.getType(stack)==3?0.6f:1;
		return base*(1+EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchPower.effectId, stack)*0.33f);
	} 
	public int fireRateMin(ItemStack stack){
		int base=this.getType(stack)==1?25:(this.getType(stack)==2?15:0);
		return base-EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchHeavy.effectId, stack)*2;
	}
	public float damageMult(ItemStack stack){
		return 1f;
	}
	public boolean allowShot(EntityPlayer player,ItemStack stack,World world){
		return this.getSlotForUse(player,stack)!=-1;
	}
	public boolean allowBlock(ItemStack stack,Block block,World world){
		float speed=BlockLauncher.getHardness(block, world);
		float mult=1+EnchantmentHelper.getEnchantmentLevel(BlockLauncher.enchHeavy.effectId, stack)*0.6F;
		if(this.getType(stack)==0){
			return speed<=2.5f*mult;
		}
		else if(this.getType(stack)==2){
			return speed<=2f*mult;
		}
		else if(this.getType(stack)==1){
			return speed>=2.5f/mult;
		}
		else if(this.getType(stack)==3){
			return block.getMaterial().getCanBurn()&&block!=Blocks.tnt;
		}
		else if(this.getType(stack)>15){
			return block==Blocks.tnt;
		}
		return true;
	}
	@Override
	public int getMaxDamage(ItemStack stack)
    {
        if(this.getType(stack)==0){
        	return 800;
        } else if(this.getType(stack)==1){
        	return 300;
        } else if(this.getType(stack)==2){
        	return 400;
        } else if(this.getType(stack)==3){
        	return 2000;
        } else if(this.getType(stack)==16){
        	return 450;
        } else if(this.getType(stack)==17){
        	return 300;
        }
        return getMaxDamage();
    }
	@Override
	@SuppressWarnings("rawtypes")
	public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List)
    {
		for(int i=0;i<18;i++){
			if(i==4) i=16;
			ItemStack stack=new ItemStack(par1);
			stack.stackTagCompound=new NBTTagCompound();
			stack.stackTagCompound.setInteger("Type", i);
			par3List.add(stack);
			ItemStack stack2=stack.copy();
			stack2.stackTagCompound.setBoolean("Sticky", true);
			par3List.add(stack2);
			ItemStack stack3=stack.copy();
			stack3.stackTagCompound.setBoolean("Bouncy", true);
			par3List.add(stack3);
			if(this.isActivator(stack)){
				ItemStack stack4=stack.copy();
				stack4.stackTagCompound.setBoolean("Harmless", true);
				par3List.add(stack4);
			}
			if(i==2){
				ItemStack stack5=stack.copy();
				stack5.stackTagCompound.setBoolean("Stack", true);
				par3List.add(stack5);
			}
		}
		/*ItemStack[] enchantedStacks=new ItemStack[12];
		for(int i=0; i<enchantedStacks.length; i++){
			enchantedStacks[i]=stack.copy();
			if(i==0 || i==3 || i>6){
				enchantedStacks[i].stackTagCompound.setBoolean("Sticky", true);
			}
			if(i==1 || i>3){
				enchantedStacks[i].stackTagCompound.setBoolean("Activator", true);
			}
			if(i==2 || i==3 || i>7){
				enchantedStacks[i].stackTagCompound.setBoolean("BowLike", true);
			}
			if(i==4 || i==6 || i==7 || i==9 || i==11){
				enchantedStacks[i].stackTagCompound.setBoolean("Powder", true);
			}
			if(i==5 || i==6 || i>9){
				enchantedStacks[i].stackTagCompound.setBoolean("Glowstone", true);
			}
			par3List.add(enchantedStacks[i]);
		}*/
    }
	@Override
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister)
    {
        super.registerIcons(par1IconRegister);
        this.rifleIcon = par1IconRegister.registerIcon("blocklauncher:block_rifle");
        this.cannonIcon = par1IconRegister.registerIcon("blocklauncher:block_cannon");
        this.shotgunIcon = par1IconRegister.registerIcon("blocklauncher:block_shotgun");
        this.throwerIcon = par1IconRegister.registerIcon("blocklauncher:block_thrower");
        this.tntcannonIcon = par1IconRegister.registerIcon("blocklauncher:tnt_launcher");
        this.tntlauncherIcon = par1IconRegister.registerIcon("blocklauncher:tnt_launcher_2");
        this.tntlauncherEmptyIcon = par1IconRegister.registerIcon("blocklauncher:tnt_launcher_2_empty");
    }
	@Override
	public String getUnlocalizedName(ItemStack p_77667_1_)
    {
		int type=this.getType(p_77667_1_);
		if(type==0){
			return "item.blockrifle";
		}
		else if(type==1){
			return "item.blockcannon";
		}
		else if(type==2){
			return "item.shotblock";
		}
		else if(type==3){
			return "item.blockthrower";
		}
		else if(type==16){
			return "item.TNTcannon";
		}
		else if(type==17){
			return "item.TNTlauncher";
		}
        return super.getUnlocalizedName();
    }
	@Override
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(ItemStack stack, int pass)
    {
		int type=this.getType(stack);
		if(type==0){
			return this.rifleIcon;
		}
		else if(type==1){
			return this.cannonIcon;
		}
		else if(type==2){
			return this.shotgunIcon;
		}
		else if(type==3){
			return this.throwerIcon;
		}
		else if(type==16){
			return this.tntcannonIcon;
		}
		else if(type==17){
			//if(stack.getTagCompound().getInteger("wait")<=0)
				return this.tntlauncherIcon;
			//else
			//	return this.tntlauncherEmptyIcon;
		}
        return super.getIcon(stack, pass);
    }
	@Override
	@SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }
	@Override
	public int getItemEnchantability()
    {
        return 10;
    }
}
